# -*- coding: utf-8 -*-


******************************************225K*******************************
def Scan5Pressure(p1,p2,p3,p4,p5,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_225K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/225K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_225K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/225K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_225K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/225K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_225K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/225K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_225K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/225K/")

def Scan3Pressure(p1,p2,p3,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_225K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/225K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_225K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/225K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_225K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/225K/")


******************************************200K*******************************
def Scan10Pressure(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p9)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p10)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")


def dinner(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p9)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p10)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

def Scan6Pressure(p1,p2,p3,p4,p5,p6,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

def Scan8Pressure(p1,p2,p3,p4,p5,p6,p7,p8,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")
  
  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_200K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/200K/")


******************************************175K*******************************

def ScanDodo(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p9)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p10)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p11)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p12)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p13)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p14)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p15)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p16)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p17)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p18)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p19)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p20)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

def Scan8Pressure(p1,p2,p3,p4,p5,p6,p7,p8,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

def Scan3Pressure(p1,p2,p3,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

def ScanDej(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p9)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p10)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p11)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p12)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p13)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p14)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

  get_ipython().magic(u'amove pmembrane %f' % p15)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E_175K","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/175K/")

******************************************ELSA PhaseI 300K*******************************

def Scan8Pressure(p1,p2,p3,p4,p5,p6,p7,p8,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

def ScanDejeuner(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p9)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p10)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p11)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p12)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

def Scan4Pressure(p1,p2,p3,p4,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

def ScanDiner(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p9)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p10)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p11)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p12)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p13)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p14)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p15)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p16)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "Elsa_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/PhaseI_300K/")

******************************************MARACA PhaseII 294K*******************************

def Scan12Pressure(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p9)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p10)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p11)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p12)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

def Scan5Pressure(p1,p2,p3,p4,p5,dt,scanlength):
  import time
  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -5, 5,scanlength, "Maraca_NDMS02E","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Phase-II_300K/")


******************************************ELSA [Fe(dpp)2(NCS)2].py 294K*******************************

def Scan15Pressure(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,dt,scanlength):
  import time

  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p7)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p8)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p9)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p10)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p11)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p12)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p13)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p14)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p15)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  myCdTe.sweep("tomorz", -10, -1,100, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")


Scan15Pressure(42,44,46,48,50,52,54,56,58,60,62,64,66,68,70,10,100)
Scan15Pressure(74,76,78,80,82,84,85,86,87,88,89,90,91,92,93,10,70)

def Scan6Pressure(p1,p2,p3,p4,p5,p6,dt,scanlength):
  import time

  get_ipython().magic(u'amove pmembrane %f' % p1)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p2)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p3)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p4)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p5)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")

  get_ipython().magic(u'amove pmembrane %f' % p6)
  time.sleep(dt)
  myCdTe.sweep("tomorz", -10, -1,scanlength, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")



myCdTe.sweep("tomorz", -10, -1,20, "ELSA_PR229","/nfs/ruche-psiche/psiche-soleil/com-psiche/Marchivie_0718/Fedpp_300K/")
