# -*- coding: utf-8 -*-

if I want a particular (max) length pAB
pA = np.zeros(2000)
pB = np.zeros(2000)
pAB = np.zeros(2000)
spos = np.linspace(-0.5, .5, 4000) # twice the sample diameter in micron steps

mylength = 0.5

# set w1 = 0.1
w1 = np.arange(0.025, 0.275, 0.02)
# find w2
w2 = np.zeros(w1.shape) # start value
curint = np.zeros(w1.shape)
for ii in range(len(w1)):
  curlen = 0
  while curlen < mylength:
          w2[ii] += 0.005
          A = w1[ii] * (R + deltaR) / deltaR # projection caesar slit 1
          B = w2[ii] * R / deltaR # projection caesar slit 2
          ndxA1 = np.nonzero(spos>(-A/2.))[0][0]
          ndxA2 = np.nonzero(spos<(A/2.))[0][-1] 
          pA = np.zeros(4000)
          pA[ndxA1:ndxA2] = 1
          ndxB1 = np.nonzero(spos>(-B/2.))[0][0]
          ndxB2 = np.nonzero(spos<(B/2.))[0][-1] 
          pB = np.zeros(4000)
          pB[ndxB1:ndxB2] = 1 
          pAB = np.convolve(pA, pB, 'same')
          pAB = pAB / pAB.max()
          # max opening angle
          opening = np.min([w1[ii]/R, w2[ii]/(R+deltaR)])
          pAB = pAB * opening
          # find the length
          tmp = pAB.max()/50.
          curlen = (np.nonzero(pAB>tmp)[0][-1] - np.nonzero(pAB>tmp)[0][0]) * 0.00025
          curint[ii] = pAB.sum()
  print "done one"

# it looks like the optimum is when the two openings match
# so for a given gauge length; we want the w1 and w2 such that the openings are the same.
A + B = length
(w1 * (R + deltaR) / deltaR) + (w2 * R / deltaR) = length 
w1/R = w2/(R+deltaR)