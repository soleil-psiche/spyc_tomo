# -*- coding: utf-8 -*-
"""
some functions for fitting

Created on Thu Jun  5 19:25:11 2014

@author: aking
"""

from scipy.optimize import leastsq
import numpy as np
exp = np.exp
sin = np.sin
cos = np.cos
import pylab
pylab.ion()

nonzero = np.nonzero
fft = np.fft.fft
ifft = np.fft.ifft


def fit_gaussian(xdata, ydata, negative=False):
    
    # fit a gaussian function
    #  y = A*exp(((x-B)/C)**2) + D
    optfunc = lambda x: (x[0]*exp(-((xdata-x[1])**2)/(2*(x[2]**2)))) + x[3] - ydata
    
    # guess the starting values
    if negative:
        A = ydata.min()-ydata.max()  
        B = xdata[nonzero(ydata==ydata.min())[0][0]]
        D = ydata.max()
        C = (xdata[nonzero(ydata<D+(A/2))[0][-1]]-xdata[nonzero(ydata<D+(A/2))[0][0]])/4.        
    else:
        A = ydata.max()-ydata.min()  
        B = xdata[nonzero(ydata==ydata.max())[0][0]]
        D = ydata.min()
        C = (xdata[nonzero(ydata>D+(A/2))[0][-1]]-xdata[nonzero(ydata>D+(A/2))[0][0]])/4.
    
    # fit to the function
    x0 = [A, B, C, D]
    fitA, fitB, fitC, fitD = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC, fitD]   
    yfit = optfunc(x1) + ydata
    return x1, yfit


def fit_gaussian_slope(xdata, ydata):
    
    # fit a gaussian function
    #  y = A*exp(((x-B)/C)**2) + D
    optfunc = lambda x: (x[0]*exp(-((xdata-x[1])**2)/(2*(x[2]**2)))) + x[3] + (xdata*x[4]) - ydata
    
    # guess the starting values
    A = ydata.max()-ydata.min()  
    B = xdata[nonzero(ydata==ydata.max())[0][0]]
    D = ydata.min()
    E = 0.
    C = (xdata[nonzero(ydata>D+(A/2))[0][-1]]-xdata[nonzero(ydata>D+(A/2))[0][0]])/4.
    
    # fit to the function
    x0 = [A, B, C, D, E]
    fitA, fitB, fitC, fitD, fitE = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC, fitD, fitE]   
    yfit = optfunc(x1) + ydata
    return x1, yfit


def correlate_1D(xdata, ydata, yref):
    """ assume same x data points"""

    # correlate the profiles
    corResult = ifft(fft(ydata)*np.conj(fft(yref)))
    corResult = np.real(corResult)
    c1 = np.nonzero(corResult == corResult.max())[0][0]
    
    # fit a quadratic through the peak
    ndx = np.mod(c1 + [-2, -1, 0, 1, 2], len(xdata))
    qy = corResult[ndx]
    qx = xdata[ndx]
    qx = np.array([qx**2, qx, [1, 1, 1, 1, 1]]).T
    a,b,c = np.linalg.lstsq(qx, qy)[0]
    shift = (-b / (2*a)) - xdata[0]
    return shift


def fit_sine(xdata, ydata):

    # fit a sine wave, in DEGREES!
    #  y = A*sin(B+x)+C
    optfunc = lambda x: (x[0]*sin((np.pi/180)*(x[1]+xdata))) + x[2] - ydata
    
    # guess the starting values
    A = (ydata.max()-ydata.min())/2. 
    B = 0
    C = (ydata.max()+ydata.min())/2. 
    
    # fit to the function
    x0 = [A, B, C]
    fitA, fitB, fitC = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC]   
    yfit = optfunc(x1) + ydata
    return x1, yfit

def fit_caesar(xdata, ydata):

    # xdata is caesar theta, ydata is csho position
    # fit caesar X and S positions plus constant offset?
    # convert to radians
    xdata = xdata*np.pi/180
    optfunc = lambda x: -x[0]*sin(xdata) -x[1]*cos(xdata) + x[2] - ydata
    x0 = [0, 0, 0]
    
    # try to fit
    fitA, fitB, fitC = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC]
    yfit = optfunc(x1) + ydata
    print str(x1)
    print "suggest dmove caesardts1 %f" % (-1.*x1[0])
    print "suggest dmove caesardtx1 %f" % (-1.*x1[1])
    print "plotting the data and the fit..."
    pylab.figure()
    pylab.plot(np.rad2deg(xdata), ydata, 'x-', label='data')
    pylab.plot(np.rad2deg(xdata), yfit, '-', label='fit result')
    pylab.legend()
    return x1, yfit
    # dmove caesardts1 negative x1[0], caesardtx1 negative x1[1]


    
def fit_exp(xdata, ydata):
	# fit exponential function y = a*np.exp(b*x)
	optfunc = lambda x: (x[0] * np.exp(x[1] * xdata)) - ydata
	# fit linear first to get gradient
	a,b = fit_linear(xdata, ydata)
	if a[0]>0:
		x0 = [1, 1]
	else:
		x0 = [1, -1]
	x1 =leastsq(optfunc, x0)[0]
	yfit = optfunc(x1) + ydata
	print str(x1)
	return x1, yfit

def fit_linear(xdata, ydata):
	# fit simple linear function y = a*x + b
	optfunc = lambda x: (x[0] * xdata) + x[1] - ydata
	x0 = [1, 0]
	x1 =leastsq(optfunc, x0)[0]
	yfit = optfunc(x1) + ydata
	print str(x1)
	return x1, yfit


