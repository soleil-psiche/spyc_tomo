# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
import time
import os
import codecs

import PyTango
cam = PyTango.DeviceProxy('i03-c-c00/dt/hamamatsu.3')
import spyc # détection automatique des actionneurs

import sys
#sys.path.append('/nfs/ruche-psiche/psiche-soleil/com-psiche/Maxime')
#import tifffile # Python 2.7 à jour requis (enum34, pathlib, futures...)
sys.path.append('/nfs/ruche-psiche/psiche-soleil/com-psiche/pelerin/scripts')
import maxime_tifffile as tifffile

import numpy as np
from matplotlib import pyplot as plt

def init(path, default='ruche'):
    global out_dir, logfile
    dp = {'ruche':'/nfs/ruche-psiche/psiche-soleil/com-psiche'}
    if not os.path.isabs(path):
        path = os.path.join(dp[default], path)
    out_dir = path
    try:
        os.makedirs(out_dir)
    except OSError:
        print("Le répertoire de sortie est déjà existant")
    print("Sauvegarde dans le répertoire {}".format(out_dir))
    logfile = codecs.open(os.path.join(out_dir, 'acquisition_images.log'), 'a', 'utf8')
    print("Utilisation du fichier journal {}".format(logfile.name))

def log(msg):
    print(msg)
    logfile.write((time.strftime('%Y-%m-%d %H:%M:%S') + ' :: ' + msg + '\n'))
    logfile.flush()

# ajouter le fast shutter -> fait
def snap(filename='test', exptime='ns', comment='', metadata=lambda :'', repeat=1, dt='lmp', sum=False, flipud=False):
    t0 = time.time()
    if comment != '':
        comment = '\n#!# Commentaire utilisateur : \n# ' + comment
    comment += '\n#!# Métadonnées supplémentaires : ' + metadata()
    if cam.state() is PyTango._PyTango.DevState.RUNNING:
        cam.stop()
        flag_livemode = True
    else:
        flag_livemode = False
    old_exptime = cam.exposuretime
    if exptime == 'ns':
        exptime = cam.exposuretime / 1000
    else:
        cam.exposuretime = exptime * 1000
    comment += '\n# ExposureTime = {} s'.format(exptime)
    if repeat == 1: # gain de temps si le os.listdir se fait pendant l'exposition
        get_ipython().magic('fwbsopen')
        time.sleep(.05)
        log("Début exposition {} s.".format(exptime))
        cam.snap()
    if not '.tif' in filename:
        filename += '.tif'
    l = [e for e in os.listdir(out_dir) if '.tif' in e]
    try:
        del e
    except NameError:
        pass
    if filename in l:
        root, ext = os.path.splitext(filename)
        if root[-1].isdigit():
            i = 1
            while root[-1-i].isdigit():
                i += 1
            num = int(root[-i:])
        else:
            root += '_0000'
            i = 4
            num = 0
            filename = root + ext
        while filename in l:
            num+=1
            filename = root[:-i] + '{num:0{i}d}'.format(num=num, i=i) + ext
    del l
    while not cam.state() is PyTango._PyTango.DevState.STANDBY:
        time.sleep(exptime/100)
    get_ipython().magic('fwbsclose')
    if repeat > 1:
        log("Démarrage d'une série de {} images avec un intervalle {}.".format(repeat, 'le plus court possible' if type(dt) in (str, unicode) else 'de {} s'.format(dt)))
        l = []
        for i in range(repeat):
            get_ipython().magic('fwbsopen')
            time.sleep(.1)
            log("Début exposition {} s.".format(exptime))
            cam.snap()
            while not cam.state() is PyTango._PyTango.DevState.STANDBY:
                time.sleep(exptime/100)
            get_ipython().magic('fwbsclose')
            time.sleep(.1)
            if flipud:
                l.append(np.flipud(cam.image))
            else:
                l.append(cam.image)
        a = np.stack(l)
        if sum:
            a = np.sum(a, axis=0)
        del l
    else:
        if flipud:
            a = np.flipud(cam.image)
        else:
            a = cam.image
    params = ''
    for k, v in locals().items():
        if k not in ['params', 'a', 'comment', 'metadata']:
            params += '\n# {} = {}'.format(k,v)
    params += '\n# {} = {}'.format('logfile', logfile.name)
    params += '\n# {} = {}'.format('out_dir', out_dir)
    comment += '\n#!# Paramètres internes : ' + params
    comment = '\n' + comment + '\n' # par soucis de lisibilité si ouverture du fichier TIFF dans un éditeur de texte
    tifffile.imsave(os.path.join(out_dir, filename), a, description = comment.encode('utf8'))
    logfile.write(time.strftime('%Y-%m-%d %H:%M:%S') + ' :: ' + filename + '\n')
    logfile.flush()
    log("Écriture dans le fichier {}.".format(filename))
    log("Description insérée dans l'en-tête du fichier TIFF : {}".format(comment))
    if flag_livemode:
        cam.start()
    cam.exposuretime = old_exptime
    log("Acquisition et sauvegarde effectuées en {:.2f} s".format(time.time()-t0))

#actuators = {k:v for k, v in globals().items() if type(v) is spyc.devices.scansldevices.SpycActuator and '_' not in k}

def metadata():
    mdt = ''
    for alias in [tomofocus, tomozoom, dtz1, dts1, dtx1, prz, ptz, ptx, pts, tomotx, tomots, pshg, psvg, s1vg, s1hg, s1vo, s1ho, filtertx]:
        mdt += '\n# {} = {} {}'.format(alias.alias, alias.value, alias.unit)
    #for k, v in actuators.items():
    #    mdt += '\n# {} = {}'.format(k, v.value)#, v.unit)
    return mdt

def preview():
    get_ipython().magic('%dmove tomorefx 2')
    time.sleep(.2)
    ref = mt.proxy.image
    get_ipython().magic('%dmove tomorefx -2')
    time.sleep(.2)
    plt.matshow(mt.proxy.image.astype(float)/ref)

def close():
    logfile.close()