# -*- coding: utf-8 -*-
import PyTango
import os
# the tango scan device
_scan = PyTango.DeviceProxy("i03-C-C00/ex/scan.1")
# data fitter device for fitting
_fitter = PyTango.DeviceProxy("i03-C-C00/ex/datafitter.1")
# in order to handle motor positions
from spyc.core import DeviceManager
dm = DeviceManager()


def mv_cen():
    '''
    Move to centroid position of the last scan
    Note this may do horrible things if multiple motors are scanned simultaneously
    '''
    # work with the Spyc RSCAN variable
    #RSCAN = get_ipython().ev('RSCAN')
    #mystring = "amove "
    #print("Fitting using sensor {} mv_cen will move:".format(RSCAN.sensor))
    #for key in RSCAN.pos.keys():
    #    print "{} to center position {}".format(key, RSCAN.pos[key])
    #    mystring = "{} {} {}".format(mystring, key, RSCAN.pos[key])
    ##print mystring
    #checkA = raw_input("move now? [yes/[no]] : ")
    #if checkA.lower() in ["yes", "y", "1", "true"]:
    #    get_ipython().magic(mystring)
    
    
    # get device scanned
    print "reading info from scan and datafitter devices"
    ndx = int(_fitter.deviceAttributeNameX[-1]) - 1
    devScanned = _scan.actuators[ndx]
    # find the corresponding motor name - this can be a spyc alias or the device name
    if devScanned in dm.actuators:
        spycalias = devScanned
    else:
        for name in dm.actuators:
            if dm.actors[name].full_name == devScanned:
                spycalias = name
                break
            
    # get the fitted position
    position = _fitter.position
    # put position - the peak center in workspace
    __builtins__['CEN'] = _fitter.position
    # check with user before moving
    checkA = str(raw_input("move {} to CEN ({}) [yes/[no]] ".format(spycalias, position))).lower()
    if checkA in ["yes", "y", "1", "true"]:
        get_ipython().magic(u'amove %s %f' % (spycalias, position))
    else:
        print "Not moving. CEN value is available in workspace"