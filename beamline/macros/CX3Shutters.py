# -*- coding: utf-8 -*-
from spyc import beamline_print
from PyTango import DeviceProxy
import time

beamline_print("Get CX3 4 colors shutters")
dev_DIO = DeviceProxy('i03-c-c00/ca/dio_o.1')

shut_delay=0.5

def sh1open():
    dev_DIO.setline('PH0')
    dev_DIO.setline('PE0')
    beamline_print("Shutter 1 Open")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH0')

def sh1close():
    dev_DIO.setline('PH0')
    dev_DIO.resetline('PE0')
    beamline_print("Shutter 1 close")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH0')

def sh2open():
    dev_DIO.setline('PH1')
    dev_DIO.setline('PE1')
    beamline_print("Shutter 2 Open")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH1')

def sh2close():
    dev_DIO.setline('PH1')
    dev_DIO.resetline('PE1')
    beamline_print("Shutter 2 close")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH1')

def sh3open():
    dev_DIO.setline('PH2')
    dev_DIO.setline('PE2')
    beamline_print("Shutter 3 Open")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH2')

def sh3close():
    dev_DIO.setline('PH2')
    dev_DIO.resetline('PE2')
    beamline_print("Shutter 3 close")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH2')

def sh4open():
    dev_DIO.setline('PH3')
    dev_DIO.setline('PE3')
    beamline_print("Shutter 4 Open")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH3')

def sh4close():
    dev_DIO.setline('PH3')
    dev_DIO.resetline('PE3')
    beamline_print("Shutter 4 close")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH3')

def sh5open():
    dev_DIO.setline('PH4')
    dev_DIO.setline('PE4')
    beamline_print("Shutter 5 Open")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH4')

def sh5close():
    dev_DIO.setline('PH4')
    dev_DIO.resetline('PE4')
    beamline_print("Shutter 5 close")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH4')

def sh6open():
    dev_DIO.setline('PH5')
    dev_DIO.setline('PE5')
    beamline_print("Shutter 6 Open")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH5')

def sh6close():
    dev_DIO.setline('PH5')
    dev_DIO.resetline('PE5')
    beamline_print("Shutter 6 close")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH5')

def sh7open():
    dev_DIO.setline('PH6')
    dev_DIO.setline('PE6')
    beamline_print("Shutter 7 Open")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH6')

def sh7close():
    dev_DIO.setline('PH6')
    dev_DIO.resetline('PE6')
    beamline_print("Shutter 7 close")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH6')

def sh8open():
    dev_DIO.setline('PH7')
    dev_DIO.setline('PE7')
    beamline_print("Shutter 8 Open")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH7')

def sh8close():
    dev_DIO.setline('PH7')
    dev_DIO.resetline('PE7')
    beamline_print("Shutter 8 close")
    time.sleep(shut_delay)
    dev_DIO.resetline('PH7')
