import h5py

def nxs_read(filename, prefix):
	f = h5py.File(filename)
	a = f['/entry/scan_data/'+prefix]
	im = np.zeros(a.shape[1::])
	a.read_direct(im)
	f.close()
	return im

