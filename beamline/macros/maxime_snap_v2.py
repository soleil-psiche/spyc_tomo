# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals
import time
import os
import codecs

import PyTango
import spyc # détection automatique des actionneurs

import sys
#sys.path.append('/nfs/ruche-psiche/psiche-soleil/com-psiche/Maxime')
#import tifffile # Python 2.7 à jour requis (enum34, pathlib, futures...)
sys.path.append('/nfs/ruche-psiche/psiche-soleil/com-psiche/pelerin/scripts')
import maxime_tifffile as tifffile

import numpy as np
from matplotlib import pyplot as plt
plt.ion()

class ScanConfig(object):

    def __init__(self, path, default='ruche', cam='i03-c-c00/dt/hamamatsu.3'):
        self.cam = cam = PyTango.DeviceProxy(cam)
        dp = {'ruche':'/nfs/ruche-psiche/psiche-soleil/com-psiche'}
        if not os.path.isabs(path):
            path = os.path.join(dp[default], path)
        self.out_dir = path
        try:
            os.makedirs(self.out_dir)
        except OSError:
            print("Le répertoire de sortie est déjà existant")
        print("Sauvegarde dans le répertoire {}".format(self.out_dir))
        self.logfile = codecs.open(os.path.join(self.out_dir, 'acquisition_images.log'), 'a', 'utf8')
        print("Utilisation du fichier journal {}".format(self.logfile.name))
        self.log('Initialisé !')

    def log(self, msg):
        print(msg)
        self.logfile.write((time.strftime('%Y-%m-%d %H:%M:%S') + ' :: ' + msg + '\n'))
        self.logfile.flush()

    def snap_old(self, filename='test', exptime='ns', comment='', metadata=lambda :'', repeat=1, dt='lmp', sum=False, flipud=False):
        t0 = time.time()
        if comment != '':
            comment = '\n#!# Commentaire utilisateur : \n# ' + comment
        comment += '\n#!# Métadonnées supplémentaires : ' + metadata()
        if self.cam.state() is PyTango._PyTango.DevState.RUNNING:
            self.cam.stop()
            flag_livemode = True
        else:
            flag_livemode = False
        old_exptime = self.cam.exposuretime
        if exptime == 'ns':
            exptime = self.cam.exposuretime / 1000
        else:
            self.cam.exposuretime = exptime * 1000
        comment += '\n# ExposureTime = {} s'.format(exptime)
        if repeat == 1: # gain de temps si le os.listdir se fait pendant l'exposition
            get_ipython().magic('fwbsopen')
            time.sleep(.05)
            self.log("Début exposition {} s.".format(exptime))
            self.cam.snap()
        if not '.tif' in filename:
            filename += '.tif'
        l = [e for e in os.listdir(out_dir) if '.tif' in e]
        try:
            del e
        except NameError:
            pass
        if filename in l:
            root, ext = os.path.splitext(filename)
            if root[-1].isdigit():
                i = 1
                while root[-1-i].isdigit():
                    i += 1
                num = int(root[-i:])
            else:
                root += '_0000'
                i = 4
                num = 0
                filename = root + ext
            while filename in l:
                num+=1
                filename = root[:-i] + '{num:0{i}d}'.format(num=num, i=i) + ext
        del l
        while not self.cam.state() is PyTango._PyTango.DevState.STANDBY:
            time.sleep(exptime/100)
        get_ipython().magic('fwbsclose')
        if repeat > 1:
            self.log("Démarrage d'une série de {} images avec un intervalle {}.".format(repeat, 'le plus court possible' if type(dt) in (str, unicode) else 'de {} s'.format(dt)))
            l = []
            for i in range(repeat):
                get_ipython().magic('fwbsopen')
                time.sleep(.1)
                self.log("Début exposition {} s.".format(exptime))
                self.cam.snap()
                while not self.cam.state() is PyTango._PyTango.DevState.STANDBY:
                    time.sleep(exptime/100)
                get_ipython().magic('fwbsclose')
                time.sleep(.1)
                if flipud:
                    l.append(np.flipud(self.cam.image))
                else:
                    l.append(self.cam.image)
            a = np.stack(l)
            if sum:
                a = np.sum(a, axis=0)
            del l
        else:
            if flipud:
                a = np.flipud(self.cam.image)
            else:
                a = self.cam.image
        params = ''
        for k, v in locals().items():
            if k not in ['params', 'a', 'comment', 'metadata']:
                params += '\n# {} = {}'.format(k,v)
        params += '\n# {} = {}'.format('logfile', logfile.name)
        params += '\n# {} = {}'.format('out_dir', out_dir)
        comment += '\n#!# Paramètres internes : ' + params
        comment = '\n' + comment + '\n' # par soucis de lisibilité si ouverture du fichier TIFF dans un éditeur de texte
        tifffile.imsave(os.path.join(self.out_dir, filename), a, description = comment.encode('utf8'))
        #self.logfile.write(time.strftime('%Y-%m-%d %H:%M:%S') + ' :: ' + filename + '\n')
        #self.logfile.flush()
        self.log("Écriture dans le fichier {}.".format(filename))
        self.log("Description insérée dans l'en-tête du fichier TIFF : {}".format(comment))
        if flag_livemode:
            self.cam.start()
        self.cam.exposuretime = old_exptime
        self.log("Acquisition et sauvegarde effectuées en {:.2f} s".format(time.time()-t0))

    def _snap(self, extra_log=None, open='%fwbsopen', close='%fwbsclose', _dt=.1, _log=True):
        # arr^ete le mode live si besoin
        if self.cam.state() is PyTango._PyTango.DevState.RUNNING:
            self.cam.stop()
            flag_livemode = True
        else:
            flag_livemode = False
        # exécute une action initiale
        get_ipython().magic(open) if type(open) in (str, unicode) else open() if open else None
        time.sleep(_dt)
        if _log:
            self.log("Début exposition {} ms.".format(self.cam.exposuretime))
            self.log(extra_log) if extra_log else None
        self.cam.snap()
        while not self.cam.state() is PyTango._PyTango.DevState.STANDBY: # attente que la caméra ai fini
            time.sleep(_dt)
        # exécute l'action terminale
        get_ipython().magic(close) if type(close) in (str, unicode) else close() if close else None
        time.sleep(_dt)
        # restaure l'état initial
        if flag_livemode:
            self.cam.start()

    def _create_tiff(self, filename, n=1):
        name, ext = os.path.splitext(filename)
        if ext.lower() not in ['.tiff', '.tif']:
            filename = name + '.tif'
        if not os.path.isabs(filename):
            filename = os.path.join(self.out_dir, filename)
        if os.path.exists(filename):
            l = filename.split('.')
            l.insert(-1, '_1')
            filename = '.'.join(l)
        tiff = tifffile.TiffWriter(filename, bigtiff=self._is_bigtiff(n))
        return tiff, filename # changement de comportment

    def _get_image(self, flipud=False, correct=False, plot=True):
        return np.flipud(self.cam.image) if flipud else self.cam.image

    def _take_ref(self):
        self.ref = self.cam.image

    def _save(self, filename='test', description='', metadata={}):
        tiff, _ = self._create_tiff(filename, 1)
        tiff.save(self._get_image(), description=description, metadata=metadata)
        tiff.close()

    def ascan(self, actuator, low, up, n_pts):
        params = [actuator.alias, low, up, n_pts]
        filename = "scan_{}_({},{},{}).tif".format(*params)
        positions = np.linspace(low, up, n_pts)
        with tifffile.TiffWriter(os.path.join(self.out_dir, filename), bigtiff=self._is_bigtiff(n_pts)) as tif:
            get_ipython().magic('%amove {} {:.4f}'.format(actuator.alias, low))
            self._snap(extra_log=filename + ' point de départ à p = {}'.format(low))
            tif.save(self.cam.image, description="Scan suivant {} de {} à {} en {} pas [cf. tag positions].".format(*params).encode('utf8'),
                    metadata={'positions':'[' + ', '.join(['{:.4f}'.format(p) for p in positions]) + ']'})
            for p in positions[1:]:
                get_ipython().magic('%amove {} {:.4f}'.format(actuator.alias, p))
                self._snap(extra_log=filename + ' p = {} {}'.format(p, actuator.unit))
                tif.save(self.cam.image)
            self.log(filename + ' fin du scan')

    def live(self, dt):
        try:
            while True:
                self._snap(_log=False)
        except KeyboardInterrupt:
            pass

    def ascan_n(self, actuator, low, up, n_pts):
        params = [actuator.alias, low, up, n_pts]
        filename = "scan_{}_({},{},{}).tif".format(*params)
        positions = np.linspace(low, up, n_pts)
        tiff, filename = self._create_tiff(filename, n_pts, out_file=True)
        # déplacements moteur et aquisition des images
        get_ipython().magic('%amove {} {:.4f}'.format(actuator.alias, low))
        self._snap(extra_log=filename + ' point de départ à p = {}'.format(low))
        tiff.save(self.cam.image, description="Scan suivant {} de {} à {} en {} pas [cf. tag positions].".format(*params).encode('utf8'),
                  metadata={'positions':'[' + ', '.join(['{:.4f}'.format(p) for p in positions]) + ']'})
        for p in positions[1:]:
            get_ipython().magic('%amove {} {:.4f}'.format(actuator.alias, p))
            self._snap(extra_log=filename + ' p = {} {}'.format(p, actuator.unit))
            tiff.save(self.cam.image)
        self.log(filename + ' fin du scan')

    def _is_bigtiff(self, n):
        return self.cam.roiWidth*self.cam.roiHeight*2*n > 4*1024**3

    def dscan(self, actuator, amplitude, n_pts):
        low = actuator.value - amplitude/2
        up = actuator.value + amplitude/2
        self.ascan(actuator, low, up, n_pts)

    def find_actuators(self):
        return {k:v for k, v in globals().items() if type(v) is spyc.devices.scansldevices.SpycActuator and '_' not in k}

    def set_actuators(self, actuators_list):
        #if actuators_list == 'auto':
        #    actuators_list = [tomofocus, tomozoom, dtz1, dts1, dtx1, prz, ptz, ptx, pts, tomotx, tomots, pshg, psvg, s1vg, s1hg, s1vo, s1ho, filtertx]
        self.actuators_list = actuators_list

    def metadata(self):
        mdt = ''
        for alias in self.actuators_list:
            mdt += '\n# {} = {} {}'.format(alias.alias, alias.value, alias.unit)
        #for k, v in actuators.items():
        #    mdt += '\n# {} = {}'.format(k, v.value)#, v.unit)
        return mdt

    def close(self):
        self.logfile.close()