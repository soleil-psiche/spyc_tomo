import PyTango
from PyTango import DevState, DeviceProxy,DevFailed
from time import sleep
import time
import os, sys

from valve_class import valve
from moveable import sensor

# will use this in sweep
from andy import mv

# ANdy notes - this seems to work.  It uses Emiliano's classes for all objects - the shutter, as a valve, and the detector as movable>sensor>ccdmar
# neither are very complicated, so could choose not to use the structure, and to write something simple that is more similar to what I've done already.  But might be better to benefit from the structure.  For the moment, nothing wrong with doing both...

# get the MARCCD specific device in order to verify waitFileOnDiskTime
specificDevice = PyTango.DeviceProxy("i03-c-c00/dt/marccd.1-specific")


def findNextFileName(prefix,ext,file_index=1):
    #
    #Prepare correct filename to avoid overwriting
    #
    psep=prefix.rfind(os.sep)
    if(psep<>-1): 
        fdir=prefix[:psep]
    else:
        fdir="."
    if(psep<>-1): prefix=prefix[psep+1:]
    if ext<>"":
        fname=prefix+"_"+"%04i"%(file_index)+"."+ext
    else:
        fname=prefix+"_"+"%04i"%(file_index)
    _dir=os.listdir(fdir)
    while(fname in _dir):
        file_index+=1
        if ext<>"":
            fname=prefix+"_"+"%04i"%(file_index)+"."+ext
        else:
            fname=prefix+"_"+"%04i"%(file_index)
    fname=fdir+os.sep+fname
    return fname

class marccd(sensor):
	"""
	BEWARE: All times are translated in seconds!
	"""
	
	def __repr__(self):
      	 	repr=self.label+"\n"
		return repr
					
      	def __call__(self, x=None):
      		print self.__repr__()
      		return self.state()						      

	def snap(self, dt=1, filename = "ccdmar_image",folder=".", filetype=0, shutter="I03-C-C06/EX/OBX.1"):
		"""
		Typical usage:
		Take an image, counting 60 seconds, save file as test_image in directory : 

		myccd.snap(60, \"test_image\", \"/nfs/ruche-psiche/psiche-soleil/com-psiche/test_directory/\")

		Optional arguments:
		filetype = 0 corresponds to marccd (.mccd) files (default),
		filetype = 1 to nxs files
		shutter device (default is experimental hutch shutter)
		"""
		# verify waitFileOnDiskTime
		print "verify waitFileOnDiskTime"
		if specificDevice.waitFileOnDiskTime!=2000:
			specificDevice.waitFileOnDiskTime=2000
		print "waitFileOnDiskTime = %d" % specificDevice.waitFileOnDiskTime

		if shutter <> "":
			shutter_valve=valve(shutter)
		if self.state() <> DevState.STANDBY:
			raise Exception("ccdmar: cannot snap if device not in STANDBY state.")
		if self.DP.read_attribute("exposureTime").value / 1000 <> dt + 3.:
			self.DP.write_attribute("exposureTime",(dt + 3.) * 1000)
		    # note the original sense here was - if a shutter is defined, open it.  Then sleep(1), then send the snap command, and wait, and then close shutter.  Nicolas has changed to: send the snap command, then open the shutter,"i03-c-c00/dt/marccd.1", "image" then wait, then close the shutter.  I have move the 'if shutter <> "" statement so it works correctly'
		self.DP.command_inout("Snap")
		if shutter <> "":
			shutter_valve.open()
		sleep(dt)
		#sleep(1)
		if shutter <> "":
			shutter_valve.close()
		timeout = 3. + self.DP.latencyTime/1000. + time.time() + 10.
		while self.state() <> DevState.STANDBY:
			sleep(0.1)
			if time.time() > timeout:
				print time.time(), timeout
				raise Exception("ccdmar: timeout while performing a snap!!!")
		if self.DP.fileGeneration:
			# try to put the image saving functionality into a method so that it can be used by  different routines
			self.saveImage(filename,folder,filetype)
		tmp=self.stats()
		print "Minimum Intensity = ", tmp[0]
		print "Maximum Intensity = ", tmp[1]
		print "Average Intensity = ", tmp[2]
		return	


	def sweep(self, motor="rz2", start_pos=None, end_pos=None, t_exp=None, filename="ccdmar_image", folder=".", filetype=0, shutter="I03-C-C06/EX/OBX.1", logfilehandle=None):
		"""
		Typical usage:
		Sweep rz2 from -5 to +5 in 60 seconds, save file as test_image in directory : 

		myccd.sweep(\"rz2\", -5, 5, 60, \"test_image\", \"/nfs/ruche-psiche/psiche-soleil/com-psiche/test_directory/\")

		Optional arguments:
		filetype = 0 corresponds to marccd (.mccd) files (default),
		filetype = 1 to nxs files
		shutter device (default is experimental hutch shutter)
		"""
		if logfilehandle!=None:
			logfilehandle.write("sweep element logfile\n")
			logfilehandle.write("time is %0.2f\n" % time.time())
			loctime = time.localtime()
			logfilehandle.write("time is %02d : %02d : %02d\n, date is %02d/%02d/%04d" % (loctime[3],loctime[4],loctime[5],loctime[2],loctime[1],loctime[0]))
			logfilehandle.write("sweep motor %s, from %0.2f to %0.2f\n" % (motor, start_pos, end_pos))
			logfilehandle.write("exposure time is %0.2f\n" % t_exp)


		# verify waitFileOnDiskTime
		#print "verify waitFileOnDiskTime"
		if specificDevice.waitFileOnDiskTime!=2000:
			specificDevice.waitFileOnDiskTime=2000
		#print "waitFileOnDiskTime = %d" % specificDevice.waitFileOnDiskTime

		# positive movements only for the moment
		if (end_pos - start_pos)<0:
			print "please sweep in the positive direction for the moment!"
			return		
		
		# save the original motor position and speed
		att = PyTango.AttributeProxy(movables[motor])
		init_pos = att.read().value
		init_v = att.get_device_proxy().read_attribute("velocity").value
		init_acc = att.get_device_proxy().read_attribute("acceleration").value
		
		# variables
		t_shut = 4.5 # shutter moving time
		t_pad = 0.5 # safety
		t_ccd = t_exp + (2*t_shut) + (2*t_pad) # time for ccd
		new_v = (end_pos - start_pos) / t_ccd # motor speed required
		t_acc = new_v / init_acc # acceleration time
		s_acc = new_v * t_acc * 0.5 # acceleration distance
		s_start = start_pos - s_acc
		s_end = end_pos + s_acc
		
		# check the velocity
		if new_v > init_v:
			print "Max motor speed is %0.2f !" % init_v
			print "Change your sweep parameters to go slower"
			return
		if new_v < 0.02:
			print "The calculated velocity is too slow"
			print "Change your sweep parameters to go faster"
			return

		# get the shutter device
		if shutter <> "":
			dev_shut = PyTango.DeviceProxy(shutter)
		
		# check that the ccd is ready
		if self.state() <> DevState.STANDBY:
			raise Exception("ccdmar: cannot snap if device not in STANDBY state.")
		# and set the ccd time
		if self.DP.read_attribute("exposureTime").value / 1000 <> t_ccd:
			self.DP.write_attribute("exposureTime", t_ccd * 1000)

		# move to the start position
		print "move to start position..."
		mv(motor, s_start)

		# put everything inside a try loop for safety
		try:
		
			# set the motor speed
			print "set motor speed %0.2f ..." % new_v
			att.get_device_proxy().write_attribute("velocity", new_v)
		
			# start the movement
			print "start movement to %0.2f ..." % s_end
			att.get_device_proxy().write_attribute("position", s_end)
			
			# wait the acceleration time, then start the ccd
			time.sleep(t_acc)
			print "start CCD"
			self.DP.command_inout("Snap")
			
			# wait the safety time, then open the shutter
			time.sleep(t_pad)
			if shutter <> "":
				print "open shutter"
				dev_shut.Open()
			
			# wait the moving time, and the exposure time, then close
			time.sleep(t_shut + t_exp)
			if shutter <> "":
				print "close shutter"
				dev_shut.Close()
			
				# wait for the shutter to close
				while dev_shut.State()  <> PyTango._PyTango.DevState.CLOSE:
					time.sleep(0.1)
				if self.state() <> DevState.STANDBY:
					print "shutter has closed, CCD still running"
			
			# wait for the ccd to finish
			timeout = self.DP.latencyTime/1000. + time.time() + 10.
			timeout_flag = bool(0)
			while self.state() <> DevState.STANDBY:
				sleep(0.1)			
				if time.time() > timeout:
					timeout_flag = bool(1)
					print "ccdmar: timeout while performing a sweep!!!"
					break
	
			# save the image and show the statistics
			if not(timeout_flag):
				if self.DP.fileGeneration:
					self.saveImage(filename,folder,filetype)
				tmp=self.stats()
				print "Minimum Intensity = ", tmp[0]
				print "Maximum Intensity = ", tmp[1]
				print "Average Intensity = ", tmp[2]
				logfilehandle.write("image saved in %s" % self.lastfilename)
		
		except KeyboardInterrupt:
			# stop the motor
			att.get_device_proxy().stop()
			print "Stopping!"
			# close the shutter
			if shutter <> "":
				dev_shut.Close()
				print "Closing shutter!"
			
		finally:
			# stop the motor in case it is still moving
			att.get_device_proxy().stop()
			sleep(0.5)
			# reset the speed
			print "reset motor speed %0.2f ..." % init_v
			att.get_device_proxy().write_attribute("velocity", init_v)
		
			# return to the start position
			print "return to original position"
			mv(motor, init_pos)
			print "finished"


						
	def sweep_sequence(motor="rz2", start_pos=None, end_pos=None, nimages=None, t_exp=None, filename="ccdmar_image", folder="./"):															
		'''
		call a series of self.sweep(self, motor="rz2", start_pos=None, end_pos=None, t_exp=None, filename="ccdmar_image", folder=...)	
		'''			
		# if we have n images, we divide the range by n+1 - make sure we get a float answer
		step = (end_pos - start_pos) / ((nimages*1.0)+1)
		starts = np.linspace(start_pos, (end_pos-step), nimages)
		ends = np.linspace(start_pos+step, end_pos, nimages)
		# logfile name
		logfileprefix = folder + os.sep + filename + "_sweeplog"
		logfileextension = "txt"
		logfilename = findNextFileName(logfileprefix,logfileextension)
		logfilehandle = open(logfilename, 'w')
		logfilehandle.write("sweep_sequence logfile\n")
		logfilehandle.write("time is %0.2f\n" % time.time())
		loctime = time.localtime()
		logfilehandle.write("time is %02d : %02d : %02d\n, date is %02d/%02d/%04d" % (loctime[3],loctime[4],loctime[5],loctime[2],loctime[1],loctime[0]))
		
		logfilehandle.write("sweep motor %s, from %0.2f to %0.2f in %d images\n" % (motor, start_pos, end_pos, nimages))
		logfilehandle.write("exposure time is %0.2f\n" % t_exp)
		logfilehandle.write("file prefix is %s, directory is %s\n" % (filename, folder))

		for ii in range(nimages):
			self.sweep(motor, starts[ii], ends[ii], t_exp, filename, folder, logfilehandle)

		logfilehandle.write("sweep_sequence finished\n" % t_exp)
		logfilehandle.write("time is %0.2f\n" % time.time())
		
		


	def saveImage(self, filename = "ccdmar_image", folder=".", filetype=0):
		# try to put the image saving functionality into a method so that it can be used by  different routines
		#Which file?
		source_nxs = self.DP.get_property("FileTargetPath")["FileTargetPath"][0]
		source_mccd = DeviceProxy(self.label + "-specific").get_property("DetectorTargetPath")["DetectorTargetPath"][0]
		if filetype == 1:
			source = source_nxs
		elif filetype == 0:
			source = source_mccd
		ll = os.listdir(source)
		ll.sort()
		if len(ll) < 1:
			raise Exception("ccdmar: data file not found!")
		if filetype == 1:
			if ll[-1].startswith("image"):
			    sourcefile = source + os.sep + ll[-1]
			else:
				raise Exception("ccdmar: spurious file found in spool !" + ll[-1])
		elif filetype == 0:
			if ll[-1].startswith("buffer"):
				sourcefile = source + os.sep + ll[-1]
			else:
				print ll
				raise Exception("ccdmar: spurious file found in spool !" + ll[-1])
		#Move file where?
		if filetype == 1:
			destination = findNextFileName(folder + os.sep + filename,"nxs")
		elif filetype == 0:
			destination = findNextFileName(folder + os.sep + filename,"mccd")
		# do the move
		tmp = os.system("mv " + sourcefile +" "+destination)
		print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
		if tmp==0:
			# mv was executed without error
			print "Data saved in :", destination
			self.lastfilename = destination
		else:
			print "WARNING! Failed to save data!!!"
			self.lastfilename = ""
		print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"	
		if filetype == 0:
			try:
				print "Removing nxs file in spool...",
				os.system("rm -f "+source + os.sep + "*.nxs")
				print "OK"
			except Exception, tmp:
				print "Failed !"
				print "Reason: ", tmp


						
	def stop(self):
		if self.state()==DevState.RUNNING:
			self.DP.command_inout("Stop")
		return self.state()
	
	def read(self):
		return self.pos()
	
	def stats(self):
		tmp=self.pos()
		return tmp.min(), tmp.max(), tmp.mean()
	
	def count(self, dt=1, filename = "ccdmar_image",folder=".", noreturn = True):
		self.snap(dt, filename = filename, folder = folder)
		if noreturn:
			return
		else:
			return self.read()

