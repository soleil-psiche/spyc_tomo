import PyTango
from PyTango import DevState, DeviceProxy,DevFailed
from time import sleep
import time
import os, sys

from valve_class import valve
from moveable import sensor

# will use this in sweep
from andy import mv

# ANDY NOTES - I take Emiliano's MARCCD macro and try to adapt to the 345
# Keep the same structure if possible - movable/sensor/mar345
# Main difference anticipated is in how the images are saved - where and with what names.
# It seems that we get a new file saved for each image on the mar pc, which has to be scp'd to the experiment directory
#
# can I add default device names?
# can I add an option to save locally if the network is buggy?


def findNextFileName(prefix,ext,file_index=1):
    #
    #Prepare correct filename to avoid overwriting
    #
    psep=prefix.rfind(os.sep)
    if(psep<>-1): 
        fdir=prefix[:psep]
    else:
        fdir="."
    if(psep<>-1): prefix=prefix[psep+1:]
    if ext<>"":
        fname=prefix+"_"+"%04i"%(file_index)+"."+ext
    else:
        fname=prefix+"_"+"%04i"%(file_index)
    _dir=os.listdir(fdir)
    while(fname in _dir):
        file_index+=1
        if ext<>"":
            fname=prefix+"_"+"%04i"%(file_index)+"."+ext
        else:
            fname=prefix+"_"+"%04i"%(file_index)
    fname=fdir+os.sep+fname
    return fname

class mar345(sensor):
	"""BEWARE: All times are translated in seconds!"""

	def __repr__(self):
      	 	repr=self.label+"\n"
		return repr
					
      	def __call__(self, x=None):
      		print self.__repr__()
      		return self.state()						      

	def snap(self, dt=1, filename = "mar345_image",folder=".", shutter="I03-C-C06/EX/OBX.1"):
		"""
		Typical usage:
		Take an image, counting 60 seconds, save file as test_image in directory : 

		my345.snap(60, \"test_image\", \"/nfs/ruche-psiche/psiche-soleil/com-psiche/test_directory/\")

		Optional arguments:
		shutter device (default is experimental hutch shutter)
		"""
		if shutter <> "":
			shutter_valve=valve(shutter)
		if self.state() <> DevState.STANDBY:
			raise Exception("mar345: cannot snap if device not in STANDBY state.")
		
		# open shutter, wait dt, close shutter
		if shutter <> "":
			shutter_valve.open()
		sleep(dt)
		if shutter <> "":
			shutter_valve.close()
#		timeout = 3. + self.DP.latencyTime/1000. + time.time() + 10.
		
		# read out the image plate		
		self.Scan()
		print "scanning image plate" 
		t0 = time.time()
		while(self.state()==DevState.RUNNING):
 			sleep(1)
 			if time.time()-t0 > 180:
 				print "timed out while scanning"
 				break
 				
 		print "finished scanning image plate"	
 		
		# copy/save the image
		self.saveImage(filename,folder)
		# image statistics
		tmp=self.stats()
		print "Minimum Intensity = ", tmp[0]
		print "Maximum Intensity = ", tmp[1]
		print "Average Intensity = ", tmp[2]
		return	


	def sweep(self, motor="rz", start_pos=None, end_pos=None, t_exp=None, filename="ccdmar_image", folder=".", shutter="I03-C-C06/EX/OBX.1"):
		"""
		Typical usage:
		Sweep rz from -5 to +5 in 60 seconds, save file as test_image in directory : 

		my345.sweep(\"rz\", -5, 5, 60, \"test_image\", \"/nfs/ruche-psiche/psiche-soleil/com-psiche/test_directory/\")

		Optional arguments:
		filetype = 0 corresponds to marccd (.mccd) files (default),
		filetype = 1 to nxs files
		shutter device (default is experimental hutch shutter)
		"""

		# positive movements only for the moment
		if (end_pos - start_pos)<0:
			print "please sweep in the positive direction for the moment!"
			return		
		
		# save the original motor position and speed
		att = PyTango.AttributeProxy(movables[motor])
		init_pos = att.read().value
		init_v = att.get_device_proxy().read_attribute("velocity").value
		init_acc = att.get_device_proxy().read_attribute("acceleration").value
		
		# variables
		t_shut = 4.5 # shutter moving time
		t_pad = 0.5 # safety
		t_ccd = t_exp + (2*t_shut) + (2*t_pad) # time for ccd
		new_v = (end_pos - start_pos) / t_ccd # motor speed required
		t_acc = new_v / init_acc # acceleration time
		s_acc = new_v * t_acc * 0.5 # acceleration distance
		s_start = start_pos - s_acc
		s_end = end_pos + s_acc
		
		# check the velocity
		if new_v > init_v:
			print "Max motor speed is %0.2f !" % init_v
			print "Change your sweep parameters to go slower"
			return

		# get the shutter device
		if shutter <> "":
			dev_shut = PyTango.DeviceProxy(shutter)
		
		# check that the ccd is ready
		if self.state() <> DevState.STANDBY:
			raise Exception("mar345: cannot sweep if device not in STANDBY state.")

		# move to the start position
		print "move to start position..."
		mv(motor, s_start)

		# put everything inside a try loop for safety
		try:
		
			# set the motor speed
			print "set motor speed %0.2f ..." % new_v
			att.get_device_proxy().write_attribute("velocity", new_v)
		
			# start the movement, wait for the acceleration time
			print "start movement to %0.2f ..." % s_end
			att.get_device_proxy().write_attribute("position", s_end)
			time.sleep(t_acc)
			
			# wait the safety time, then open the shutter
			time.sleep(t_pad)
			if shutter <> "":
				print "open shutter"
				dev_shut.Open()
			
			# wait the moving time, and the exposure time, then close
			print "start counting"
			time.sleep(t_shut + t_exp)
			print "finished counting"
			if shutter <> "":
				print "close shutter"
				dev_shut.Close()
			
				# wait for the shutter to close
				while dev_shut.State()  <> PyTango._PyTango.DevState.CLOSE:
					time.sleep(0.1)
				if self.state() <> PyTango._PyTango.DevState.STANDBY:
					print "shutter has closed, mar345 still running"
			
			# read out the image plate
			self.Scan()
			t0 = time.time()
			while self.State()==DevState.RUNNING:
				sleep(1)
		 		if time.time()-t0 > 180:
 					print "timed out while scanning"
 					break
 				
 			print "finished scanning image plate"	
			# copy / save the image
			self.saveImage(filename,folder)
			# statistics
			tmp=self.stats()
			print "Minimum Intensity = ", tmp[0]
			print "Maximum Intensity = ", tmp[1]
			print "Average Intensity = ", tmp[2]
		
		except KeyboardInterrupt:
			# stop the motor
			att.get_device_proxy().stop()
			print "Stopping!"
			if shutter <> "":
				# close the shutter
				dev_shut.Close()
				print "Closing shutter!"
			
		finally:
			# stop the motor in case it is still moving
			att.get_device_proxy().stop()
			sleep(0.5)	
			# reset the speed
			print "reset motor speed %0.2f ..." % init_v
			att.get_device_proxy().write_attribute("velocity", init_v)
		
			# return to the start position
			print "return to original position"
			mv(motor, init_pos)
			print "finished"

						
																		
			
	def saveImage(self, filename = "mar345_image", folder="."):
		# currently very simple - 
		
		# get the current filename and path from the MAR345 DeviceServer
		source_mar = self.saveFileName
		# we want to copy this to our experiment directory
		#folder = "/nfs/ruche-psiche/psiche-soleil/tempdata/com-psiche/LTEE_0913/"
		destination = findNextFileName(folder + os.sep + filename,"mar3450")
		
		# do the copy
		os.system("scp detecteur@dt-pcmar1:"+source_mar+" "+destination)
		print "Data saved in :", destination

						
	def stop(self):
		if self.state()==DevState.RUNNING:
			self.DP.command_inout("Stop")
		return self.state()
	
	def read(self):
		return self.pos()
	
	def stats(self):
		tmp=self.pos()
		return tmp.min(), tmp.max(), tmp.mean()
	
	def count(self, dt=1, filename = "ccdmar_image",folder=".", noreturn = True):
		self.snap(dt, filename = filename, folder = folder)
		if noreturn:
			return
		else:
			return self.read()

