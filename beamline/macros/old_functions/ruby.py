import PyTango


def ruby_on():
	'''make sure detector is safe and rz2 is at zero, then move in laser'''
	# check detector position
	att = PyTango.AttributeProxy("i03-c-c07/dt/det2-mt_ts/position")
	dts2pos = att.read().value
	print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
	print "dts2 measurement position is %0.2f" % dts2pos
	print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
	get_ipython().magic(u'amove rz2 0')
	if dts2pos<50:
		print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
		print "Moving dts2 to safe position!"
		get_ipython().magic(u'amove dts2 50')
		print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
		print "Remember to return to the measurement position!"
		print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
	get_ipython().magic(u'amove prl 100')

def ruby_off():
	get_ipython().magic(u'amove prl 0')
	print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
	print "Remember to return dts2 to measurement position!"
	print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
