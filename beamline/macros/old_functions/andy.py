# -*- coding: utf-8 -*-
'''

Simple tools for moving and alignment.
Lots copied or adapted from Cristal, Galaxies, Nicolas Jaouen, etc.
Auto alignment script borrowed from ID27.
Aim to have mv/mvr/ascan/dscan, using diode read from Keithley.

Andy King, 2/7/2013

'''

# ~~~~~~~~~~~~~~~ IMPORT MODULES ~~~~~~~~~~~~~~~~~~~

import PyTango
import time
import numpy as np
import sys
from math import isnan

# ~~~~~~~~~~~~~~~ BEAMLINE CONFIG ~~~~~~~~~~~~~~~~~~~

# add primary slits and mirror benders - not for users -
# should have a mode "expert" and mode "user"
# beamstop x and z
# goniometer tz,tx,ts below rotation rz
# above rotation translations tx2, ts2 - RENAME THESE?
# slits on the table - call them s2

# temp note !!!  I update the names to the new convention in the version of this file which is used by spyc !!
# likewise, I remove the line which append the sys.path

movables = {
		# primary slits :
		"pshg":"I03-C-C02/OP/FENT_H.1/gap",
		"psho":"I03-C-C02/OP/FENT_H.1/position",
		"psvg":"I03-C-C02/OP/FENT_V.1/gap",
		"psvo":"I03-C-C02/OP/FENT_V.1/position",
		# mirror benders... :
		"mbend1":"I03-C-C03/OP/MIR1-MT_C1/position",
		"mbend2":"I03-C-C03/OP/MIR1-MT_C2/position",
		# monochromator... :
		"mono_rx2":"I03-C-C04/OP/MONO1-MT_Rx.2/position",
		# large press :
		"prz":"I03-C-C05/EX/PRS.1-MT_Rz.1/position",
		"pts":"I03-C-C05/EX/PRS.1-MT_Ts.1/position",
		"ptx":"I03-C-C05/EX/PRS.1-MT_Tx.1/position",
		"ptz":"I03-C-C05/EX/PRS.1-MT_Tz.1/position",
		# caesar :
		"theta":"I03-C-C05/EX/CAESAR/theta",
		# slits in optic hutch :
		"s1hg":"I03-C-C08/OP/FENT_H.1/gap",
		"s1ho":"I03-C-C08/OP/FENT_H.1/position",
		"s1vg":"I03-C-C08/OP/FENT_V.1/gap",
		"s1vo":"I03-C-C08/OP/FENT_V.1/position",
		"cs1hg":"I03-C-C05/OP/FENT_H.1/gap",
		"cs1ho":"I03-C-C05/OP/FENT_H.1/position",
		"cs1vg":"I03-C-C05/OP/FENT_V.1/gap",
		"cs1vo":"I03-C-C05/OP/FENT_V.1/position",
		"cs2hg":"I03-C-C05/EX/FENT_H.2-Temporaire/gap",
		"cs2ho":"I03-C-C05/EX/FENT_H.2-Temporaire/position",
		"cs2vg":"I03-C-C05/EX/FENT_V.2-Temporaire/gap",
		"cs2vo":"I03-C-C05/EX/FENT_V.2-Temporaire/position",
		# beamstop, goniometer and ruby pressure in monochromatic hutch :
		"bsx":"I03-C-C07/EX/BEAMSTOP-MT_Tx/position",
		"bsz":"I03-C-C07/EX/BEAMSTOP-MT_Tz/position",
		"ts2":"I03-C-C07/EX/GONIO.2-MT_Ts.1/position",
		"tx2":"I03-C-C07/EX/GONIO.2-MT_Tx.1/position",
		"tz2":"I03-C-C07/EX/GONIO.2-MT_Tz.1/position",
		"rz2":"I03-C-C07/EX/GONIO.2-MT_Rz.1/position",
		"cs2":"I03-C-C07/EX/GONIO.2-MT_Ts.2/position",
		"cx2":"I03-C-C07/EX/GONIO.2-MT_Tx.2/position",
		"prl":"I03-C-C07/EX/PRL.1-MT_Tx.1/position",
		"cs":"I03-C-C07/DT/TOMO-MT_TS.1/position",
		"rz3":"i03-c-cx3/ex/gonio.3-mt_rz.1/position",
		# slits in monochromatic hutch :
		"s2hg":"I03-C-C07/OP/FENT_H.1/gap",
		"s2ho":"I03-C-C07/OP/FENT_H.1/position",
		"s2vg":"I03-C-C07/OP/FENT_V.1/gap",
		"s2vo":"I03-C-C07/OP/FENT_V.1/position",
        # gonio3
        "rz3":"i03-c-cx3/ex/gonio.3-mt_rz.1/position",
        "ts3":"i03-c-cx3/ex/gonio.3-mt_ts.1/position",
        "cs3":"i03-c-cx3/ex/gonio.3-mt_ts.2/position",
        "tx3":"i03-c-cx3/ex/gonio.3-mt_tx.1/position",
        "cx3":"i03-c-cx3/ex/gonio.3-mt_tx.2/position",
        "tz3":"i03-c-cx3/ex/gonio.3-mt_tz.1/position",
        "kbvc1":"i03-c-c08/op/kb.1-mt_c.1/position",
        "kbvc2":"i03-c-c08/op/kb.1-mt_c.2/position",
        "kbvrx":"i03-c-c08/op/kb.1-mt_rx.1/position",
        "kbvtx":"i03-c-c08/op/kb.1-mt_tx.1/position",
        "kbvtz":"i03-c-c08/op/kb.1-mt_tz.1/position",
        "kbhc1":"i03-c-c08/op/kb.2-mt_c.1/position",
        "kbhc2":"i03-c-c08/op/kb.2-mt_c.2/position",
        "kbhrz":"i03-c-c08/op/kb.2-mt_rz.1/position",
        "kbhtx":"i03-c-c08/op/kb.2-mt_tx.1/position"
            }

# the sensors
sensors = {
		"diode":"i03-C-C03/mi/K6485.1/value",
		"roi1":"i03-C-CX1/dt/dtc-mca_xmap.1/roi00_01",
		"roi2":"i03-C-CX1/dt/dtc-mca_xmap.1/roi00_02",
		"roi3":"i03-C-CX1/dt/dtc-mca_xmap.1/roi00_03",
		"roi4":"i03-C-CX1/dt/dtc-mca_xmap.1/roi00_04",
		"roi5":"i03-C-CX1/dt/dtc-mca_xmap.1/roi00_05",
		# example:
		"dummy":"my/dummy/sensor/value"
			}

# the tango scan device
_scan = PyTango.DeviceProxy("i03-C-C00/ex/scan.1")

# device server for the keithley
_keithley1 = PyTango.DeviceProxy("i03-C-C03/mi/K6485.1")

# data fitter device for fitting
_fitter = PyTango.DeviceProxy("i03-C-C00/ex/datafitter.1")

# this works inside a module, but not in the base workspace
__builtins__['movables'] = movables

# shutters
_feshutter = PyTango.DeviceProxy("TDL-I03-C/VI/TDL.1")
_ehshutter = PyTango.DeviceProxy("I03-C-C06/EX/OBX.1")

## add the two python folders to the path
#sys.path.append("/home/experiences/psiche/com-psiche/python_cristal")
#sys.path.append("/home/experiences/psiche/com-psiche/python_samba")

# ~~~~~~~~~~~~~~~ COMPLEX FUNCTIONS ~~~~~~~~~~~~~~~~~~~


# these sections should be moved into different files
def centerSX(halfScanRange, oscillationAngle, rzCenter):
    '''
    usage: centerSX(halfScanRange, oscillationAngle, rzCenter)
    From ID27.
    Align cell on center of rotation.
    '''
    # move rz3 to starting point
    mv("rz2", rzCenter)
    # center in cx3
    dscan("cx2", -halfScanRange, halfScanRange, 20, 0.3)
    cen1 = _fitter.position
    mv("cx2", cen1)
    print "center is {}".format(cen1)
    # move in rz3, find center in cx3
    mv("rz2", rzCenter+oscillationAngle)
    dscan("cx2", -halfScanRange, halfScanRange, 20, 0.3)
    cen2 = _fitter.position
    print "center is {}".format(cen2)
    # move in rz3, find center in cx3
    mv("rz2", rzCenter-oscillationAngle)
    dscan("cx2", -halfScanRange, halfScanRange, 20, 0.3)
    cen3 = _fitter.position
    print "center is {}".format(cen3)
    # return rz3 to starting point
    mv("rz2", rzCenter)
    # calculate the required correction in cs2
    deltaS = -(cen2 - cen3) / (2 * np.sin(oscillationAngle * np.pi / 180))

    ##### change the sign of the correction for the replacement cs2
    deltaS = -deltaS
    #####

    print "delta cs2 calculated as {}".format(deltaS)
    checkA = str(raw_input("Apply this correction? (yes/[no])  ")).lower()
    if checkA in ["yes", "y", "1", "true"]:
        mvr("cs2", deltaS)
    else:
        print "Not correcting cs2"
    # calculate the required correction in cx3
    deltaX = (cen2 + cen3 - 2*cen1) / (2 * (1 - np.cos(oscillationAngle * np.pi / 180)))

    ##### change the sign of the correction for the replacement cx3
    deltaX = -deltaX
    #####

    print "delta cx2 calculated as {}".format(deltaX)
    # ask the user, and convert response to lowercase string
    checkB = str(raw_input("Apply this correction? (yes/[no])  ")).lower()
    if checkB in ["yes", "y", "1", "true"]:

        mvr("cx2", deltaX)

        # do we correct the centre of rotation with respect to the beam?
        print "Should the center of rotation be corrected (in tx2)?"
        # ask the user, and convert response to lowercase string
        checkC = str(raw_input("Apply this correction? (yes/[no])  ")).lower()
        if checkC in ["yes", "y", "1", "true"]:
            mvr("tx2", -deltaX)
        else:
            print "Not correcting tx2"

    else:
        print "Not correcting cx2"



# ~~~~~~~~~~~~~~~ COMPLEX FUNCTIONS ~~~~~~~~~~~~~~~~~~~

# these sections should be moved into different files

def centerS(halfScanRange, oscillationAngle, rzCenter):
	'''
	usage: centerS(halfScanRange, oscillationAngle, rzCenter)
	From ID27.
	Align cell on center of rotation, in the beam direction only.
	'''
	# move rz to starting point
	mv("rz3", rzCenter)
	# center in tx3
	dscan("cx3", -halfScanRange, halfScanRange, 20, 0.1)
	cen1 = _fitter.position
	mv("cx3", cen1)
	print "center is {}".format(cen1)
	# move in rz, find center in tx3
	mv("rz3", rzCenter+oscillationAngle)
	dscan("cx3", -halfScanRange, halfScanRange, 20, 0.1)
	cen2 = _fitter.position
	print "center is {}".format(cen2)
	# move in rz, find center in tx3
	mv("rz3", rzCenter-oscillationAngle)
	dscan("cx3", -halfScanRange, halfScanRange, 20, 0.1)
	cen3 = _fitter.position
	print "center is {}".format(cen3)
	# return rz to starting point
	mv("rz3", rzCenter)
	# calculate the required correction in ts2
	deltaS = -(cen2 - cen3) / (2 * np.sin(oscillationAngle * np.pi / 180))

	##### change the sign of the correction for the replacement tx2
	deltaS = -deltaS
	#####

	print "delta cs3 calculated as {}".format(deltaS)
	checkA = str(raw_input("Apply this correction? (yes/[no])  ")).lower()
	if checkA in ["yes", "y", "1", "true"]:
		mvr("cs3", deltaS)
	else:
		print "Not correcting cs3"


# ~~~~~~~~~~~~~~~ COMPLEX FUNCTIONS ~~~~~~~~~~~~~~~~~~~

# these sections should be moved into different files

def rubyon():
	'''
	usage: put the PRL objective ON
	'''
	# move rz to 0°
	mv("rz", 90)
	# move prl to 100
	mv("prl", 100)

def rubyoff():
	'''
	usage: put the PRL objective OFF
	'''
	# move prl to 0
	mv("prl", 0)
	# move rz to 0
	mv("rz", 0)



# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def shopen():
	_ehshutter.Open()
	t0 = time.time()
	while _ehshutter.State()  <> PyTango._PyTango.DevState.OPEN:
			time.sleep(0.1)
			if time.time()-t0 > 6:
				print "time out waiting for shutter to open!"
				break

def shclose():
	_ehshutter.Close()
	t0 = time.time()
	while _ehshutter.State()  <> PyTango._PyTango.DevState.CLOSE:
			time.sleep(0.1)
			if time.time()-t0 > 6:
				print "time out waiting for shutter to close!"
				break
def feopen():
	_feshutter.Open()
	t0 = time.time()
	while _feshutter.State()  <> PyTango._PyTango.DevState.OPEN:
			time.sleep(0.1)
			if time.time()-t0 > 6:
				print "time out waiting for shutter to open!"
				break
def feclose():
	_feshutter.Close()
	t0 = time.time()
	while _feshutter.State()  <> PyTango._PyTango.DevState.CLOSE:
			time.sleep(0.1)
			if time.time()-t0 > 6:
				print "time out waiting for shutter to close!"
				break


# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def wm(motor):
	'''
	usage: wm("motor name")
	'''
	# call mv without a position argument to display current position
	mv(motor)
	lowerLimit, upperLimit = get_lm(motor)
	print "Limits of %s are %s, %s" % (motor, lowerLimit, upperLimit)

# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def mv(motor, newPosition=None):
	'''
	usage: mv("motor name", position)
	Move to absolute position
	If no second argument, display current position.
	'''
	# get the attribute proxy for the motor position
	att = PyTango.AttributeProxy(movables[motor])
	# read and show the position
 	position = att.read().value
	print "current value is %0.3f" % position

	# move only if a new position is given, and it is not the current position
	if newPosition!=None:
		try:
			att.write(newPosition)
			while att.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]:
				sys.stdout.write("\rmoving...")
				# to print *now*!
				sys.stdout.flush()
				time.sleep(0.5)
				sys.stdout.write("%s" % "\b"*9)
				sys.stdout.write("...moving")
				sys.stdout.flush()
				time.sleep(0.5)
				sys.stdout.write("%s" % "\b"*9)
				sys.stdout.flush()

		except KeyboardInterrupt:
			print "Stopping!"
			name = movables[motor]
       		# name is the address of the device and the value moved
			device = PyTango.DeviceProxy(name[:name.rindex('/')])
        	# strip off the value, leaving only the address of the device
			device.stop()
			# stop the device
			# note this is a DeviceProxy, not an AttributeProxy
		except:
			print "Requested move to %0.2f failed! - software limit set?" % newPosition
			lowerLimit, upperLimit = get_lm(motor)
			print "Limits of %s are %s, %s" % (motor, lowerLimit, upperLimit)

		finally:
			position = att.read().value
			print "Now value is %0.3f" % position
	return position

# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def mvr(motor, increment=0):
	'''
	usage: mvr("motor name", increment)
	Relative movement by increment
	'''
	# get the attribute proxy for the motor position
	att = PyTango.AttributeProxy(movables[motor])
	# read the position
 	position = att.read().value
	# move the motor using mv
	mv(motor, position+increment)

# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def ascan(motor, p1, p2, nsteps, ctime=1, sensor="diode"):
	'''
	usage: ascan("motor name", p1, p2, nsteps, ctime=1, sensor="diode"):
	Absolute scan of motor from position p1 to p2 in nsteps.
	Optional parameters are the count time (default 1 second),
	and the sensor (default is the diode).
	Returns to starting position after scan.
	Includes a "sleep" of 0.1 seconds.
	There is other deadtime of about 2 seconds, so don't worry about this!
	'''

	# initialise the data fitter
	print "reset the data fitter"
	_fitter.Init()
	# reset the scan server
	_scan.Clean()
	# add sensors : the diode by default
	_scan.sensors = [sensors[sensor]]
	# add motors
	_scan.actuators = [movables[motor]]
	# save the motor name
	_motor_scanned = motor
	# calculate trajectory
	trajectories = [np.linspace(p1, p2, nsteps+1)]
	_scan.trajectories = trajectories
	# count times for these points
	_scan.integrationTimes = np.ones(trajectories[0].shape) * ctime
	# delay time - like _sleep in SPEC?
	_scan.actuatorsdelay = 0.1
	# after the scan (0 don't move, 1 back to first point, 2 back to original position)
	_scan.afterRunActionType=2
	# some display
	print "scanning motor %s from %0.2f to %0.2f in %d steps of %0.2f seconds, reading %s" % (motor, p1, p2, nsteps, ctime, sensor)
	# start the scan
	_scan.Start()
	# while loop for safety
	try:
		while _scan.state() not in [PyTango.DevState.ON, PyTango.DevState.ALARM]:

			# update user on progress
			mystring = "\rscan %0.1f %% complete..." % _scan.scanCompletion
			sys.stdout.write(mystring)
			sys.stdout.flush()
			time.sleep(2)
			sys.stdout.write("%s" % "\b"*len(mystring))
			sys.stdout.flush()

		print "returning %s to original position" % motor

	except KeyboardInterrupt:
		_scan.abort()
	finally:
		# fit the scan data
		print "calling the data fitter"
		_fitter.StartFit()
		# wait a little while if needed
		count = 0
		while (isnan(_fitter.position) and count<10):
		    count+=1
		    time.sleep(0.1)
		if isnan(_fitter.position):
		    print "data fitter failed!"
		# put position - the peak center in workspace
		# note centroid is probably center of mass!
		__builtins__['CEN'] = _fitter.position
		print "Scan finished"

# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def dscan(motor, d1, d2, nsteps, ctime=1, sensor="diode"):
	'''
	usage: dscan("motor name", p1, p2, nsteps, ctime=1, sensor="diode"):
	Relative scan of motor from delta d1 to d2 in nsteps.
	Optional parameters are the count time (default 1 second),
	and the sensor (default is the diode).
	Uses "ascan" to perform the scan.
	Ascan returns to starting position after scan.
	Ascan includes a "sleep" of 0.1 seconds.
	There is other deadtime of about 2 seconds, so don't worry about this!
	'''

	# get the attribute proxy for the motor position
	att = PyTango.AttributeProxy(movables[motor])
	# read the position
 	position = att.read().value
	# calculate the absolute start and end points
	p1 = position+d1
	p2 = position+d2
	# launch the ascan
	ascan(motor, p1, p2, nsteps, ctime, sensor)

# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def timescan(npoints, ctime=1, sensor="diode"):
	'''
	usage: timescan(npoints, ctime=1, sensor="diode")
	Without moving anything, acquire as a function of time with
	the specified sensor.
	'''
	# initialise the data fitter
	print "reset the data fitter"
	_fitter.Init()
	# reset the scan server
	_scan.Clean()
	# give a name to the scan
	_scan.runName = 'timescan'
	# select sensor
	_scan.sensors = [sensors[sensor]]
	# give count times
	_scan.integrationTimes = np.ones((npoints)) * ctime
	# some display
	print "time scan of %d steps of %0.2f seconds, reading %s" % (npoints, ctime, sensor)
	# start the scan
	_scan.Start()
	# while loop for safety
	try:
		while _scan.state() not in [PyTango.DevState.ON, PyTango.DevState.ALARM]:

			# update user on progress
			mystring = "\rscan %0.1f %% complete..." % _scan.scanCompletion
			sys.stdout.write(mystring)
			sys.stdout.flush()
			time.sleep(2)
			sys.stdout.write("%s" % "\b"*len(mystring))
			sys.stdout.flush()

	except KeyboardInterrupt:
		_scan.abort()
	finally:
		print "not calling data fitter for time scans!"


# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def set(motor, newValue):
	'''
	usage: set("motor name", new value)
	Reset the current position of the motor to the given value.
	Needs to update limits accordingly!
	'''
	name = movables[motor]
    # name is the address of the device and the value moved
	device = PyTango.DeviceProxy(name[:name.rindex('/')])
    # strip off the value, leaving only the address of the device
	# get the initial value
	oldPos = device.position
	# recalculate the offset
	device.ComputeNewOffset(newValue)
	# confirm that it has worked
	newPos = device.position
	print "value of %s reset from %0.2f to %0.2f" % (motor, oldPos, newPos)
	# deal with any software limits
	lowerLimit, upperLimit = get_lm(motor)
	if lowerLimit=="Not specified":
		newLowerLimit=None
	else:
		newLowerLimit=float(lowerLimit)+newPos-oldPos
	if upperLimit=="Not specified":
		newUpperLimit=None
	else:
		newUpperLimit=float(upperLimit)+newPos-oldPos
	# set the new limits
	print "software limits updated:"
	set_lm(motor, newLowerLimit, newUpperLimit)


# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def set_lm(motor, lowerLimit, upperLimit):
	'''
	set_lm(motor, lowerLimit, upperLimit)
	Reset the current position of the motor to the given value.
	'''
	# treat input values
	if lowerLimit==None:
		lowerLimit = "Not specified"
	if upperLimit==None:
		upperLimit = "Not specified"
	# convert to strings
	lowerLimit = str(lowerLimit)
	upperLimit = str(upperLimit)
    # name is the address of the device and the value moved
	att = PyTango.AttributeProxy(movables[motor])
	# get the configuration of the attribute
	conf = att.get_config()
	# get the initial values
	oldLowerLimit = conf.min_value
	oldUpperLimit = conf.max_value
	# modify values, pass as strings
	conf.min_value = lowerLimit
	conf.max_value = upperLimit
	# write the config
	att.set_config(conf)
	# display
	print "limits of %s set to %s, %s" % (motor, lowerLimit, upperLimit)

# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def get_lm(motor,):
	'''
	get_lm(motor)
	Read the current software limits of the motor.
	'''
    # name is the address of the device and the value moved
	att = PyTango.AttributeProxy(movables[motor])
	# get the configuration of the attribute
	conf = att.get_config()
	# get the initial values
	lowerLimit = conf.min_value
	upperLimit = conf.max_value
	# return the values (as strings)
	return lowerLimit, upperLimit

# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def mv_cen():
	'''
	Move to centroid position of the last scan
	Note this may do horrible things if multiple motors are scanned simultaneously
	'''
	# get device scanned
	devScanned = _scan.actuators[0]
	# find the corresponding motor key
	motor = (motor for motor,dev in movables.items() if dev.lower()==devScanned).next()
	# get the fitted position
	position = _fitter.position
	checkA = str(raw_input("move {} to CEN ({}) [yes/[no]] ".format(motor, position))).lower()
	if checkA in ["yes", "y", "1", "true"]:
		mv(motor, position)
	else:
		print "Not moving. CEN value is available in workspace"

def ct(sensor="diode"):
	'''
	usage ct(sensor="diode")
	Count - by default the diode value
	'''
	att = PyTango.AttributeProxy(sensors[sensor])
	value = att.read().value
	print "sensor {} ({}) : value = {}".format(sensors[sensor], sensor, value)

# ~~~~~~~~~~~~~~~ BASIC FUNCTIONS ~~~~~~~~~~~~~~~~~~~

def reconfig():
	print "Good try ;-)"

