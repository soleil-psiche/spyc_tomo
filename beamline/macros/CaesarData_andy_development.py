# -*- coding: utf-8 -*-

'''
OK:  Make a class for caesar developments
This can inherit from the current caesar, and just adds what we want to test
'''
from CaesarData import CaesarData
import pylab
pylab.ion()
import numpy as np


class CaesarDataNew(CaesarData):

    def __init__(self):
        ''' initialise CaesarData'''
        CaesarData.__init__(self)
        # now we have all the normal stuff - just add the methods we want to test


    def amorphousDataTreatmentDevelopment(self):
        '''Try applying latest test version of treatment to Caesar acquistion'''
        if self.caesar_image != "":
            print "correcting the current Caesar data - development version of the code"
            self.__amorphousTreatmentDevelopment()
            self._CaesarData__showImage()
        else:
            print "Load Caesar data first!"


    def __amorphousTreatmentDevelopment(self, lowE=None, highE=None, Qnchan=None, Efactor=None, tthfactor=None, niterations=None):
        '''Convert every pixel into Q, allow volume correction if wanted'''

        # parameters
        print "Fit using a reduced energy range - "
        energy = self.energy * 1.0
        if lowE == None:
            lowE = raw_input("Starting energy [default 20] :")
            lowE = 20.0 if lowE == '' else float(lowE)
        if highE == None:
            highE = raw_input("Final energy [default 60] :")
            highE = 60.0 if highE == '' else float(highE)
        if Qnchan == None:
            Qnchan = raw_input('How many channels in Q (smooth data - 512? crystalline - 2048?) [1024] : ')
            Qnchan = 1024 if Qnchan == '' else int(Qnchan)
        if Efactor == None:
            Efactor = raw_input('Factor for adjusting E spectrum (bigger is faster, smaller more stable) [0.2] : ')
            Efactor = 0.2 if Efactor == '' else float(Efactor)
        if tthfactor == None:
            tthfactor = raw_input('Factor for adjusting the prenormalisation (bigger is faster, smaller more stable) [0.02] : ')
            tthfactor = 0.02 if tthfactor == '' else float(tthfactor)
        if niterations == None:
            niterations = raw_input('How many iterations [20] : ')
            niterations = 20 if niterations == '' else int(niterations)

        # get the data
        ndx1 = np.nonzero(self.energy>lowE)[0][0]
        ndx2 = np.nonzero(self.energy>highE)[0][0]
        energy = energy[ndx1:ndx2]
        egrid,tthgrid = np.meshgrid(self.energy[ndx1:ndx2], self.caesar_tth)
        Echannel, tthchannel = np.meshgrid(np.arange(len(self.energy[ndx1:ndx2])), np.arange(len(self.caesar_tth)))
        # calc Q for every pixel
        Qgrid = 4*np.pi*egrid*np.sin(np.deg2rad(tthgrid/2.))/12.398
        # get I for every pixel
        Igrid = self.caesar_image[:, ndx1:ndx2] * 1.
        # turn these into lists
        Elist = egrid.flatten()
        Echannel = Echannel.flatten()
        tthlist = tthgrid.flatten()
        tthchannel = tthchannel.flatten()
        Qlist = Qgrid.flatten()
        Ilist = Igrid.flatten()
        # organise the data into Q channels
        Qstep = (Qlist.max()-Qlist.min()) / (Qnchan-1)
        Qchannel = np.round((Qlist - Qlist.min()) / Qstep)
        Qchannel = Qchannel.astype(int)
        # guess some profiles as a starting point
        Eprofile_guess = np.ones(ndx2-ndx1)
        for ii in range(len(Eprofile_guess)):
            Eprofile_guess[ii] = Ilist[np.nonzero(Echannel==ii)].mean()
        Eprofile_guess = Eprofile_guess/Eprofile_guess.max()
        # we have prenormalised so tthprofile should be close to ones
        tthprofile_guess = np.ones(len(self.caesar_tth))
        # where are we starting from?
        Inorm = (Ilist / Eprofile_guess[Echannel]) * tthprofile_guess[tthchannel]
        Qprofile_guess = np.ones(Qnchan)
        for ii in range(Qnchan):
            Qprofile_guess[ii] = Inorm[np.nonzero(Qchannel==ii)].mean()

        # initially - how far are we from a consistent profile in Q
        dif0 = Inorm - Qprofile_guess[Qchannel]

        # prepare a figure
        pylab.figure()
        pylab.subplot(3,2,1)
        pylab.title('Qprofile_guess')
        pylab.subplot(3,2,3)
        pylab.title('Eprofile_guess')
        pylab.subplot(3,2,4)
        pylab.title('Eprofile_adjust')
        pylab.subplot(3,2,5)
        pylab.title('tthprofile_guess')
        pylab.subplot(3,2,6)
        pylab.title('tthprofile_adjust')

        # Now, iterate to optimise Eprofile_guess, and maybe tthprofile_guess
        for iteration in range(niterations):

            # normalise - energy and tth
            Inorm = (Ilist / Eprofile_guess[Echannel]) * tthprofile_guess[tthchannel]

            # make a new Q profile
            for ii in range(Qnchan):
                Qprofile_guess[ii] = Inorm[np.nonzero(Qchannel==ii)].mean()

            # new difference
            dif1 = Inorm - Qprofile_guess[Qchannel]

            # now - use this difference (as a f(energy)) to adjust Eprofile_guess
            Eprofile_adjust = np.zeros(Eprofile_guess.shape)
            for ii in range(len(Eprofile_guess)):
                Eprofile_adjust[ii] = dif1[np.nonzero(Echannel==ii)].mean()

            # apply this to the E profile
            Eprofile_guess = Eprofile_guess * (1 + Eprofile_adjust*Efactor)
            # normalise to max = 1
            Eprofile_guess = Eprofile_guess/Eprofile_guess.max()

            # now - if we adjust the tth profile
            tthprofile_adjust = np.zeros(tthprofile_guess.shape)
            for ii in range(len(tthprofile_guess)):
                tthprofile_adjust[ii] = dif1[np.nonzero(tthchannel==ii)].mean()
            # apply this to the tth profile
            tthprofile_guess = tthprofile_guess * (1 - tthprofile_adjust*tthfactor)
            # normalise to mean = 1
            tthprofile_guess = tthprofile_guess/tthprofile_guess.mean()

            if np.mod(iteration, 3)==0:
                pylab.subplot(3,2,1)
                pylab.plot(Qprofile_guess)
                pylab.subplot(3,2,3)
                pylab.plot(Eprofile_guess)
                pylab.subplot(3,2,4)
                pylab.plot(Eprofile_adjust)
                pylab.subplot(3,2,5)
                pylab.plot(tthprofile_guess)
                pylab.subplot(3,2,6)
                pylab.plot(tthprofile_adjust)
                pylab.draw()

        # finished iterations - apply the final profiles
        # normalise - energy and tth
        Inorm = (Ilist / Eprofile_guess[Echannel]) * tthprofile_guess[tthchannel]
        # make a new Q profile
        for ii in range(Qnchan):
            Qprofile_guess[ii] = Inorm[np.nonzero(Qchannel==ii)].mean()

        # put results back into self
        self.fitted_spectrum = Eprofile_guess # any row of this image
        self.fitted_spectrum_energy = energy * 1. # includes the crop
        self.fitted_volume_correction = tthprofile_guess # keep a copy of this
        self.caesar_image_original = self.caesar_image * 1. # backup the initial image
        imnew = np.zeros(self.caesar_image.shape)
        imEnorm = Inorm.reshape(len(self.caesar_tth), ndx2-ndx1)
        imnew[:, ndx1:ndx2] = imEnorm
        self.caesar_image = imnew
        # use __convertImage to produce the profiles in Q and d
        self._CaesarData__convertImage() # this gives an image in d as well

        imQnorm,Qgrid = myConvertImage(imEnorm, self.caesar_tth, energy) # convert to Q
        self.caesar_image_Q = imQnorm
        self.caesar_image_Qx = Qgrid
        pQ = myMakeProfile(imQnorm, 0) # simple version - use the imQ
        self.caesar_image_Q_profile = pQ
        pQ2 = myMakeProfileIm2(imQnorm, 0, exclude=3) # reject outliers version
        self.caesar_image_Q_profile_cor = pQ2[0,:]

        self._CaesarData__showImage()
        # use my_plot to show the result of the optimisation
        pylab.figure()
        my_plot(self.caesar_image_Q, self.caesar_image_Qx, self.caesar_tth)


def myConvertImage(im, tth, energy, test_norm=False):
    ''' convert data from energy to dspacing and Q
        return Nan outside the energy range'''
    # convert the spectrum, if available
    theta = tth/2
    # preallocate an output image
    npoints = 4096
    im2 = np.zeros([im.shape[0], npoints])
    # dspacing limits for the whole acquistion
    dmax = 12.398 / (2*np.sin(theta[0]*np.pi/180)*energy[0]) # lowest angle, lowest energy
    dmin = 12.398 / (2*np.sin(theta[-1]*np.pi/180)*energy[-1]) # highest angle, highest energy
    Qmin = 2*np.pi/dmax
    Qmax = 2*np.pi/dmin
    Qgrid = np.linspace(Qmin, Qmax, npoints)
    # convert line by line
    for ii in range(im.shape[0]):
        # energy profile
        prof = im[ii, :]
        # as d spacing
        dsp = 12.398 / (2*np.sin(theta[ii]*np.pi/180)*energy)
        # as Q
        Q = 2*np.pi/dsp
        if test_norm:
            print 'test normalising' # these two factors largely compensate for each other.
            prof = prof / (Q[-1]-Q[0])
            prof = prof / np.arctan(10./(1220*np.sin(np.deg2rad(tth[ii]))))
        # as Q - flip profiles for increasing
        im2[ii, :] = np.interp(Qgrid, Q, prof, np.nan, np.nan)

    return im2, Qgrid


def myMakeProfileIm2(im, axis=0, exclude=3):
    '''
    make nan mean profile, but excluding extreme values (defined in sigma)
    '''
    count = (np.logical_not(np.isnan(im))).sum(axis)
    p = np.ones(im.shape[1-axis]) * np.nan
    for ii in range(im.shape[1-axis]):
        if count[ii]>1:
            if axis==0:
               data = im[:, ii]
            else:
                data = im[ii, :]
            data =  np.sort(data) # sort into ascending order, nans at the end
            data = data[0:count[ii]] # remove nans
            mystd = data.std()
            mymean = data.mean()
            ndx1 = np.nonzero(data>(mymean-(mystd*exclude)))[0][0]
            ndx2 = np.nonzero(data<(mymean+(mystd*exclude)))[0][-1]
            p[ii] = data[ndx1:ndx2].mean()
    if axis==0:
        pim = np.tile(p, (im.shape[0], 1))
    else:
        pim = np.tile(p.reshape(len(p), 1), (1, im.shape[1]))
    return pim


def myMakeProfileIm(im, axis=0):
    '''
    make nan mean profile image for normalising
    '''
    p = np.nansum(im, axis)
    count = (np.logical_not(np.isnan(im))).sum(axis)
    p = p/count
    if axis==0:
        pim = np.tile(p, (im.shape[0], 1))
    else:
        pim = np.tile(p.reshape(len(p), 1), (1, im.shape[1]))
    return pim


def myMakeProfile(im, axis=0):
    '''
    make nan mean profile for normalising
    '''
    p = np.nansum(im, axis)
    count = (np.logical_not(np.isnan(im))).sum(axis)
    p = p/count
    return p


def my_plot(qimage, Qgrid, caesar_tth, colour=None, npoints=7):
    ndx = np.arange(0, qimage.shape[0], np.round(qimage.shape[0]/npoints))
    if colour != None:
        for ii in range(len(ndx)):
            pylab.plot(Qgrid, qimage[ndx[ii],:], colour, label=('two theta %0.1f' % caesar_tth[ndx[ii]]))
    else:
        for ii in range(len(ndx)):
            pylab.plot(Qgrid, qimage[ndx[ii],:], label=('two theta %0.1f' % caesar_tth[ndx[ii]]))
    pylab.legend()


