# -*- coding: utf-8 -*-
import PyTango
from PyTango import DevState,DeviceProxy,DevFailed
from time import sleep
import time
import os, sys
from PIL import Image
from valve_class import valve
from moveable import sensor
import numpy as np
import sys
import os.path
import glob
from spyc import beamline_print
from spyc.core import DeviceManager
dm = DeviceManager()

# redundant things or changes
#import h5py
#from andy import mv
# andy 1/2015 - change mv to spyc amove, use beamline_print for messages
#path_to_spool = "/nfs/srv1/spool1/perkinelmer"


att_dts2 = PyTango.AttributeProxy("i03-c-c07/dt/det2-mt_ts/position")
att_dtx2 = PyTango.AttributeProxy("i03-c-c07/dt/det2-mt_tx/position")
att_dtz2 = PyTango.AttributeProxy("i03-c-c07/dt/det2-mt_tz/position")
att_cs2 = PyTango.AttributeProxy("i03-c-c07/ex/gonio.2-mt_ts.2/position")
att_cx2 = PyTango.AttributeProxy("i03-c-c07/ex/gonio.2-mt_tx.2/position")
att_tz2 = PyTango.AttributeProxy("i03-c-c07/ex/gonio.2-mt_tz.1/position")
#att_shut = PyTango.AttributeProxy("i03-c-c07/ex/prl.1-mt_tx.1/position")
#att_shut = PyTango.AttributeProxy("i03-c-c07/ex/tomo.2-mt_tz.1/position")

# temp for furnace
att_furnace = PyTango.AttributeProxy("i03-c-c00/ca/sai.1/averagechannel2")

# use the ds_SingleShotDIO (portE) to control the fast shutter -> fastshutter in spyc
dev_shut = PyTango.DeviceProxy('i03-c-c00/ca/dio_o.1')
t_shut = 0.25 # declare this here once and for all
# with the fastshutter, a=time.time();get_ipython().magic(u'amove fastshutter 1');b=time.time();b-a=~0.5
# take half of this as the moving time

# make a simple version to use the accumulation mode in the DS




def findNextFileName(prefix,ext,file_index=1):
    #
    #Prepare correct filename to avoid overwriting
    #
    psep=prefix.rfind(os.sep)
    if(psep<>-1):
        fdir=prefix[:psep]
    else:
        fdir="."
    if(psep<>-1): prefix=prefix[psep+1:]
    if ext<>"":
        fname=prefix+"_"+"%04i"%(file_index)+"."+ext
    else:
        fname=prefix+"_"+"%04i"%(file_index)
    _dir=os.listdir(fdir)
    while(fname in _dir):
        file_index+=1
        if ext<>"":
            fname=prefix+"_"+"%04i"%(file_index)+"."+ext
        else:
            fname=prefix+"_"+"%04i"%(file_index)
    fname=fdir+os.sep+fname
    return fname


def findFirstIndex(prefix,ext,file_index=1):
    #
    # Returns the first index that will be used by findNextFileName
    #
    psep=prefix.rfind(os.sep)
    if(psep<>-1):
        fdir=prefix[:psep]
    else:
        fdir="."
    if(psep<>-1): prefix=prefix[psep+1:]
    if ext<>"":
        fname=prefix+"_"+"%04i"%(file_index)+"."+ext
    else:
        fname=prefix+"_"+"%04i"%(file_index)
    _dir=os.listdir(fdir)
    while(fname in _dir):
        file_index+=1
        if ext<>"":
            fname=prefix+"_"+"%04i"%(file_index)+"."+ext
        else:
            fname=prefix+"_"+"%04i"%(file_index)
    return file_index


class PE(sensor):

    '''
    BEWARE: All times are translated in seconds!
    '''
    def __init__(self, device="i03-c-c07/dt/perkinelmer.1", attribute="image"):
        # initialise camera, and setup the focus and zoom motors of detector
        sensor.__init__(self, device, attribute)
        #beamline_print("you must define the fast shutter position using myPE.setShutterPosition() - using tomotx = 0 by default")
        #self.setShutterPosition()

#    def setShutterPosition(self, motorpos=0):
#        ''' define the position at which tomotx cuts the beam (open = -ve, closed = +ve)'''
#        self.fsopen_pos = motorpos-1
#        self.fsclose_pos = motorpos+1
#        beamline_print("using shutter open: tomotz = %0.1f" % self.fsopen_pos)
#        beamline_print("using shutter closed: tomotz = %0.1f" % self.fsclose_pos)

    def fsopen(self):
        #att_shut.write(self.fsopen_pos)
        #get_ipython().magic(u'amove fastshutter 0')
        #dev_shut.portE=0
        #dev_shut.resetline('PE0')
        dev_shut.resetline('PG7')

    def fsclose(self):
        #att_shut.write(self.fsclose_pos)
        #get_ipython().magic(u'amove fastshutter 1')
        #dev_shut.portE=1
        #dev_shut.setline('PE0')
        dev_shut.setline('PG7')

    def snap(self, t_exp=1, filename = "PE_image",folder="."):
        '''
        Typical usage:
        Take an image, counting 5 seconds, save file as test_image in directory :

        myPE.snap(5, \"test_image\", \"/nfs/ruche-psiche/psiche-soleil/com-psiche/test_directory/\")

        Snap an image.  Uses the fast shutter
        '''

        # make sure that we use the first image
        dp = PyTango.DeviceProxy('i03-c-c07/dt/perkinelmer.1-specific')
        dp.keepFirstImage=True

        # check that the ccd is ready
        if self.state() <> DevState.STANDBY:
            raise Exception("PE: cannot snap if device not in STANDBY state.")

        # set the exposure time - no limit in ACCUMULATION
        if self.DP.acquisitionMode == 'SINGLE' and t_exp>5:
            beamline_print("Device must be in ACCUMULATION mode for >5 seconds exposures")
            beamline_print("quitting...")
            return
        else:
            self.DP.write_attribute("exposureTime", t_exp * 1000)

        # trigger the shutter
        self.fsopen()
        # wait the shutter opening time
        time.sleep(t_shut)

        # then start the detector
        beamline_print("start PE for image aquisition")
        self.DP.command_inout("Snap")

        # wait the acquistion time - the shutter time
        time.sleep(t_exp)

        # trigger the shutter
        self.fsclose()
        # wait the shutter opening time
        time.sleep(t_shut)

        # now wait for the image to appear
        timeout = time.time() + 30.
        timeout_flag = False
        while self.state() <> DevState.STANDBY:
            beamline_print("waiting for detector to finish")
            sleep(0.1)
            if time.time() > timeout:
                timeout_flag = True
                beamline_print("PE: timeout >30 seconds while performing a snap!!!")
                break

        # get the image
        im = self.image * 1.0

        # convert the image ready for saving
        im = Image.fromarray(im)
        self.output_im = im

        # save the image and show the statistics
        if not timeout_flag:
            # save the image
            self.saveImage(filename,folder)

            tmp=self.stats()
            beamline_print("Minimum Intensity = ", tmp[0])
            beamline_print("Maximum Intensity = ", tmp[1])
            beamline_print("Average Intensity = ", tmp[2])

            beamline_print("finished - remember that fast shutter is closed! - fastshutteropen to open")



    def sweep(self, motor="rz2", start_pos=None, end_pos=None, t_exp=None, filename="pe_image", folder=".", logfilehandle=None, return_to_start=True):
        '''
        Typical usage:
        Sweep rz from -5 to +5 in 5 seconds, save file as test_image in directory :

        myPE.sweep(\"rz2\", -5, 5, 5, \"test_image\", \"/nfs/ruche-psiche/psiche-soleil/com-psiche/test_directory/\")

        Sweep integrating image, using fast shutter
        Optional arguments: logfilehandle=None, return_to_start=True
        '''

        # make sure that we use the first image
        dp = PyTango.DeviceProxy('i03-c-c07/dt/perkinelmer.1-specific')
        dp.keepFirstImage=True

        # with prl with acceleration of 10, a movement of 1 mm is about 0.6 seconds
        # with the fastshutter, a=time.time();get_ipython().magic(u'amove fastshutter 1');b=time.time();b-a=~0.5
        # take half of this as the moving time?
        t_shut = .25 # shutter moving time
        t_pad = 0.1 # safety time

        # treat the exposure time and number of frames
        t_exp = t_exp * 1.0 # time for ccd

        # check that the ccd is ready
        if self.state() <> DevState.STANDBY:
            raise Exception("PE: cannot snap if device not in STANDBY state.")

        # set the exposure time - no limit in ACCUMULATION
        if self.DP.acquisitionMode == 'SINGLE' and t_exp>5:
            beamline_print("Device must be in ACCUMULATION mode for >5 seconds exposures")
            beamline_print("quitting...")
            return
        else:
            self.DP.write_attribute("exposureTime", t_exp * 1000)

        # positive movements only for the moment
        #if (end_pos - start_pos)<0:
        #    beamline_print("please sweep in the positive direction for the moment!")
        #    return

        # save the original motor position and speed
        init_pos = dm.actors[motor].proxy.position
        init_v = dm.actors[motor].proxy.velocity
        init_acc = dm.actors[motor].proxy.read_attribute("acceleration").value

        # calculate the new values
        new_v = np.abs(end_pos - start_pos) / t_exp # motor speed required
        t_acc = new_v / init_acc # acceleration time
        s_acc = new_v * t_acc * 0.5 # acceleration distance
        s_shut = t_shut * new_v # shutter distance
        s_pad = t_pad * new_v # pad distance
        if end_pos > start_pos:
            s_start = start_pos - s_acc - s_shut - s_pad
            s_end = end_pos + s_acc + s_shut + s_pad
        else:
            s_start = start_pos + s_acc + s_shut + s_pad
            s_end = end_pos - s_acc - s_shut - s_pad

        # check the velocity
        if new_v > init_v:
            beamline_print("Max motor speed is %0.2f !" % init_v)
            beamline_print("Change your sweep parameters to go slower")
            return
        if new_v < 0.006231:
            beamline_print("The calculated velocity is too slow")
            beamline_print("Change your sweep parameters to go faster")
            return

        # check that the ccd is ready
        if self.state() <> DevState.STANDBY:
            raise Exception("PE: cannot snap if device not in STANDBY state.")

        # write to the log file
        if logfilehandle != None:
            logfilehandle.write("##################################################\n")
            logfilehandle.write("timestamp = %0.2f\n" % time.time())
            loctime = time.localtime()
            logfilehandle.write("time = %02d : %02d : %02d\n" % (loctime[3],loctime[4],loctime[5]))
            logfilehandle.write("date = %02d/%02d/%04d\n" % (loctime[2],loctime[1],loctime[0]))
            logfilehandle.write("sweep = motor %s, from %0.2f to %0.2f\n" % (motor, start_pos, end_pos))
            logfilehandle.write("exposuretime = %0.2f\n" % t_exp)
            #logfilehandle.write("nbFrames = %d\n" % nframes)

            # save some other motor positions
            # detector S/X/Z
            #logfilehandle.write("dts2 = %0.2f\n" % att_dts2.read().value)
            #logfilehandle.write("dtx2 = %0.2f\n" % att_dtx2.read().value)
            #logfilehandle.write("dtz2 = %0.2f\n" % att_dtz2.read().value)
            logfilehandle.write("cs2 = %0.2f\n" % att_cs2.read().value)
            logfilehandle.write("cx2 = %0.2f\n" % att_cx2.read().value)
            logfilehandle.write("tz2 = %0.2f\n" % att_tz2.read().value)
            logfilehandle.write("furnace = %0.2f\n" % (att_furnace.read().value*200.))
            logfilehandle.flush()

        # move to the start position
        beamline_print("move to start position...")
        #mv(motor, s_start)
        get_ipython().magic(u'amove %s %0.2f' % (motor, s_start))

        # put everything inside a try loop for safety
        try:

            # trigger the shutter
            beamline_print("close fast shutter")
            self.fsclose()
            # wait to be sure that the shutter is closed
            time.sleep(t_shut*3)

            '''# wait the shutter to close
            timeout = time.time() + 10.
            timeout_flag = bool(0)

            while att_shut.get_device_proxy().state() <> DevState.STANDBY:
                sleep(0.2)
                beamline_print("waiting for shutter to close")
                if time.time() > timeout:
                    timeout_flag = bool(1)
                    beamline_print("Motor: timeout >10 seconds for shutter close!!!")
                    break'''


            # set the motor speed
            beamline_print("set motor speed %0.2f ..." % new_v)
            dm.actors[motor].proxy.velocity = new_v

            # start the movement
            beamline_print("start movement to %0.2f ..." % s_end)
            dm.actors[motor].proxy.position = s_end

            # wait the acceleration time and the safety time
            time.sleep(t_acc + t_pad)

            # trigger the shutter
            beamline_print("open shutter")
            self.fsopen()

            # wait the shutter opening time
            time.sleep(t_shut)

            # then start the detector
            beamline_print("start PE for image aquisition")
            self.DP.command_inout("Snap")

            # write to logfile
            #if logfilehandle != None:
            #    real_start_pos = att.read().value
            #    logfilehandle.write("rz2Start = %0.2f\n" % real_start_pos)
            #    logfilehandle.flush()

            # wait the acquistion time - the shutter time
            time.sleep(t_exp - t_shut)

            # trigger the shutter
            beamline_print("close shutter")
            self.fsclose()

            # wait the shutter opening time
            time.sleep(t_shut)

            # write to logfile
            #if logfilehandle != None:
            #    real_end_pos = att.read().value
            #    logfilehandle.write("rz2End = %0.2f\n" % real_end_pos)
            #    av_pos = (real_start_pos+real_end_pos)/2.0
            #    logfilehandle.write("rz2 = %0.2f\n" % av_pos)
            #    logfilehandle.flush()

            # now wait for the image to appear
            timeout = time.time() + 30.
            timeout_flag = False
            while self.state() <> DevState.STANDBY:
                beamline_print("waiting for detector to finish")
                sleep(0.5)
                if time.time() > timeout:
                    timeout_flag = True
                    beamline_print("PE: timeout >30 seconds while performing a sweep!!!")
                    break

            # get the image
            try:
                im = self.image * 1.0
            except:
                print "sleep another 3 seconds and try again!!!"
                time.sleep(3)
                im = self.image * 1.0

            #  convert
            im = Image.fromarray(im)
            self.output_im = im

            # save the image and show the statistics
            if not timeout_flag:

                # save the image
                self.saveImage(filename,folder)

                tmp=self.stats()
                beamline_print("Minimum Intensity = ", tmp[0])
                beamline_print("Maximum Intensity = ", tmp[1])
                beamline_print("Average Intensity = ", tmp[2])
                if logfilehandle != None:
                    logfilehandle.write("FileName = %s\n" % os.path.basename(self.lastfilename))
                    logfilehandle.write("FullFileName = %s\n" % self.lastfilename)
                    logfilehandle.flush()

        except KeyboardInterrupt:
            # stop the motor
            # stop the motor
            dm.actors[motor].proxy.stop()
            beamline_print("Stopping!")
            # in this case close the logfile if there is one
            if logfilehandle != None:
                logfilehandle.write("sweep was aborted by ctrl-C !\n")
                logfilehandle.flush()
                logfilehandle.close()
            raise KeyboardInterrupt('sweep was aborted by control-c!')

        finally:
            # stop the motor in case it is still moving
            # stop the motor in case it is still moving
            dm.actors[motor].proxy.stop()
            sleep(0.5)
            # reset the speed
            beamline_print("reset motor speed %0.2f ..." % init_v)
            dm.actors[motor].proxy.velocity = init_v
            if return_to_start:
                # return to the start position
                beamline_print("return to original position")
                #mv(motor, init_pos)
                get_ipython().magic(u'amove %s %0.2f' % (motor, init_pos))
            else:
                # don't return to start position
                beamline_print("not returning to original position")
                # mv without new position just displays the current position
                #mv(motor)
                get_ipython().magic(u'wm %s' % motor)

            if logfilehandle != None:
                logfilehandle.flush()
            beamline_print("finished - remember that fast shutter is closed! - fastshutteropen to open")
        return

    def sweep_sequence(self, motor="rz2", start_pos=None, end_pos=None, nimages=None, t_exp=None, filename="pe_image", folder="./"):
        '''
        call a series of self.sweep(self, motor="rz2", start_pos=None, end_pos=None, t_exp=None, filename="pe_image", folder=...)
        '''
        # if we have n images, we divide the range by n+1 - make sure we get a float answer
        step = (end_pos - start_pos) / ((nimages*1.0)+1)
        starts = np.linspace(start_pos, (end_pos-step), nimages)
        ends = np.linspace(start_pos+step, end_pos, nimages)

        # logfile name
        logfileprefix = folder + os.sep + filename + "_sweeplog"
        logfileextension = "txt"
        logfilename = findNextFileName(logfileprefix,logfileextension)
        logfilehandle = open(logfilename, 'w')
        logfilehandle.write("# sweep_sequence logfile\n")
        logfilehandle.write("# time is %0.2f\n" % time.time())
        loctime = time.localtime()
        logfilehandle.write("# time is %02d : %02d : %02d\n" % (loctime[3],loctime[4],loctime[5]))
        logfilehandle.write("# date is %02d/%02d/%04d\n" % (loctime[2],loctime[1],loctime[0]))

        logfilehandle.write("# sweep motor %s, from %0.2f to %0.2f in %d images\n" % (motor, start_pos, end_pos, nimages))
        logfilehandle.write("# exposure time is %0.2f\n" % t_exp)
        logfilehandle.write("# file prefix is %s, directory is %s\n" % (filename, folder))
        logfilehandle.write("# Detector motors in S,X,Z: dts2, dtx2, dtz2\n")
        logfilehandle.write("# Sample position S,X,Z: cs2, cx2, tz2\n")
        logfilehandle.write("# Rotation is : rz2\n")
        logfilehandle.write("FilePrefix = %s\n" % filename)
        firstnumber = findFirstIndex(folder + os.sep +filename,"tif")
        lastnumber = firstnumber + nimages -1
        logfilehandle.write("LastImage = %d\n" % lastnumber)
        logfilehandle.write("FirstImage = %d\n" % firstnumber)
        logfilehandle.write("FirstImageName = %s_%04i.%s\n" % (filename,firstnumber,"tif"))
        logfilehandle.write("LastImageName = %s_%04i.%s\n" % (filename,lastnumber,"tif"))
        logfilehandle.write("TimeStampStart = %0.2f\n" % time.time())

        logfilehandle.flush()

        # get the initial motor position to come back afterwards
        att = PyTango.AttributeProxy(movables[motor])
        init_pos = att.read().value

        for ii in range(nimages):
            self.sweep(motor, starts[ii], ends[ii], t_exp, filename, folder, logfilehandle, return_to_start=False)

        # return to the initial motor position
        beamline_print("return to original position after sequence")
        #mv(motor, init_pos)
        get_ipython().magic(u'amove %s %0.2f' % (motor, init_pos))

        logfilehandle.write("##################################################\n")
        logfilehandle.write("# sweep_sequence finished\n")
        logfilehandle.write("# time is %0.2f\n" % time.time())
        loctime = time.localtime()
        logfilehandle.write("# time is %02d : %02d : %02d\n" % (loctime[3],loctime[4],loctime[5]))
        logfilehandle.write("# date is %02d/%02d/%04d\n" % (loctime[2],loctime[1],loctime[0]))
        logfilehandle.write("TimeStampEnd = %0.2f\n" % time.time())
        logfilehandle.close()


    def saveImage(self, filename = "pe_image", folder="."):
        '''internal method, make private?'''

        fileprefix = folder + os.sep + filename
        myfilename = findNextFileName(fileprefix, "tif")
        self.output_im.save(myfilename)
        beamline_print("image saved as %s" % myfilename)
        self.lastfilename = myfilename
        return


    def stop(self):
        if self.state()==DevState.RUNNING:
            self.DP.command_inout("Stop")
        return self.state()

    def read(self):
        return self.pos()

    def stats(self):
        tmp=self.pos()
        return tmp.min(), tmp.max(), tmp.mean()


