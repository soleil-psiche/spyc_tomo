# -*- coding: utf-8 -*-


from andy import *
import numpy as np
import pylab
pylab.ion()
import PyTango
import time
import fitting





def scan_slit(direction="horizontal", roi=[0,0,696,519], scanrange=0.5, npoints=20, camera="i03-c-c07/dt/vg.1"):
    ''' scan slit s1 offset, measure the position of the intensity in the image

    scan_slit(direction="horizontal"/"vertical", roi=[startX,startY,endX,endY], scanrange=0.5, npoints=20, camera="i03-c-c07/dt/vg.1")

    possible help for which bender to bend -

    # in the plot - a linear slope means that we need to change the bending - so both benders in the same sense

    # in vertical scan... c1 positive makes slope more negative.
    # in vertical scan... c2 positive makes slope more negative
    # in vertical scan... c1 positive and c2 negative reduces curvature

    # in horizontal scan...  c1 positive makes slope more negative
    # in horizontal scan...  c2 positive makes slope negative
    # in horizontal scan...  c1 positive and c2 negative increases curvature
    # horizontal - kbhc2 is the right hand side of the profile...'''

    if direction=="horizontal":
        offset = "i03-c-c08/op/fent_h.1/position"
        gap = "i03-c-c08/op/fent_h.1/gap"
        offsetmotor = "s3ho"
        gapmotor = "s3hg"
    elif direction == "vertical":
        offset = "i03-c-c08/op/fent_v.1/position"
        gap = "i03-c-c08/op/fent_v.1/gap"
        offsetmotor = "s3vo"
        gapmotor = "s3vg"
    else:
        print "direction should be vertical or horizontal - quitting..."
        return

    # get the devices
    camDP = PyTango.DeviceProxy(camera)
    offsetAP = PyTango.AttributeProxy(offset)
    gapAP = PyTango.AttributeProxy(gap)
    camDP.stop()
    # remeber the original positions
    offset0 = offsetAP.read().value
    gap0 = gapAP.read().value

    # close to 50 microns
    print "closing gap to 50 mu"
    #mv(gapmotor, 0.05)
    get_ipython().magic(u'amove %s %f' % (gapmotor, 0.05))
    # scan offset
    offsetpos = np.linspace(offset0-scanrange, offset0+scanrange, npoints)
    data = np.zeros(offsetpos.shape)
    data_int = np.zeros(offsetpos.shape)
    data_sigma = np.zeros(offsetpos.shape)

    # stop live
    camDP.stop()

    for ii in range(npoints):
        #mv(offsetmotor, offsetpos[ii])
        get_ipython().magic(u'amove %s %f' % (offsetmotor, offsetpos[ii]))
        time.sleep(0.1)
        camDP.snap()
        while camDP.State() == PyTango._PyTango.DevState.RUNNING:
            time.sleep(0.1)
        time.sleep(0.1)
        # calculate the centre of the intensity
        try:
            im = camDP.image[roi[1]:roi[3], roi[0]:roi[2]]
        except:
            print 'failed to read the camera!!! sleep 3 seconds and try again'
            time.sleep(3)
            try:
                im = camDP.image[roi[1]:roi[3], roi[0]:roi[2]]
            except:
                print 'failed to read the camera!!! sleep 3 seconds and try again'
                print "call init in case it helps"
                camDP.Init()
                time.sleep(3)
                im = np.zeros((10,10))
                print "no image for this point;;;"


        im = im - np.median(im)
        # threshold at 10% of max to remove noise
        threshold = np.max(im) * 0.1
        im[np.nonzero(im<threshold)]=0
        if direction=="horizontal":
            profile = im.sum(0)
        else:
            profile = im.sum(1)
        # fit this with a gaussian
        xdata = np.arange(len(profile))
        xfit,profilefit = fitting.fit_gaussian(xdata, profile)
        data[ii] = xfit[1]
        data_int[ii] = xfit[0]
        data_sigma[ii] = xfit[2]

    # restart live
    camDP.start()

    print "done scan... reset gap and offset positions"
    #mv(gapmotor, gap0)
    #mv(offsetmotor, offset0)
    get_ipython().magic(u'amove %s %f' % (gapmotor, gap0))
    get_ipython().magic(u'amove %s %f' % (offsetmotor, offset0))

    pylab.figure(55)
    pylab.subplot(3,1,1)
    pylab.plot(offsetpos, data, '-x')
    pylab.xlabel('position')
    pylab.subplot(3,1,2)
    pylab.plot(offsetpos, data_int, '-x')
    pylab.xlabel('intensity')
    pylab.subplot(3,1,3)
    pylab.plot(offsetpos, data_sigma, '-x')
    pylab.xlabel('sigma (peak width)')


def scan_z(roi=[0,0,696,519], scanrange=0.5, npoints=50, camera="i03-c-c07/dt/vg.1"):
    ''' scan kbvtz, measure the position of the intensity in the image'''

    # get the device
    kbz = "i03-c-c08/op/kb.1-mt_tz.1/position"
    kbzAP = PyTango.AttributeProxy(kbz)
    camDP = PyTango.DeviceProxy(camera)
    camDP.stop()
    # remeber the original positions
    kbz0 = kbzAP.read().value

    # scan offset
    kbzpos = np.linspace(kbz0-scanrange, kbz0+scanrange, npoints)
    data = np.zeros(kbzpos.shape)
    data_int = np.zeros(kbzpos.shape)
    data_sigma = np.zeros(kbzpos.shape)

    for ii in range(npoints):
        get_ipython().magic(u'amove kbvtz %0.3f' % kbzpos[ii])
        time.sleep(0.1)
        camDP.snap()
        while camDP.state() == PyTango._PyTango.DevState.RUNNING:
            time.sleep(0.1)
        time.sleep(0.1)
        # calculate the centre of the intensity
        im = camDP.image[roi[1]:roi[3], roi[0]:roi[2]]
        im = im - np.median(im)
        # threshold at 10% of max to remove noise
        threshold = np.max(im) * 0.1
        im[np.nonzero(im<threshold)]=0
        profile = im.sum(1)
        # fit this with a gaussian
        xdata = np.arange(len(profile))
        xfit,profilefit = fitting.fit_gaussian(xdata, profile)
        data[ii] = xfit[1]
        data_int[ii] = xfit[0]
        data_sigma[ii] = xfit[2]


    print "done scan... reset kbz position"
    get_ipython().magic(u'amove kbvtz %0.3f' % kbz0)

    pylab.figure(55)
    pylab.subplot(3,1,1)
    pylab.plot(kbzpos, data, '-x')
    pylab.subplot(3,1,2)
    pylab.plot(kbzpos, data_int, '-x')
    pylab.subplot(3,1,3)
    pylab.plot(kbzpos, data_sigma, '-x')



def scan_rx(roi=[0,0,696,519], scanrange=0.5, npoints=50, camera="i03-c-c07/dt/vg.1"):
    ''' scan kbvrx, measure the position of the intensity in the image'''

    # get the device
    kbrx = "i03-c-c08/op/kb.1-mt_rx.1/position"
    kbrxAP = PyTango.AttributeProxy(kbrx)
    camDP = PyTango.DeviceProxy(camera)
    camDP.stop()
    # remeber the original positions
    kbrx0 = kbrxAP.read().value

    # scan offset
    kbrxpos = np.linspace(kbrx0-scanrange, kbrx0+scanrange, npoints)
    data = np.zeros(kbrxpos.shape)
    data_int = np.zeros(kbrxpos.shape)
    data_sigma = np.zeros(kbrxpos.shape)

    for ii in range(npoints):
        get_ipython().magic(u'amove kbvrx %0.3f' % kbrxpos[ii])
        time.sleep(0.1)
        camDP.snap()
        while camDP.state() == PyTango._PyTango.DevState.RUNNING:
            time.sleep(0.1)
        time.sleep(0.1)
        # calculate the centre of the intensity
        im = camDP.image[roi[1]:roi[3], roi[0]:roi[2]]
        im = im - np.median(im)
        # threshold at 10% of max to remove noise
        threshold = np.max(im) * 0.1
        im[np.nonzero(im<threshold)]=0
        profile = im.sum(1)
        # fit this with a gaussian
        xdata = np.arange(len(profile))
        xfit,profilefit = fitting.fit_gaussian(xdata, profile)
        data[ii] = xfit[1]
        data_int[ii] = xfit[0]
        data_sigma[ii] = xfit[2]


    print "done scan... reset kbvrx position"
    get_ipython().magic(u'amove kbvrx %0.3f' % kbrx0)

    pylab.figure(55)
    pylab.subplot(3,1,1)
    pylab.plot(kbrxpos, data, '-x')
    pylab.subplot(3,1,2)
    pylab.plot(kbrxpos, data_int, '-x')
    pylab.subplot(3,1,3)
    pylab.plot(kbrxpos, data_sigma, '-x')



def scan_rz(roi=[0,0,696,519], scanrange=0.5, npoints=50, camera="i03-c-c07/dt/vg.1"):
    ''' scan kbhrz, measure the position of the intensity in the image'''

    # get the device
    kbrz = "i03-c-c08/op/kb.2-mt_rz.1/position"
    kbrzAP = PyTango.AttributeProxy(kbrz)
    camDP = PyTango.DeviceProxy(camera)
    camDP.stop()
    # remeber the original positions
    kbrz0 = kbrzAP.read().value

    # scan offset
    kbrzpos = np.linspace(kbrz0-scanrange, kbrz0+scanrange, npoints)
    data = np.zeros(kbrzpos.shape)
    data_int = np.zeros(kbrzpos.shape)
    data_sigma = np.zeros(kbrzpos.shape)

    for ii in range(npoints):
        get_ipython().magic(u'amove kbhrz %0.3f' % kbrzpos[ii])
        time.sleep(0.1)
        camDP.snap()
        while camDP.state() == PyTango._PyTango.DevState.RUNNING:
            time.sleep(0.1)
        time.sleep(0.1)
        # calculate the centre of the intensity
        im = camDP.image[roi[1]:roi[3], roi[0]:roi[2]]
        im = im - np.median(im)
        # threshold at 10% of max to remove noise
        threshold = np.max(im) * 0.1
        im[np.nonzero(im<threshold)]=0
        profile = im.sum(0)
        # fit this with a gaussian
        xdata = np.arange(len(profile))
        xfit,profilefit = fitting.fit_gaussian(xdata, profile)
        data[ii] = xfit[1]
        data_int[ii] = xfit[0]
        data_sigma[ii] = xfit[2]


    print "done scan... reset kbhrz position"
    get_ipython().magic(u'amove kbhrz %0.3f' % kbrz0)

    pylab.figure(55)
    pylab.subplot(3,1,1)
    pylab.plot(kbrzpos, data, '-x')
    pylab.subplot(3,1,2)
    pylab.plot(kbrzpos, data_int, '-x')
    pylab.subplot(3,1,3)
    pylab.plot(kbrzpos, data_sigma, '-x')


def scan_ts(roi=[0,0,696,519], scanrange=0.5, npoints=50, camera="i03-c-c07/dt/vg.1"):
    '''find the S position of the focal point'''

    # get the device
    ts3 = "i03-c-cx3/ex/gonio.3-mt_ts.1/position"
    ts3AP = PyTango.AttributeProxy(ts3)
    camDP = PyTango.DeviceProxy(camera)
    cam.DP.stop()

    # remeber the original positions
    ts30 = ts3AP.read().value

    # scan ts
    ts3pos = np.linspace(ts30-scanrange, ts30+scanrange, npoints)
    data = np.zeros(ts3pos.shape)
    data_int = np.zeros(ts3pos.shape)
    data_sigma = np.zeros(ts3pos.shape)

    for ii in range(npoints):
        get_ipython().magic(u'amove ts3 %0.3f' % ts3pos[ii])
        time.sleep(0.1)
        camDP.snap()
        while camDP.state() == PyTango._PyTango.DevState.RUNNING:
            time.sleep(0.1)
        time.sleep(0.1)
        # calculate the centre of the intensity
        im = camDP.image[roi[1]:roi[3], roi[0]:roi[2]]
        im = im - np.median(im)
        # threshold at 10% of max to remove noise
        threshold = np.max(im) * 0.1
        im[np.nonzero(im<threshold)]=0
        profile = im.sum(0)
        # fit this with a gaussian
        xdata = np.arange(len(profile))
        xfit,profilefit = fitting.fit_gaussian(xdata, profile)
        data[ii] = xfit[1]
        data_int[ii] = xfit[0]
        data_sigma[ii] = xfit[2]

    print "done scan... reset ts3 position"
    get_ipython().magic(u'amove ts3 %0.3f' % ts30)

    pylab.figure(55)
    pylab.subplot(3,1,1)
    pylab.plot(ts3pos, data, '-x')
    pylab.subplot(3,1,2)
    pylab.plot(ts3pos, data_int, '-x')
    pylab.subplot(3,1,3)
    pylab.plot(ts3pos, data_sigma, '-x')









