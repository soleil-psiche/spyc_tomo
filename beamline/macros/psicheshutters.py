# -*- coding: utf-8 -*-

from spyc import beamline_print
import PyTango
import time

# safety shutters
feshutter = PyTango.DeviceProxy("TDL-I03-C/VI/TDL.1")
ehshutter = PyTango.DeviceProxy("I03-C-C06/EX/OBX.1")
# fast shutters
beamline_print("Get the fast shutter device")
dev_shut = PyTango.DeviceProxy('i03-c-c00/ca/dio_o.1')
dev_shut_5V = PyTango.DeviceProxy('i05-c-c00/ca/dio.2')

def shopen():
    ehshutter.Open()
    t0 = time.time()
    while ehshutter.State()  <> PyTango._PyTango.DevState.OPEN:
            time.sleep(0.1)
            if time.time()-t0 > 10:
                print "time out waiting for shutter to open!"
                break

def shclose():
    ehshutter.Close()
    t0 = time.time()
    while ehshutter.State()  <> PyTango._PyTango.DevState.CLOSE:
            time.sleep(0.1)
            if time.time()-t0 > 10:
                print "time out waiting for shutter to close!"
                break
def feopen():
    feshutter.Open()
    t0 = time.time()
    while feshutter.State()  <> PyTango._PyTango.DevState.OPEN:
            time.sleep(0.1)
            if time.time()-t0 > 10:
                print "time out waiting for shutter to open!"
                break
def feclose():
    feshutter.Close()
    t0 = time.time()
    while feshutter.State()  <> PyTango._PyTango.DevState.CLOSE:
            time.sleep(0.1)
            if time.time()-t0 > 10:
                print "time out waiting for shutter to close!"
                break

def fastshutteropen():
    dev_shut.resetline('PG7')
    beamline_print("Opening fast shutter")

def fastshutterclose():
    dev_shut.setline('PG7')
    beamline_print("Closing fast shutter")

def fwbsopen():
    dev_shut.resetline('PF1')
    beamline_print("Opening fast white beam shutter")

def fwbsclose():
    dev_shut.setline('PF1')
    beamline_print("Closing fast white beam shutter")

def tomofurnaceON():
    dev_shut.setline('PF7')
    beamline_print("Power ON tomo furnace Z")

def tomofurnaceOFF():
    dev_shut.resetline('PF7')
    beamline_print("Power OFF tomo furnace Z")

def tomofurnaceUP():
    dev_shut.resetline('PF6')
    beamline_print("direction UP tomo furnace Z")

def tomofurnaceDOWN():
    dev_shut.setline('PF6')
    beamline_print("direction DOWN tomo furnace Z")
