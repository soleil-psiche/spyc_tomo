# -*- coding: utf-8 -*-
import time
import numpy as np
ifft = np.fft.ifft
fft = np.fft.fft
#from Detector import Detector
from Tomo import Tomo
import subprocess
import PyTango
import os
import pylab
pylab.ion()
# handling motors with spyc
from spyc.core import DeviceManager
dm = DeviceManager()
import glob
'''
A class to run the RoToPec experiment
Need to be able to do a tomoscan type acquistion
Base this on the Tomo class (i.e. inherits from Camera and Detector, and Tomo)
'''

# as in Tomo, get the sbox
sbox = PyTango.DeviceProxy('flyscan/clock/spietbox-timebase.1')
ftpclient = PyTango.DeviceProxy('flyscan/core/ftp-client.3')

class rotopec(Tomo):

    def __init__(self, mycamera="orca3", config="rotopec"):
        # initialise detector(camera)
        # camera handles taking and displaying images
        # detector handles the optics, focusing, alignment etc
        # tomo handles data acquisition

        Tomo.__init__(self, mycamera, config)

        # preallocate with default values
        self.nproj = 3000
        self.timestep = 400.0 # this is in ms
        self.nrefbefore = 31
        self.nrefafter = 31
        self.ndark = 31
        self.refMotor = 'rotopecx'
        self.refDisplacement = 11
        self.scanrange = 180.0
        self.send_triggers = True # set this to false for user generated triggers
        self.main_shutter_for_darks = True
        #self.shutteropen = 'feopen'
        #self.shutterclose = 'feclose'

    # some things are tomo specific and don't make sense in the rotopec context
    def doTomo(self):
        print "this is disabled for the rotopec"
    def tomoscan(self):
        print "this is disabled for the rotopec"
    def alignSample(self):
        print "this is disabled for the rotopec"
    def centreAxis(self):
        print "this is disabled for the rotopec"
    def setFlyscanParameters(self):
        print "this is disabled for the rotopec"
    def tomoscan(self):
        print "this is disabled for the rotopec"
    # some things are tomo specific and don't make sense in the rotopec context



    def setScanParameters(self):
        '''User select time scan parameters for Rotopec'''
        # get parameters from the user

        userinput = raw_input("scan name [%s]:  " % self.scanname)
        if userinput != '':
            self.scanname = userinput

        userinput = raw_input("number of images [%d]:  " % self.nproj)
        if userinput != '':
            self.nproj = int(userinput)

        userinput = raw_input("number of references before [%d]:  " % self.nrefbefore)
        if userinput != '':
            self.nrefbefore = int(userinput)

        userinput = raw_input("number of references after [%d]:  " % self.nrefafter)
        if userinput != '':
            self.nrefafter = int(userinput)

        userinput = raw_input("number of darks after [%d]:  " % self.ndark)
        if userinput != '':
            self.ndark = int(userinput)

        userinput = raw_input("Use main shutter for darks? [y]/n:  ")
        self.main_shutter_for_darks = True if userinput.lower() in ['', 'y', 'yes'] else False

        print "The exposure time is %d milliseconds" % self.proxy.exposuretime
        readouttime = self.calcReadoutTime() * 1000
        self.safetytime = np.max([(self.proxy.exposuretime/1000.) * 0.05, 0.002])
        mintimestep = self.proxy.exposuretime + readouttime + (self.safetytime*1000)
        print "minimum timestep is %0.2f milliseconds (%0.2f fps)" % (mintimestep, (1000./mintimestep))

        userinput = raw_input("time interval (/milliseconds) [%0.1f]:  " % self.timestep)
        if userinput != '':
            if float(userinput) > mintimestep:
                self.timestep = float(userinput)
            else:
                print "timestep is too short! Re-run setScanParameters()"





    def doTimeScan(self):
        '''do a tomo with the rotopec
        sort out filenames, move sample out for refs, move back - pause and prompt to start rotation
        after scan take refs again and darks
        This is based on tomoscan from Tomo'''

        # calculations first
        readtime = self.calcReadoutTime()

        if hasattr(self.proxy, 'exposureAccTime'):
            print "in accumulation mode..."
            acc_nframes = self.proxy.exposureTime/self.proxy.exposureAccTime
            max_frames = 1000
            # use these values
            nframes = self.nproj * acc_nframes
            nblocks = np.ceil(1.0 * nframes / max_frames)
            if nblocks != 1:
                print "there will be small gaps between blocks of frames"
            proj_per_block = np.ceil((1.0 * self.nproj) / nblocks)
            frames_per_block = proj_per_block * acc_nframes
            new_nproj = proj_per_block * nblocks
            # adjust the timestep according to new nproj
            time_per_scan = self.nproj * self.timestep
            new_timestep = time_per_scan / new_nproj
            # account for small gap between sequences - allow 500 ms
            sbox_timestep = (time_per_scan - (500*nblocks)) / new_nproj
            sbox_timestep = sbox_timestep / acc_nframes
            if new_nproj != self.nproj:
                print "adjusting nproj from %d to %d" % (self.nproj, new_nproj)
                self.nproj = new_nproj
            if new_timestep != self.timestep:
                print "adjusting timestep from %0.2f to %0.2f" % (self.timestep, new_timestep)
                self.timestep = new_timestep
            if sbox_timestep < (self.proxy.exposureacctime + self.safetytime + readtime):
                print "Camera exposure acc time is too long for the timescan configuration"
                return
        else:
            print "in single mode"
            print "there will be one continuous sequence of frames"
            acc_nframes = 1.
            nblocks = 1
            proj_per_block = self.nproj
            frames_per_block = self.nproj
            sbox_timestep = self.timestep
            if sbox_timestep < (self.proxy.exposuretime + self.safetytime + readtime):
                print "Camera exposure time is too long for the timescan configuration"
                return

        fastshuttermemo = self.fast_shutter_for_snap

        # now do stuff
        try:
            # write the logfile first, to make sure the scan name is ok
            self.writeLogFile(flyscan=False)
            # writeLogFile returns the good name, with right appended number
            self.scanname = self.savename
            # make a folder for this scan to be moved to - keep all data in the flyscan spool
            #scandir = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/%s' % self.scanname
            scandir = '%s/%s' % (self.spool_directory, self.scanname)
            os.system("mkdir %s" % scandir)
            os.system("mkdir %s/images" % scandir)
            os.system("mkdir %s/refA" % scandir)
            os.system("mkdir %s/refB" % scandir)
            os.system("mkdir %s/dark" % scandir)

            # we don't need to read the images into python
            self.stopLive()
            self.readimage = False

            # some camera configuration
            self.proxy.resetfileindex()
            self.proxy.fileGeneration = True
            self.proxy.fileFormat = 'EDF'
            self.proxy.filePrefix = 'orca'
            self.proxy.fileNbFrames = 1
            subdirwindows = self.spool_subdirectory.replace('/', '\\')
            #NORMAL ROTOPEC BEHAVIOR :
            # open the shutter
            try:
                get_ipython().magic(self.shutter_open_command)
                print "opened shutter"
            except:
                print "front end or shutter responding slowly! Probably nothing to worry about - continuing"

            self.fastshutteropen()

            if self.fast_shutter_for_snap:
                self.fast_shutter_for_snap = False

            # are we doing references?
            if self.nrefbefore != 0:
                # first references - write directly into the good folder

                if self.measurementPosition == None:
                    self.measurementPosition = dm.actors[self.refMotor].value*1.
                    print "Using the current %s position (%f) for measurementPosition" % (self.refMotor, self.measurementPosition)

                print "move in x for references"
                get_ipython().magic(u'dmove %s %0.2f' % (self.refMotor, self.refDisplacement))
                time.sleep(2)
                #self.proxy.fileTargetPath = '\\\\srv3\\spool1\\flyscan-data\\psiche-soleil\\com-psiche\\%s\\refA' % self.scanname
                self.proxy.fileTargetPath = '\\\\srv3\\spool1\\FILETRANSFER\\com-psiche\\%s\\%s\\refA' % (subdirwindows, self.scanname)
                self.proxy.nbFrames = self.nrefbefore*1.0
                self.proxy.triggerMode = 'INTERNAL_SINGLE'
                self.proxy.fileGeneration = True
                self.snap()
                print "moving back x after references"
                retry = 0
                marge = self.pixelsize / 1000 # 1 of a pixel in mm
                diff = 50. # to start with
                while (diff > marge) and retry<10:
                    get_ipython().magic(u'amove %s %0.2f' % (self.refMotor, self.measurementPosition))
                    diff = np.abs(dm.actors[self.refMotor].get() - self.measurementPosition)
                    retry+=1
                    if retry == 10:
                        print "Tried 10 times to reach good tomorefx position - still at %f distance" % diff

            # Prompt to start the rotopec
            #raw_input('Start the RoToPEC rotation! Then hit enter to continue acquisition : ')


            # for Zaslansky_0418 - open fe and fwbs after prompt
            # open the shutter
            #try:
            #    get_ipython().magic(self.shutter_open_command)
            #    print "opened shutter"
            #except:
            #    print "front end or shutter responding slowly! Probably nothing to worry about - continuing"
            #
            #self.fastshutteropen()

            if self.fast_shutter_for_snap:
                self.fast_shutter_for_snap = False



            # main scan
            # program speitbox for timescan
            if self.send_triggers:
                sbox.mode = 0
                sbox.pulsePeriod = sbox_timestep
                sbox.sequenceLength = int(frames_per_block)
                sbox.pulseWidth = 0.0009
                sbox.firstpulseatfirstposition = False # note this is different to FlyScan
                sbox.Prepare()

            # prepare camera
            self.proxy.triggerMode = 'EXTERNAL_MULTI'
            self.proxy.nbFrames = proj_per_block
            #self.proxy.fileTargetPath = '\\\\srv3\\spool1\\flyscan-data\\psiche-soleil\\com-psiche\\%s\\images' % self.scanname
            self.proxy.fileTargetPath = '\\\\srv3\\spool1\\FILETRANSFER\\com-psiche\\%s\\%s\\images' % (subdirwindows, self.scanname)
            self.proxy.resetfileindex()

            raw_input('Hit enter to start the acquisition sequence !!!  : ')
            print "Starting the acquisition..."

            # scan loop
            for ii in range(int(nblocks)):
                # start the camera, the speitbox
                self.proxy.snap()
                # wait for the camera to respond
                timeout = 0
                while (self.proxy.state() != PyTango.DevState.RUNNING) and (timeout<300):
                    time.sleep(0.1)
                    timeout+=1
                if timeout==300:
                    print "WAITED 30 SECONDS FOR THE CAMERA TO RESPOND !!! - problem ? ---"
                time.sleep(0.1)
                # start the speitbox
                if self.send_triggers:
                    sbox.start()
                else:
                    print "Waiting for the user generated pulses !"
                # wait for the camera to finish writing
                while (self.proxy.state() == PyTango._PyTango.DevState.RUNNING):
                    time.sleep(0.1)
                time.sleep(0.1)

            if self.nrefafter != 0:
                # last references
                print "move in x for references"
                get_ipython().magic(u'dmove %s %0.2f' % (self.refMotor, self.refDisplacement))
                time.sleep(2)
                self.proxy.triggerMode = 'INTERNAL_SINGLE'
                self.proxy.nbFrames = self.nrefafter
                #self.proxy.fileTargetPath = '\\\\srv3\\spool1\\flyscan-data\\psiche-soleil\\com-psiche\\%s\\refB' % self.scanname
                self.proxy.fileTargetPath = '\\\\srv3\\spool1\\FILETRANSFER\\com-psiche\\%s\\%s\\refB' % (subdirwindows, self.scanname)
                self.proxy.resetfileindex()
                self.proxy.fileGeneration = True
                self.snap()
                print "moving back x after references"
                get_ipython().magic(u'amove %s %0.2f' % (self.refMotor, self.measurementPosition))
                # wait for the camera to finish writing
                while (self.proxy.state() == PyTango._PyTango.DevState.RUNNING):
                    time.sleep(0.1)
                time.sleep(0.1)
                # update the reference in mt
                self.ref = self.proxy.image

            if self.ndark != 0:
                # darks
                # close the shutter / fast shutter
                if fastshuttermemo and (not(self.main_shutter_for_darks)):
                    # close the fast shutter
                    get_ipython().magic(self.fast_shutter_close_command)
                else:
                    get_ipython().magic(self.shutter_close_command)
                # take the darks
                time.sleep(2)
                self.proxy.triggerMode = 'INTERNAL_SINGLE'
                time.sleep(0.5)
                self.proxy.nbFrames = self.ndark
                self.proxy.resetfileindex()
                #self.proxy.fileTargetPath = '\\\\srv3\\spool1\\flyscan-data\\psiche-soleil\\com-psiche\\%s\\dark' % self.scanname
                self.proxy.fileTargetPath = '\\\\srv3\\spool1\\FILETRANSFER\\com-psiche\\%s\\%s\\dark' % (subdirwindows, self.scanname)
                self.proxy.fileGeneration = True
                self.snap()
                # wait for the camera to finish writing
                while (self.proxy.state() == PyTango._PyTango.DevState.RUNNING):
                    time.sleep(0.1)
                time.sleep(0.1)
                # update the dark in mt
                self.dark = self.proxy.image

            # switch off saving
            print "scan finished successfully"
            self.proxy.fileGeneration = False
            self.writeParFile(1024.5, flyscan=False)

        except KeyboardInterrupt:
            print "stop the camera"
            # program speitbox
            sbox.Abort()
            time.sleep(0.1)
            sbox.mode = 0
            sbox.pulsePeriod = 50
            sbox.sequenceLength = 100
            sbox.Prepare()
            # prepare camera
            sbox.start()
            self.proxy.stop()

        finally:
            # restore the fast shutter configuration
            self.fast_shutter_for_snap = fastshuttermemo
            # close the fast shutter - should the main shutter also close?
            self.fastshutterclose()
            # put the camera back into internal trigger
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            self.proxy.fileGeneration = False
            self.proxy.nbframes = 1


    def centreAxis(self, xpos=None):
        '''Find the rotation axis, and position at the specified position in the image
        Default is the centre of the image
        Special rotopec version'''

        # update the references
        toto = raw_input('do updateRef ? [y]/n')
        toto = True if toto in ['', 'y'] else False
        if toto:
            self.updateRef()
        #self.updateDark()
        # take the images
        #get_ipython().magic(u'amove tomorz 0')
        print('Taking an image at the current position for 0 degrees')
        self.takeFlat()
        im0 = self.flat
        print('Now, you have to do a 180 degree rotation with the rotopec...')
        raw_input('Hit enter when you are ready to continue : ')
        get_ipython().magic(u'feopen')
        get_ipython().magic(u'fwbsopen')
        self.takeFlat()
        im180flip = self.flat[:, ::-1]

        # an array for the line by line output
        sizey, sizex = im0.shape
        c1 = np.zeros((sizey))
        c2 = np.zeros((sizey))

        # what is the global centre position?
        line0, line180 = im0.sum(0), im180flip.sum(0)
        corResult = ifft(fft(line0)*np.conj(fft(line180)))
        corResult = np.abs(corResult)
        gc = np.nonzero(corResult==corResult.max())
        gc = float(gc[0])
        gc = np.mod(gc+(sizex/2), sizex)

        print "global result is %02f" % gc
        pylab.figure(201)
        pylab.clf()
        pylab.plot([0,sizey], [gc,gc], 'b-')

        # correlate for each rowim
        for ii in range(sizey):
            # find the nearest pixel
            corResult = ifft(fft(im0[ii,:])*np.conj(fft(im180flip[ii,:])))
            corResult = np.abs(corResult)
            corResultLocal = np.nonzero(corResult==corResult.max())[0][0]
            c1[ii] = corResultLocal

            # pick out the five pixels around the peak
            # must stop this from going outside the matrix
            xdata = c1[ii]+[-2, -1, 0, 1, 2]
            xdata_ndx = np.mod(xdata, sizex).tolist()
            ydata = corResult[xdata_ndx]
            # fit this with a quadratic
            # ydata = a(xdata)^2 + b(xdata) + c
            xdata = np.array(xdata)
            xdata = np.array([xdata**2,xdata,np.ones(len(xdata))]).T
            a,b,c = np.linalg.lstsq(xdata, ydata)[0]
            # find the maximum of this, derivate
            # dy/dx = 2a(xdata) + b = 0
            # x = -b / 2a
            c2[ii] = -b / (2*a)

        # need to get everything centred more or less
        c1 = np.mod(c1+(sizex/2.), sizex)
        c2 = np.mod(c2+(sizex/2.), sizex)
        pylab.plot(np.arange(sizey), c1, 'g-')
        pylab.plot(np.arange(sizey), c2, 'r-')

        # calculate rotc
        # select the data close to the global result
        deltac = sizey*5*np.pi/180/2
        ndx = np.nonzero(np.abs(c1-gc)<deltac)[0]

        # make data for fitting
        fitx = np.array(range(sizey))[ndx]
        fity = c2[ndx]
        pylab.plot(fitx, fity, 'g+')

        # fit with a straight line
        fitx = np.array([fitx, np.ones_like(fitx)]).T
        m,c = np.linalg.lstsq(fitx, fity)[0]

        # display the fit
        x = np.array(range(sizey))
        x = np.array([x, np.ones_like(x)]).T
        y = np.dot(x, [m, c])
        pylab.plot(x,y,'g')

        # results
        if xpos==None:
            xpos = sizex/2
        deltaX = ((xpos-gc)/2)
        deltaXmm = deltaX*self.pixelsize/1000.
        # here I add a negative sign for flipped images
        deltaRotC = -((m*180/np.pi)/2)
        print "\nShould move tomo X by %0.2f mm (%0.2f pixels)" % (deltaXmm, deltaX)
        check = raw_input('Apply this correction? [yes/no] : ')
        if check=='yes' or check=='y':
            print 'move to the new position'
            get_ipython().magic(u'dmove %s %0.3f' % (self.reference_motor_alias, deltaXmm))
            self.setMeasurementPos()
        else:
            print 'Not applying correction'

        print "\nShould move rotc by %0.2f degrees" %  deltaRotC
        check = raw_input('Apply this correction? [yes/no] : ')
        if check=='yes' or check=='y':
            self.tomorotcOn()
            get_ipython().magic(u'dmove %s %0.3f' % (self.camera_rotation_motor, deltaRotC))
            self.tomorotcOff()
        else:
            print 'Not applying correction'
            print 'Can correct motor position tomorotc manually'



    #def fastTimeScan(self):
        #'''do a tomo with the rotopec
        #similar to doTimeScan, but dump files onto the flyscan-tmp-data SSD spool for ftp'''

        ## calculations first
        #readtime = self.calcReadoutTime()

        #print "there will be one continuous sequence of frames"
        #acc_nframes = 1.
        #nblocks = 1
        #proj_per_block = self.nproj
        #frames_per_block = self.nproj
        #sbox_timestep = self.timestep
        #if sbox_timestep < (self.proxy.exposuretime + self.safetytime + readtime):
            #print "Camera exposure time is too long for the timescan configuration"
            #return

        #fastshuttermemo = self.fast_shutter_for_snap


        #ftpclient.DeleteRemainingFiles()
        #ftpclient.Start()

        ## now do stuff
        #try:
            ## write the logfile first, to make sure the scan name is ok
            #self.writeLogFile(flyscan=False)
            ## writeLogFile returns the good name, with right appended number
            #self.scanname = self.savename
            ## make a folder for this scan to be moved to - keep all data in the flyscan spool
            #scandir = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/%s' % self.scanname
            #os.system("mkdir %s" % scandir)
            #os.system("mkdir %s/images" % scandir)
            #os.system("mkdir %s/refA" % scandir)
            #os.system("mkdir %s/refB" % scandir)
            #os.system("mkdir %s/dark" % scandir)

            ## we don't need to read the images into python
            #self.stopLive()
            #self.readimage = False

            ## some camera configuration
            #self.proxy.resetfileindex()
            #self.proxy.fileGeneration = True
            #self.proxy.fileFormat = 'EDF'
            #self.proxy.filePrefix = 'orca'
            #self.proxy.fileNbFrames = 1


            ## are we doing references?
            #if self.nrefbefore != 0:
                ## first references - write directly into the good folder
                #self.proxy.fileTargetPath = '\\\\srv3\\spool1\\flyscan-data\\psiche-soleil\\com-psiche\\%s\\refA' % self.scanname
                #self.proxy.nbFrames = self.nrefbefore*1.0
                #self.proxy.triggerMode = 'INTERNAL_SINGLE'

                #if self.measurementPosition == None:
                    #self.measurementPosition = dm.actors[self.refMotor].value*1.
                    #print "Using the current %s position (%f) for measurementPosition" % (self.refMotor, self.measurementPosition)

                #print "move in x for references"
                #get_ipython().magic(u'dmove %s %0.2f' % (self.refMotor, self.refDisplacement))
                #self.snap()
                #print "moving back x after references"
                #retry = 0
                #marge = self.pixelsize / 1000 # 1 of a pixel in mm
                #diff = 50. # to start with
                #while (diff > marge) and retry<10:
                    #get_ipython().magic(u'amove %s %0.2f' % (self.refMotor, self.measurementPosition))
                    #diff = np.abs(dm.actors[self.refMotor].get() - self.measurementPosition)
                    #retry+=1
                    #if retry == 10:
                        #print "Tried 10 times to reach good tomorefx position - still at %f distance" % diff

            ## Prompt to start the rotopec
            #raw_input('Start the RoToPEC rotation! Then hit enter to continue acquisition : ')
            #print "Starting the acquisition..."

            ## for Zaslansky_0418 - open fe and fwbs after prompt
            ## open the shutter
            #try:
                #get_ipython().magic(self.shutter_open_command)
                #print "opened shutter"
            #except:
                #print "front end or shutter responding slowly! Probably nothing to worry about - continuing"

            #self.fastshutteropen()

            #if self.fast_shutter_for_snap:
                #self.fast_shutter_for_snap = False



            ## main scan
            ## program speitbox for timescan
            #sbox.mode = 0
            #sbox.pulsePeriod = sbox_timestep
            #sbox.sequenceLength = int(frames_per_block)
            #sbox.pulseWidth = 0.0009
            #sbox.firstpulseatfirstposition = False # note this is different to FlyScan
            #sbox.Prepare()

            ## prepare camera
            #self.proxy.triggerMode = 'EXTERNAL_MULTI'
            #self.proxy.nbFrames = proj_per_block
            #self.proxy.fileTargetPath = 'S:\\spool1\\orca\\flyscan-tmp-data'
            #self.proxy.resetfileindex()

            ## scan loop

                ## start the camera, the speitbox
            #self.proxy.snap()
            ## wait for the camera to respond
            #timeout = 0
            #while (self.proxy.state() != PyTango.DevState.RUNNING) and (timeout<300):
                #time.sleep(0.1)
                #timeout+=1
            #if timeout==300:
                #print "WAITED 30 SECONDS FOR THE CAMERA TO RESPOND !!! - problem ? ---"
            #time.sleep(0.1)
            ## start the speitbox
            #sbox.start()
            ## wait for the camera to finish writing
            #while (self.proxy.state() == PyTango._PyTango.DevState.RUNNING):
                #time.sleep(0.1)
            #time.sleep(0.1)

            #if self.nrefafter != 0:
                ## last references
                #self.proxy.triggerMode = 'INTERNAL_SINGLE'
                #self.proxy.nbFrames = self.nrefafter
                #self.proxy.fileTargetPath = '\\\\srv3\\spool1\\flyscan-data\\psiche-soleil\\com-psiche\\%s\\refB' % self.scanname
                #self.proxy.resetfileindex()

                #print "move in x for references"
                #get_ipython().magic(u'dmove %s %0.2f' % (self.refMotor, self.refDisplacement))
                #self.snap()
                #print "moving back x after references"
                #get_ipython().magic(u'amove %s %0.2f' % (self.refMotor, self.measurementPosition))
                ## wait for the camera to finish writing
                #while (self.proxy.state() == PyTango._PyTango.DevState.RUNNING):
                    #time.sleep(0.1)
                #time.sleep(0.1)
                ## update the reference in mt
                #self.ref = self.proxy.image

            #if self.ndark != 0:
                ## darks
                #self.proxy.triggerMode = 'INTERNAL_SINGLE'
                #time.sleep(0.5)
                #self.proxy.nbFrames = self.ndark
                #self.proxy.resetfileindex()
                #self.proxy.fileTargetPath = '\\\\srv3\\spool1\\flyscan-data\\psiche-soleil\\com-psiche\\%s\\dark' % self.scanname
                ## close the shutter / fast shutter
                #if fastshuttermemo:
                    ## close the fast shutter
                    #get_ipython().magic(self.fast_shutter_close_command)
                #else:
                    #get_ipython().magic(self.shutter_close_command)
                ## take the darks
                #self.snap()
                ## wait for the camera to finish writing
                #while (self.proxy.state() == PyTango._PyTango.DevState.RUNNING):
                    #time.sleep(0.1)
                #time.sleep(0.1)
                ## update the dark in mt
                #self.dark = self.proxy.image

            ## switch off saving
            #print "scan finished successfully"
            #self.proxy.fileGeneration = False
            #self.writeParFile(1024.5, flyscan=False)

        #except KeyboardInterrupt:
            #print "stop the camera"
            ## program speitbox
            #sbox.Abort()
            #time.sleep(0.1)
            #sbox.mode = 0
            #sbox.pulsePeriod = 50
            #sbox.sequenceLength = 100
            #sbox.Prepare()
            ## prepare camera
            #sbox.start()
            #self.proxy.stop()

        #finally:
            ## restore the fast shutter configuration
            #self.fast_shutter_for_snap = fastshuttermemo
            ## close the fast shutter - should the main shutter also close?
            #self.fastshutterclose()
            #loop = True
            #while loop:
                #mylist = glob.glob('/nfs/srv3/spool1/flyscan-tmp-data/*')
                #if len(mylist)==0:
                    #loop=False
                ## move all files from ftp directory to final destination
                #print "move all files to the image directory"
                #os.system('rsync -vrWt --size-only --exclude=*temp* /nfs/srv3/spool1/flyscan-tmp-data/* %s/images/' % scandir)
                #time.sleep(2)

