import PyTango
dev = PyTango.DeviceProxy("i03-c-c08/dt/vg.1")
import pylab as plt
plt.ion() # switch on interactive figure mode?

def read_and_plot(label_string=""):

	im = dev.image

	roi = im #[100:399, 150:450]
	inth = roi.sum(0)
	intv = roi.sum(1)
	profh = roi[217, :]
	profv = roi[:, 254]

	inth=(inth*1.0)*profh.max()/inth.max()
	intv=(intv*1.0)*profv.max()/intv.max()
	profh=(profh*1.0)
	profv=(profv*1.0)


	plt.subplot(2,1,1)
	plt.plot(profh, label="single profile")
	plt.plot(inth, label="integrated profile")
	plt.title("Horizontal")
	plt.legend()
	plt.subplot(2,1,2)
	plt.plot(profv, label="single profile")
	plt.plot(intv, label="integrated profile")
	plt.title("Vertical")
	plt.legend()
