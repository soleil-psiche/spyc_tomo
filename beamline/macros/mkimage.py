# -*- coding: utf-8 -*-

import numpy as np
import pylab
# default to gray
pylab.gray()
import PyTango
import time
import sys
import h5py
import os
import glob
from PIL import Image
sys.path.append('/usr/Local/configFiles/PyBeamlineRoot/beamline/macros/tomo14')
#import edf_read_dirty


def nxs_read(filename, prefix):
	f = h5py.File(filename)
	a = f['/entry/scan_data/'+prefix]
	im = np.zeros(a.shape[1::])
	a.read_direct(im)
	f.close()
	return im

class mkimage:

    def __init__(self):

        # define the parameters that we need
        self.pixelsize = ''
        self.zstep = ''
        self.imagedir = ''
        # get the devices
        tmp = raw_input('Which camera to use? hamamatsu.1 or hamamatsu.3 ? 1/[3] :')
        tmp = 3 if tmp.lower() in ['3', '', 'hamamatsu.3'] else 1
        if tmp == 1:
            self.hamamatsu = PyTango.DeviceProxy('i03-c-c00/dt/hamamatsu.1')
        else:
            self.hamamatsu = PyTango.DeviceProxy('i03-c-c00/dt/hamamatsu.3')
        self.ptz = PyTango.DeviceProxy('i03-c-c05/ex/prs.1-mt_tz.1')
        self.sbox = PyTango.DeviceProxy('flyscan/clock/spietbox-timebase.1')
        print "set up the experiment details"
        self.setup_parameters()
        print "set up the camera for flyimage"
        self.setup_flyimage()


    def setup_parameters(self):
        '''
        setup the parameters
        '''
        default = "/nfs/ruche-psiche/psiche-soleil/com-psiche/mkimage" if self.imagedir == "" else self.imagedir
        tmp = raw_input('Experiment directory to save the images [%s]  : ' % default)
        self.imagedir = default if tmp == "" else tmp
        if not os.path.isdir(self.imagedir):
            os.mkdir(self.imagedir)
            print "Created the directory %s to save images" % self.imagedir

        default = 6.5 if self.pixelsize == "" else self.pixelsize
        tmp = raw_input('Detector pixel size / microns [%0.2f]  : ' % default)
        self.pixelsize = default if tmp == "" else float(tmp)

        default = 18.0 if self.zstep == "" else self.zstep
        tmp = raw_input('Z step for images / microns [%0.2f]  : ' % default)
        self.zstep = default if tmp == "" else float(tmp)


    def setup_flyimage(self):
        '''
        setup the camera - this might crash the device server
        if so - restart using the icon on the detector PC
        '''
        self.hamamatsu.stop() # stop camera if running
        time.sleep(0.5)
        self.hamamatsu.triggerMode = "EXTERNAL_MULTI" # like flyscan
        self.hamamatsu.acquisitionMode = 'SINGLE'
        self.hamamatsu.fileTargetPath = '\\\\srv3\\spool1\\flyscan-tmp-data'
        self.hamamatsu.fileFormat = 'NXS'
        self.hamamatsu.filePrefix = 'mkimage'
        self.hamamatsu.fileNbFrames = 100

    def saveimage(self):
        '''
        Save the current image without overwriting
        '''
        # make a new filename
        updatename = True
        count = 0
        while updatename:
            imagename = 'mkimage_%04d.tif' % count
            imagepath = os.path.join(self.imagedir, imagename)
            if os.path.exists(imagepath):
                # this image already exists!
                count+=1
            else:
                # this name is ok !
                updatename = False
                im = Image.fromarray(self.image)
                im.save(imagepath)
                print "saved image as %s [32bit tif format probably, open with ImageJ]" % imagepath



    def flyimage(self, ptz_start=None, ptz_end=None, roi=[1, 1, 2048, 2048]):
        '''
        This is the good version !
        mkimage.flyimage(ptz_start=None, ptz_end=None, roi=[1, 1, 2048, 2048]):
        roi definition is [startX,startY,endX,endY]
        '''
        # this more or less works -
        # need to add the ROI to speed up file writing
        # wait for temp files to be converted
        # read files while returning to start

        # preamble as before
        print "______________________________________________________________"
        print "note this is the good version"
        print "______________________________________________________________"

        if ptz_start==None or ptz_end==None:
            print "Must supply values for ptz movement"
            return

        if ptz_start > ptz_end:
            # invert values to scan in positive direction
            ptz_start, ptz_end = ptz_end, ptz_start

        nsteps = np.ceil((ptz_end - ptz_start) / (self.zstep/1000.))
        ptz_orig = self.ptz.position

        # set up timing
        steptime = (self.zstep/1000.) / self.ptz.velocity
        if steptime < (self.hamamatsu.exposuretime / 1000.):
            print "exposure time (%f) is longer than the steptime (%f) ! " % ((self.hamamatsu.exposuretime / 1000.), steptime)
            print "Need either bigger z steps or a shorter exposuretime"
            return
        # apply these values
        self.sbox.mode = 0
        self.sbox.pulsePeriod = steptime*1000
        self.sbox.firstPulseAtFirstPosition = False
        self.sbox.sequenceLength = int(nsteps)
        self.sbox.pulseWidth=0.0009
        self.sbox.Prepare()

        # stop camera in case it is running
        self.hamamatsu.Stop()
        # set up camera
        self.hamamatsu.triggerMode = "EXTERNAL_MULTI" # like flyscan
        self.hamamatsu.nbFrames = int(nsteps)
        self.hamamatsu.fileGeneration = True

        # set the ROI
        startX,startY,endX,endY = roi[0],roi[1],roi[2],roi[3]
        sizeX = endX-startX
        sizeY = endY-startY
        startY = np.floor(startY/4.)*4
        sizeY = np.ceil(sizeY/4.)*4
        startX = np.floor(startX/4.)*4
        sizeX = np.ceil(sizeX/4.)*4
        newroi = np.int32([startX, startY, sizeX, sizeY])
        self.hamamatsu.setroi(newroi)

        # use the new roi for the image size
        output = np.zeros((nsteps, newroi[2]))
        xstart = -(self.pixelsize/1000.)*newroi[2]/2
        xend = (self.pixelsize/1000.)*newroi[2]/2
        # clear the spool - on srv3
        os.system("rm -f /nfs/srv3/spool1/flyscan-tmp-data/*nxs")

        print "move ptz to start position"
        print (u'amove ptz %0.2f' % ptz_start)
        get_ipython().magic(u'amove ptz %0.2f' % ptz_start)

        # new figure
        handle = pylab.figure()
        fignum = handle.number
        try:
            print "start camera"
            self.hamamatsu.snap()
            time.sleep(1)
            print "start moving"
            self.ptz.position = ptz_start + (nsteps * self.zstep/1000.)
            print "start spietbox"
            self.sbox.start()

            # display position
            sys.stdout.write("scanning ptz...\nptz = ")
            while self.ptz.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]:
                # display position
                sys.stdout.write("%2.3f" % self.ptz.position)
                sys.stdout.flush()
                time.sleep(0.2)
                sys.stdout.write("%s" % "\b"*6)

        except KeyboardInterrupt:
            print "stop moving"
            self.ptz.Stop()
            self.hamamatsu.Stop()
            time.sleep(1)
            self.sbox.Abort()

        try:
            # return press while reading...
            print "return to original position"
            self.ptz.position = ptz_orig

            # wait for all temp files to be finalised
            ll = glob.glob("/nfs/srv3/spool1/flyscan-tmp-data/temp*nxs*")
            while len(ll)!=0:
                time.sleep(2)
                ll = glob.glob("/nfs/srv3/spool1/flyscan-tmp-data/temp*nxs*")
                print "waiting for temp files %d remaining" % len(ll)

            # build the image
            # multiple frames - read .nxs from the spool
            ll = glob.glob("/nfs/srv3/spool1/flyscan-tmp-data/*nxs")
            ll.sort()
            ndx = 0
            for ii in range(len(ll)):

                f=h5py.File(ll[ii])
                a=f['entry']
                b=a['scan_data']
                c=b['mkimage_image']
                for jj in range(c.shape[0]):

                    nxsimage = c[jj, :, :]
                    line = nxsimage.sum(0)
                    output[ndx, :] = line
                    ndx+=1
                f.close()

            # show the result
            pylab.imshow(output, extent=[xstart, xend, ptz_end, ptz_start])
            #pylab.axis("tight")
            pylab.xlabel("relative x position, mm")
            pylab.ylabel("ptz position, mm")
            pylab.title("assembled image")

        except KeyboardInterrupt:
            print "stop moving"
            self.ptz.Stop()
        finally:
            self.hamamatsu.triggerMode = "INTERNAL_SINGLE"
            self.hamamatsu.fileGeneration = False
            # get the image in self
            self.image = output * 1.
            self.saveimage()


# old things - save just in case


def __setup_flyimage_edf():
    '''
    setup the camera - this might crash the device server
    if so - restart using the icon on the detector PC
    '''
    hamamatsu.triggerMode = "EXTERNAL_MULTI" # like flyscan
    hamamatsu.acquisitionMode = 'SINGLE'
    hamamatsu.fileTargetPath = 'Y:\\flyscan-tmp-data'
    hamamatsu.fileFormat = 'EDF'
    hamamatsu.filePrefix = 'mkimage'
    hamamatsu.fileNbFrames = 1


def __flyimage_edf(ptz_start=None, ptz_end=None, roi=[1, 1, 2048, 2048]):
    '''
    This is the good version ! - using edf files while problem reading nxs :-(
    mkimage.flyimage(ptz_start=None, ptz_end=None, roi=[1, 1, 2048, 2048]):
    roi definition is [startX,startY,endX,endY]
    '''
    # this more or less works -
    # need to add the ROI to speed up file writing
    # wait for temp files to be converted
    # read files while returning to start

    # preamble as before
    print "______________________________________________________________"
    print "note this is the good version, but using edf files"
    print "______________________________________________________________"

    if ptz_start==None or ptz_end==None:
        print "Must supply values for ptz movement"
        return

    if ptz_start > ptz_end:
        # invert values to scan in positive direction
        ptz_start, ptz_end = ptz_end, ptz_start

    nsteps = np.ceil((ptz_end - ptz_start) / 0.018)
    ptz_orig = ptz.position

    # set up timing
    steptime = 0.018 / ptz.velocity
    #cpt.initialDelay0 = 0.001
    #cpt.pulseNumber = nsteps
    #cpt.delayCounter0 = (steptime*1000) - 1
    #cpt.pulseWidthCounter0 = 1
    sbox.mode = 0
    sbox.pulsePeriod = steptime*1000
    sbox.firstPulseAtFirstPosition = False
    sbox.sequenceLength = int(nsteps)
    sbox.pulseWidth=0.0009
    sbox.Prepare()

    # stop camera in case it is running
    hamamatsu.Stop()
    # set up camera
    hamamatsu.triggerMode = "EXTERNAL_MULTI" # like flyscan
    hamamatsu.nbFrames = int(nsteps)
    hamamatsu.fileGeneration = True
    hamamatsu.ResetFileIndex()

    # set the ROI
    startX,startY,endX,endY = roi[0],roi[1],roi[2],roi[3]
    sizeX = endX-startX
    sizeY = endY-startY
    startY = 2048 - startY - sizeY
    startY = np.floor(startY/4.)*4
    sizeY = np.ceil(sizeY/4.)*4
    startX = np.floor(startX/4.)*4
    sizeX = np.ceil(sizeX/4.)*4
    newroi = np.int32([startX, startY, sizeX, sizeY])
    hamamatsu.setroi(newroi)

    # use the new roi for the image size
    output = np.zeros((nsteps, newroi[2]))
    xstart = -self.pixelsize*newroi[2]/2
    xend = self.pixelsize*newroi[2]/2
    # clear the spool - on srv3
    os.system("rm -f /nfs/srv3/spool1/flyscan-tmp-data/*edf")

    print "move ptz to start position"
    print (u'amove ptz %0.2f' % ptz_start)
    get_ipython().magic(u'amove ptz %0.2f' % ptz_start)

    # new figure
    handle = pylab.figure()
    fignum = handle.number
    try:
        print "start camera"
        hamamatsu.snap()
        time.sleep(1)
        print "start moving"
        ptz.position = ptz_start + (0.018*nsteps)
        print "start spietbox"
        sbox.start()

        # display position
        sys.stdout.write("scanning ptz...\nptz = ")
        while ptz.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]:
            # display position
            sys.stdout.write("%2.3f" % ptz.position)
            sys.stdout.flush()
            time.sleep(0.2)
            sys.stdout.write("%s" % "\b"*6)

    except KeyboardInterrupt:
        print "stop moving"
        ptz.Stop()
        cpt.Stop()
        hamamatsu.Stop()

    try:
        # return press while reading...
        print "return to original position"
        ptz.position = ptz_orig

        ## wait for all temp files to be finalised
        #ll = glob.glob("/nfs/srv3/spool1/flyscan-tmp-data/temp*nxs*")
        #while len(ll)!=0:
        #    time.sleep(2)
        #    ll = glob.glob("/nfs/srv3/spool1/flyscan-tmp-data/temp*nxs*")
        #    print "waiting for temp files %d remaining" % len(ll)

        # build the image
        # multiple frames - read .nxs from the spool
        ll = glob.glob("/nfs/srv3/spool1/flyscan-tmp-data/*edf")
        ll.sort()
        ndx = 0
        for ii in range(len(ll)):
            im = edf_read_dirty.edf_read_dirty(ll[ii], 1024, [int(sizeX), int(sizeY)], [0,0,int(sizeX),int(sizeY)],  datatype=np.uint16)
            line = im.sum(0)
            output[ii, :] = line

        # show the result
        pylab.imshow(output, extent=[xstart, xend, ptz_end, ptz_start])
        #pylab.axis("tight")
        pylab.xlabel("relative x position, mm")
        pylab.ylabel("ptz position, mm")
        pylab.title("assembled image")

    except KeyboardInterrupt:
        print "stop moving"
        ptz.Stop()
    finally:
        #hamamatsu.resetroi()
        hamamatsu.triggerMode = "INTERNAL_SINGLE"
        hamamatsu.fileGeneration = False

    return output


def __mkimage(ptz_start=None, ptz_end=None, roi=[1, 1, 2048, 2048]):
    '''
    mkimage(ptz_start=None, ptz_end=None, roi=[1, 1, 2048, 2048])
    roi is [startx, starty, endx, endy]
    '''

    print "______________________________________________________________"
    print "note this version is slow - use mkimage.flyimage to go faster"
    print "______________________________________________________________"

    if ptz_start==None or ptz_end==None:
        print "Must supply values for ptz movement"
        return

    if ptz_start > ptz_end:
        # invert values to scan in positive direction
        ptz_start, ptz_end = ptz_end, ptz_start

    # stop camera in case it is running
    hamamatsu.Stop()
    hamamatsu.triggerMode = "INTERNAL_SINGLE"
    hamamatsu.nframes = 1
    hamamatsu.fileGeneration = False
    hamamatsu.Init()

    nsteps = np.ceil((ptz_end - ptz_start) / 0.018)
    output = np.zeros((nsteps, roi[2]-roi[0]))
    xstart = -pixelsize*(roi[2]-roi[0])/2
    xend = pixelsize*(roi[2]-roi[0])/2
    get_ipython().magic(u'amove ptz %0.2f' % ptz_start)

    handle = pylab.figure()
    fignum = handle.number

    for ii in range(int(nsteps)):
        hamamatsu.snap()

        time.sleep(0.1)
        while hamamatsu.status() == "Acquisition is Running ...\n\n":
            time.sleep(0.1)
        time.sleep(0.1)

        get_ipython().magic(u'dmove ptz 0.018')
        line = hamamatsu.image[roi[1]:roi[3], roi[0]:roi[2]]
        line = line.sum(0)
        output[ii, :] = line

        print ii
        if np.mod(ii, 5)==0:
            pylab.figure(fignum)
            pylab.imshow(output, extent=[xstart, xend, ptz_end, ptz_start])
            #pylab.axis("tight")
            pylab.gray()
            pylab.draw()

    pylab.imshow(output, extent=[xstart, xend, ptz_end, ptz_start])
    #pylab.axis("tight")
    pylab.xlabel("relative x position, mm")
    pylab.xlabel("ptz position, mm")
    pylab.title("assembled image")
    return output

def __fastimage(ptz_start=None, ptz_end=None, roi=[1, 1, 2048, 2048]):
    '''
    mkimage(ptz_start=None, ptz_end=None, roi=[1, 1, 2048, 2048])
    roi is [startx, starty, endx, endy]
    '''
    print "______________________________________________________________"
    print "note this version is slow - use mkimage.flyimage to go faster"
    print "______________________________________________________________"

    if ptz_start==None or ptz_end==None:
        print "Must supply values for ptz movement"
        return

    if ptz_start > ptz_end:
        # invert values to scan in positive direction
        ptz_start, ptz_end = ptz_end, ptz_start

    nsteps = np.ceil((ptz_end - ptz_start) / 0.018)
    output = np.zeros((nsteps, roi[2]-roi[0]))
    xstart = -pixelsize*(roi[2]-roi[0])/2
    xend = pixelsize*(roi[2]-roi[0])/2
    ptz_orig = ptz.position

    # stop camera in case it is running
    hamamatsu.Stop()
    hamamatsu.triggerMode = "INTERNAL_SINGLE"
    hamamatsu.nbFrames = 1
    hamamatsu.fileGeneration = False
    #hamamatsu.Init()
    print "skipping Init..."

    print "move ptz to start position"
    get_ipython().magic(u'amove ptz %0.2f' % ptz_start)

    handle = pylab.figure()
    fignum = handle.number
    sys.stdout.write("\n\n ptz = %0.3f" % ptz_start)

    try:
        for ii in range(int(nsteps)):

            # snap the camera
            hamamatsu.snap()

            # start the movement without waiting
            ptz.position = ptz_start + 0.018*(ii+1)

            # make sure the camera has finished
            while hamamatsu.status() == "Acquisition is Running ...\n\n":
                time.sleep(0.1)
            time.sleep(0.1)

            # get the line
            line = hamamatsu.image[roi[1]:roi[3], roi[0]:roi[2]]
            line = line.sum(0)
            output[ii, :] = line

            # update figure
            if np.mod(ii, 5)==0:
                pylab.figure(fignum)
                pylab.imshow(output, extent=[xstart, xend, ptz_end, ptz_start])
                #pylab.axis("tight")
                pylab.gray()
                pylab.draw()

            # make sure movement has finished
            while ptz.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]:
                time.sleep(0.1)

            # display position
            sys.stdout.write("%s" % "\b"*6)
            sys.stdout.write("%2.3f" % ptz.position)
            sys.stdout.flush()

    except KeyboardInterrupt:
        print "stop moving"
        ptz.Stop()
        hamamatsu.Stop()
    finally:
        sys.stdout.write("\n\n")
        print "return to original position"
        get_ipython().magic(u'amove ptz %0.2f' % ptz_orig)

    pylab.imshow(output, extent=[xstart, xend, ptz_end, ptz_start])
    #pylab.axis("tight")
    pylab.xlabel("relative x position, mm")
    pylab.xlabel("ptz position, mm")
    pylab.title("assembled image")
    return output





















