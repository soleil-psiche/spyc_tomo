# -*- coding: utf-8 -*-

import numpy as np
# data fitter device for fitting
import PyTango
_fitter = PyTango.DeviceProxy("i03-C-C00/ex/datafitter.1")


def centerS(gonio, halfScanRange, oscillationAngle, rzCenter):
    '''
    usage: centerS(halfScanRange, oscillationAngle, rzCenter)
    From ID27.
    Align cell on center of rotation, in the beam direction only.
    Now using Spyc as much as possible
    '''
    if gonio=="gonio3":
        rzmotor = "rz3"
        xmotor = "cx3"
        smotor = "cs3"
    elif gonio=="gonio2":
        rzmotor = "rz2"
        xmotor = "cx2"
        smotor = "cs2"
    else:
        print "Please specify which goniometer (gonio2 / gonio3)"
        #return
        
    # move to center rz
    get_ipython().magic(u'amove %s %f' % (rzmotor, rzCenter))
    # do the three scans
    get_ipython().magic(u'dscan %s %f %f 20 0.1' % (xmotor, -halfScanRange, halfScanRange))
    # get the centre
    cen1 = _fitter.position
    print "center is {}".format(cen1)
    
    # move to positive rz
    get_ipython().magic(u'amove %s %f' % (rzmotor, (rzCenter+oscillationAngle)))
    # do the three scans
    get_ipython().magic(u'dscan %s %f %f 20 0.1' % (xmotor, -halfScanRange, halfScanRange))
    # get the centre
    cen2 = _fitter.position
    print "center is {}".format(cen1)
    
    # move to negative rz
    get_ipython().magic(u'amove %s %f' % (rzmotor, (rzCenter-oscillationAngle)))
    # do the three scans
    get_ipython().magic(u'dscan %s %f %f 20 0.1' % (xmotor, -halfScanRange, halfScanRange))
    # get the centre
    cen3 = _fitter.position
    print "center is {}".format(cen1)
    
    # return to centre
    get_ipython().magic(u'amove %s %f' % (rzmotor, rzCenter))
    
    # calculate the required correction in cs
    deltaS = -(cen2 - cen3) / (2 * np.sin(oscillationAngle * np.pi / 180))        
    ##### change the sign of the correction for the replacement cs
    deltaS = -deltaS
    #####
    print "displacement in {} calculated as {}".format(smotor, deltaS)
    checkA = str(raw_input("Apply this correction? (yes/[no])  ")).lower()
    if checkA.lower() in ["yes", "y", "1", "true"]:
        get_ipython().magic(u'dmove %s %f' % (smotor, deltaS))
    else: 
        print "Not correcting %s" % smotor

    