import PyTango
from PyTango import DevState,DeviceProxy,DevFailed
from time import sleep
import time
import os, sys
from PIL import Image
from valve_class import valve
from moveable import sensor
import numpy as np
import sys
import os.path
import glob
import h5py

# will use this in sweep
from andy import mv

# ugly ugly
path_to_spool = "/nfs/srv1/spool1/perkinelmer"

att_dts2 = PyTango.AttributeProxy("i03-c-c07/dt/det2-mt_ts/position")
att_dtx2 = PyTango.AttributeProxy("i03-c-c07/dt/det2-mt_tx/position")
att_dtz2 = PyTango.AttributeProxy("i03-c-c07/dt/det2-mt_tz/position")
att_cs2 = PyTango.AttributeProxy("i03-c-c07/ex/gonio.2-mt_ts.2/position")
att_cx2 = PyTango.AttributeProxy("i03-c-c07/ex/gonio.2-mt_tx.2/position")
att_tz2 = PyTango.AttributeProxy("i03-c-c07/ex/gonio.2-mt_tz.1/position")


#from FindNextFileName import findNextFileName

# this will be "simple_PE" class, but written using motor control via spyc
# also, extract findNextFileName to be a single, common function

# special things - limit the max exposure time to 5 seconds
# automatically take darks?
# fast shutter driven automatically by detector?


def findNextFileName(prefix,ext,file_index=1):
    #
    #Prepare correct filename to avoid overwriting
    #
    psep=prefix.rfind(os.sep)
    if(psep<>-1): 
        fdir=prefix[:psep]
    else:
        fdir="."
    if(psep<>-1): prefix=prefix[psep+1:]
    if ext<>"":
        fname=prefix+"_"+"%04i"%(file_index)+"."+ext
    else:
        fname=prefix+"_"+"%04i"%(file_index)
    _dir=os.listdir(fdir)
    while(fname in _dir):
        file_index+=1
        if ext<>"":
            fname=prefix+"_"+"%04i"%(file_index)+"."+ext
        else:
            fname=prefix+"_"+"%04i"%(file_index)
    fname=fdir+os.sep+fname
    return fname


def findFirstIndex(prefix,ext,file_index=1):
	#
	# Returns the first index that will be used by findNextFileName
	#
    psep=prefix.rfind(os.sep)
    if(psep<>-1): 
        fdir=prefix[:psep]
    else:
        fdir="."
    if(psep<>-1): prefix=prefix[psep+1:]
    if ext<>"":
        fname=prefix+"_"+"%04i"%(file_index)+"."+ext
    else:
        fname=prefix+"_"+"%04i"%(file_index)
    _dir=os.listdir(fdir)
    while(fname in _dir):
        file_index+=1
        if ext<>"":
            fname=prefix+"_"+"%04i"%(file_index)+"."+ext
        else:
            fname=prefix+"_"+"%04i"%(file_index)
    return file_index


def nxs_read(filename, prefix):
	f = h5py.File(filename)
	a = f['/entry/scan_data/'+prefix]
	im = np.zeros(a.shape[1::])
	a.read_direct(im)
	f.close()
	return im


class PE(sensor):

	'''
	BEWARE: All times are translated in seconds!
	'''		      

	def test(self):
		'''test spyc commands'''
		get_ipython().magic(u'dmove rz2 2')
		get_ipython().magic(u'dmove rz2 -2')
		get_ipython().magic(u'dmove rz2 2')
		get_ipython().magic(u'dmove rz2 -2')

		print "done?"



	def snap(self, t_exp=1, filename = "ccdmar_image",folder="."):
		'''
		Typical usage:
		Take an image, counting 5 seconds, save file as test_image in directory : 

		myccd.snap(5, \"test_image\", \"/nfs/ruche-psiche/psiche-soleil/com-psiche/test_directory/\")

		Optional arguments:
		filetype = 0 corresponds to marccd (.mccd) files (default),
		filetype = 1 to nxs files
		shutter device (default is experimental hutch shutter)
		'''
		
		t_exp = t_exp*1.0
		
		# hard coded shutter device
		att_shut = PyTango.AttributeProxy("i03-c-c07/ex/prl.1-mt_tx.1/position")
		shut_close = 2.25
		shut_open = 1.25		

		# variables
		# with prl with acceleration of 10, a movement of 1 mm is about 0.6 seconds
		t_shut = .3 # shutter moving time
		
		# check that the ccd is ready
		if self.state() <> DevState.STANDBY:
			raise Exception("PE: cannot snap if device not in STANDBY state.")
			
		if t_exp > 5:
			nframes = np.ceil(t_exp / 5.0)
			t_exp = nframes * 5.0
			# set exposure time to 5 seconds
			self.DP.write_attribute("exposureTime", 5000)
			# we set the number of frames after acquiring the first dark
			if not self.DP.fileGeneration:
				print "must generate files in order to sum multi frames !"
				print "setting file generation = True"
				self.DP.filegeneration = True
		else:
		# and set the ccd time
			nframes = 1
			self.DP.write_attribute("exposureTime", t_exp * 1000)
			self.DP.write_attribute("nbFrames", nframes)

				
		# trigger the shutter
		print "close shutter"
		att_shut.get_device_proxy().write_attribute("position", shut_close)
		# wait the shutter to close
		timeout = time.time() + 10.
		timeout_flag = bool(0)
		while att_shut.get_device_proxy().state() <> DevState.STANDBY:
			sleep(0.1)
			print "waiting for shutter to close"			
			if time.time() > timeout:
				timeout_flag = bool(1)
				print "Motor: timeout >10 seconds for shutter close!!!"
				break
		
		# set the number of frames for the acquistion
		self.DP.write_attribute("nbFrames", nframes)
		
		# trigger the shutter
		print "open shutter"
		att_shut.get_device_proxy().write_attribute("position", shut_open)
			
		# wait the shutter opening time
		time.sleep(t_shut)

		# then start the detector
		print "start PE for image aquisition"
		self.DP.command_inout("Snap")

		# wait the acquistion time - the shutter time
		time.sleep(t_exp - t_shut)

		# trigger the shutter
		print "close shutter"
		att_shut.get_device_proxy().write_attribute("position", shut_close)

		# wait the shutter opening time
		time.sleep(t_shut)

		# now wait for the image to appear
		timeout = time.time() + 30.
		timeout_flag = False
		while self.state() <> DevState.STANDBY:
			print "waiting for detector to finish"
			sleep(0.1)			
			if time.time() > timeout:
				timeout_flag = True
				print "PE: timeout >30 seconds while performing a snap!!!"
				break

		# find where files are saved by the detector
		dir_nxs = self.DP.get_property("FileTargetPath")["FileTargetPath"][0]
		prefix_nxs = self.DP.get_property("FilePrefix")["FilePrefix"][0]
		# replace the first part of the path 
		dir_nxs = path_to_spool + os.sep + dir_nxs[34::]

		# get the image
		im = np.zeros(self.image.shape)
		if nframes==1:
			# one image only - read from device
			im = self.image * 1.0
		else:
			# multiple frames - read .nxs from the spool
			ll = glob.glob(dir_nxs + prefix_nxs + '*nxs')
			ll.sort()
			ll = ll[int(-nframes)::]
			for i in range (0,int(nframes)):
				print "reading " + ll[i] + " from " + prefix_nxs
				im = im + nxs_read(ll[i], prefix_nxs)
		
		# convert the image ready for saving
		im = Image.fromarray(im)
		self.output_im = im
			
		# save the image and show the statistics
		if not timeout_flag:
			# save the image
			self.saveImage(filename,folder)

			tmp=self.stats()
			print "Minimum Intensity = ", tmp[0]
			print "Maximum Intensity = ", tmp[1]
			print "Average Intensity = ", tmp[2]


	def sweep(self, motor="rz2", start_pos=None, end_pos=None, t_exp=None, filename="pe_image", folder=".", logfilehandle=None, return_to_start=True):
		'''
		Typical usage:
		Sweep rz from -5 to +5 in 5 seconds, save file as test_image in directory : 

		myPE.sweep(\"rz2\", -5, 5, 5, \"test_image\", \"/nfs/ruche-psiche/psiche-soleil/com-psiche/test_directory/\")

		Optional arguments:
		Save images as tiffs via Python
		With the cooling unit, don't do dark subtration automatically
		'''
		# treat the exposure time and number of frames
		t_exp = t_exp * 1.0 # time for ccd
		
		if t_exp > 5:
			nframes = np.ceil(t_exp / 5.0)
			t_exp = nframes * 5.0
			# set exposure time to 5 seconds
			self.DP.write_attribute("exposureTime", 5000)
			# we set the number of frames after acquiring the first dark
			if not self.DP.fileGeneration:
				print "must generate files in order to sum multi frames !"
				print "setting file generation = True"
				self.DP.filegeneration = True
		else:
		# and set the ccd time
			nframes = 1
			self.DP.write_attribute("exposureTime", t_exp * 1000)
			self.DP.write_attribute("nbFrames", nframes)

		# positive movements only for the moment
		if (end_pos - start_pos)<0:
			print "please sweep in the positive direction for the moment!"
			return		
		
		# save the original motor position and speed
		att = PyTango.AttributeProxy(movables[motor])
		init_pos = att.read().value
		init_v = att.get_device_proxy().read_attribute("velocity").value
		init_acc = att.get_device_proxy().read_attribute("acceleration").value

		# hard coded shutter device
		att_shut = PyTango.AttributeProxy("i03-c-c07/ex/prl.1-mt_tx.1/position")
		shut_close = 2.25
		shut_open = 1.25		

		# variables
		# with prl with acceleration of 10, a movement of 1 mm is about 0.6 seconds
		t_shut = .3 # shutter moving time
		t_pad = 0.1 #

		new_v = (end_pos - start_pos) / t_exp # motor speed required
		t_acc = new_v / init_acc # acceleration time
		s_acc = new_v * t_acc * 0.5 # acceleration distance
		s_shut = t_shut * new_v # shutter distance
		s_pad = t_pad * new_v # pad distance 
		s_start = start_pos - s_acc - s_shut - s_pad
		s_end = end_pos + s_acc + s_shut + s_pad
		
		# check the velocity
		if new_v > init_v:
			print "Max motor speed is %0.2f !" % init_v
			print "Change your sweep parameters to go slower"
			return
		if new_v < 0.006231:
			print "The calculated velocity is too slow"
			print "Change your sweep parameters to go faster"
			return

		# check that the ccd is ready
		if self.state() <> DevState.STANDBY:
			raise Exception("PE: cannot snap if device not in STANDBY state.")

		# write to the log file
		if logfilehandle != None:
			logfilehandle.write("##################################################\n")
			logfilehandle.write("timestamp = %0.2f\n" % time.time())
			loctime = time.localtime()
			logfilehandle.write("time = %02d : %02d : %02d\n" % (loctime[3],loctime[4],loctime[5]))
			logfilehandle.write("date = %02d/%02d/%04d\n" % (loctime[2],loctime[1],loctime[0]))
			logfilehandle.write("sweep = motor %s, from %0.2f to %0.2f\n" % (motor, start_pos, end_pos))
			logfilehandle.write("exposuretime = %0.2f\n" % t_exp)
			logfilehandle.write("nbFrames = %d\n" % nframes)

			# save some other motor positions
			# detector S/X/Z	
			logfilehandle.write("dts2 = %0.2f\n" % att_dts2.read().value)		
			logfilehandle.write("dtx2 = %0.2f\n" % att_dtx2.read().value)		
			logfilehandle.write("dtz2 = %0.2f\n" % att_dtz2.read().value)		
			logfilehandle.write("cs2 = %0.2f\n" % att_cs2.read().value)		
			logfilehandle.write("cx2 = %0.2f\n" % att_cx2.read().value)		
			logfilehandle.write("tz2 = %0.2f\n" % att_tz2.read().value)		


			logfilehandle.flush()

		# move to the start position
		print "move to start position..."
		mv(motor, s_start)

		# put everything inside a try loop for safety
		try:
			
			# trigger the shutter
			print "close shutter"
			att_shut.get_device_proxy().write_attribute("position", shut_close)
			# wait the shutter to close
			timeout = time.time() + 10.
			timeout_flag = bool(0)
			while att_shut.get_device_proxy().state() <> DevState.STANDBY:
				sleep(0.1)
				print "waiting for shutter to close"			
				if time.time() > timeout:
					timeout_flag = bool(1)
					print "Motor: timeout >10 seconds for shutter close!!!"
					break

			## get a dark, one frame only
			#print "acquire a dark image before the sequence"
			#self.DP.write_attribute("nbFrames", 1)
			#self.DP.command_inout("Snap")

			## now wait for the dark to appear
			#timeout = time.time() + 30.
			#timeout_flag = bool(0)
			#while self.state() <> DevState.STANDBY:
			#	sleep(0.1)			
			#	if time.time() > timeout:
			#		timeout_flag = bool(1)
			#		print "PE: timeout >30 seconds while acquiring dark!!!"
			#		break				

			## get the dark
			#darkbefore = self.image * 1.0

			# set the number of frames for the acquistion
			self.DP.write_attribute("nbFrames", nframes)

			# set the motor speed
			print "set motor speed %0.2f ..." % new_v
			att.get_device_proxy().write_attribute("velocity", new_v)
		
			# start the movement
			print "start movement to %0.2f ..." % s_end
			att.get_device_proxy().write_attribute("position", s_end)
			
			# wait the acceleration time and the safety time
			time.sleep(t_acc + t_pad)

			# trigger the shutter
			print "open shutter"
			att_shut.get_device_proxy().write_attribute("position", shut_open)
			
			# wait the shutter opening time
			time.sleep(t_shut)

			# then start the detector
			print "start PE for image aquisition"
			self.DP.command_inout("Snap")

			# write to logfile
			if logfilehandle != None:
				real_start_pos = att.read().value
				logfilehandle.write("rz2Start = %0.2f\n" % real_start_pos)
				logfilehandle.flush()

			# wait the acquistion time - the shutter time
			time.sleep(t_exp - t_shut)

			# trigger the shutter
			print "close shutter"
			att_shut.get_device_proxy().write_attribute("position", shut_close)

			# wait the shutter opening time
			time.sleep(t_shut)

			# write to logfile
			if logfilehandle != None:
				real_end_pos = att.read().value
				logfilehandle.write("rz2End = %0.2f\n" % real_end_pos)
				av_pos = (real_start_pos+real_end_pos)/2.0
				logfilehandle.write("rz2 = %0.2f\n" % av_pos)
				logfilehandle.flush()

			# now wait for the image to appear
			timeout = time.time() + 30.
			timeout_flag = False
			while self.state() <> DevState.STANDBY:
				print "waiting for detector to finish"
				sleep(0.1)			
				if time.time() > timeout:
					timeout_flag = True
					print "PE: timeout >30 seconds while performing a sweep!!!"
					break

			# find where files are saved by the detector
			dir_nxs = self.DP.get_property("FileTargetPath")["FileTargetPath"][0]
			prefix_nxs = self.DP.get_property("FilePrefix")["FilePrefix"][0]
			# replace the first part of the path 
			dir_nxs = path_to_spool + os.sep + dir_nxs[34::]

			# get the image
			im = np.zeros(self.image.shape)
			if nframes==1:
				# one image only - read from device
				im = self.image * 1.0
			else:
				# multiple frames - read .nxs from the spool
				ll = glob.glob(dir_nxs + prefix_nxs + '*nxs')
				ll.sort()
				ll = ll[int(-nframes)::]
				for i in range (0,int(nframes)):
					print "reading " + ll[i] + " from " + prefix_nxs
					im = im + nxs_read(ll[i], prefix_nxs)
			
			## get a dark, one frame only
			#print "acquire a dark image after the sequence"
			#self.DP.write_attribute("nbFrames", 1)
			#time.sleep(0.1)
			#self.DP.command_inout("Snap")

			# now wait for the dark to appear
			#timeout = time.time() + 30.
			#timeout_flag = bool(0)
			#while self.state() <> DevState.STANDBY:
			#	sleep(0.1)			
			#	if time.time() > timeout:
			#		timeout_flag = bool(1)
			#		print "PE: timeout >30 seconds while acquiring dark!!!"
			#		break				

			## get the dark
			#darkafter = self.image * 1.0

			#imcor = np.zeros(im.shape)
			## correct the images with the dark
			#imcor = im - (darkbefore * nframes/2) - (darkafter * nframes/2)
			#tmp1 = darkbefore
			#tmp2 = darkafter
			#tmp3 = im
			#tmp4 = imcor
			
			#  convert
			#im = Image.fromarray(imcor)
			im = Image.fromarray(im)
			self.output_im = im

			# save the image and show the statistics
			if not timeout_flag:

				# save the image
				self.saveImage(filename,folder)
				# clean the spool
				print "delete .nxs files from spool"
				os.system("rm -f " + dir_nxs + prefix_nxs + '*nxs') 				
				
				tmp=self.stats()
				print "Minimum Intensity = ", tmp[0]
				print "Maximum Intensity = ", tmp[1]
				print "Average Intensity = ", tmp[2]
				if logfilehandle != None:
					logfilehandle.write("FileName = %s\n" % os.path.basename(self.lastfilename))
					logfilehandle.write("FullFileName = %s\n" % self.lastfilename)
					logfilehandle.flush()

		except KeyboardInterrupt:
			# stop the motor
			att.get_device_proxy().stop()
			print "Stopping!"
			# in this case close the logfile if there is one
			if logfilehandle != None:
				logfilehandle.write("sweep was aborted by ctrl-C !\n")
				logfilehandle.flush()
				logfilehandle.close()

		finally:
			# stop the motor in case it is still moving
			att.get_device_proxy().stop()
			sleep(0.5)
			# reset the speed
			print "reset motor speed %0.2f ..." % init_v
			att.get_device_proxy().write_attribute("velocity", init_v)
			if return_to_start:
				# return to the start position
				print "return to original position"
				mv(motor, init_pos)
			else:
				# don't return to start position
				print "not returning to original position"
				# mv without new position just displays the current position 
				mv(motor)
			if logfilehandle != None:
				logfilehandle.flush()
			print "finished"

		#return tmp1, tmp2, tmp3, tmp4
		return
						
	def sweep_sequence(self, motor="rz2", start_pos=None, end_pos=None, nimages=None, t_exp=None, filename="pe_image", folder="./"):													
		'''
		call a series of self.sweep(self, motor="rz2", start_pos=None, end_pos=None, t_exp=None, filename="pe_image", folder=...)	
		'''			
		# if we have n images, we divide the range by n+1 - make sure we get a float answer
		step = (end_pos - start_pos) / ((nimages*1.0)+1)
		starts = np.linspace(start_pos, (end_pos-step), nimages)
		ends = np.linspace(start_pos+step, end_pos, nimages)

		# logfile name
		logfileprefix = folder + os.sep + filename + "_sweeplog"
		logfileextension = "txt"
		logfilename = findNextFileName(logfileprefix,logfileextension)
		logfilehandle = open(logfilename, 'w')
		logfilehandle.write("# sweep_sequence logfile\n")
		logfilehandle.write("# time is %0.2f\n" % time.time())
		loctime = time.localtime()
		logfilehandle.write("# time is %02d : %02d : %02d\n" % (loctime[3],loctime[4],loctime[5]))
		logfilehandle.write("# date is %02d/%02d/%04d\n" % (loctime[2],loctime[1],loctime[0]))
		
		logfilehandle.write("# sweep motor %s, from %0.2f to %0.2f in %d images\n" % (motor, start_pos, end_pos, nimages))
		logfilehandle.write("# exposure time is %0.2f\n" % t_exp)
		logfilehandle.write("# file prefix is %s, directory is %s\n" % (filename, folder))
		logfilehandle.write("# Detector motors in S,X,Z: dts2, dtx2, dtz2\n")
		logfilehandle.write("# Sample position S,X,Z: cs2, cx2, tz2\n")
		logfilehandle.write("# Rotation is : rz2\n")
		logfilehandle.write("FilePrefix = %s\n" % filename)
		firstnumber = findFirstIndex(folder + os.sep +filename,"tif")
		lastnumber = firstnumber + nimages -1
		logfilehandle.write("LastImage = %d\n" % lastnumber)
		logfilehandle.write("FirstImage = %d\n" % firstnumber)
		logfilehandle.write("FirstImageName = %s_%04i.%s\n" % (filename,firstnumber,"tif"))
		logfilehandle.write("LastImageName = %s_%04i.%s\n" % (filename,lastnumber,"tif"))
		logfilehandle.write("TimeStampStart = %0.2f\n" % time.time())

		logfilehandle.flush()
		
		# get the initial motor position to come back afterwards
		att = PyTango.AttributeProxy(movables[motor])
		init_pos = att.read().value

		for ii in range(nimages):
			self.sweep(motor, starts[ii], ends[ii], t_exp, filename, folder, logfilehandle, return_to_start=False)

		# return to the initial motor position
		print "return to original position after sequence"
		mv(motor, init_pos)
		
		logfilehandle.write("##################################################\n")
		logfilehandle.write("# sweep_sequence finished\n")
		logfilehandle.write("# time is %0.2f\n" % time.time())
		loctime = time.localtime()
		logfilehandle.write("# time is %02d : %02d : %02d\n" % (loctime[3],loctime[4],loctime[5]))
		logfilehandle.write("# date is %02d/%02d/%04d\n" % (loctime[2],loctime[1],loctime[0]))
		logfilehandle.write("TimeStampEnd = %0.2f\n" % time.time())
		logfilehandle.close()


	def saveImage(self, filename = "pe_image", folder="."):
		'''internal method, make private?'''
		
		fileprefix = folder + os.sep + filename
		myfilename = findNextFileName(fileprefix, "tif")
		self.output_im.save(myfilename)
		print "image saved as %s" % myfilename
		self.lastfilename = myfilename
		return

						
	def stop(self):
		if self.state()==DevState.RUNNING:
			self.DP.command_inout("Stop")
		return self.state()
	
	def read(self):
		return self.pos()
	
	def stats(self):
		tmp=self.pos()
		return tmp.min(), tmp.max(), tmp.mean()


