# -*- coding: utf-8 -*-

# this is supposed to be the definite bulky control function
# integrates - more or less - with mt synchro functions

# History
# v 1.0 Henry Proudhon based on the code from Maxime Pelerin

from __future__ import division, print_function, unicode_literals
import time
import os
import sys
import socket
import json
import PyTango
import codecs

# psiche
import updateon

from threading import Thread

class Data_Acquisition(Thread):
    """Data acquisition for the current sample.

    This class implements a simple data acquisition protocol
    which runs in a separate thread.
    """

    def __init__(self, bulky_instance, sample_name, dt=0.25):
        self.bulky_instance = bulky_instance
        self.dt = dt
        self.sample_name = sample_name
        self.data_acquisition = True
        #self.basler_acquisition = True
        Thread.__init__(self)

    def run(self):
        run_dir = os.path.dirname(self.bulky_instance.run_file)
        print('*** out_directory = %s' % run_dir)
        filename = os.path.join(run_dir, self.sample_name + '_data.log')
        self.bulky_instance.log("Acquisition started on " + filename)
        with open(filename, 'a') as data_file:
            i=0
            while self.data_acquisition:
                data = self.bulky_instance.get_log_data()
                data_file.write(('{} '*len(data)).format(*data) + '\n')
                time.sleep(self.dt)
                i+=1
        self.bulky_instance.log("Acquisition stopped " + filename)


class Bulky:
    """Class to handle the mechanical device Bulky.

    The Machine can do tension / compression testing and is adapted to X-ray tomography.

    :note: The machine is supposed to be configured with a positive motion for tension
    and negative for compression. Double check this before using this class.
    """

    def __init__(self, config_dir='/home/experiences/psiche/com-psiche'):

        # hard coded parameters
        self.v_rapide = 0.001  # mm/s
        self.v_lent = 0.0003  # mm/s
        self.failure_threshold = 50
        self.delta_total = 1
        self.acq = None
        self._tomo_counter = 0

        # get the required devices infos from the json config file
        config_path = os.path.join(config_dir, 'bulky_config.json')
        print('reading configuration for Bulky from file %s' % config_path)
        with open(config_path) as f:
            self.config = json.load(f)
        self.motor = PyTango.DeviceProxy(config['motor'])
        self._min_speed = 5.229e-5
        self.sai = PyTango.DeviceProxy(config['sai'])
        self.ptz = PyTango.DeviceProxy(config['ptz'])
        if not config['loadcell'] in ['100lbs', '500lbs', '1000lbs']:
            print('Wrong load cell type: {}. Load cell must be one of 100lbs, 500lbs, 1000lbs')
        self.loadcell_gain = config['loadcell_%s_gain' % config['loadcell']]

        # set the path to the json run file and make sure the file exists
        run_dir = os.path.join(mt.spool_directory, 'bulky')
        self.run_file = os.path.join(run_dir, 'bulky_run.json')
        if not os.path.exists(run_dir):
            os.mkdir(run_dir)
        if not os.path.exists(self.run_file):
            # create a default run config file in this directory
            run_config = {'delta':1e-2, 'velocity':2e-4, 'do_ref':False, 'failure':0.5}
            with open(self.run_file, 'w') as f:
                json.dump(run_config, f)
                print('created the bulky_run.json file in %s' % run_dir)

    def new_sample(self, sample_name):
        """Prepare things for a new sample to be tested.

        If the acquisistion is still running, it can be stopped.
        The sample_name is used to change the mt scan parameters, data is logged and a new acquisition is started.
        """
        try:
            if self.acq and self.acq.is_alive():
                print("Attention, une acquisition est toujours en cours !")
                rep = raw_input("Voulez-vous l'interrompre ? [Y]")
                if rep.lower() in ['', 'y', 'o', 'yes', 'oui']:
                    print("Tentative d'arrêt de la précédente acquisition")
                    self.acq.data_acquisition = False
        except:
            pass

        # set scan parameters + start synchro rotation
        mt.scanname = sample_name
        mt.setScanParameters()

        # log verbose output
        run_dir = os.path.dirname(self.run_file)
        self.logfile = codecs.open(os.path.join(run_dir, mt.scanname + '.log'), 'a', 'utf8')
        self.log('{} projections, {} references'.format(mt.nproj, mt.nref))
        self.log("Exposure time (ms) {}".format(mt.proxy.exposuretime))
        self.log("Load cell installed: {}".format(config['loadcell']))
        self.log("Load cell gain : {} N/V".format(config['loadcell_%s_gain' % config['loadcell']]))

        # log data (machine readable)
        self._tomo_counter = 0
        self.data_log_file = open(os.path.join(run_dir,  mt.scanname + '_log.dat'), 'a')
        self.data_log_file.write('#time number displacement force speed\n')
        self.log_data()
        updateon.updateon(self.config['updateon_hutch'], self.config['updateon_camera'])

        # start data acquisition
        self.acq = Data_Acquisition(self, sample_name)
        self.acq.start()

    def end_sample(self):
        """End the current sample.

        This is typically called after a specimen broke or when a test is finished.
        """
        if ptomorz.state is PyTango._PyTango.DevState.MOVING:
            mt.synchroFinalRefs()
        updateon.updateon(self.config['updateon_hutch'], self.config['updateon_camera'])
        self.acq.data_acquisition = False
        ptomorz.value = 0
        time.sleep(.5)
        self.logfile.close()
        self.data_log_file.close()
        print("\033[34;1mFermeture des fichiers log.\033[0m")

    def get_log_data(self):
        """Function to log expermental data at a given time."""
        return [time.time(), self._tomo_counter, self.motor.position, self.charge(), self.motor.velocity]

    def log(self, text):
        """Write some text to the verbose log file."""
        print(text)
        self.logfile.write('{} -> {}\n'.format(time.strftime('%F %H:%M:%S') + '{:.3f}'.format(time.time()%1)[1:], text))
        self.logfile.flush()

    def charge(self):
        """Return the current load in N.

        The load is read from the sai and multiplied by the load cell gain.
        """
        charge_V = getattr(self.sai, 'averagechannel' + str(self.config['sai_channel']))
        return charge_V * self.loadcell_gain

    def log_data(self):
        """Write the current data to the data log file.

        The file is flushed so that it can be freely accessed by other threads.
        """
        data = self.get_log_data()
        self.data_log_file.write(('{} '*len(data)).format(*data) + '\n')
        self.data_log_file.flush()

    def apply_load(self, force, delta, dt=.1, mode='abs', velocity=None, verbose=True):
        """Move the motor until the load reach a certain value.

        The crosshead is move at the given speed by steps of `delta` in position
        until the specified load is reach. After this the speed is changed back to the old value.
        TODO update this for compression load.

        @param float force: the target force (N).
        @param str mode: absolute or relative loading.
        @param float velocity: the motor velocity to use for this loading sequence (mm/s).
        """
        if not velocity:
            velocity = self.v_rapide
        v0 = self.motor.velocity
        self.motor.velocity = velocity
        p0 = self.motor.position
        c0 = self.charge()
        delta = np.abs(delta) # absolute value
        if mode.lower().startswith('rel'):
            force += c0
            positive = True if force > 0 else False
        elif mode.lower().startswith('abs'):
            positive = True if force > c0 else False
        else:
            print('Argument mode invalide !')
            return
        if verbose:
            self.log("Début du chargement jusqu'à {} N à {} mm/s".format(force, self.motor.velocity))
        try:
            self.motor.stop()
            if positive:
                self.motor.position += delta
                while self.charge() < force:
                    time.sleep(dt)
            else:
                self.motor.position -= delta
                while self.charge() > force:
                    time.sleep(dt)
            self.motor.stop()
        except BaseException as e:
            self.motor.stop()
            self.log('Erreur: Interruption inopinée de la boucle')
            if verbose:
                self.log('Position initiale : {} mm'.format(p0))
                self.log('Position actuelle : {} mm'.format(self.motor.position))
                self.log('Charge initiale : {} N'.format(c0))
                self.log('Charge actuelle : {} N'.format(self.charge()))
                self.log(e.__repr__())
            raise e
        finally:
            self.motor.velocity = v0

    #def initial_scan():
    #    mt.synchroStartRotation()
    #    mt.synchroSnap()
    #    if tomorz.state is PyTango._PyTango.DevState.MOVING:
    #        mt.synchroFinalRefs()

    def do_a_tomo(self, bulkyinfo=None):
        """Launch a tomography acquisistion and log some data before and after the scan."""
        self.extra_log_values()
        self.log('Début scan tomographie n°{}'.format(self._tomo_counter))
        self.log_data()
        mt.synchroSnap(bulkyinfo=bulkyinfo) # for fast synchro sequence
        self.log_data()
        self.log(self.get_log_data())
        self.log('Fin scan tomographie n°{} (initial)'.format(self._tomo_counter))
        self._tomo_counter += 1


    def apply_displacement(self, delta, dt=.1, velocity=None, tomo=True, tomo_wait=0, verbose=True):
        """Move the motor by an increment of displacement.

        The crosshead is moved by the ammount `delta` at the given speed.
        After this the speed is changed back to the old value.

        @param float delta: the increment of displacement (mm).
        @param float velocity: the motor velocity to use for this loading sequence (mm/s).
        @param bool tomo: do a tomo scan after this displacement.
        @param float tomo_wait: delay to apply befor acquiring a tomo scan.
        """
        if not velocity:
            velocity = self.v_rapide
        try:
            # recreate _dmove helper function
            self.motor.position += delta
            while self.motor.state() == PyTango._PyTango.DevState.MOVING:
                time.sleep(dt)
            print('Info : [dmove] fin du mouvement')
            print("Load : {:.2f} N".format(self.charge()))
        except:
            print('Erreur : [dmove] interruption moteur')
        finally:
            self.motor.stop()
        # AFTER THE INCREMENT, DO A TOMO
        if tomo:
            time.sleep(tomo_wait)
            self.do_a_tomo()
        else:
            print('not doing a tomo')


    def extra_log_values(self):
        self.log("ptomotx {} mm ; ptomots {} mm ; ptomotz {} mm".format(ptomotx.value, ptomots.value, ptomotz.value))

    def Loop(self, delta_total, dt=.1, restart=False, verbose=True):
        """Run a continuous loading sequence with tomographic imaging.

        This function is used to run a mechanical test and can run until failure.

        @param float delta_total: total displacement for this test.
        @param bool restart: flag to use if you want to restart the test
        after a Ctrl+C for instance (meaning the rotation is still on.
        """
        if not restart:
            mt.synchroStartRotation()
        v0 = self.motor.velocity
        with open(self.run_file, 'r') as f:
            run_config = json.load(f)
        self.motor.velocity = run_config['velocity']
        p00 = self.motor.position
        c0 = self.charge()
        ptz0 = self.ptz.position
        try:
            self.motor.position += delta_total
            i = 0
            self._tomo_counter += 1
            if not restart:
                if 'scan_velocity' in run_config:
                    self.log('Changement vitesse moteur à {}'.format(run_config['scan_velocity']))
                    self.motor.velocity = run_config['scan_velocity']
                self.extra_log_values()
                self.log('Début scan tomographie n°{}'.format(i))
                p1 = self.motor.position
                self.log_data()
                mt.synchroSnap() # for fast synchro sequence
                self.log_data()
                self.log('Fin scan tomographie n°{} (initial)'.format(i))
                if 'scan_velocity' in run_config:
                    self.log('Changement vitesse moteur à {}'.format(run_config['velocity']))
                    self.motor.velocity = run_config['velocity']
            while self.charge() > run_config['failure']:
                i = self._tomo_counter
                try:
                    with open(self.run_file, 'r') as f:
                        run_config = json.load(f)
                except:
                    print("\033[31;1mErreur de lecture du fichier JSON\033[0m")
                    print("Conserve les précédentes valeurs : {}".format(run_config))
                delta = run_config['delta']
                if run_config.get('do_ref', False):
                    mt.synchroTakeRefs()
                if run_config['velocity'] < self._min_speed:
                    print("\033[31;1mLa vitesse demandée n'est pas réalisable avec le moteur de bulky\033[0m")
                self.motor.velocity = max(run_config['velocity'], self._min_speed)
                p0 = self.motor.position
                self.log("Début de l'incrément de chargement n°{} de {} mm à {} mm/s".format(i+1, delta, self.motor.velocity))

                # ptz correction if needed
                if run_config.get('correct_ptz', False):
                    #deltaptz = max(0, delta / 2)
                    deltaptz = delta / 2  # works both in tension and compression
                    self.ptz.position += deltaptz
                    self.log("Correct vertical position {} mm".format(deltaptz))
                    while self.ptz.state() is not PyTango._PyTango.DevState.STANDBY:
                        time.sleep(0.1)

                while abs(self.motor.position - p0) < abs(delta):
                    time.sleep(dt)
                    print('\r{:8.5f} < {}'.format((self.motor.position - p0), delta), end='')
                    sys.stdout.flush() #
                print('\n############')
                if 'scan_velocity' in run_config:
                    self.log('Changement vitesse moteur à {}'.format(run_config['scan_velocity']))
                    self.motor.velocity = run_config['scan_velocity']
                self._tomo_counter += 1
                self.log('Début scan tomographie n°{}'.format(i))
                self.extra_log_values()
                p1 = self.motor.position
                self.log_data()
                mt.synchroSnap() # for fast synchro sequence
                self.log_data()
                time.sleep(1)
                if mt.proxy.state() is PyTango._PyTango.DevState.FAULT: #not PyTango._PyTango.DevState.STANDBY:
                    mt.proxy.init()
                    self.log("Erreur détecteur -- reinitialisation")
                    print("\033[31;1mErreur détecteur -- reinitialisation\033[0m")
                    self.log("Début scan de substitution")
                    self._tomo_counter += 1
                    self.log_data()
                    mt.synchroSnap()
                    self.log_data()
                    self.log("Fin scan de substitution")
                self.log('Fin scan tomographie n°{}, increment {} mm'.format(i, self.motor.position - p1))
                if 'scan_velocity' in run_config:
                    self.log('Changement vitesse moteur à {}'.format(run_config['velocity']))
                    self.motor.velocity = run_config['velocity']
                print("Charge cellule : {:.2f} N".format(self.charge()))
                print(i, delta) #
                print('\n############')
                sys.stdout.flush() #
            self.motor.stop()
            self.log('Arrêt moteur')
        except BaseException as e:
            self.motor.stop()
            self.ptz.stop()
            self.log('\033[31;1mErreur: Interruption inopinée de la boucle\033[0m')
            if verbose:
                self.log('Position initiale : {} mm'.format(p00))
                self.log('Position actuelle : {} mm'.format(self.motor.position))
                self.log('Charge initiale : {} N'.format(c0))
                self.log('Charge actuelle : {} N'.format(self.charge()))
                self.log(e.__repr__())
            raise e
        finally:
            self.motor.velocity = v0
