# -*- coding: utf-8 -*-

import time
import numpy as np
import pylab
pylab.ion()
import os
# simple bulky recording
#try:
#    from spyc.core import DeviceManager
#    dm = DeviceManager()

# visualise - should sit watching the bulky directory, and plot
# new log files... new figure for each new file? Updating only the live one.
# read both the log files in order to plot the tomo positions

# XXX.log  - human readable:
#2019-12-07 13:31:56.266 -> Début scan tomographie n°0
#2019-12-07 13:32:10.230 -> Fin scan tomographie n°0 (initial)

#XXX_data.log # from the Acq() thread - written every dt seconds
#XXX_log.dat # written to from the Loop() method with log_data() at before and after tomo
# these seem to be both machine readable column files - do we need both?
# should _log.dat be combined with the .log?

# advantage of using Maxime's _log.dat is that there is one file per sample, even if the synchro series is restarted

def visualise(filename):

    fh = open(filename, 'rt')
    tomofilename = filename.replace('_data.log', '_log.dat')


    # read from a data file
    pylab.figure() # open a new figure
    pylab.title('Loading: %s' % filename.split(os.sep)[-1]) # open a new figure
    pylab.xlabel('displacement / mm')
    pylab.ylabel('load / N')
    plotp = [] # plot data
    plotl = [] # plot data
    tomop = [] # tomo point data
    tomol = [] # tomo point data

    timestep = 0.5
    # loop for recording and display
    loop = True
    print "starting loop - hit ctrl-c to end"
    while loop:
      try:
        # read from the data file
        fh.seek(-150, 2)
        line = fh.read().splitlines()[-2]
        t,toto,d,f,toto2 = line.split()
        d = float(d)
        f = float(f)
        plotp.append(d)
        plotl.append(f)
        # read the tomo positions
        tomofh = open(tomofilename, 'rt')
        tomolines = tomofh.readlines()
        tomolines = tomolines[1:] # skip the header line
        tomofh.close()
        tomop = np.zeros(len(tomolines))
        tomol = np.zeros(len(tomolines))
        for ii in range(len(tomolines)):
            try:
                tomop[ii] = float(tomolines[ii].split()[2])
                tomol[ii] = float(tomolines[ii].split()[3])
            except:
                pass
        xmin = np.min((np.array(plotp).min(), tomop.min()))-0.5
        xmax = np.max((np.array(plotp).max(), tomop.max()))+0.5
        # plot
        pylab.clf()
        pylab.plot(plotp, plotl)
        pylab.plot(tomop, tomol, 'r*')
        pylab.title('Loading: %s' % filename.split(os.sep)[-1]) # open a new figur
        pylab.xlabel('displacement / mm')
        pylab.ylabel('load / N')
        pylab.xlim(xmin, xmax)
        pylab.draw()
        time.sleep(timestep)
      except Exception:
        time.sleep(.05)
      except KeyboardInterrupt:
        print "stopping the loop"
        loop = False
    fh.close()




## motor alias : bulky
## loadcell alias : load

#spooldir = '/nfs/srv3/spool1/flyscan-data-Lacourt_0319'

#def record(timestep=1):
  #'''Start recording every N seconds, write to a specified filename'''

  ## calibration
  #loadcal = 500. # N per volt, 1000lbs load cell
  ##loadcal = 41.2 # N per volt, 100lbs load cell ???

  ## get the name
  #getname = True
  #while getname:
    #name = raw_input('Please enter the sample name: ')
    #filename = spooldir + os.sep + name + '.dat'
    #if os.path.exists(filename):
        #print 'this name already exists! Please choose another'
    #else:
        #getname = False

  #try:
    ## open file and figure
    #print "open data file: %s" % filename
    #fh = open(filename, 'wt')
    #fh.write('#calibration %f N per volt\n' % loadcal)
    #fh.write('#time position load rawPos rawV ptz\n')
    ## reference positions
    #load0 = dm.actors["load"].value * 1.
    #position0 = dm.actors["bulky"].value * 1.
    #time0 = time.time()
    #plotl = [0]
    #plotp = [0]
    #plott = [0]
    #pylab.figure() # open a new figure
    #pylab.title('Loading: %s' % name) # open a new figure
    #pylab.xlabel('displacement / mm')
    #pylab.ylabel('load / N')


    ## loop for recording and display
    #loop = True
    #print "starting record loop - hit ctrl-c to end and save data"
    #while loop:
      #try:
        #loadN = (dm.actors["load"].value - load0) * loadcal
        #rawPos = dm.actors["bulky"].value
        #position = rawPos - position0
        #timeA = time.time()
        #timeS = timeA - time0

        #rawV = dm.actors["load"].value * 1.
        #ptz = dm.actors["ptz"].value * 1

        #fh.write('%f %f %f %f %f %f\n' % (timeA, position, loadN, rawPos, rawV, ptz))
        #pylab.clf()
        #plotp.append(position)
        #plotl.append(loadN)
        #pylab.plot(plotp, plotl)
        #pylab.title('Loading: %s' % name) # open a new figure
        #pylab.xlabel('displacement / mm')
        #pylab.ylabel('load / N')
        #pylab.draw()
        #time.sleep(timestep)
      #except KeyboardInterrupt:
        #print "stopping the loop"
        #loop = False
  #except:
    #print "problem occured?"
  #finally:
    #print "closing file: %s" % filename
    #fh.close()

