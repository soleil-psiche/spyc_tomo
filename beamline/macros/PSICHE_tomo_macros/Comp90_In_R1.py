#loads = [-10,-10,-5,-5,-5]
#suffixs = 'cdefg'

mt.refDisplacement = -12
tension.speed = 0.58
radio_dir = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/radios'
# adjust parameters
sample = 'Comp90_In_R1'
loads = [-5,-5,-5]
suffixs = 'hij' # pb with flyscan just before taking scan 'i'
ptz_scan = 16.0
ptz_ball1 = 14.5
ptz_ball2 = 17.5

def take_radios(prefix):
    print('taking radiograph of the lead balls')
    %amove ptz ptz_ball1
    mt.autoSaveOn()
    ndx = 1
    mt.setupSaving(radio_dir, prefix, ndx)
    mt.snap()
    %amove ptz ptz_ball2
    mt.snap()
    mt.autoSaveOff()
    %amove ptz ptz_scan

# take radios before loading the sample
#take_radios(prefix = '%s_ini_ball' % sample)
for i, load in enumerate(loads):
    mt.scanname = '%s_%c_' % (sample, suffixs[i])
    print('next scan will be %s' % mt.scanname)
    Henry_traction_machine.measure(loads[i], '%s.log' % mt.scanname)
    take_radios(prefix = '%sball' % mt.scanname)
    mt.doTomo()
