# -*- coding: utf-8 -*-


# function to build and display a mosaic image
# eventually to integrate into Tomo/Detector/Camera structure
# should give x coordinate in tomotx/tomots...
# this could use tomoright to work at any tomorz angle

import PyTango
import numpy as np
# pylab for figures
import pylab
pylab.ion()
# try handling reference motor with spyc
from spyc.core import DeviceManager
dm = DeviceManager()
# to save tifs
from PIL import Image
import os
from FindNextFileName import findNextFileName

#def mosaic(mt=None, rangex=[-1, 1], rangez=[-1, 1], bin=3, motor='tomotz'):
def mosaic(mt=None, xstart=None, xend=None, xsteps=None, zstart=None, zend=None, zsteps=None, bin=3, motor='tomotz'):
    # work with tomorefx
    # in tomo_spiral, we can specify the start and end positions in z, or a start or end and a number
    # of scans, or just the number of scans for a relative range about the current position
    
    # need the Tomo object to work
    if mt==None:
        print "Can\'t work without the Tomo object (mt)... quitting"
        return
    mt.stopLive()

    # get the image size
    imsize = np.array([mt.proxy.roiHeight, mt.proxy.roiWidth])
    # will crop slightly before binned to ensure integers
    imsizesmall = np.floor(imsize/bin)
    imsizecrop = imsizesmall*bin
    imsizemm = imsizecrop * mt.pixelsize / 1000
    
    # get the starting postions
    if motor=='tomotz':
      print "using tomotz"
      tzpos = dm.actors["tomotz"].get()
    elif motor=='ptz':
      print "using ptz"
      tzpos = dm.actors["ptz"].get()
    else:
      print "z motor not recognised"
      return
    # other motors
    rzpos = dm.actors["tomorz"].get()
    refxpos = dm.actors["tomorefx"].get()
    txpos = dm.actors["tomotx"].get()
    tspos = dm.actors["tomots"].get()
    
    # deal with scan limits - in z
    if ((zstart==None) & (zend==None)) & (zsteps != None):
        # special case - we don't specify start and stop, instead we go relative to the current pos.
        print "will scan %d steps relative to the current %s position (%f)" % (zsteps, motor, tzpos)
        zstart = tzpos - (imsizemm[0]*(zsteps/2.))
        zend = tzpos + (imsizemm[0]*(zsteps/2.)) + 0.01 # to be sure to get the last point
    elif ((zstart!=None) & (zend==None)) & (zsteps != None):
        # scan relative to the zstart position
        print "will scan %d steps relative to the %s start position (%f)" % (zsteps, motor, zstart)
        zend = zstart + (imsizemm[0]*zsteps) + 0.01 # to be sure to get the last point
    elif ((zstart==None) & (zend!=None)) & (zsteps != None):
        # scan relative to the zend position
        print "will scan %d steps relative to the %s end position (%f)" % (zsteps, motor, zend)
        zstart = zend - (imsizemm[0]*zsteps) - 0.01 # to be sure to get the last point
    elif ((zstart!=None) & (zend!=None)) & (zsteps == None):
        # scan from zstart to zend
        if zend<zstart:
            zstart,zend = zend, zstart
        # keep the centre of the scan the same
        zsteps = np.ceil((zend - zstart) / imsizemm[0])
        zcen = (zstart + zend) / 2.
        zlen = xsteps * imsizem[0]
        zstart = zcen - (zlen/2.)
        zend = zcen + (zlen/2.) + 0.01 # to be sure to get the last point
        print "will scan %d steps relative to the %s start position (%f)" % (zsteps, motor, zstart, zend)
    else:
        print "check the input arguments for the z limits (zstart, zend, zsteps) - not happy as is - quitting"
        return
        
    # deal with scan limits - in tomorefx
    if ((xstart==None) & (xend==None)) & (xsteps != None):
        # special case - we don't specify start and stop, instead we go relative to the current pos.
        print "will scan %d steps relative to the current tomorefx position (%f)" % (xsteps, refxpos)
        xstart = refxpos - (imsizemm[1]*(xsteps/2.))
        xend = refxpos + (imsizemm[1]*(xsteps/2.)) + 0.01 # to be sure to get the last point
    elif ((xstart!=None) & (xend==None)) & (xsteps != None):
        # scan relative to the xstart position
        print "will scan %d steps relative to the tomorefx start position (%f)" % (xsteps, xstart)
        xend = xstart + (imsizemm[1]*xsteps) + 0.01 # to be sure to get the last point
    elif ((xstart==None) & (xend!=None)) & (xsteps != None):
        # scan relative to the xend position
        print "will scan %d steps relative to the tomorefx end position (%f)" % (xsteps, xend)
        xstart = xend - (imsizemm[1]*xsteps) - 0.01 # to be sure to get the last point
    elif ((xstart!=None) & (xend!=None)) & (xsteps == None):
        # scan from xstart to xend
        if xend<xstart:
            xstart,xend = xend, xstart
        # keep the centre of the scan the same
        xsteps = np.ceil((xend - xstart) / imsizemm[1])
        xcen = (xstart + xend) / 2.
        xlen = xsteps * imsizem[1]
        xstart = xcen - (xlen/2.)
        xend = xcen + (xlen/2.) + 0.01 # to be sure to get the last point
        print "will scan %d steps relative in tomorefx from %f to %f" % (xsteps, xstart, xend)
    else:
        print "check the input arguments for the x limits (xstart, xend, xsteps) - not happy as is - quitting"
        return
        
    # measurement positions
    zpos = np.arange(zstart, zend, imsizemm[0])
    xpos = np.arange(xstart, xend, imsizemm[1])    
        
    # deal with the binning
    bin = float(bin)

    # take a reference and a dark...
    mt.updateRef()
    if mt.dark=='':
        get_ipython().magic(mt.shutter_close_command)
        mt.updateDark()
        get_ipython().magic(mt.shutter_open_command)
    # bin the ref and the dark    
    ref = mt.ref[0:imsizecrop[0], 0:imsizecrop[1]]
    ref = ref.reshape(imsizesmall[0], bin, imsizesmall[1], bin).mean(3).mean(1)
    dark = mt.dark[0:imsizecrop[0], 0:imsizecrop[1]]
    dark = dark.reshape(imsizesmall[0], bin, imsizesmall[1], bin).mean(3).mean(1)
    

    # prepare the output image
    output = np.zeros((imsizesmall[0]*len(zpos), imsizesmall[1]*len(xpos)))
    # this is the extent of the image in tomorefx
    imext = [xpos[-1]+(imsizemm[1]/2), xpos[0]-(imsizemm[1]/2), zpos[0]-(imsizemm[0]/2), zpos[-1]+(imsizemm[0]/2)]
    # in tomots/tomotx
    tmp = np.mod(np.round(rzpos/90), 4)
    if tmp==0: # tomorz = 0 degrees, tomotx // tomorefx
        sam_xpos = txpos + (xpos - refxpos)
        sam_imext = [sam_xpos[-1]+(imsizemm[1]/2), sam_xpos[0]-(imsizemm[1]/2), zpos[0]-(imsizemm[0]/2), zpos[-1]+(imsizemm[0]/2)]
    elif tmp==1: # tomorz = 90 degrees, tomots // tomorefx
        sam_xpos = tspos + (xpos - refxpos)
        sam_imext = [sam_xpos[-1]+(imsizemm[1]/2), sam_xpos[0]-(imsizemm[1]/2), zpos[0]-(imsizemm[0]/2), zpos[-1]+(imsizemm[0]/2)]
    elif tmp==2: # tomorz = 180 degrees, tomotx // -tomorefx
        sam_xpos = txpos - (xpos - refxpos)
        sam_imext = [sam_xpos[-1]-(imsizemm[1]/2), sam_xpos[0]+(imsizemm[1]/2), zpos[0]-(imsizemm[0]/2), zpos[-1]+(imsizemm[0]/2)]
    elif tmp==3: # tomorz = 270 degrees, tomots // -tomorefx
        sam_xpos = tspos - (xpos - refxpos)
        sam_imext = [sam_xpos[-1]-(imsizemm[1]/2), sam_xpos[0]+(imsizemm[1]/2), zpos[0]-(imsizemm[0]/2), zpos[-1]+(imsizemm[0]/2)]
    else:
        print "no sample translator perpendicular to beam"
        sam_xpos = xpos
            
    # create a new figure each time
    fig = pylab.figure()
    pylab.clf()
    pylab.imshow(output, extent=sam_imext)
    pylab.title("mosaic image to extend field of view")
    if (tmp==0) or (tmp==2):
        pylab.xlabel("tomotx position, mm")
    elif (tmp==1) or (tmp==3):
        pylab.xlabel("tomots position, mm")
    else:
        pylab.xlabel("tomorefx position, mm")
        
    if motor=='tomotz':
      pylab.ylabel("tomotz position, mm")
    elif motor=='ptz':
      pylab.ylabel("ptz position, mm")
    pylab.draw()
    
    # the loop - tomotz is faster than tomorefx, but ptz is slower
    if motor=='tomotz':
        for xx in range(len(xpos)):
            for zz in range(len(zpos)): 
                get_ipython().magic(u'amove tomorefx %0.2f tomotz %0.2f' % (xpos[xx],zpos[zz]))
                # snap an image from mt
                mt.snap()
                im = mt.image[0:imsizecrop[0], 0:imsizecrop[1]]
                # downsize and add to output - magic from stackoverflow
                im = im.reshape(imsizesmall[0], bin, imsizesmall[1], bin).mean(3).mean(1)
                im = (im-dark)/(ref-dark)
                # where in the output
                a = (len(zpos)-zz-1)*imsizesmall[0]
                b = a + imsizesmall[0]
                c = (len(xpos)-xx-1)*imsizesmall[1]
                d = c + imsizesmall[1]
                output[a:b, c:d] = im    
                pylab.clf()
                pylab.imshow(output, extent=sam_imext)
                pylab.title("mosaic image to extend field of view")
                if (tmp==0) or (tmp==2):
                    pylab.xlabel("tomotx position, mm")
                else:
                    pylab.xlabel("tomots position, mm")
                pylab.ylabel("tomotz position, mm")
                pylab.draw()
                
    elif motor=='ptz':
        for zz in range(len(zpos)):
            for xx in range(len(xpos)): 
                get_ipython().magic(u'amove tomorefx %0.2f ptz %0.2f' % (xpos[xx],zpos[zz]))
                # snap an image from mt
                mt.snap()
                im = mt.image[0:imsizecrop[0], 0:imsizecrop[1]]
                # downsize and add to output - magic from stackoverflow
                im = im.reshape(imsizesmall[0], bin, imsizesmall[1], bin).mean(3).mean(1)
                im = (im-dark)/(ref-dark)
                # where in the output
                a = (len(zpos)-zz-1)*imsizesmall[0]
                b = a + imsizesmall[0]
                c = (len(xpos)-xx-1)*imsizesmall[1]
                d = c + imsizesmall[1]
                output[a:b, c:d] = im    
                pylab.clf()
                pylab.imshow(output, extent=sam_imext)
                pylab.title("mosaic image to extend field of view")
                if (tmp==0) or (tmp==2):
                    pylab.xlabel("tomotx position, mm")
                else:
                    pylab.xlabel("tomots position, mm")
                pylab.ylabel("ptz position, mm")
                pylab.draw()
                
    print "return to original position"
    if motor=='tomotz':
        get_ipython().magic(u'amove tomorefx %0.2f tomotz %0.2f' % (refxpos, tzpos))
    elif motor=='ptz':
        get_ipython().magic(u'amove tomorefx %0.2f ptz %0.2f' % (refxpos, tzpos))

    # save the output in the reconstruction directory?
    save = raw_input('save image? [y]/n')
    save = False if save.lower() in ['n', 'no', 'false'] else True
    if save:
        name = raw_input('name for saving? : ')
        # make a directory if required
        if not os.path.exists('/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/Mosiacs/'):
            os.mkdir('/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/Mosiacs')
        # save the image        
        name = findNextFileName('/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/Mosiacs', name,'tif',0)
        im = Image.fromarray(output*1.0)
        im.save(name)
        print "image saved as %s" % name
    return output

