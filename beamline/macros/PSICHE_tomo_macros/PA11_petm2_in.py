# microstep switch set to A (1/40) so motor speed is /4
#loads = [-20,-40,-40]
#suffixs = 'abc' # scan at 'c' around 24 N (zero is around 18 N)

mt.refDisplacement = -12
tension.speed = 1.0
# adjust parameters
sample = 'PA11_petm2_in'
loads = [-5,-10,-15,-20,-20,-20,-50,-50,-70,-70]
suffixs = 'defghijklm'

for i, load in enumerate(loads):
    mt.scanname = '%s_%c_' % (sample, suffixs[i])
    print('next scan will be %s' % mt.scanname)
    Henry_traction_machine.measure(loads[i], '%s.log' % mt.scanname, relax_time=300)
    mt.doTomo()
