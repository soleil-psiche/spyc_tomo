

f = open('log/fast_series.txt', 'wt')
t0 = time.time()


def tomosnap(nscans = 1):
    %amove tz1 -3.5

    for ii in range(nscans):
        sbox.start()
        tstart = time.time()-t0
        astart = np.mod(tomorz.value, 360)

        f.write('%f, %f\n' % (tstart, astart))
        sys.stdout.write('%f, %f\n' % (tstart, astart))
        sys.stdout.flush()

        while sbox.state() != PyTango._PyTango.DevState.STANDBY:
            time.sleep(0.01)    
        time.sleep(0.1)    

    %amove tz1 4.0

