import numpy as np


def edf_read_dirty(filename, headerlength=512, imsize=[2048, 2048], \
    roi=[0, 750, 2048, 1], datatype=np.uint32, accfac=1):
    '''
    dirty and nasty edf_read for short term use!
    simple_edf_read(filename, headerlength=512, imsize=[2048,2048], roi=[0, 750, 2048, 1], datatype=np.uint32)
    imsize = xsize, ysize
    roi is x start, y start, xrange, yrange
    accfac = 1 - if this is an integer > 1, divide by this to convert to 16bit
    '''
    
    # open the file
    f = open(filename, 'rb')
    # how long is one value?
    bitlength = np.dtype(datatype).itemsize
    # skip to the start
    f.seek(headerlength+(bitlength*imsize[0]*roi[1]))
    # read the block (full lines)
    #a = f.read(bitlength*imsize[0]*roi[3])
    # sort into an image
    #b = np.fromstring(a, datatype)
    #c = np.reshape(b, (800, 2048))
    # as one line
    c = np.reshape(np.fromstring(f.read(bitlength*imsize[0]*roi[3]), datatype), (roi[3], imsize[0]))
    # close the file
    f.close()
    # crop this for the x roi
    c = c[:, roi[0]:(roi[0]+roi[2])]
    # convert to 16 bit ?
    if (datatype==np.uint32) and (accfac>1):
        c = np.uint16(c/accfac)
    
    return c
    

def vol_read_dirty(filename, volsize=None, roi=None, datatype=None):
    '''
    vol_read_dirty(filename, volsize=[2048, 2048, 1], roi=[0,0,0,2048,2048,1], datatype=np.float32)
    use the same data reordering as Henry Proudhon
    '''
    # open the file
    f = open(filename, 'rb')
    # if no volsize given, try to read the .info
    if volsize==None:
        try:
            pars = {}
            f2 = open(filename+'.info', 'rt')
            lines = f2.readlines()
            f2.close()
            for line in lines:
                parts = line.split("=")
                if len(parts)==2:
                    name = parts[0].strip()
                    try:
                        val = float(parts[1].strip())
                        pars.update({name:val})
                    except:
                        print "failed to read %s" % name
            if pars.has_key('NUM_Z'):
                volsize = [pars['NUM_X'], pars['NUM_Y'], pars['NUM_Z']]
            else:
                volsize = [pars['NUM_X'], pars['NUM_Y'], 1]
            volsize = [int(x) for x in volsize]
        except:
            print "Failed to read .info file"
            c = None
            return c
            
    # if no roi given, try to read first slice only 
    if roi==None:
         print "reading first slice only - if you want more, specify the roi"
         roi = [0,0,0,volsize[0],volsize[1],1]
            
    # if no datatype is given, infer from the filename
    if datatype==None:
        if filename[-4::]=='.vol':
            datatype = np.float32
        elif filename[-4::]=='.raw':
            datatype = np.uint8
        else:
            datatype = np.float32
            print "guessing datatype is float"
    
    # how long is one value?
    bitlength = np.dtype(datatype).itemsize
    # skip n slices to the starting point
    f.seek(bitlength*volsize[0]*volsize[1]*roi[2])
    # read the block of m whole slices
    c = np.reshape(np.fromstring(f.read(bitlength*volsize[0]*volsize[1]*roi[5]), datatype), (roi[5], volsize[1], volsize[0]))
    # close the file
    f.close()
    # reorder the data
    c = c.transpose(2,1,0)
    # crop to the x,y roi
    c = c[roi[0]:(roi[0]+roi[3]), roi[1]:(roi[1]+roi[4]), :]
    
    return c
    
    
