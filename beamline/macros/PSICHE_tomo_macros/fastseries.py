

# fast series acquisition
from FindNextFileName import findNextFileName
import PyTango
sbox = PyTango.DeviceProxy('flyscan/clock/spietbox-timebase.1')
ham = PyTango.DeviceProxy('i03-c-c00/dt/hamamatsu.1')
rz = PyTango.DeviceProxy('i03-c-c07/ex/tomo.2-mt_rz.1')


#############

isotherm = True

get_ipython().magic('feopen')

# params
nproj = 700
exptime = 0.0065 # in seconds

# note exptime must be > readout time
safetime = 0.0005
interval = 80. # seconds  !!!!!! !! !
nscans = 54.

# angle log
angles = []

# setup spietbox
sbox.mode = 1
sbox.firstpulseatfirstposition=False
sbox.pulseperiod = 180./nproj
sbox.sequencelength = nproj
sbox.prepare()

# setup rotation
rotspeed = 180 / (nproj * (exptime+safetime))
get_ipython().magic('setspeed tomorz %f' % rotspeed)

# open fast shutter and start moving
get_ipython().magic('amove tz1 50')
tomorz.proxy.forward()

# acquisition loop
# 51 references at start
sbox.sequencelength = 51
sbox.prepare()
get_ipython().magic('dmove tomotz -5')
sbox.start()
time.sleep(60*(exptime+safetime))
get_ipython().magic('dmove tomotz 5')
 
sbox.sequencelength = nproj
sbox.prepare()
t0 = time.time()
for ii in range(int(nscans)):
    # wait for time
    tstart = t0 + (interval*ii)
    while time.time() < tstart:
        time.sleep(0.05)
    if isotherm:
        get_ipython().magic('amove tz1 50')
    # read the angle
    angles.append(rz.position*1.)
    # start the speitbox
    sbox.start()
    time.sleep(6)
    if isotherm:
        get_ipython().magic('amove tz1 70')

# 51 references at end    

time.sleep(800*(exptime+safetime))
get_ipython().magic('amove tz1 50')
sbox.sequencelength = 51
sbox.prepare()
get_ipython().magic('dmove tomotz -5')
sbox.start()
time.sleep(60*(exptime+safetime))
get_ipython().magic('dmove tomotz 5')

# 51 dark at end
sbox.sequencelength = 51
sbox.prepare()
get_ipython().magic('amove tz1 70')
sbox.start()
tomorz.proxy.stop()

# save the angles
filename = findNextFileName('/nfs/ruche-psiche/psiche-soleil/com-psiche/3Dmagination', 'AL4CU_scanangles_series8_', 'pickle')
pickle.dump(angles, open(filename, 'wb'))

