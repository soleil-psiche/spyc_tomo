
# Tomo2... 
# refactor a bit, get rid of unused stuff, allow switching between flyscan/not flyscan, define new experiment, etc
# better selection of shutter device

# imports
import time
import numpy as np
ifft = np.fft.ifft
fft = np.fft.fft
from Detector import Detector
import subprocess
import PyTango
import sys
import h5py
import os
import pickle
# import par file tools
from spyc.core import DeviceManager
dm = DeviceManager()
# flyscan server
from fs.client import *

class Tomo(Detector):
    
    def __init__(self, mycamera="i03-c-c00/dt/hamamatsu.1"):    
        # initialise detector(camera)
        # camera handles taking and displaying images
        # detector handles the optics, focusing, alignment etc
        # tomo handles data acquisition

        Detector.__init__(self, mycamera)

        # preallocate with default values
        self.experimentname = "toto_XXYY"
        self.collection_directory = 'Y:\\flyscan-data\\psiche-soleil\\com-psiche' # where the orca writes images
        self.parfile_directory = "/nfs/naslowcost/spool1/flyscan-data/psiche-soleil/com-psiche/" # where python writes
        self.reconstruction_directory = "/work/experiences/psiche/king/toto_XXYY/" # where reconstruction is done
        self.backup_directory = "/nfs/ruche-psiche/psiche-soleil/com-psiche/toto_XXYY/" # where data is archived
        self.sample_logfile = "/nfs/ruche-psiche/psiche-soleil/com-psiche/toto_XXYY/scan_log.pickle"
        self.sample_logfile2 = "/nfs/ruche-psiche/psiche-soleil/king/logs/toto_XXYY/scan_log.pickle"

        # default scan parameters
        self.scanmode = "FlyScan" # or "NotFlyScan" for home made version
        self.scanname = None
        self.nproj = 1500
        self.nref = 21
        self.ndark = 21
        self.scanrange = float(360)
        self.flyscanSafetyTime = 0.005 # time in addition to the camera readout time
        self.block = 60
        self.initial_sleep = 0.0 # put beam on sample before starting scan
        self.rotationAxisPos = 1024.5
        self.calcRotationAxis = True        

        self.shutter_open_command = "amove tomotz -1"
        self.shutter_close_command = "amove tomotz -11"

        # devices that will be needed for scans
        print "Getting devices for tomography ..."
        # flyscan merger for filenames
        merger = PyTango.DeviceProxy('flyscan/core/data-merger.1')
        # get the rotation stage device for tomoscan
        self.tomorz = PyTango.AttributeProxy('I03-C-C07/EX/TOMO.2-MT_Rz.1/position')
        self.tomorzDP = PyTango.DeviceProxy('I03-C-C07/EX/TOMO.2-MT_Rz.1')
        # speitbox for triggering camera
        self.sbox = PyTango.DeviceProxy('flyscan/clock/spietbox-timebase.1')
        # spyc motors for logging
        if dm.actors.has_key("dts2"):
            self.detpos = dm.actors["dts2"]
        elif dm.actors.has_key("dts1"):
            self.detpos = dm.actors["dts1"]
        else:
            print "Could not find the detector S position actor for logging"
            self.detpos = None
        # get the energy for the log file
        self.mono = PyTango.AttributeProxy('i03-c-c04/op/mono1/energy')


# ------------------- SWITCH BETWEEN MODES - FLYSCAN AND OTHER -------------------------------

    def flyScanOn(self):
        '''setup detector for FlyScans'''
        self.proxy.put_property({'FileTargetPath': ['S:\\spool1\\orca\\flyscan-tmp-data']})
        self.proxy.put_property({'FileNbFrames': [10]})
        self.proxy.put_property({'FilePrefix': ['orca']})
        self.proxy.put_property({'FileFormat': ['NXS']})
        self.proxy.put_property({'FileWriteMode': ['SYNCHRONOUS']})
        self.proxy.put_property({'FileMemoryMode': ['NO_COPY']})
        self.proxy.Init()
        self.setSingleMode()
        self.scanmode = "FlyScan"
        print "Ready to work in FlyScan mode.  Run mt.setScanParameters() to set scan parameters"


    def flyScanOff(self):
        '''setup detector for home made, non-FlyScans'''
        self.scanmode = "NotFlyScan"
        print "Ready to work in non-FlyScan, home made mode.  Run mt.setScanParameters() to set scan parameters"
        # orca writes directly to NAS
        self.collection_directory = 'Y:\\flyscan-data\\psiche-soleil\\com-psiche'

# ------------------- SWITCH BETWEEN MODES - FLYSCAN AND OTHER -------------------------------

# ------------------------------ DEFINE A NEW EXPERIMENT -------------------------------------
    def newExperiment(self):
        '''Setup the directories and log files for a new experiment'''
        userinput = raw_input("experiment name (usually Name_MMYY):  ")
        if userinput != '':
            self.experimentname = userinput
            # set the paths and create directories
            # May eventually want a dedicated directory on SRV3 and on PSICHEGPU/STOCKAGE
            self.parfile_directory = "/nfs/naslowcost/spool1/flyscan-data/psiche-soleil/com-psiche/" # where python writes
            # reconstruction directory - on ISEI, or eventually PSICHEGPU
            self.reconstruction_directory = "/work/experiences/psiche/king/%s/" % self.experimentname # where reconstruction is done on ISEI
            subprocess.call(["ssh", "king@195.221.10.5", "\'mkdir %s\'" % self.reconstruction_directory])
            # backup directory on ruche, where logfile is saved
            self.backup_directory = "/nfs/ruche-psiche/psiche-soleil/com-psiche/%s/" % self.experimentname # where data is archived
            subprocess.call(["mkdir", self.backup_directory])
            self.sample_logfile = "/nfs/ruche-psiche/psiche-soleil/com-psiche/%s/scan_log.pickle" % self.experimentname # experiment logfile
            # second logfile in my home directory
            self.sample_logfile2 = "/nfs/ruche-psiche/psiche-soleil/king/logs/%s/scan_log.pickle" % self.experimentname # my backup
            subprocess.call(["mkdir", "/nfs/ruche-psiche/psiche-soleil/king/logs/"+self.experimentname])
        else:
            print "supply an experiment name to complete setup"

# ------------------------------ DEFINE A NEW EXPERIMENT -------------------------------------

# ------------------------------ SETUP A NEW ACQUISITION -------------------------------------
    def setScanParameters(self):
        '''User select tomo scan parameters'''
        userinput = raw_input("scan name [%s]:  " % self.scanname)
        if userinput != '':
            self.scanname = userinput       
        userinput = raw_input("scan range 180 or 360 [%d]:  " % self.scanrange)
        if userinput != '':
            self.scanrange = float(userinput)
        userinput = raw_input("number of projections [%d]:  " % self.nproj)
        if userinput != '':
            self.nproj = int(userinput)
        '''# avoid doing even numbers over 360 degrees
        if self.scanrange==360 and np.mod(self.nproj, 2)==0:
            print "WARNING: scanrange 360 and even number of projections could mean duplicated data!"
            print "If this is a regular tomography scan, choose an odd number of projections!"
            userinput = raw_input("Regular tomography scan? [yes]:  ")
            if userinput=="yes" or userinput=="y" or userinput=='':
                self.nproj = self.nproj+1
                print "setting %d projections" % self.nproj
        '''
        userinput = raw_input("number of references [%d]:  " % self.nref)
        if userinput != '':
            self.nref = int(userinput)
        userinput = raw_input("number of darks [%d]:  " % self.ndark)
        if userinput != '':
            self.ndark = int(userinput)
        # push these parameters to the flyscan config
        if self.scanmode=="FlyScan":
            print "Apply these parameters to the FlyScan config"
            self.setFlyScanParameters()


    def setFlyScanParameters(self):
        ''' Calculate the parameters for flyscan and apply the configuration'''
        # the trajectory:
        fss.cfgs.tomo_1D.continuous_actuators.rz.trajectory = [(0.0, float(self.scanrange), self.nproj)]
        fss.cfgs.tomo_1D.daq.dims = [self.nproj]
        # the axis speed:
        readout_time = self.calcReadoutTime()
        frame_time = (self.proxy.exposureTime/1000) + readout_time + self.flyscanSafetyTime
        frame_angle = float(self.scanrange)/self.nproj
        rot_speed = frame_angle/frame_time
        fss.cfgs.tomo_1D.continuous_actuators.rz.parameters.continuous_velocity = rot_speed
        # the spietbox
        fss.cfgs.tomo_1D.timebases.clock1.parameters.mode = 1.0 # position mode
        fss.cfgs.tomo_1D.timebases.clock1.parameters.pulse_period = frame_angle
        # camera
        fss.cfgs.tomo_1D.sensors.orca.parameters.exposure_time = self.proxy.exposureTime
        # hooks - should eventually split darks and refs into two scripts
        fss.cfgs.tomo_1D.hooks.dark_and_bright.parameters.num_dark_images = self.ndark
        fss.cfgs.tomo_1D.hooks.dark_and_bright.parameters.num_ref_images = self.nref
        fss.cfgs.tomo_1D.hooks.dark_and_bright.parameters.exposure_time_in_ms = self.proxy.exposureTime
        fss.cfgs.tomo_1D.hooks.dark_and_bright.sample_inout_motor_device_name = dm.actors[self.refMotor].device_name
        fss.cfgs.tomo_1D.hooks.dark_and_bright.parameters.sample_inout_relative_offset_in_mm = self.refDisplacement
        fss.cfgs.tomo_1D.hooks.dark_and_bright.parameters.initial_sleep = self.initial_sleep
        # stop the camera!
        self.stopLive()
        time.sleep(1)
        # set the updated config
        fss.set_cfg("tomo_1D")

# ------------------------------ SETUP A NEW ACQUISITION -------------------------------------

    def FlyScan(self):
        ''' This should launch flyscan with the current config'''

        print "Launching FlyScan with current configuration"
        self.stopLive()
        #self.pressureCheck()
        print "BYPASSING THE PRESSURE CHECK !!!!!!!!!!!!!!!!!!!!"    
        print "REMEMBER, SHUTTER OPEN AND CLOSED DISABLED IN DARK AND BRIGHT"
        # launch the FlyScan
        try:
            fss.start()
            sys.stdout.write('FlyScan is running... \n tomorz = 000 / %03d' % self.scanrange)
            sys.stdout.flush()
            while fss.state == PyTango._PyTango.DevState.RUNNING:
                
                sys.stdout.write('\b'*9)
                sys.stdout.write('%03d / %03d' % (self.tomorz.read().value, self.scanrange))
                sys.stdout.flush()
                time.sleep(0.5)

        except KeyboardInterrupt:
            print "STOPPING SCAN..."
            fss.abort()
        print "FlyScan has finished..."
        
        try: # working up to this section
            # FlyScan has finished - now we rename the files, and start PSICHE processing
            # where are the data?!
            self.my_nxs_file = merger.currentNexusFile
            # update the logfile, and make sure the scan name is ok
            self.writeLogFile()
            # rename the file and the folder
            self.__renameNexus()
            # after the scan - calculate centre of rotation, write par file
            # calc centre, write par file
            if self.calcRotationAxis:
                axis = self.findAxisAfterScan()
            else:
                axis = self.rotationAxisPos

            self.writeParFile(axis)
            self.writeTransferScript()
            print "finished post scan treatments"
        except:
            print "problem in the log/rename/parfile/scripts section"


    def writeLogFile(self):
        '''Write useful settings to the log file
            Could (try) to write all log files directly on Ruche
            Log is a pickle dictionary, with unique scannames and all meta data'''

        if os.path.isfile(self.sample_logfile):
            print "recording scan in log file: %s" % self.sample_logfile
            log = pickle.load(open(self.sample_logfile))
        else:
            print "will create log file: %s" % self.sample_logfile
            log = {"filename":["scanname, nxs_filename, nproj, scanrange, exposuretime, nref, ndark, optic, detpos, energy"]}
        # test the filename
        suffix = 0
        self.savename = self.scanname
        while self.savename in log:
            suffix+=1
            self.savename = self.scanname+"_"+str(suffix)
        print "scan name will become %s" % self.savename
        # add to log
        if self.detpos==None:
            detposvalue = None
        else:
            detposvalue = self.detpos.value
        if not flyscan:
            self.my_nxs_file = None
        #log.update({self.savename:[self.scanname, self.my_nxs_file, self.nproj, self.scanrange, self.proxy.exposuretime, self.nref, self.ndark, self.optic, detposvalue, self.mono.read().value]})
        log.update({self.savename:[self.scanname, self.my_nxs_file, self.nproj, self.scanrange, self.proxy.exposuretime, self.nref, self.ndark, self.optic, detposvalue]})
        pickle.dump(log, open(self.sample_logfile, 'wb'))
        # save to the other backup as well
        pickle.dump(log, open(self.sample_logfile2, 'wb'))

    def __renameNexus(self):
        '''rename the FlyScan nexus file to something user friendly - 
        we have written the logfile and have savename - a name not already used in this experiment'''
        # rename the nxs files
        my_nxs_dir = self.my_nxs_file[0:self.my_nxs_file.rfind(os.sep)]
        new_nxs_file = my_nxs_dir + os.sep + self.savename + ".nxs"
        # rename the main images file
        os.system("mv %s %s" % (self.my_nxs_file, new_nxs_file))
        # rename the folder
        new_nxs_dir = my_nxs_dir[0:my_nxs_dir.rfind(os.sep)] + os.sep + self.savename
        os.system("mv %s %s" % (my_nxs_dir, new_nxs_dir))
        # resulting nxs name and path:
        new_nxs_file = new_nxs_dir + os.sep + self.savename + ".nxs"
        # update file in self
        self.my_nxs_file = new_nxs_file
        # meta data nxs files are not renamed, but are in the renamed folder
        print "after renaming, data are in folder: %s" % new_nxs_dir    





    def tomoscan(self):
        '''set up everything, launch scan'''

        if flyscan:
            print "should use FlyScan!"
            print "10 second pause to hit crtl-C if you want"
            time.sleep(10)
        print "allez, go..."

        # check the vaccuum
        self.pressureCheck()
        
        # write the logfile first, to make sure the scan name is ok
        self.writeLogFile()
        self.scanname = self.savename
        # we don't need to read the images into python
        self.stopLive()
        self.readimage = False
        self.proxy.put_property({'FileTargetPath': [self.collection_directory+'\\'+self.scanname]})
        self.proxy.fileGeneration = True
        
        # open the shutter
        get_ipython().magic(u'feopen')
        try:
            #self.writeTransferLoopScript()

            # first references
            self.proxy.put_property({'FilePrefix': ['refstart_']})
            self.proxy.nbFrames = self.nref*1.0
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            self.proxy.Init()
            print "move to tomorz = 0 to start scan"
            get_ipython().magic(u'amove tomorz 0')

            print "move in x for references - hardcoded for Gelebart!"
            get_ipython().magic(u'amove tomotz 17')
            get_ipython().magic(u'dmove %s %0.2f' % (self.refMotor, self.refDisplacement))
            self.snap()
            print "moving back x after references"
            get_ipython().magic(u'dmove %s %0.2f' % (self.refMotor, -self.refDisplacement))
            get_ipython().magic(self.shutter_open_command)
            
            # main scan
            self.proxy.put_property({'FilePrefix': [self.scanname]})
            '''
            # this is a slow, but working solution
            self.proxy.nbFrames = 1.0
            self.proxy.Init()
            rzpos = np.linspace(0, self.scanrange, self.nproj, endpoint=False)

            sys.stdout.write("\ntomorz =   0.000")
            
            for ii in range(self.nproj):
                self.tomorz.write(rzpos[ii]) # doesn't wait
                sys.stdout.write("\b"*7)
                sys.stdout.write(("%0.3f" % rzpos[ii]).rjust(7))
                sys.stdout.flush()
                self.snap() # waits for the snap to finish
                # make sure that movement has finished to ensure synchronisation
                while self.tomorz.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]:
                    time.sleep(0.1)

            time.sleep(0.5)
            '''
            # try semi flyscan option
            print "semi flyscan option"
            # calculations
            blockangle = self.block*self.scanrange/self.nproj
            accfac = self.proxy.exposuretime / self.proxy.exposureacctime
            # program speitbox
            self.sbox.mode = 1
            self.sbox.pulsePeriod = self.scanrange/(self.nproj*accfac)
            self.sbox.sequenceLength = int(self.block*accfac)
            self.sbox.Prepare()
            # prepare camera
            self.proxy.triggerMode = 'EXTERNAL_MULTI'
            self.proxy.nbFrames = self.block
            self.proxy.init()
            readtime = self.calcReadoutTime()
            frametime = (self.proxy.exposureacctime/1000) + readtime + self.flyscanSafetyTime
            # prepare rotation
            speed = self.sbox.pulsePeriod / frametime
            get_ipython().magic(u'setspeed tomorz %0.3f' % speed)
            # block start angles
            startangles = np.linspace(0, self.scanrange, self.nproj/self.block, False)

            # scan loop
            sys.stdout.write("\ntomorz =   0.000")
            for ii in range(len(startangles)):
                # move to start of block - wait to arrive
                self.tomorz.write(startangles[ii])
                sys.stdout.write("\b"*7)
                sys.stdout.write(("%0.3f" % startangles[ii]).rjust(7))
                sys.stdout.flush()
                while self.tomorz.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]:
                    time.sleep(0.1)
                time.sleep(0.1)
                # start the camera, the speitbox, and then the movement			
                self.proxy.snap()
                self.sbox.start()
                self.tomorz.write(startangles[ii]+blockangle+0.05)
                # wait for the camera to finish writing
                while (self.proxy.status() == "Acquisition is Running ...\n\n") or \
                    (self.tomorz.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]):
                    time.sleep(0.1)
                time.sleep(0.1)

            # after the loop
            time.sleep(0.5)
            print "reset motor speed"
            get_ipython().magic(u'setspeed tomorz 30')  
            #'''
            # last references
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            self.proxy.put_property({'FilePrefix': ['refend%06d_' % (self.nproj+1)]})
            self.proxy.nbFrames = self.nref*1.0
            self.proxy.Init()
            print "move to tomorz = 0 for references"
            get_ipython().magic(u'amove tomorz 0')
            print "move in x for references - hardcoded for Gelebart!"
            get_ipython().magic(u'amove tomotz 17')
            get_ipython().magic(u'dmove %s %0.2f' % (self.refMotor, self.refDisplacement))
            self.snap()
            saveref = self.proxy.image
            print "moving back x after references"
            get_ipython().magic(u'dmove %s %0.2f' % (self.refMotor, -self.refDisplacement))
            get_ipython().magic(self.shutter_open_command)

            # darks
            self.proxy.put_property({'FilePrefix': ['dark']})
            self.proxy.nbFrames = self.ndark*1.0
            self.proxy.Init()
            get_ipython().magic(self.shutter_close_command) 
            self.snap()
            savedark = self.proxy.image
            get_ipython().magic(self.shutter_open_command) 
    
            # after scan - image to find axis
            self.proxy.put_property({'FilePrefix': ['after_']})
            self.proxy.nbFrames = 1
            self.proxy.Init()
            self.snap()
            im0 = self.proxy.image
            get_ipython().magic(u'amove tomorz 180')
            self.snap()
            im180 = self.proxy.image
            # make sure that we have a correct image in self
            self.image = im180
            get_ipython().magic(u'amove tomorz 0')

            # try to find the rotation axis
            if self.calcRotationAxis:
                axis = self.findAxisAfterScan()
            else:
                axis = self.rotationAxisPos
            
            # switch off saving
            print "scan finished successfully"
            self.proxy.fileGeneration = False
            self.writeParFile(axis)
            self.writeTransferScript()
            get_ipython().magic(self.shutter_close_command) 

        except KeyboardInterrupt:
            print "STOPPING SCAN"
            self.tomorzDP.stop()
            time.sleep(0.5)
            get_ipython().magic(u'setspeed tomorz 30')

            print "stop the camera"
            # program speitbox
            self.sbox.mode = 0
            self.sbox.pulsePeriod = 50
            self.sbox.sequenceLength = 100
            self.sbox.Prepare()
            # prepare camera
            self.sbox.start()
            self.proxy.stop()

# ------------------------------ Calculate rotation axis -------------------------------------
    def findAxisAfterScan(self, im0=None, im180=None, ref=None, dark=None):
        '''from two images, calc axis position'''
        # if we are working in FlyScan mode, get the images from the file
        # otherwise, images are passed in as arguments 
        try:
            if flyscan and im0==None:
                print "flyscan axis calc"
                # find the nxs file
                mydir = self.my_nxs_file[0:self.my_nxs_file.rfind(os.sep)]
                # get the images
                f = h5py.File(self.my_nxs_file, 'r')
                daq = f.keys()[0]
                daq_grp = f[daq]
                scan_data = daq_grp["scan_data"]
                orca_data = scan_data["orca"]
                im0 = orca_data[0]
                if self.scanrange==360:
                    im180 = orca_data[int(self.nproj/2.)]
                else:
                    im180 = orca_data[-1]
                f.close()
                # get a ref
                f = h5py.File(mydir+os.sep+'post_ref.nxs', 'r')
                daq = f.keys()[0]
                data = f[daq]
                ref = data[(self.nref-1)]
                f.close()
                # get a dark
                f = h5py.File(mydir+os.sep+'post_dark.nxs', 'r')
                daq = f.keys()[0]
                data = f[daq]
                dark = data[(self.ndark-1)]
                f.close()
    
            im0 = (im0*1. - dark)/(ref - dark)
            im180 = (im180*1. - dark)/(ref - dark)
            im180flip = im180[:, ::-1]
            sizex, sizey = im0.shape[1], im0.shape[0] 
            c2 = np.zeros(sizex)
            for ii in range(sizey):
                 # correlate row by row, summing the results
                corResult = ifft(fft(im0[ii,:])*np.conj(fft(im180flip[ii,:])))
                corResult = np.abs(corResult)
                c2 = c2+corResult
            corResultTotal = np.nonzero(c2==c2.max())[0][0]
            # pick out the five pixels around the peak
            # must stop this from going outside the matrix
            xdata = corResultTotal+[-2, -1, 0, 1, 2]
            xdata_ndx = np.mod(xdata, sizex).tolist()
            ydata = c2[xdata_ndx]
            # fit this with a quadratic
            # ydata = a(xdata)^2 + b(xdata) + c
            xdata = np.array(xdata)
            xdata = np.array([xdata**2,xdata,np.ones(len(xdata))]).T
            a,b,c = np.linalg.lstsq(xdata, ydata)[0]
            # find the maximum of this, derivate
            # dy/dx = 2a(xdata) + b = 0
            # x = -b / 2a        
            corResultFine = -b / (2*a)
            if corResultFine > (sizex/2.):
                axis = corResultFine/2.
            else:
                axis = (sizex-1+corResultFine)/2.
        except:
            axis = (self.proxy.roiwidth+1)/2.
            print "Axis calculation failed: Default to axis position: %0.1f..." % axis
        return axis
# ------------------------------ Calculate rotation axis -------------------------------------

# --------------------------------- Write a par file -----------------------------------------
    def writeParFile(self, axispos):
        '''write a pyHST2 parfile to go with the data'''
        # open and read the template file
        f = open('/nfs/ruche-psiche/psiche-soleil/com-psiche/tomo_commissioning_sept2014/pyHST2_parfile_template.par', 'r')
        lines = f.readlines()    
        f.close()

        # open the file to write to:
        if flyscan:
            myfile = self.my_nxs_file
            mydir = myfile[0:myfile.rfind(os.sep)]
            nxs_name = self.my_nxs_file.split(os.sep)[-1][0:-4:]
            dirname = self.my_nxs_file.split(os.sep)[-2]
            parfilename = '%s/%s.par' % (mydir, nxs_name)    
        else:
            parfilename = '%s/%s/%s.par' % (self.parfile_directory, self.scanname, self.scanname)
            # might have to create this directory
            if not os.path.exists('%s/%s' % (self.parfile_directory,self.scanname)):
                os.mkdir('%s/%s' % (self.parfile_directory, self.scanname))           
            dirname = self.scanname
            nxs_name = self.scanname

        f2 = open(parfilename, 'w')

        # modify the lines that need modifying
        lines[3] = 'FILE_PREFIX = %s/%s/%s_edfs/%s\n' % (self.reconstruction_directory, dirname, nxs_name, nxs_name)
        if flyscan:
            lines[4] = 'NUM_FIRST_IMAGE = 0 # No. of first projection file\n' # start from 0 in flyscan!
            lines[5] = 'NUM_LAST_IMAGE = %d # No. of last projection file\n' % (self.nproj-1)            
            lines[26] = 'FF_NUM_FIRST_IMAGE = 0 # No. of first flat-field file\n' # start from 1
            lines[27] = 'FF_NUM_LAST_IMAGE = %d # No. of last flat-field file\n' % self.nproj
        else:
            lines[4] = 'NUM_FIRST_IMAGE = 1 # No. of first projection file\n' # start from 1
            lines[5] = 'NUM_LAST_IMAGE = %d # No. of last projection file\n' % self.nproj
            lines[26] = 'FF_NUM_FIRST_IMAGE = 1 # No. of first flat-field file\n' # start from 1
            lines[27] = 'FF_NUM_LAST_IMAGE = %d # No. of last flat-field file\n' % (self.nproj+1)
        lines[31] = 'FF_FILE_INTERVAL = %d # Interval between flat-field files\n' % self.nproj
        lines[12] = 'NUM_IMAGE_1 = %d # Number of pixels horizontally\n' % self.proxy.roiWidth
        lines[13] = 'NUM_IMAGE_2 = %d # Number of pixels vertically\n' % self.proxy.roiHeight
        lines[19] = 'BACKGROUND_FILE = %s/%s/%s_edfs/dark.edf\n' % (self.reconstruction_directory,dirname, nxs_name)
        lines[25] = 'FF_PREFIX = %s/%s/%s_edfs/ref\n' % (self.reconstruction_directory,dirname, nxs_name)

        lines[36] = 'ANGLE_BETWEEN_PROJECTIONS = %0.6f # Increment angle in degrees\n' % (float(self.scanrange)/self.nproj)
        lines[38] = 'ROTATION_AXIS_POSITION = %0.3f # Position in pixels\n' % axispos
        # reconstruct the whole of the centre slice
        lines[43] = 'START_VOXEL_1 =      1 # X-start of reconstruction volume\n'
        lines[44] = 'START_VOXEL_2 =      1 # Y-start of reconstruction volume\n'
        lines[45] = 'START_VOXEL_3 =      %d # Z-start of reconstruction volume\n' % round(self.proxy.roiHeight/2.)
        lines[46] = 'END_VOXEL_1 =      %d # Z-start of reconstruction volume\n' % self.proxy.roiWidth
        lines[47] = 'END_VOXEL_2 =      %d # Z-start of reconstruction volume\n' % self.proxy.roiWidth
        lines[48] = 'END_VOXEL_3 =      %d # Z-start of reconstruction volume\n' % round(self.proxy.roiHeight/2.)
        lines[71] = 'OUTPUT_FILE = %s/%s/%s_slice.vol\n' % (self.reconstruction_directory,dirname, nxs_name)

        # write the new par file
        f2.writelines(lines)
        f2.close()
        print "pyHST par file %s written and closed" % parfilename
# --------------------------------- Write a par file -----------------------------------------

    def writeTransferScript(self):
        '''write a shell script to tar and rsync the scan to ISEI'''
        if not flyscan:
            # same thing for few slices
            lines = []
            lines.append('# !/bin/bash\n')
            lines.append('\n')
            lines.append('echo  _______________ transfer scan %s to cluster for reconstruction  _______________ \n' % self.savename)
            # where are the data?

            # where do they go?
            targetdir = self.reconstruction_directory+self.scanname+os.sep
            # write the command 
            lines.append('rsync -Pav *.p* king@195.221.10.5:%s/%s\n' % (self.reconstruction_directory, self.scanname))        
            lines.append('echo  _______________ completed transfer of scan %s to cluster  _______________ \n' % self.savename)
            # open the file to write to:
            transferfilename = '%s/%s/fast_transfer_%s.sh' % (self.parfile_directory,self.scanname,self.savename)
            f2 = open(transferfilename, 'w')
            f2.writelines(lines)
            f2.close()
            subprocess.call(["chmod", "777", transferfilename])

            # at the same time, write a script to backup to Ruche
            lines = []
            lines.append('# !/bin/bash\n')
            lines.append('\n')
            lines.append('echo _______________ transfer scan %s to RUCHE for backup _______________  \n' % self.savename)
            # where do they go?
            # write the command
            lines.append('cp -vR %s/%s %s/\n' % (self.parfile_directory,self.scanname, self.backup_directory))
            lines.append('chmod 777 -R %s/%s/\n' % (self.backup_directory,self.scanname))        
            lines.append('echo  _______________ completed transfer of scan %s to RUCHE _______________  \n' % self.savename)   
            # open the file to write to:
            backupfilename = '%s/%s/backup_%s.sh' % (self.parfile_directory,self.scanname,self.savename)
            f2 = open(backupfilename, 'w')
            f2.writelines(lines)
            f2.close()
            subprocess.call(["chmod", "777", backupfilename])
    
        elif flyscan:
            # write a script for flyscan format
            lines = []
            lines.append('# !/bin/bash\n')
            lines.append('\n')
            lines.append('echo  _______________ transfer scan %s to cluster for reconstruction  _______________ \n' % self.savename)
            # where are the data?
            mydir = self.my_nxs_file[0:self.my_nxs_file.rfind(os.sep)]
            dirname = self.my_nxs_file.split(os.sep)[-2]
            # where do they go?
            targetdir = self.reconstruction_directory+dirname+os.sep
            # write the command
            lines.append('rsync -Pav %s/* king@195.221.10.5:%s\n' % (mydir, targetdir))        
            lines.append('echo  _______________ completed transfer of scan %s to cluster  _______________ \n' % self.savename)
            # open the file to write to:
            transferfilename = '%s/transfer_%s.sh' % (mydir, self.savename)
            f2 = open(transferfilename, 'w')
            f2.writelines(lines)
            f2.close()
            subprocess.call(["chmod", "777", transferfilename])
            
            # same thing for few slices
            lines = []
            lines.append('# !/bin/bash\n')
            lines.append('\n')
            lines.append('echo  _______________ transfer scan %s to cluster for reconstruction  _______________ \n' % self.savename)
            # where are the data?
            mydir = self.my_nxs_file[0:self.my_nxs_file.rfind(os.sep)]
            dirname = self.my_nxs_file.split(os.sep)[-2]
            # where do they go?
            targetdir = self.reconstruction_directory+dirname+os.sep
            # write the command
            lines.append('rsync -Pav %s/*.p* king@195.221.10.5:%s\n' % (mydir, targetdir))        
            lines.append('echo  _______________ completed transfer of scan %s to cluster  _______________ \n' % self.savename)
            # open the file to write to:
            transferfilename = '%s/fast_transfer_%s.sh' % (mydir, self.savename)
            f2 = open(transferfilename, 'w')
            f2.writelines(lines)
            f2.close()
            subprocess.call(["chmod", "777", transferfilename])

            # at the same time, write a script to backup to Ruche
            lines = []
            lines.append('# !/bin/bash\n')
            lines.append('\n')
            lines.append('echo _______________ transfer scan %s to RUCHE for backup _______________  \n' % self.savename)
            # write the command
            lines.append('chmod -R 777 %s\n' % mydir)
            lines.append('cp -vR %s %s/\n' % (mydir, self.backup_directory))        
            lines.append('echo  _______________ completed transfer of scan %s to RUCHE _______________  \n' % self.savename)   
            # open the file to write to:
            backupfilename = '%s/backup_%s.sh' % (mydir, self.savename)
            f2 = open(backupfilename, 'w')
            f2.writelines(lines)
            f2.close()
            subprocess.call(["chmod", "777", backupfilename])


    def alignmentMemo(self):
        '''print a memo list of what to align'''
        print "detector focus"
        print "axis perpendicular to beam - table tilt"
        print "rotation axis centred wrt detector"
        print "rotation axis parallel to detector columns - tomorotc"
        print "set energy"
        print "set exposure time"

    def showScanParameters(self):
        '''Display the current parameters'''
        print "scan name = %s" % self.scanname
        #print "collection directory = %s" % self.collection_directory
        print "reconstruction directory = %s" % self.reconstruction_directory
        print "%d projections over %d degrees" % (self.nproj, self.scanrange)
        print "%d references, %d darks" % (self.nref, self.ndark)


    def pressureCheck(self):
        '''check VFM and mono'''
        dp = PyTango.DeviceProxy('i03-c-c03/vi/jpen.1')
        dp2 = PyTango.DeviceProxy('i03-c-c04/vi/jpen.1')
        vfm = dp.read_attribute('pressure').value
        mono = dp2.read_attribute('pressure').value
        print "VFM pressure: %e   Mono pressure: %e " % (vfm, mono)
        if (vfm>5e-7) or (mono>5e-7):
            time.sleep(10)
            vfm = dp.read_attribute('pressure').value
            mono = dp2.read_attribute('pressure').value
            print "Second try:  VFM pressure: %e   Mono pressure: %e " % (vfm, mono)
            if (vfm>5e-7) or (mono>5e-7):
                print "Help! Vaccuum problem...?"
                print "close front end..."
                get_ipython().magic(u'feclose')
        else:
            print "Pressure OK"
        


        
