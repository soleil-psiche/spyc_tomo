

import PyTango
import time
import os
import pylab
pylab.ion()

#from bulky_1116 import *

log_dir = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/log_1116'
bulky = PyTango.DeviceProxy('i03-c-c07/ex/tomo.2-mt_tz.1')
#loadcell = PyTango.DeviceProxy('i03-c-mines/ca/sai_14b.1')
loadcell = PyTango.DeviceProxy('i03-c-c00/ca/sai.1')

def measure(delta=0, filename='', relax_time=300):

    if filename == '':
        print 'must give a filename'
        return
    log_path = os.path.join(log_dir, filename)
    if os.path.exists(log_path):
        print 'file %s exist already - appending new data' % log_path
        f = open(log_path, 'at')
    else:
        print 'opening file %s' % log_path
        f = open(log_path, 'wt') 
        f.write('#time, position, loadcell\n') 

    t0 = 0. #time.time()
    p0 = 0. #bulky.position
    tt,pp,ll = read_and_write_data(f, t0, p0)

    # open a figure
    pylab.figure()

    # move the motor
    bulky.position = bulky.position + delta

    finished = False
    relax = False
    while not finished:
    
        try:
            tt,pp,ll = read_and_write_data(f, t0, p0)
            if relax and (tt - t0_relax > relax_time):
                finished = True
                print('Load after relaxation %0.2f' % ll)
            if relax == False and bulky.state() == PyTango._PyTango.DevState.STANDBY:
                # start relax
                print('Maximum Load %0.2f ' % ll)
                print('move finished, waiting %d sec' % relax_time)
                relax = True
                t0_relax = tt

            pylab.plot(pp, ll , 'x')
            pylab.axis('tight')
            pylab.draw()

            time.sleep(0.1)

        except KeyboardInterrupt:

            print "STOPPING ACQUISITION + MOTOR, CLOSING THE FILE..."
            bulky.Stop()
            f.close()
            finished = True
    f.close()
    print('finished, closing the file')


def read_and_write_data(f, t0, p0):
    if loadcell.state() == PyTango._PyTango.DevState.STANDBY:
        loadcell.start()
        time.sleep(0.05)
        
    # read data
    tt = time.time()-t0
    pp = bulky.position -p0
    ll = loadcell.averagechannel2
    f.write('%f, %f, %f\n' % (tt,pp,ll))
    return tt,pp,ll
