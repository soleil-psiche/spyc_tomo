# -*- coding: utf-8 -*-

# this is supposed to be the definite bulky control function
# integrates - more or less - with mt synchro functions

# needs to be turned into a single class, probably

from __future__ import division, print_function, unicode_literals
import time
import os
import sys
import socket
import json
import PyTango
import codecs

# thread for logging data for the current sample
# Data_Acquisition class object
from threading import Thread
class Data_Acquisition(Thread):

    def __init__(self, sample_name, dt=0.25):
        self.dt = dt
        self.sample_name = sample_name
        self.data_acquisition = True
        #self.basler_acquisition = True
        Thread.__init__(self)

    def run(self):
        print('*** out_directory = %s' % spool_directory)
        filename = os.path.join(spool_directory, self.sample_name + '_data.log')
        log("Acquisition started on " + filename)
        with open(filename, 'a') as data_file:
            i=0
            while self.data_acquisition:
                data = get_log_data()
                data_file.write(('{} '*len(data)).format(*data) + '\n')
                time.sleep(self.dt)
                i+=1
        log("Acquisition stopped " + filename)


# hard coded parameters
v_rapide = 10e-3
v_lent = .3e-3
gain_500lbs = 220  # N/V
failure_threshold = 50
delta_total = 1
#%%
# get the required devices - hard coded for the moment
mot = PyTango.DeviceProxy('i03-c-c05/ex/bulky.1')
_min_speed = 5.229e-5
sai = PyTango.DeviceProxy('i03-c-c00/ca/sai.1')
sai_channel = 2
ptzDP = PyTango.DeviceProxy('i03-c-c05/ex/prs.1-mt_tz.1')
#%%


#def setup_bulky_directories():
ruche_directory = mt.reconstruction_directory
spool_directory = os.path.join(mt.spool_directory, 'bulky_dct')
if not os.path.exists(spool_directory):
    os.mkdir(spool_directory)
    # create a default config file in this directory
    config = {'delta':1e-2, 'velocity':2e-4, 'do_ref':False}
    config_file = os.path.join(spool_directory, 'config_tmp.json')
    with open(config_file, 'w') as f:
        json.dump(config, f)
        print('created the config_tmp.json file in %s' % spool_directory)


def new_sample(sample_name):
    global logfile, data_log_file, Acq
    try:
        if Acq.is_alive():
            print("Attention, une acquisition est toujours en cours !")
            rep = raw_input("Voulez-vous l'interrompre ? [Y]")
            if rep.lower() in ['', 'y', 'o', 'yes', 'oui']:
                print("Tentative d'arrêt de la précédente acquisition")
                Acq.data_acquisition = False
    except:
        pass
    # log verbose output
    logfile = codecs.open(os.path.join(spool_directory, mt.scanname + '.log'), 'a', 'utf8')
    log('{} projections, {} references'.format(mt.nproj, mt.nref))
    log("Exposure time (ms) {}".format(mt.proxy.exposuretime))
    # log data (machine readable)
    data_log_file = open(os.path.join(spool_directory,  mt.scanname + '_log.dat'), 'a')
    data_log_file.write(get_log_data(header=True) + '\n')
    data = get_log_data()
    data_log_file.write(('{} '*len(data)).format(*data) + '\n')
    data_log_file.flush()
    log("Load cell gain : {} N/V".format(gain_500lbs))
    Acq = Data_Acquisition(sample_name)
    Acq.start()


def end_sample():
    Acq.data_acquisition = False
    time.sleep(.5)
    logfile.close()
    data_log_file.close()
    print("\033[34;1mFermeture des fichiers log.\033[0m")

def close_log_files():
    Acq.data_acquisition = False
    time.sleep(.5)
    logfile.close()
    data_log_file.close()
    print("\033[34;1mFermeture des fichiers log.\033[0m")


# functions for writing to logs
def get_log_data(header=False):
    if header:
        return "#time number displacement force speed"
    else:
        return [time.time(), mot.position, charge(sai), mot.velocity]

def log(text):
    print(text)
    logfile.write('{} -> {}\n'.format(time.strftime('%F %H:%M:%S') + '{:.3f}'.format(time.time()%1)[1:], text))
    logfile.flush()

def charge(sai=sai, channel=sai_channel, gain=gain_500lbs, zero=-1.07):
    return (getattr(sai, 'averagechannel' + str(channel)) - zero)*gain

def log_data():
    #data = [time.time(), i_counter, mot.position, charge(sai), mot.velocity]
    #data = [time.time(), i_counter, 0, charge(), 1]
    data = get_log_data()
    data_log_file.write(('{} '*len(data)).format(*data) + '\n')
    data_log_file.flush()
    #print(data)

def Load(force, dt=.1, delta=delta_total, mode='abs', velocity=v_rapide, verbose=True):
    v0 = mot.velocity
    mot.velocity = velocity
    p0 = mot.position
    c0 = charge()
    delta = np.abs(delta) # absolute value
    if mode.lower().startswith('rel'):
        force += c0
        positive = True if force>0 else False
    elif mode.lower().startswith('abs'):
        positive = True if force>c0 else False
    else:
        print('Argument mode invalide !')
        return
    if verbose:
        log("Début du chargement jusqu'à {} N à {} mm/s".format(force, mot.velocity))
    try:
        mot.stop()
        if positive:
            mot.position += delta
            while charge() < force:
                time.sleep(dt)
        else:
            mot.position -= delta
            while (charge() > force) and ((p0-mot.position)<delta_total):
                time.sleep(dt)
        mot.stop()
    except BaseException as e:
        mot.stop()
        log('Erreur: Interruption inopinée de la boucle')
        if verbose:
            log('Position initiale : {} mm'.format(p0))
            log('Position actuelle : {} mm'.format(mot.position))
            log('Charge initiale : {} N'.format(c0))
            log('Charge actuelle : {} N'.format(charge()))
            log(e.__repr__())
        raise e
    finally:
        mot.velocity = v0
