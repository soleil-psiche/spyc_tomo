# -*- coding: utf-8 -*-

# this is supposed to be the definite bulky control function
# integrates - more or less - with mt synchro functions

# needs to be turned into a single class, probably

from __future__ import division, print_function, unicode_literals
import time
import os
import sys
import socket
import json
import PyTango
import codecs

# psiche
import updateon

# thread for logging data for the current sample
# Data_Acquisition class object
from threading import Thread
class Data_Acquisition(Thread):

    def __init__(self, sample_name, dt=0.25):
        self.dt = dt
        self.sample_name = sample_name
        self.data_acquisition = True
        #self.basler_acquisition = True
        Thread.__init__(self)

    def run(self):
        print('*** out_directory = %s' % spool_directory)
        filename = os.path.join(spool_directory, self.sample_name + '_data.log')
        log("Acquisition started on " + filename)
        with open(filename, 'a') as data_file:
            i=0
            while self.data_acquisition:
                data = get_log_data()
                data_file.write(('{} '*len(data)).format(*data) + '\n')
                time.sleep(self.dt)
                i+=1
        log("Acquisition stopped " + filename)


# hard coded parameters
v_rapide = 10e-3
v_lent = .3e-3
gain_1000lbs = 440
gain_500lbs = 220  # N/V
failure_threshold = 50
delta_total = 1
bulky_measurement_position = '' #u'amove dts1 70' #
bulky_intervention_position = '' # u'amove dts1 100 tomorz 0' #
updateon_hutch = 'OH'
updateon_camera = 3
#%%
# get the required devices - hard coded for the moment
mot = PyTango.DeviceProxy('i03-c-c05/ex/tomo.1-mt_furnacetz.1')
_min_speed = 5.229e-5
sai = PyTango.DeviceProxy('i03-c-c00/ca/sai.1')
sai_channel = 1
ptzDP = PyTango.DeviceProxy('i03-c-c05/ex/tomo.1-mt_tz.1')
#%%


#def setup_bulky_directories():
ruche_directory = mt.reconstruction_directory
spool_directory = os.path.join(mt.spool_directory, 'bulky')
config_file = os.path.join(spool_directory, 'config_tmp.json')
if not os.path.exists(spool_directory):
    os.mkdir(spool_directory)
if not os.path.exists(config_file):
    # create a default config file in this directory
    config = {'delta':1e-2, 'velocity':2e-4, 'do_ref':False}
    with open(config_file, 'w') as f:
        json.dump(config, f)
        print('created the config_tmp.json file in %s' % spool_directory)


''' # only for CdM?
try:
    with open('xlab/config_xlab.json') as f:
        config = json.load(f)
    host = socket.gethostname()
    flag = False
    for k,v in config.items():
        if host.startswith(k):
            load_config(v)
            flag = True
            break
except IOError as e:
    print("\033[31;1mImpossible de lire le fichier de configuration\033[0m")
    print(e)
# Ajouter une configuration par défaut
# if not flag:
#     load_config(defaut)

def load_config():
    pass
'''


def new_sample(sample_name):
    global logfile, data_log_file, _tomo_counter, Acq
    try:
        if Acq.is_alive():
            print("Attention, une acquisition est toujours en cours !")
            rep = raw_input("Voulez-vous l'interrompre ? [Y]")
            if rep.lower() in ['', 'y', 'o', 'yes', 'oui']:
                print("Tentative d'arrêt de la précédente acquisition")
                Acq.data_acquisition = False
    except:
        pass
    # bring back the detector if needed
    if bulky_measurement_position:
        get_ipython().magic(bulky_measurement_position)
    # set scan parameters + start synchro rotation
    mt.scanname = sample_name
    mt.setScanParameters()
    # log verbose output
    logfile = codecs.open(os.path.join(spool_directory, mt.scanname + '.log'), 'a', 'utf8')
    log('{} projections, {} references'.format(mt.nproj, mt.nref))
    log("Exposure time (ms) {}".format(mt.proxy.exposuretime))
    # log data (machine readable)
    _tomo_counter = 0
    data_log_file = open(os.path.join(spool_directory,  mt.scanname + '_log.dat'), 'a')
    data_log_file.write(get_log_data(header=True) + '\n')
    data = get_log_data()
    data_log_file.write(('{} '*len(data)).format(*data) + '\n')
    data_log_file.flush()
    log("Load cell gain : {} N/V".format(gain_1000lbs))
    updateon.updateon(updateon_hutch, updateon_camera)
    Acq = Data_Acquisition(sample_name)
    Acq.start()


def end_sample():
    if ptomorz.state is PyTango._PyTango.DevState.MOVING:
        mt.synchroFinalRefs()
    updateon.updateon(updateon_hutch, updateon_camera)
    if bulky_intervention_position:
        get_ipython().magic(bulky_intervention_position)
    Acq.data_acquisition = False
    time.sleep(.5)
    logfile.close()
    data_log_file.close()
    print("\033[34;1mFermeture des fichiers log.\033[0m")

def close_log_files():
    Acq.data_acquisition = False
    time.sleep(.5)
    logfile.close()
    data_log_file.close()
    print("\033[34;1mFermeture des fichiers log.\033[0m")
    #updateon.updateon('OH', 4)


# functions for writing to logs
def get_log_data(header=False):
    if header:
        return "#time number displacement force speed"
    else:
        return [time.time(), _tomo_counter, mot.position, charge(sai), mot.velocity]

def log(text):
    print(text)
    logfile.write('{} -> {}\n'.format(time.strftime('%F %H:%M:%S') + '{:.3f}'.format(time.time()%1)[1:], text))
    logfile.flush()

def charge(sai=sai, channel=sai_channel, gain=gain_1000lbs):
    return getattr(sai, 'averagechannel' + str(channel))*gain

def log_data():
    #data = [time.time(), i_counter, mot.position, charge(sai), mot.velocity]
    #data = [time.time(), i_counter, 0, charge(), 1]
    data = get_log_data()
    data_log_file.write(('{} '*len(data)).format(*data) + '\n')
    data_log_file.flush()
    #print(data)

def Load(force, dt=.1, delta=delta_total, mode='abs', velocity=v_rapide, verbose=True):
    v0 = mot.velocity
    mot.velocity = velocity
    p0 = mot.position
    c0 = charge()
    delta = np.abs(delta) # absolute value
    if mode.lower().startswith('rel'):
        force += c0
        positive = True if force>0 else False
    elif mode.lower().startswith('abs'):
        positive = True if force>c0 else False
    else:
        print('Argument mode invalide !')
        return
    if verbose:
        log("Début du chargement jusqu'à {} N à {} mm/s".format(force, mot.velocity))
    try:
        mot.stop()
        if positive:
            mot.position += delta
            while charge() < force:
                time.sleep(dt)
        else:
            mot.position -= delta
            while (charge() > force) and ((p0-mot.position)<delta_total):
                time.sleep(dt)
        mot.stop()
    except BaseException as e:
        mot.stop()
        log('Erreur: Interruption inopinée de la boucle')
        if verbose:
            log('Position initiale : {} mm'.format(p0))
            log('Position actuelle : {} mm'.format(mot.position))
            log('Charge initiale : {} N'.format(c0))
            log('Charge actuelle : {} N'.format(charge()))
            log(e.__repr__())
        raise e
    finally:
        mot.velocity = v0

#def initial_scan():
#    mt.synchroStartRotation()
#    mt.synchroSnap()
#    if tomorz.state is PyTango._PyTango.DevState.MOVING:
#        mt.synchroFinalRefs()
def do_a_tomo(bulkyinfo=None):
    global _tomo_counter
    extra_log_values()
    if mt.dimax_n_tomos > 1:
        log('Start {} tomograms at {} seconds separation'.format(mt.dimax_n_tomos,mt.dimax_interval_tomos))
        log('Start tomos n°{} to n°{}'.format(_tomo_counter, _tomo_counter+mt.dimax_n_tomos))

    else:
        log('Début scan tomographie n°{}'.format(_tomo_counter))
    log_data()
    mt.synchroSnap(bulkyinfo=bulkyinfo) # for fast synchro sequence
    log_data()
    log(get_log_data())
    log('Fin scan tomographie n°{} (initial)'.format(_tomo_counter))
    _tomo_counter += mt.dimax_n_tomos


def Load_step(delta, mode, dt=.1, velocity=v_rapide, tomo=True, tomo_wait=0, verbose=True):
    global _tomo_counter

    if mode=='load':
        # use the Load helper function
        Load(delta, dt=.1, delta=delta_total, mode='abs', velocity=velocity, verbose=True)
    elif mode=='displacement':
        # recreate _dmove helper function
        try:
            mot.position += delta
            while mot.state() == PyTango._PyTango.DevState.MOVING:
                time.sleep(dt)
            print('Info : [dmove] fin du mouvement')
            print("Load : {:.2f} N".format(charge()))
        except:
            print('Erreur : [dmove] interruption moteur')
        finally:
            mot.stop()
    else:
        print('mod must be load or displacement !')
        return
    # AFTER THE INCREMENT, DO A TOMO
    if tomo:
        time.sleep(tomo_wait)
        do_a_tomo()
    else:
        print('not doing a tomo')


def extra_log_values():
    log("ptomotx {} mm ; ptomots {} mm ; ptomotz {} mm".format(ptomotx.value, ptomots.value, ptomotz.value))

def Loop(delta_total=delta_total, failure=failure_threshold, dt=.1, restart=False, verbose=True):
    global _tomo_counter
    if not restart:
        mt.synchroStartRotation()
    v0 = mot.velocity
    with open(config_file, 'r') as f:
        cfg = json.load(f)
    mot.velocity = cfg['velocity']
    p00 = mot.position
    c0 = charge()
    ptz0 = ptzDP.position
    try:
        mot.position += delta_total
        i = 0
        _tomo_counter += 1
        if not restart:
            extra_log_values()
            log('Début scan tomographie n°{}'.format(i))
            p1 = mot.position
            log_data()
            mt.synchroSnap() # for fast synchro sequence
            log_data()
            log('Fin scan tomographie n°{} (initial)'.format(i))
        while charge() > failure:
            i = _tomo_counter
            try:
                with open(config_file, 'r') as f:
                    cfg = json.load(f)
            except:
                print("\033[31;1mErreur de lecture du fichier JSON\033[0m")
                print("Conserve les précédentes valeurs : {}".format(cfg))
            delta = cfg['delta']
            if cfg.get('do_ref', False):
                mt.synchroTakeRefs()
            if cfg['velocity']<_min_speed:
                print("\033[31;1mLa vitesse demandée n'est pas réalisable avec le moteur de bulky\033[0m")
            mot.velocity = max(cfg['velocity'], _min_speed)
            p0 = mot.position
            log("Début de l'incrément de chargement n°{} de {} mm à {} mm/s".format(i+1, delta, mot.velocity))
            # ptz correction
            if cfg.get('correct_ptz', False):
                deltaptz = max(0, delta/2)
                ptzDP.position += deltaptz
                log("Correct vertical position {} mm".format(deltaptz))
                while ptzDP.state() is not PyTango._PyTango.DevState.STANDBY:
                    time.sleep(0.1)

            while (mot.position - p0) < delta:
                time.sleep(dt)
                print('\r{:8.5f} < {}'.format((mot.position - p0), delta), end='')
                sys.stdout.flush() #
            print('\n############')
            _tomo_counter += 1
            log('Début scan tomographie n°{}'.format(i))
            extra_log_values()
            p1 = mot.position
            log_data()
            mt.synchroSnap() # for fast synchro sequence
            log_data()
            time.sleep(1)
            if mt.proxy.state() is PyTango._PyTango.DevState.FAULT: #not PyTango._PyTango.DevState.STANDBY:
                mt.proxy.init()
                log("Erreur détecteur -- reinitialisation")
                print("\033[31;1mErreur détecteur -- reinitialisation\033[0m")
                log("Début scan de substitution")
                _tomo_counter += 1
                log_data()
                mt.synchroSnap()
                log_data()
                log("Fin scan de substitution")
            log('Fin scan tomographie n°{}, increment {} mm'.format(i, mot.position - p1))
            print("Charge cellule : {:.2f} N".format(charge()))
            print(i,delta) #
            print('\n############')
            sys.stdout.flush() #
        mot.stop()
        log('Arrêt moteur')
    except BaseException as e:
        mot.stop()
        ptzDP.stop()
        log('\033[31;1mErreur: Interruption inopinée de la boucle\033[0m')
        if verbose:
            log('Position initiale : {} mm'.format(p00))
            log('Position actuelle : {} mm'.format(mot.position))
            log('Charge initiale : {} N'.format(c0))
            log('Charge actuelle : {} N'.format(charge()))
            log(e.__repr__())
        raise e
    finally:
        mot.velocity = v0




def Dimax_Loop(delta_total=delta_total, velocity=0.001, failure=failure_threshold, dt=.1, verbose=True):
    v0 = mot.velocity
    mot.velocity = velocity
    p00 = mot.position
    c0 = charge()
    mt.fastshutteropen()
    try:
        mot.position += delta_total
        extra_log_values()
        log('start dimax series of tomos in ring buffer')
        log_data()
        while np.abs(charge()) > np.abs(failure):
            print('load is {} N'.format(charge()))
            time.sleep(dt)
        print('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n')
        print('\n\n%%%%%        FAILURE DETECTED !!!!       %%%%%%\n\n')
        print('\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n')
        time.sleep(3)
    except BaseException as e:
        mot.stop()
        log('\033[31;1mErreur: Interruption inopinée de la boucle\033[0m')
        if verbose:
            log('Position initiale : {} mm'.format(p00))
            log('Position actuelle : {} mm'.format(mot.position))
            log('Charge initiale : {} N'.format(c0))
            log('Charge actuelle : {} N'.format(charge()))
            log(e.__repr__())
        raise e
    finally:
        mt.fastshutterclose()
        mot.stop()
        mot.velocity = v0







# helper functions for moving things

def set_ref(value=None):
    global charge_0
    if value is None:
        value = charge()
    charge_0 = value

set_ref()

dead_time = .1
def _load(force_max, force_0 = charge_0):
    try:
        mot.position += delta_inc
        print(force_0)
        while capteur() < (force_0 + force_max):
            time.sleep(dead_time)
    except:
        print('Erreur\037: [load] interruption moteur')
    finally:
        mot.stop()

def _dmove(delta):
    try:
        mot.position += delta
        while mot.state() == PyTango._PyTango.DevState.MOVING:
            time.sleep(dead_time)
        print('Info : [dmove] fin du mouvement')
        print("Load : {:.2f} N".format(charge()))
    except:
        print('Erreur : [dmove] interruption moteur')
    finally:
        mot.stop()
        mt.snap()

def _amove(position):
    try:
        mot.position = position
        while mot.state() == PyTango._PyTango.DevState.MOVING:
            time.sleep(dead_time)
        print('Info\037: [amove] fin du mouvement')
    except:
        print('Erreur\037: [amove] interruption moteur')
    finally:
        mot.stop()

def _unload(force_min, force_0=charge_0):
    try:
        mot.position += - delta_inc
        while capteur() > (force_0 + force_min):
            time.sleep(dead_time)
    except:
        print('Erreur\037: [unload] interruption moteur')
    finally:
        mot.stop()
