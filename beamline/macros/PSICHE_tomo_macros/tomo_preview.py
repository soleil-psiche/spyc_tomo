

# do a fast, stitched, preview of a sample using binning
# this needs two parts - an acquisition part and a reconstruction part
# this the acquisition

import os
import PyTango
import numpy as np

# spyc for motors
from spyc.core import DeviceManager
dm = DeviceManager()

# attribute proxy for position of tomorefx
tomorefxAP = PyTango.AttributeProxy('I03-C-C07/EX/TOMO.2-MT_Tx.2/position')

def tomo_preview(mt, nameroot, tomorefxsteps=3, binning=4):

    # make a folder for the results
    spooldir = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/'
    previewdir = spooldir + nameroot + '_preview'
    if os.path.isdir(previewdir):
        print "This name (%s) already exists. Please choose another... " % nameroot
        return
    else:
        os.mkdir(previewdir)

    # initial setup for the camera
    mt.setSingleMode()
    mt.setBinning(1) # make sure start with no binning

    # initial position refx
    tomorefx_initial = tomorefxAP.read().value * 1.
    exptime_initial = mt.proxy.exposuretime * 1.
    pixelsize_initial = mt.pixelsize * 1.

    # scan positions - field of view in x
    roiwidth = mt.proxy.roiwidth * 1.
    # 10% overlap, whole number pixels
    xstep = np.round(0.9 * roiwidth) * mt.pixelsize / 1000. # mm
    steps = np.arange(tomorefxsteps)
    steps = steps - steps.mean() # centre the range
    tomorefx_positions = tomorefx_initial + (steps*xstep)

    # setup binning
    mt.setBinning(binning)
    binnedpix = pixelsize_initial * binning
    mt.pixelsize = binnedpix
    binnedexptime = exptime_initial / (binning**2)
    mt.setExpTime(binnedexptime/1000.)
    
    # set scan parameters
    nproj = int(np.round(roiwidth * tomorefxsteps * 0.75 / binning))
    mt.nproj = nproj
    mt.scanrange = 180
    mt.nref = 0
    mt.ndark = 0
    mt.calib_images = 0
    mt.setFlyScanParameters()

    # do the scans
    get_ipython().magic(mt.fast_shutter_open_command)
    for ii, refxpos in enumerate(tomorefx_positions):
        print "step %d..." % (ii+1)
        mt.scanname = nameroot + "_preview_%d" % ii
        get_ipython().magic('amove tomorefx %f' % refxpos)
        if ii == (tomorefxsteps-1):
            print "final step - do references"
            mt.nref = 11
            mt.ndark = 11
            mt.setFlyScanParameters()
        mt.doTomo()
        # move the finished scan to the common directory        
        os.system("mv %s%s %s" % (spooldir, mt.savename, previewdir)) 
        
    # after - reset initial things
    print "return to initial position"
    get_ipython().magic('amove tomorefx %f' % tomorefx_initial)
    print "reset binning, pixelsize, and exposure time"
    mt.setBinning(1) # make sure finish with no binning
    mt.setExpTime(exptime_initial/1000.)
    mt.pixelsize = pixelsize_initial

