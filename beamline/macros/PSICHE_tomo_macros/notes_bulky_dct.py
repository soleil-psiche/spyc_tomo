# -*- coding: utf-8 -*-
sample = 'ET11-2'
for i in range(3):
    mt.scanname = '%s_dct_2_z%d_' % (sample, i)
    print('next scan will be %s' % mt.scanname)
    %dmove mtomotz 0.3
    mt.doTomo()

sample = 'ET8-6'
%amove mtomotz 23.75
mt.scanname = '%s_dct_top_' % sample
%doTomo
%amove mtomotz 24.00
mt.scanname = '%s_dct_cen_' % sample
%doTomo
%amove mtomotz 24.25
mt.scanname = '%s_dct_bot_' % sample
%doTomo

%amove mtomotz 24.00
%amove s2vg 0.2
%dmove dts2 4  # go to 12 mm
mt.scanname = '%s_dct_cen2_' % sample
%doTomo

# thursday night dct scan series at 12 mm with 0.2 mm vertical opening
%amove mtomotz 23.37
for i in range(6):
    mt.scanname = '%s_dct_0_z%d_' % (sample, i)
    print('next scan will be %s' % mt.scanname)
    %dmove mtomotz 0.18
    mt.doTomo()

# moving to high resolution detector
# new sample ET11-4 600 um cross section
%amove mtomotz 24.00
%amove s2vg 0.3
%dmove dts2 ??
mt.scanname = 'ET11-4_dct_0_cen_'
%doTomo  # scan friday lunch

# install Perkin-Elmer at d=1m
setScanParameters for dct scan
fastshutterclose()
4000 images
snap
%doTomo

# 600 microns cross section sample ET11-4
ET11_4_
sample = 'ET11_4'
# friday night: high res dct scan series at ~9 mm with 0.2 mm vertical opening
%amove mtomotz 23.37
%amove s2vg 0.2
for i in range(6):
    mt.scanname = '%s_dct_2_z%d_' % (sample, i)
    print('next scan will be %s' % mt.scanname)
    %dmove mtomotz 0.18
    mt.doTomo()
%shclose


# Ti7Al
sample = 't9'
%amove mtomotz 20.2
mt.scanname = '%s_dct_top_' % sample
%doTomo
%amove mtomotz 19.75
mt.scanname = '%s_dct_cen_' % sample
%doTomo
%amove mtomotz 19.3
mt.scanname = '%s_dct_bot_' % sample
%doTomo

# 800 microns cross section sample ET11-3
sample = 'ET11_3'
# friday night: high res dct scan series at ~9 mm with 0.2 mm vertical opening
%amove mtomotz 16.75
%amove s2vg 0.12
for i in range(8):
    mt.scanname = '%s_dct_0_z%d_' % (sample, i)
    print('next scan will be %s' % mt.scanname)
    %dmove mtomotz 0.1  # should have been -.1 !
    mt.doTomo()
%shclose

# redo scan z6 as flyscan died
%amove mtomotz 17.45
mt.scanname = '%s_dct_0_z6_' % sample
print('next scan will be %s' % mt.scanname)
mt.doTomo()
%amove mtomotz 16.75
mt.scanname = '%s_dct_0_z8_' % sample  # %setScanParameters
print('next scan will be %s' % mt.scanname)
mt.doTomo()
%amove mtomotz 16.65
mt.scanname = '%s_dct_0_z9_' % sample
print('next scan will be %s' % mt.scanname)
mt.doTomo()

# in situ sample ET11-5
sample = 'ET11_5'
%amove mtomotz 23.75
mt.scanname = '%s_dct_0_top_' % sample
%doTomo
%amove mtomotz 24.00
mt.scanname = '%s_dct_0_cen_' % sample
%doTomo
%amove mtomotz 24.25
mt.scanname = '%s_dct_0_bot_' % sample
%doTomo

# load 3: cen NF + FF mtomotz 24.07
dmove mtomotz -0.25
mt.scanname = '%s_dct_3_top_' % sample
%doTomo
%dmove mtomotz 0.5
mt.scanname = '%s_dct_3_bot_' % sample
%doTomo
%shclose