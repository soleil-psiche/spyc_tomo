# microstep switch set to A (1/40) so motor speed is /4
#loads = [-20,-20,-20,-20,-20,-20,-20,-20]
#suffixs = 'abcdefgh' # scan at 'g' around 100 N (zero is around 18 N)

#loads = [-50,-40,-30,-20,-20,-10,-10,-10,-10,-10,-10,-10,-10,-10,-10,-10,-10,-10]
#suffixs = 'ijklmnopqrstuvwxyz'

mt.refDisplacement = -12
tension.speed = 1.0
# adjust parameters
sample = 'PA66GF30_trans3'
loads = [-15,-15,-15,-15,-15,-15]
suffixs = 'abcdef'

for i, load in enumerate(loads):
    mt.scanname = '%s_a%c_' % (sample, suffixs[i])
    print('next scan will be %s' % mt.scanname)
    Henry_traction_machine.measure(loads[i], '%s.log' % mt.scanname, relax_time=180)
    mt.doTomo()
