# preload 'a'
# load 'bcde' with [-10,-10,-10,-10]
mt.refDisplacement = -12
tension.speed=0.58
radio_dir = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/radios'
# adjust parameters
sample = 'Comp0_In_R1'
loads = [-5,-5,-5] # broken just at the begining...
suffixs = 'fgh'
ptz_scan = 17.1
ptz_ball1 = 15.6
ptz_ball2 = 18.6

for i, load in enumerate(loads):
    mt.scanname = '%s_%c_' % (sample, suffixs[i])
    print('next scan will be %s' % mt.scanname)
    Henry_traction_machine.measure(loads[i], '%s.log' % mt.scanname)
    print('taking radiograph of the lead balls after loading')
    %amove ptz ptz_ball1
    mt.autoSaveOn()
    prefix = '%s_ball' % mt.scanname
    ndx = 1
    mt.setupSaving(radio_dir, prefix, ndx)
    mt.snap()
    %amove ptz ptz_ball2
    mt.snap()
    mt.autoSaveOff()
    %amove ptz ptz_scan
    mt.doTomo()
