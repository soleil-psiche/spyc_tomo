import time
from spyc.core import DeviceManager as dm

loadcell = PyTango.DeviceProxy('i03-c-c00/ca/sai.1')


        get_ipython().magic(u'setlimits %s None None' % self.focusMotor)

def NewSample(s_name, wait=15):
    global abs_displacement, sample_name, wait_period
    abs_displacement = 0
    sample_name = s_name
    wait_period = wait
    %amove tomorz 0
    %setspeed bulky 0.002

def InitScan():
    mt.scanname = sample_name+'_0mm'
    mt.doTomo()
    mt.doTomo()

def NewStep(step, repeat=0):
    global abs_displacement,sample_name,wait_period
    #step = input('Displacement step (mm) (default 0.02): ')
    abs_displacement = abs_displacement + step
    mt.scanname = sample_name+'_%smm' % abs_displacement
    Henry_traction_machine.measure(step, sample_name+'.log', wait_period)
    mt.doTomo()
    if repeat!=0:
        mt.doTomo()
    print('Load after scan', round(loadcell.averagechannel2,3))

def EndSample(zero=0.15):
    global zero
    %feclose
    %setspeed bulky 0.1
    %amove bulky zero
    %dmove tomorz 180

def AlignC():
    global tmp
    mt.startLive()
    mt.fastshutteropen()
    time.sleep(0.1)
    %dmove tomorz 90
    tmp = 1
    while tmp!=0:
        tmp = float(input('Ok? [0/tomots]'))
        %dmove tomots tmp
    tmp = 1
    %dmove tomorz -90
    while tmp!=0:
        tmp = float(input('Ok? [0/tomotx]'))
        %dmove tomotx tmp

def Auto(step=0.02, repeat=0):
    global loadcell
    loadcell = PyTango.DeviceProxy('i03-c-c00/ca/sai.1')
    if repeat==0:
        while loadcell.averagechannel2 > 0.2:
            AlignC()
            NewStep(step)
        tmp = input('End Sample? [y/n]')
        if tmp=='y'
            EndSample()
        else:
            return 
    else:
        for i in range(repeat):
            AlignC()
            NewStep(step)

def SetZero():
    b = dm.actors['bulky']
    val = b.value
    bm, bM = b.limits
    bm = float(bm)
    bM = float(bM)
    bm, bM = bm+val, bM+val
    b.set_limits([bm,bM])
    
def test():
    rz = dm.actuators['tomorz']
    rz.value = 180
    #%dmove tomorz 180
