# microstep switch set to A (1/40) so motor speed is /4
#loads = [-20,-20,-20,-20,-20,-20,-20]
#suffixs = 'abcdefg' # scan at 'g' around 45 N (zero is around 18 N)

# loads (with scans)
#loads = [-10,-10,-10,-10,-10,-10,-20]
#suffixs = 'hijklmno'

mt.refDisplacement = -12
tension.speed = 1.0
# adjust parameters
sample = 'PA11_petm4_in'
loads = [-10,-10,-10,-10,-10,-10,-20,-20,-50,-50,-50]
suffixs = 'pqrstuvwxyz'

for i, load in enumerate(loads):
    mt.scanname = '%s_%c_' % (sample, suffixs[i])
    print('next scan will be %s' % mt.scanname)
    Henry_traction_machine.measure(loads[i], '%s.log' % mt.scanname, relax_time=300)
    mt.doTomo()
