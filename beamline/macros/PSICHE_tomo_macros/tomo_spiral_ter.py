# -*- coding: utf-8 -*-
# spiral acquisition macro

import os
import PyTango
import numpy as np

# spyc for motors
from spyc.core import DeviceManager
dm = DeviceManager()

def tomo_spiral(mt, nameroot, zstart=None, zend=None, overlapmin=0.1, motor='ptz', fast=False):

    # could add some extra info to mt for spiral scans - ref interval, sense, etc
    # easiest will be refs on first scan and last scan
    # change syntax to start/end position plus number of scans 
    pixelsize = mt.pixelsize * 1.    

    # maximum step size
    maxstep = mt.proxy.roiHeight * (1. - overlapmin)
    maxstepmm = pixelsize * maxstep / 1000 # mm

    # deal with scan limits
    if (zstart==None) & (zend==None):
        print "Need to supply at least the start or end tomotz position !"
        print "quitting..."
        return

    # make a folder for the results
    spooldir = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/'
    spiraldir = spooldir + nameroot + '_spiral'
    if os.path.isdir(spiraldir):
        print "This name (%s) already exists. Please choose another... " % nameroot
        return
    else:
        os.mkdir(spiraldir)

    # if we have a start and an end
    if (zstart!=None) & (zend!=None):
        if zstart > zend:
            zstart,zend = zend,zstart
            
    displacement = zend - zstart
    nsteps = 0
    zstep = displacement / nsteps
    zsteppix = np.round(1000 * zstep / pixelsize)
    zstep = zsteppix * pixelsize / 1000
    
    print "will cover %0.2f to %0.2f in %d steps (%d scans)" % (zstart, zend, nsteps, (nsteps+1))
    print "step size is %0.2f mm  /  %0.2f  pixels" % (zstep, zsteppix)
    zpos = np.arange(zstart, zstart+(nsteps*zstep)+0.0001, zstep)
    
    mynref = mt.nref * 1.    
    myndark = mt.ndark * 1.
    for ii, myzpos in enumerate(zpos):

        # treat flyscan parameters
        if fast:
            update=False
            if ii == 0:
                # first scan - refs, but no darks
                mt.nref = int(mynref * 1.)
                mt.ndark = int(0)
                update=True
            if ii == 1:
                # first scan - refs, but no darks
                mt.nref = int(0)
                mt.ndark = int(0)
                update=True
            if ii==(len(zpos)-1):
                # last scan - refs and darks
                mt.nref = int(mynref * 1.)
                mt.ndark = int(myndark * 1.)             
                update=True
            if update:
                mt.setFlyScanParameters()

        # move in z
        get_ipython().magic('amove %s %f' % (motor, myzpos))
        # fix the scanname
        mt.scanname = nameroot + "_spiral_%02d" % ii
        # do the scan
        mt.doTomo()
        # copy the scan to the spiral folder
        os.system("mv %s%s %s" % (spooldir, mt.savename, spiraldir)) 

    
