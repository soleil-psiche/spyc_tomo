 mt = Tomo.Tomo()

 mt.setOptic()
select optic [1x/2.5x/5x/7.5x/10x/20x] :  1x
select scintillator type [simple/tiltable] :  simple
Safe to move to the nominal focus/zoom position? yes/[no] :  yes


mt.changeOptic()

atk mt


mt.startLive()

# roughly focused 1x..
tomofocus   49.4060     mm        [15.0000, 69.1000]
tomozoom    82.10       mm        [60.40, 180.00]   

# focused 5x 
tomofocus   65.0740    mm     [57.0000, 96.0000]   STANDBY
tomozoom    167.00     mm     [135.92, 183.00]     STANDBY
pixel size 1.2771 from needle.

# focused 7.5x
tomofocus   65.4620    mm     [59.1000, 96.0000]   STANDBY
tomozoom    167.00     mm     [136.32, 183.00]     STANDBY
pixel size 0.853 from needle.

# composite movement of table and detector
tomotablez XXXX


mt.setScanParameters()

####################################################

# two scans in sequence
# prefix = "CDIT4"
# stop the camera!
mt.stopLive()
# detector at good distance
amove dts2 35
# first scan - bottom of sample
mt.scanname = "CDIT4_bot_10"
mt.FlyScan()
# move table downwards
# tomotablez -1
# second scan - top of sample
# mt.scanname = "CDIT4_top_9"
# mt.FlyScan()
# detector at safe distance
# amove dts2 285
# move table upwards
# tomotablez 1

####################################################

Pour refaire le CDIT4_top_5 car image sombre :

# stop the camera!
mt.stopLive()
# detector at good distance
amove dts2 35
#second scan - top of sample
mt.scanname = "CDIT4_top_5_bis"
mt.FlyScan()
# detector at safe distance
# amove dts2 285
# move table upwards
tomotablez 1.2

############################

Correction de position de caméra 22h30 :
Spyc (MonoTomo) [616]: dmove dtx2 0.177
start positions:
        dtx2: -145.46 mm [-500.00, 50.00]
dtx2: -145.28 STANDBY


# sequence
mt.scanname = "CDI_3_top_seriesB_6"
mt.FlyScan()
dmove tomotz 1.4
mt.scanname = "CDI_3_bot_seriesB_6"
mt.FlyScan()
dmove tomotz 1.4
mt.scanname = "CDI_3_base_seriesB_6"
mt.FlyScan()
dmove tomotz -2.8
%shclose
tomotablez
# sequence
for ii in range(10):
  mt.scanname = "CDE_2_top"
  mt.FlyScan()

for ii in range(5):
  mt.scanname = "CDI_3_top_seriesB_5"
  mt.FlyScan()
  %dmove tomotz 1.4
  mt.scanname = "CDI_3_bot_seriesB_5"
  mt.FlyScan()
  %dmove tomotz -1.4
  mt.scanname = "CDI_3_base_seriesB_5"
  mt.FlyScan()
  %dmove tomotz -1.4

Spyc (MonoTomo) [67]: wm tomorefx
----------------------------------------------------------------
  Name       Position   Unit   Limits                  State
----------------------------------------------------------------
  tomorefx   -0.02860   mm     [-30.00000, 10.00000]   STANDBY
----------------------------------------------------------------



# movie loop
# make a new directory:
! mkdir /nfs/ruche-psiche/psiche-soleil/com-psiche/Bornert_0315/movie_Peuplier
# copy paste all this, after modifying the directory name:
mt.stopLive()
mt.proxy.nbFrames = 1
mt.setupSaving('/nfs/ruche-psiche/psiche-soleil/com-psiche/Bornert_0315/movie_Peuplier', 'photo')
mt.autoSaveOn()
# start a loop  -  use crtl-C to stop!
while True:
  mt.takeIm()
  time.sleep(1)
# movie loop


 A FAIRE :
mt.setScanParameters() 
mt.FlyScan()
test image
mt.startLive()
amove tomorz 10
paste (movie)
(04189-03419)/2
tomotablez -resultat






for ii in range(25):
  %dmove tomorefx 10
  %dmove tomorefx -10
  time.sleep(5)
  mt.takeIm()
  %dmove tomorefx -10
  %dmove tomorefx 10
  time.sleep(5)
  mt.takeIm()


5x and 50mu LuAG
  Name        Position   Unit   Limits               State
--------------------------------------------------------------
  tomofocus   61.0910    mm     [57.0000, 92.3390]   STANDBY
  tomozoom    163.34     mm     [132.26, 183.00]     STANDBY
--------------------------------------------------------------


while True:
  mt.takeIm()

# DCT sequence
for ii in range(9):
  %dmove tomotz -0.04 
  mt.FlyScan()
%shclose
%feclose

im = np.zeros((2048,2048))

for ii in range(4,13):
  name = 'DCT__'+str(ii)
  os.chdir('../'+name)
  f = h5py.File(name+'.nxs')
  a =  a = f['flyscan_00000']
  b = a['scan_data']
  c = b['orca']
  stack = c[380:390, :, :]
  f.close()
  stack = np.float32(stack)
  med = np.median(stack, 0)
  tmp = stack.sum(0) - (10*med)
  tmp = np.roll(tmp, (ii-4)*31, axis=0) 
  #im = (ii*(tmp>500)) + im
  im = tmp + im

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ANDY - file target path was:
S:\spool1\orca\flyscan-tmp-data
file number frames 10


PROBLEM STATE
Spyc (MonoTomo) [92]: mt.setFlyScanParameters()
for the current vertical roi: 800 lines, starting line 624
readout time is 0.0040 seconds
total time per image is 0.0440 seconds
max frame rate is 22.735 fps
camera is STOPPED
Current scan configuration is now 'tomo_1D'

Spyc (MonoTomo) [93]: mt.FlyScan()
Launching FlyScan with current configuration
camera is STOPPED
VFM pressure: 7.815600e-08   Mono pressure: 2.505000e-08
Pressure OK
_______________FlyScan has finished...
recording scan in log file: /nfs/ruche-psiche/psiche-soleil/com-psiche/Bornert_0315/scan_log.pickle
scan name will become 20a_bot__2
mv: ne peut évaluer `/nfs/naslowcost/spool1/flyscan-data/psiche-soleil/com-psiche/flyscan-2015-03-20-16-03/flyscan_00335.nxs': Aucun fichier ou répertoire de ce type
mv: ne peut évaluer `/nfs/naslowcost/spool1/flyscan-data/psiche-soleil/com-psiche/flyscan-2015-03-20-16-03': Aucun fichier ou répertoire de ce type
after renaming, data are in folder: /nfs/naslowcost/spool1/flyscan-data/psiche-soleil/com-psiche/20a_bot__2
!!! default to axis position: 1024.5...
problem in the log/rename/parfile/scripts section


