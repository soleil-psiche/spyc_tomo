# adjust parameters
sample = 'Comp0_N_In_R1'
suffixs = 'abcdefghijklmnopqrstuvwxyz'

mt.nref = 31
mt.ndark = 31
mt.calib_images = 8
mt.setFlyScanParameters()

mt.scanname = '%s_%c_' % (sample, suffixs[0])
mt.doTomo()

mt.nref = 31
mt.ndark = 0
mt.calib_images = 0
mt.setFlyScanParameters()

%setspeed tension 0.018

raw_input('start logging and loading')


for i in range(1, 26):
    mt.scanname = '%s_%c_' % (sample, suffixs[i])
    print('next scan will be %s' % mt.scanname)
    mt.doTomo()
    


