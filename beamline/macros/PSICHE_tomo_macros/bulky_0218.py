# -*- coding: utf-8 -*-

import time
import numpy as np
import pylab
pylab.ion()
import os
# simple bulky recording
from spyc.core import DeviceManager
dm = DeviceManager()

# visualise - should sit watching the bulky directory, and plot
# new log files... new figure for each new file? Updating only the live one.

def visualise(filename):

    fh = open(filename, 'rt')

    # read from a data file
    pylab.figure() # open a new figure
    pylab.title('Loading: %s' % filename.split(os.sep)[-1]) # open a new figure
    pylab.xlabel('displacement / mm')
    pylab.ylabel('load / N')
    plotp = []
    plotl = []
    timestep = 2
    # loop for recording and display
    loop = True
    print "starting loop - hit ctrl-c to end"
    while loop:
      try:
        fh.seek(-100, 2)
        line = fh.read().splitlines()[-2]
        t,toto,d,f,toto2 = line.split()
        d = float(d)
        f = float(f)
        pylab.clf()
        plotp.append(d)
        plotl.append(f)
        pylab.plot(plotp, plotl)
        pylab.title('Loading: %s' % filename.split(os.sep)[-1]) # open a new figure
        pylab.xlabel('displacement / mm')
        pylab.ylabel('load / N')
        pylab.draw()
        time.sleep(timestep)
      except Exception:
        time.sleep(.05)
      except KeyboardInterrupt:
        print "stopping the loop"
        loop = False
    fh.close()




# motor alias : bulky
# loadcell alias : load

spooldir = '/nfs/srv3/spool1/flyscan-data-Lacourt_0319'

def record(timestep=1):
  '''Start recording every N seconds, write to a specified filename'''

  # calibration
  loadcal = 500. # N per volt, 1000lbs load cell
  #loadcal = 41.2 # N per volt, 100lbs load cell ???

  # get the name
  getname = True
  while getname:
    name = raw_input('Please enter the sample name: ')
    filename = spooldir + os.sep + name + '.dat'
    if os.path.exists(filename):
        print 'this name already exists! Please choose another'
    else:
        getname = False

  try:
    # open file and figure
    print "open data file: %s" % filename
    fh = open(filename, 'wt')
    fh.write('#calibration %f N per volt\n' % loadcal)
    fh.write('#time position load rawPos rawV ptz\n')
    # reference positions
    load0 = dm.actors["load"].value * 1.
    position0 = dm.actors["bulky"].value * 1.
    time0 = time.time()
    plotl = [0]
    plotp = [0]
    plott = [0]
    pylab.figure() # open a new figure
    pylab.title('Loading: %s' % name) # open a new figure
    pylab.xlabel('displacement / mm')
    pylab.ylabel('load / N')


    # loop for recording and display
    loop = True
    print "starting record loop - hit ctrl-c to end and save data"
    while loop:
      try:
        loadN = (dm.actors["load"].value - load0) * loadcal
        rawPos = dm.actors["bulky"].value
        position = rawPos - position0
        timeA = time.time()
        timeS = timeA - time0

        rawV = dm.actors["load"].value * 1.
        ptz = dm.actors["ptz"].value * 1

        fh.write('%f %f %f %f %f %f\n' % (timeA, position, loadN, rawPos, rawV, ptz))
        pylab.clf()
        plotp.append(position)
        plotl.append(loadN)
        pylab.plot(plotp, plotl)
        pylab.title('Loading: %s' % name) # open a new figure
        pylab.xlabel('displacement / mm')
        pylab.ylabel('load / N')
        pylab.draw()
        time.sleep(timestep)
      except KeyboardInterrupt:
        print "stopping the loop"
        loop = False
  except:
    print "problem occured?"
  finally:
    print "closing file: %s" % filename
    fh.close()

