# -*- coding: utf-8 -*-
"""
Created on Fri Sep 30 12:17:17 2016

Clean pink beam calculations for PSICHE

@author: aking
"""

# imports
import numpy as np
from pylab import *
ion()
import pickle
import sys
import os
from collections import OrderedDict
import time

# global energy
# should redo this to 150 or 200 keV...  but requires redoing all input data too.
energy = np.arange(1, 101)


class PinkBeam:

    def __init__(self, datadirectory=None):
    
        self.datadirectory = datadirectory
        self.getFilters()
        self.getOptics()
        self.getSpectrum()
        # variables
        self.setup = OrderedDict()
        self.detectedspectrum = np.zeros(energy.shape)
        self.flux = 0.
        self.exposuretime = 0.
        

    def defineSetup(self):
        '''
        Define the beamline setup:
        a dictionary with keys 'mirror':angle, 'filter1':[material, thickness], 'filter2':...
        'scintillator':[material, thickness], 'current':stored current
        'mono_energy':
        '''
        self.setup = OrderedDict()
        self.detectedspectrum = np.zeros(energy.shape)
        # ring current
        tmp = raw_input('What is the stored current/mA ? [450] : ')
        tmp = 450. if tmp == '' else float(tmp)
        self.setup.update({'current':tmp})
        # optic
        tmp = raw_input('What is the optic ? 1x / [2.5x] / 5x / 7.5x / 10x / 20x : ')
        optic = '2.5x' if tmp == '' else tmp
        self.setup.update({'optic':optic})
        # scintillator
        tmp = raw_input('What is the scintillator material ? [LuAG] / YAG / LSO : ')
        material = 'LuAG' if tmp == '' else tmp
        tmp = raw_input('What is the scintillator thickness / mm ? [0.25] : ')
        thickness = 0.25 if tmp == '' else float(tmp)
        self.setup.update({'scintillator':[material, thickness]})
        tmp = raw_input('Define pink beam filter(s) ? [y]/n : ')
        addfilter = 'y' if tmp.lower() in ['', 'yes', 'y'] else 'n'
        if addfilter=='y':
            tmp = raw_input('What is the mirror angle / mrad ? [0.] : ')
            mirror = 0. if tmp == '' else float(tmp)
            self.setup.update({'mirror':mirror})
            count = 1
            while addfilter == 'y':
                material = raw_input('Filter %d material ? : ' % count)
                thickness = float(raw_input('Filter %d [%s] thickness / mm ? : ' % (count, material)))
                if material in self.filter_data.keys():
                    filter_key = 'filter%d' % count
                    self.setup.update({filter_key:[material, thickness]})        
                    print 'Added %s : %s, %0.2f mm' % (filter_key, material, thickness)
                    count += 1
                else:
                    print 'Material %s not available' % material
                    print 'Available filters are: %s' % self.filter_data.keys()
                tmp = raw_input('Add another filter ? [y]/n : ')
                addfilter = 'y' if tmp.lower() in ['', 'yes', 'y'] else 'n'            
        else:
            tmp = raw_input('Monochromatic beam energy / keV ? [25] : ')
            mono_energy = 25 if tmp == '' else float(tmp)
            self.setup.update({'mono_energy':mono_energy})
        tmp = raw_input('Define a sample ? [y]/n : ')
        addsample = 'y' if tmp.lower() in ['', 'yes', 'y'] else 'n'
        if addsample:
            count = 1
            while addsample == 'y':
                material = raw_input('Sample %d material ? : ' % count)
                thickness = float(raw_input('Sample %d [%s] thickness / mm ? : ' % (count, material)))
                if material in self.filter_data.keys():
                    sample_key = 'sample%d' % count
                    self.setup.update({sample_key:[material, thickness]})        
                    print 'Added %s : %s, %0.2f mm' % (sample_key, material, thickness)
                    count += 1
                else:
                    print 'Material %s not available' % material
                    print 'Available sample materials are: %s' % self.filter_data.keys()
                tmp = raw_input('Add another sample component ? [y]/n : ')
                addsample = 'y' if tmp.lower() in ['', 'yes', 'y'] else 'n'            
        self.showSetup()
        
    def updateSetup(self, itemname, itemparA, itemparB=None):
        '''
        Add/replace a filter to the existing setup
        arguments: 
        filtername - 'filter1', 'filter2', etc, or 'mirror' or 'scintillator' or
                    'optic' or 'current'
        itemparA - filter/scintillator material or mirror angle (mrad), or optic name, or current
        itemparB - filter/scintillator thickness, or nothing for mirror
        '''
        if itemparB == None:
            self.setup.update({itemname:itemparA})
        else:
            self.setup.update({itemname:[itemparA, itemparB]})
        print self.setup
        # finally, call calculateExposure() automatically
        self.calculateExposure()
        
    def updateSetupInteractive(self):
        '''
        Interactive version
        '''
        self.showSetup()
        mydict = {'c':'current', 'm':'mirror', 'f':'filter', 's':'sample', 'x':'scintillator', 'o':'optic'}
        tmp = raw_input('which element to update/add? \n[c]urrent / [m]irror / [f]ilter / [s]ample / [x]scintillator / [o]ptic : ')        
        if tmp not in 'cmfsxo':
            print 'input %s not understood - should be one of c,m,f,s,x,o' % tmp
            return
        else:
            itemname = mydict[tmp]
            print "----------------------------------------"
            print '              currently:                '
            for key in self.setup.keys():
                if itemname in key:
                    print "%s : %s" % (key, self.setup[key]) 
            print "----------------------------------------"
        if tmp in 'fs':
            tmp2 = raw_input('Which number %s to modify? or new number to add a new %s : ' % (itemname, itemname))
            try:
                # is this a number?
                toto = int(tmp2)
                itemname = itemname + tmp2
            except:
                print 'input %s not understood - should be a number' % tmp2
                return
        if self.setup.has_key(itemname):
            print "Modify %s : %s" % (itemname, self.setup[itemname])
        else:
            print "define a new %s" % itemname
        if tmp in 'cm':
            tmp3 = raw_input('Enter the new value for the %s' % itemname)
            try:
                # is this a number?
                tmp3 = int(tmp3)
                self.setup.update({itemname:tmp3})
            except:
                print 'input %s not understood - should be a number' % tmp3
                return   
        if tmp == 'o':
            tmp3 = raw_input('Enter the new optic [1x/2.5x/5x/7.5x/10x/20x/50x] : ')
            if tmp3 not in ['1x', '2.5x', '5x', '7.5x', '10x', '20x', '50x']:
                print 'input %s not understood - should be one of 1x/2.5x/5x/7.5x/10x/20x/50x' % tmp
                return
            else:
                self.setup.update({itemname:tmp3})
        if tmp in 'fsx':
            tmp3 = raw_input('Enter the material for the %s : ' % itemname)
            tmp4 = raw_input('Enter the thickness for the %s : ' % itemname)
            try:
                # is this a number?
                tmp4 = float(tmp4)
            except:
                print 'input %s not understood - should be a number' % tmp4
                return
            self.setup.update({itemname:[tmp3, tmp4]})
        # finally, call calculateExposure() automatically
        self.calculateExposure()


        
        
    def showSetup(self):
        '''
        Show the current setup
        '''
        print "----------------------------------------"
        print "              Current setup:            "
        for key in self.setup.keys():
            print "%s : %s" % (key, self.setup[key])
        print "----------------------------------------"

    def exportSpectrum(self, filename=None):
        '''
        Save a text file with the current setup and detected spectrum
        '''
        if filename == None:
            tmp = raw_input('filename for data file? [PSICHE_beam_data.txt] ')
            filename = 'PSICHE_beam_data.txt' if tmp == '' else tmp
            if os.path.isfile(filename):
                tmp = raw_input('file %s exists. Overwrite? [y]/n : ' % filename)
                tmp = 'y' if tmp.lower() in ['', 'y', 'yes'] else 'n'
            if tmp=='n':
                print "export with a different filename"
                return
            else:
                f = open(filename, 'wt')
                f.write('PSICHE imaging beam configuration\n')
                a = time.localtime()
                f.write('Generated %d/%d/%d at %02dh%02d:%02d\n\n' % (a[2],a[1],a[0],a[3],a[4],a[5]))
                f.write('____ Config _____\n')
                for key in self.setup.keys():
                    f.write('%s : %s\n' % (key, self.setup[key]))
                f.write('\n____ Estimated parameters _____\n')
                f.write('flux (photons/mm^2/s) : %E\n' % self.flux)
                f.write('exposure time (one frame, ms) : %f\n' % self.exposuretime)                    
                f.write('\n____ Detected spectrum (AU) _____\n')
                for ii in range(len(energy)):
                    f.write('%f,%f\n' % (energy[ii], self.detectedspectrum[ii]))
                f.close()
                print "file %s written and closed" % filename
    
    def calculateExposure(self, mono_energy=None, return_results=False):
        '''
        Using the setup defined, calculate the exposure time
        '''
        self.flux = 0.
        self.exposuretime = 0.
        # what is theflux at this ring current?
        powerin = 3*77 * self.setup['current']/430. # 300 use Mario's measured value
        myphotonsAU = self.photonsAU * self.setup['current']/430.
        myphotonsReal = self.photonsReal * self.setup['current']/430.    
        # make the spectrum for the scintillator
        absspec = 1 - self.makeFilter(self.setup['scintillator'][0], self.setup['scintillator'][1])
        # get the optic data
        pixelsize, NA = self.optics_data[self.setup['optic']]
        
        # monochromatic exposure time (for 45000 mean value in flatfield)
        monoexptime = 45000 / (NA**2 * absspec * pixelsize**2 * myphotonsAU * energy**2 * 1.3E-4)

        # define the sample components
        nsam = 0
        samndx = []
        for key in self.setup.keys():
            if key.startswith('sample'):
                nsam += 1
                samndx.append(int(key[6:]))
        # define sample
        samndx.sort()
        mysamples = np.zeros((100, nsam))
        totalsam = np.ones(100)
        for ii in range(nsam):
            samname = 'sample%d' % samndx[ii]
            mysamples[:,ii] = self.makeFilter(self.setup[samname][0], self.setup[samname][1])
            totalsam = totalsam * mysamples[:,ii]
            
        # if it is a pink beam setup:
        if not self.setup.has_key('mono_energy'):
            # get the mirror
            mirrorspec = self.makeMirror(self.setup['mirror'])
            # how many filters?
            nfilt = 0
            filtndx = []
            for key in self.setup.keys():
                if key.startswith('filter'):
                    nfilt += 1
                    filtndx.append(int(key[6:]))
            # define filters
            filtndx.sort()
            myfilters = np.zeros((100, nfilt))
            for ii in range(nfilt):
                filtername = 'filter%d' % filtndx[ii]
                myfilters[:,ii] = self.makeFilter(self.setup[filtername][0], self.setup[filtername][1])              
            # define power at each step - initial+mirror+filters+...
            power = np.zeros(nfilt+2) 
            power[0] = (myphotonsAU * energy).sum()
            # calculate the flux after the mirror
            photonfluxpink = myphotonsAU * mirrorspec
            totalfilter = mirrorspec * 1.
            power[1] = (photonfluxpink * energy).sum()
            # calculate the flux after each filter
            for ii in range(nfilt):
                photonfluxpink = photonfluxpink * myfilters[:,ii]
                power[ii+2] = (photonfluxpink * energy).sum()     
                totalfilter = totalfilter * myfilters[:,ii]
            # calculate the detected spectrum    
            detectedspectrum = absspec * photonfluxpink * energy
            meanenergy = (detectedspectrum * energy).sum() / detectedspectrum.sum()
            # calculate the exposure time
            exptimepink = 45000 / (NA**2 * absspec * pixelsize**2 * photonfluxpink * energy).sum()
            # same calculations for the beam passing through the sample
            photonfluxpinksam =  photonfluxpink * totalsam
            powersam = (photonfluxpinksam * energy).sum()     
            # calculate the detected spectrum    
            detectedspectrumsam = absspec * photonfluxpinksam * energy
            meanenergysam = (detectedspectrumsam * energy).sum() / detectedspectrumsam.sum()
            # calculate the exposure time
            exptimepinksam = 45000 / (NA**2 * absspec * pixelsize**2 * photonfluxpinksam * energy).sum()
            # sample attenuation
            samtrans = 100. * exptimepink / exptimepinksam
            samattenuation = 100 - samtrans
            # display som results
            print "Mean detected beam energy is %0.1f keV" % meanenergy 
            print "At this energy, mono exp time would be %d ms" % int(monoexptime[round(meanenergy)])
            # flux is photons x bandwidth
            print "Mono flux on sample would be %0.3f photons * 10E12 / mm^2" % (myphotonsReal[round(meanenergy)]*1.3E-4*meanenergy/1E12)
            # pink beam results
            print "Pink beam exp time is %0.3f ms" % float(exptimepink)
            #print "count rate = %0.1f" % (45000./exptimepink)
    
            # flux is sum(filtered photons) 
            pinkphotons = myphotonsReal * totalfilter
            print "Pink flux on sample %0.3f photons * 10E12 / mm^2" % (pinkphotons.sum()/1E12)
            flux = pinkphotons.sum()
            exptime = exptimepink
            print "Sample attenuation is %0.1f%%" % samattenuation    
            print "Beam hardens from %0.1f to %0.1f" % (meanenergy, meanenergysam)
            # calculate the percentage absorbed in each element
            print "Power absorbed in the different elements:"
            permirror = 100*(power[0]-power[1])/power[0]
            # per - one percentage for each filter
            per = np.zeros(nfilt)
            for ii in range(nfilt):
                per[ii] = 100*(power[ii+1]-power[ii+2])/power[0]      
            perscint = 100*detectedspectrum.sum()/power[0]
            print "mirror: %0.2f" % permirror
            for ii in range(0,nfilt):
                print "filter %d: %0.3f" % (filtndx[ii],  per[ii])
            print "scintillator in direct beam: %0.2f" % perscint
            
            print "sample: %0.2f" % (100*(power[-1]-powersam)/power[0])
            plot(energy, detectedspectrum)
            plot(energy, detectedspectrumsam, '.-')
            
            print "Heat disapation in filters:"
            # calc heat in filters...?
            Q = np.zeros(nfilt)
            thickness = np.zeros(nfilt)
            K = np.zeros(nfilt)        
            Tm = np.zeros(nfilt)        
            temp = np.zeros(nfilt)        
            dist = 2.5E-3 # for worst case diffusion distance
    
            # estimate the beam size at the filters to be 10.7 x 2.1 rather than 9 x 2... 
            print 'worst case scenario - filters independent'
            for ii in range(nfilt):
                filtername = 'filter%d' % filtndx[ii]
                filtermat = self.setup[filtername][0]
                thickness[ii] = self.setup[filtername][1]
                K[ii] = self.filter_data[filtermat][3]
                Tm[ii] = self.filter_data[filtermat][2]
                # calc
                Q[ii] = powerin * (per[ii]/100) / (10.7 * 2.1 * thickness[ii] * 1E-9) # power per m**3
                temp[ii] = ((Q[ii] * dist**2) / (2*K[ii])) + 17
                print "%s : %s, %0.2fmm : %0.2fC (Tm = %0.2fC)" % (filtername, filtermat, thickness[ii], temp[ii], Tm[ii])
                
            ## best case - perfect sharing?
            #print 'best case scenario - perfect sharing over all filters'
            #Qshare = powerin * ((per.sum())/100) / (10.7 * 2.1 * (thickness.sum()) * 1E-9)
            #Kshare = (K * thickness).sum() / thickness.sum()
            #Tshare = ((Qshare * dist**2) / (2*Kshare)) +17
            #print "T share = %0.1f" % Tshare
            
            # sandwich version
            if nfilt>1:
                print "rough sandwich calculation for the first two elements"
                if temp[1]>temp[0]: # filter B is hotter - diffuse to filter A and out through A
                    QB = powerin * (per[1]/100) / (10.7 * 2.1 * thickness[1] * 1E-9) # filter B as before
                    QA = powerin * ((per[0]+per[1])/100) / (10.7 * 2.1 * thickness[0] * 1E-9) # filter A gets the power from both
                    distA = dist # diffuse out through filter A
                    distB = thickness[1]* 1E-3 # small diffusion distance to A            
                    TA = ((QA * distA**2) / (2*K[0])) + 17 # filter A temperature given total power
                    TB = ((QB * distB**2) / (2*K[1])) + TA # filter B is still hotter than A
                else: # filter A is hotter, so other way around
                    QA = powerin * (per[0]/100) / (10.7 * 2.1 * thickness[0] * 1E-9) # filter A as before
                    QB = powerin * ((per[0]+per[1])/100) / (10.7 * 2.1 * thickness[1] * 1E-9) # filter B gets the power from both
                    distA = thickness[0]* 1E-3 # small diffusion distance to B
                    distB = dist # diffuse out through B           
                    TB = ((QB * distB**2) / (2*K[1])) + 17 # filter B for the total power
                    TA = ((QA * distA**2) / (2*K[0])) + TB # filter A hotter than B
                print "sandwich: TA = %0.1f (mp %d), TB = %0.1f (mp %d)" % (TA, Tm[0], TB, Tm[1])
            
        else: # monochromatic beam case
            monoenergy = self.setup['mono_energy']
            print "mono exp time at %0.1f keV %d ms" % (monoenergy, int(monoexptime[round(monoenergy)]))
            print "count rate = %0.1f" % (1000*45000/monoexptime[round(monoenergy)]) # note ms -> s
            # flux is photons x bandwidth
            print "mono flux on sample (%dmA) %0.3f photons * 10E12 / mm^2" % (self.setup['current'], myphotonsReal[round(monoenergy)]*1.3E-4*monoenergy/1E12)    
            flux = myphotonsReal[round(monoenergy)]*1.3E-4*monoenergy            
            detectedspectrum = np.zeros(energy.shape)
            exptime =  float(monoexptime[round(monoenergy)])  
            # sample attenuation
            samtrans = 100. * totalsam[round(monoenergy)]
            samattenuation = 100 - samtrans
            print "sample attenuation is %0.1f%%" % samattenuation
        self.detectedspectrum = detectedspectrum
        self.flux = flux
        self.exposuretime = exptime
        if return_results:
            return detectedspectrum
    
    
    def makeFilter(self, material, thickness):
        '''
        Calculate a filter spectrum for a material of given thickness
        '''
        if thickness == 0:
            filter_spec = np.ones(100)
        else:
            filter_spec = self.filter_data[material][1]
            t0 = self.filter_data[material][0]
            ratio = thickness/t0
            filter_spec = filter_spec**ratio
        return filter_spec
    
    def makeMirror(self, mirror_angle):
        '''
        Calculate the filter spectrum from the mirror at a given angle (mrad)
        '''
        # normally, critical angle in degrees = (1/E)*sqrt(density)
        # density Ir is 22.65 g/cm^3
        # this corresponds to E = 83.06/angle(mrad)
        # this behaviour - 10 keV range above critical angle - seems to correspond to reality
        mirror_spec = np.ones(100)
        if mirror_angle!=0:    
            crit_energy = 82.5/mirror_angle # maybe ~86 wouldbe better here
            print "critical energy about %0.1f keV" % crit_energy
            ndx = np.nonzero(energy>crit_energy)[0]
            if len(ndx)!=0:
                ndx = ndx[0] # first point above crit energy
                for ii in range(ndx, 100):
                    # after ten steps, intensity down by factor of 10
                    mirror_spec[ii] = 0.79432823**(ii-ndx+1)
        return mirror_spec
    
    
    #def scintillator(material, thickness):
    #    if thickness == 0:
    #        abs_spec = np.arange(1, 101)
    #    else:
    #        key = {'Al':0, 'Fe':1, 'Cu':2, 'Sn':3, 'W':4, 
    #        'Au':5, 'Mo':6, 'Ag':7, 'Si':8, 'LSO':9, 'LuAG':10, 'YAG':11, 'C':12}
    #        filter_spec = filt[:, key[material]]
    #        tref = {'Al':0.5, 'Fe':0.5, 'Cu':0.5, 'Sn':0.1, 'W':0.1, 
    #        'Au':0.1, 'Mo':0.1, 'Ag':0.1, 'Si':1.0, 'LSO':0.05, 'LuAG':0.05, 'YAG':0.05, 'C':0.5}
    #        t0 = tref[material]
    #        ratio = thickness/t0
    #        filter_spec = filter_spec**ratio
    #        abs_spec = 1 - filter_spec
    #        abs_spec = abs_spec * np.arange(1,101)
    #    return abs_spec  

    def getSpectrum(self):
        '''
        Read the mono brightness data, and calculate the spectrum in photons    
        '''
        # start from the brightness, measured in the EH, via the monochromator
        if self.datadirectory == None:
            path = "absorption_profiles"
        else:
            path = self.datadirectory
        filename = path + os.sep + "mono_brightness.dat"
        a = np.loadtxt(filename)
        energy0 = a[:, 0]        
        brightness0 = a[:, 1]
        # interpolate to 1-100 keV
        brightness = np.interp(energy, energy0, brightness0, 0, 0)
        # get the LuAG 50 mu absorption spectrum
        # the calculation
        #  brightness  = NA**2 x abs(E) x pixelsize**2 x photonsAU(E) x E * deltaE * exptime
        absspec = 1 - self.makeFilter('LuAG', 0.05)
        NA = 0.14
        pixelsize = 1.3
        # exptime = 350 ms
        photonsAU = brightness / (NA**2 * pixelsize**2 * absspec * energy**2 * 1.3E-4 * 350)
        # TM's plot seems to show for high energies, the flux drops at a rate of 10x per 25 kev
        # extrapolate using this - looks reasonably consistent with the observations between 30-50 keV
        lastflux = photonsAU[49]
        for ii in range(50, 100):
            photonsAU[ii] = lastflux*(0.91201084**(ii-49))
        # for low energies, it drops to zero very fast
        photonsAU[13:17] = np.linspace(0, photonsAU[17], 4, False)
        # now, we should modify this to account for absorption betweeen the 
        # exit of the mirror chamber (wher we have the power measurement)
        # use 5m N2, 1mm diamond, and 0.1mm Al to approximate this    
        filename = path + os.sep + "N5000_C0p5_Al0p1_1_100keV.dat"    
        a = np.loadtxt(filename)
        correction = a[:,1]
        photonsAU = photonsAU/correction
        photonsAU[np.nonzero(isnan(photonsAU))]=0
        # this should now have the right shape
        # treat photon flux quantitatively
        # we have a graph of photon flux in arbitary units versus energy(keV)
        # the integral is 300 W in 9x2mm - in fact 3x77 = 231 W (77 W in 2x3 mm)
        ev = 1.602E-19
        kev = 1000*ev
        #300W = sum(photonsAU * const * energy * kev)
        const = (3*77)/(photonsAU * energy * kev).sum() # use Mario's measured power
        photonsReal = photonsAU * const / (2*9) # for photons per mm^2
        # now, we should probably re-apply the absorption to get the flux at the sample position
        self.photonsReal = photonsReal*correction
        self.photonsAU = photonsAU*correction

       
    def getOptics(self):
        '''
        Get the optics data
        '''
        self.optics_data = {
            '1x':[6.5, 0.055], 
            '2.5x':[3,0.115], 
            '5x':[1.3, 0.14],
            '7.5x':[0.87, 0.21],
            '10x':[0.65, 0.28],
            '20x':[0.325, 0.28],
            '50x':[0.13, 0.42]}
        
    def getFilters(self):
        '''
        Read the filter data, or call the method to generate it
        '''
        if self.datadirectory == None:
            path = "absorption_profiles"
        else:
            path = self.datadirectory
        datafile = path + os.sep + "psiche_filter_data.pickle"
        if os.path.isfile(datafile):
            self.filter_data = pickle.load(open(datafile, "rb"))
        else:
            self.make_filter_database(datafile)
        
    def make_filter_database(self, datafilename):
        '''
        Read all of the XOP dat files and build a dictionary to use
        Save the dictionary as psiche_filter_data.pickle
        dictionary is key : [thickness, spectrum, Tm, conductivity]
        To add a material generate the dat file, add it to the folder, and update this function
        '''
        my_filters = {}
        if self.datadirectory == None:
            path = "absorption_profiles"
        else:
            path = self.datadirectory
        filename = path + os.sep + "al_500microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'Al':[0.5, a[:,1], 660, 235]})
        
        filename = path + os.sep + "fe_500microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'Fe':[0.5, a[:,1], 1538, 80]})
        
        filename = path + os.sep + "cu_500microns_1_100keV.dat"   
        a = np.loadtxt(filename)
        my_filters.update({'Cu':[0.5, a[:,1], 1080, 400]})
        
        filename = path + os.sep + "sn_100microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'Sn':[0.1, a[:,1], 232, 67]})
        
        filename = path + os.sep + "w_100microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'W':[0.1, a[:,1], 3422, 174]})
        
        filename = path + os.sep + "au_100microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'Au':[0.1, a[:,1], None, None]})
    
        filename = path + os.sep + "mo_100microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'Mo':[0.1, a[:,1], 2600, 139]})
    
        filename = path + os.sep + "ag_100microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'Ag':[0.1, a[:,1], 960, 430]})
    
        filename = path + os.sep + "si_1000microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'Si':[1.0, a[:,1], None, None]})
        
        filename = path + os.sep + "lso_50microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'LSO':[0.05, a[:,1], None, None]})
    
        filename = path + os.sep + "luag_50microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'LuAG':[0.05, a[:,1], None, None]})
        
        filename = path + os.sep + "yag_50microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'YAG':[0.05, a[:,1], None, None]})
        
        filename = path + os.sep + "c_500microns_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'C':[0.5, a[:,1], 700, 220]})
        
        filename = path + os.sep + "polycarbonate_10mm.dat"    
        a = np.loadtxt(filename)    
        my_filters.update({'poly':[10.0, a[:,1], 147, 0.2]})
        
        filename = path + os.sep + "SiC_1mm.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'SiC':[1.0, a[:,1], None, None]})
        
        filename = path + os.sep + "Ni_500mu_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'Ni':[0.5, a[:,1], 1455, 91]})
        
        filename = path + os.sep + "Pb_500mu_1_100keV.dat"    
        a = np.loadtxt(filename)
        my_filters.update({'Pb':[0.5, a[:,1], 328, 35]})
    
        pickle.dump(my_filters, open(datafilename, "wb"))
        print "Generated and saved the database in %s" % datafilename
        self.filter_data = my_filters
        
        
        




