#loads = [-5,-5,-5,-5,-5,-5,-5,-2]
#suffixs = 'abcdefgh' # scan at 'h' around 100 N

mt.refDisplacement = -12
tension.speed = 0.58
# adjust parameters
sample = 'Comp90_N_Ex_C3'
loads = [-1,-1,-1,-1,-1,-1]
suffixs = 'ijklmn'

for i, load in enumerate(loads):
    mt.scanname = '%s_%c_' % (sample, suffixs[i])
    print('next scan will be %s' % mt.scanname)
    Henry_traction_machine.measure(loads[i], '%s.log' % mt.scanname, relax_time=180)
    mt.doTomo()
