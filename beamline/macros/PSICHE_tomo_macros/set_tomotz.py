# -*- coding: utf-8 -*-


# try handling reference motor with spyc
from spyc.core import DeviceManager
dm = DeviceManager()


def set_ptomotz():

    curpos = dm.actors['ptomotz'].value

    tmp = raw_input('Which z stage are you using [short]/long : ')
    choice = None
    if tmp.lower() in ['', 's', 'short']:
        print "setting config for short Z"
        dm.actors['ptomotz'].proxy.put_property({'AxisPositionRatio': ['0.0005']})
        dm.actors['ptomotz'].proxy.Init()
    elif tmp.lower() in ['l', 'long']:
        print "setting config for long Z"
        dm.actors['ptomotz'].proxy.put_property({'AxisPositionRatio': ['0.00025']})
        dm.actors['ptomotz'].proxy.Init()
    else:
        print 'did not understand %s - not doing anything' % tmp


def set_mtomotz():
    # modify for new short z and powerbrick
    curpos = dm.actors['mtomotz'].value

    tmp = raw_input('Which z stage are you using [short]/long : ')
    choice = None
    if tmp.lower() in ['', 's', 'short']:
        print "setting config for short Z"
        dm.actors['mtomotz'].proxy.put_property({'UserEncoderRatio': ['-0.000004802766']})
        dm.actors['mtomotz'].proxy.Init()
    elif tmp.lower() in ['l', 'long']:
        print "setting config for long Z"
        dm.actors['mtomotz'].proxy.put_property({'UserEncoderRatio': ['0.000004802766']})
        dm.actors['mtomotz'].proxy.Init()
    else:
        print 'did not understand %s - not doing anything' % tmp
