#loads = [-10,-10,-10,-5]
#suffixs = 'abcd' # scan at 'd' around 90 N

mt.refDisplacement = -12
tension.speed = 0.58
radio_dir = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/radios'
# adjust parameters
sample = 'Comp90_N_Ex_C1'
loads = [-2,-2,-1,-1,-1,-1,1]
suffixs = 'efghijk'

for i, load in enumerate(loads):
    mt.scanname = '%s_%c_' % (sample, suffixs[i])
    print('next scan will be %s' % mt.scanname)
    Henry_traction_machine.measure(loads[i], '%s.log' % mt.scanname, relax_time=180)
    mt.doTomo()
