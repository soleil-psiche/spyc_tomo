# -*- coding: utf-8 -*-

# for Carmen beamtime March 2016

# line profiles for density measurement, and build composite image for alignment and monitoring
# line profiles could be flyscan


import numpy as np
import pylab
pylab.ion()
import PyTango
import time
import sys
import h5py
import os
import glob
import pickle
from FindNextFileName import findNextFileName

# get the camera - update change to hamamatsu.3 as main camera
import Camera
mycamera = Camera.Camera('i03-c-c00/dt/hamamatsu.3')
myPE = Camera.Camera('i03-c-c07/dt/perkinelmer.1')
# drive with speitbox to keep compatible with flyscan cabling
sbox = PyTango.DeviceProxy('flyscan/clock/spietbox-timebase.1')

# handling motors with spyc
from spyc.core import DeviceManager
dm = DeviceManager()

# hard coded log file for the beamtime - maybe put all this directly on srv3
image_logfile = '/home/experiences/psiche/com-psiche/tomo_logs/Xenia_1218/image_log.pickle'
profile_logfile = '/home/experiences/psiche/com-psiche/tomo_logs/Xenia_1218/profile_log.pickle'
image_dir = '/nfs/srv3/spool1/flyscan-data-Xenia_1218/images'
image_dir_windows = '\\\\srv3\\spool1\\flyscan-data-Xenia_1218\\images'
profile_dir = '/nfs/srv3/spool1/flyscan-data-Xenia_1218/profiles'
profile_dir_windows = '\\\\srv3\\spool1\\flyscan-data-Xenia_1218\\profiles'
diffraction_dir = '/nfs/ruche-psiche/psiche-soleil/com-psiche/Xenia_1218/diffraction'
ruche_profile_dir = '/nfs/ruche-psiche/psiche-soleil/com-psiche/Xenia_1218/profiles'
ruche_image_dir = '/nfs/ruche-psiche/psiche-soleil/com-psiche/Xenia_1218/images'

# save profiles like tomoscans in /nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/'

##################################################################################################
def mkimage(imagename=None, z_start=None, z_end=None, roi=[0, 0, 2048, 2048], pixelsize=0.0013):
    '''
    mkimage(imagename, z_start, z_end, roi=[startx, starty, sizex, sizey])
    works in the experimental hutch! (not optics hutch)
    z is the table2z position
    scans table2z and dtz2 together to build up an image
    assume that the slits are centred w.r.t. the camera
    '''
    # hoping that the slits are off the table - should be

    # check name
    if imagename==None:
        print "Must supply an image name"
        return
    # deal with the scan range
    if z_start==None or z_end==None:
        print "Must supply values for table z movement"
        return
    # scan in positive sense always
    if z_start > z_end:
        z_start, z_end = z_end, z_start

    # original speeds and positions
    dtz2_orig_velocity = dm.actors['dtz2'].speed * 1.
    dtz2_orig_position = dm.actors['dtz2'].value * 1.
    s2hg_orig_position = dm.actors['s2hg'].value * 1.
    table2z_orig_position = dm.actors['table2z'].value * 1.
    table_det_offset = dtz2_orig_position + table2z_orig_position
    # ->  det_pos = offset - table_pos
    det_start = table_det_offset - z_start
    det_end = table_det_offset - z_end

    # display scan range
    print "Will scan table2z from %0.1f to %0.1f (initial position %0.1f) ..." % (z_start, z_end, table2z_orig_position)

    # open the slits
    get_ipython().magic('amove s2hg 2.8')
    print " opening the slits"

    # set the speed of the detector z - this is still good (0617)
    get_ipython().magic('setspeed dtz2 0.1')

    # do steps of 25 microns
    nsteps = np.ceil((z_end - z_start) / 0.025)
    frametime = 250 # ms

    # setup the spietbox
    sbox.mode = 0
    sbox.pulsePeriod = frametime
    sbox.sequenceLength = int(nsteps)
    sbox.firstPulseAtFirstPosition = False
    sbox.pulseWidth=0.001
    sbox.Prepare()

    # stop camera in case it is running
    mycamera.proxy.Stop()
    # set up camera
    mycamera.setExpTime(0.07)
    mycamera.proxy.nbFrames = int(nsteps)
    mycamera.proxy.resetfileindex()
    mycamera.proxy.fileGeneration = True
    mycamera.proxy.triggerMode = "EXTERNAL_MULTI"
    mycamera.proxy.fileFormat = 'NXS'
    mycamera.proxy.filePrefix = 'mkimage'
    mycamera.proxy.fileTargetPath = '\\\\srv3\\spool1\\flyscan-tmp-data'
    mycamera.proxy.fileNbFrames = 10

    # set the ROI
    mycamera.setROI(roi[0],roi[1],roi[2],roi[3])
    startX = mycamera.proxy.roiX * 1.
    sizeX = mycamera.proxy.roiWidth * 1.

    # what is the real x range of the figure?
    tomorefxpos = dm.actors['tomorefx'].value * 1.
    txstart = tomorefxpos - ((1024.5 - startX) * pixelsize)
    txend = txstart + (sizeX * pixelsize)

    # clear the spool - on srv3
    os.system("rm -f /nfs/srv3/spool1/flyscan-tmp-data/mkimage*nxs")

    # move to the start
    get_ipython().magic('amove table2z %0.3f dtz2 %0.3f' % (z_start, det_start))

    # open the shutters
    get_ipython().magic('fastshutteropen')
    get_ipython().magic('shopen')

    # do the acquisition
    try:
        # snap the camera
        mycamera.proxy.snap()
        time.sleep(1)
        # start the sbox
        sbox.start()
        # start the movement
        get_ipython().magic('amove table2z %0.3f dtz2 %0.3f' % (z_end, det_end))

        while mycamera.proxy.state() == PyTango._PyTango.DevState.RUNNING:
            time.sleep(1)
        time.sleep(1)

    except KeyboardInterrupt:
        # stop all the motors
        print "stop moving"
        dm.actors['dtz2'].proxy.Stop()
        dm.actors['table2z'].proxy.Stop()
        mycamera.proxy.Stop()
        time.sleep(1)
        sbox.abort()

    finally:
        # reset the detector speed, return to original positions
        print "returning to original positions - table, detector, slits"
        get_ipython().magic('setspeed dtz2 %0.3f' % dtz2_orig_velocity)
        get_ipython().magic('amove table2z %0.3f dtz2 %0.3f s2hg %0.3f' % (table2z_orig_position, dtz2_orig_position, s2hg_orig_position))
        mycamera.setROI()

    # Build the image
    # Wait for the files. We expect nsteps / 10 nxs files
    nfiles = np.ceil(nsteps/10.)
    ll = glob.glob("/nfs/srv3/spool1/flyscan-tmp-data/mkimage*nxs*")
    timeout = False
    timelim = time.time() + 30
    while (len(ll) < nfiles) and not (time.time()>timelim):
        if time.time()>timelim:
            timeout = True
        print "wait for files to appear"
        time.sleep(1)
        ll = glob.glob("/nfs/srv3/spool1/flyscan-tmp-data/mkimage*nxs*")
    if timeout:
        print "time out waiting for the files to appear in nfs/srv3/spool1/flyscan-tmp-data/"
        return

    # Read the images, build the output
    output_image = np.zeros((nsteps, sizeX))
    ll.sort()
    ndx = 0
    for ii in range(len(ll)):
        f=h5py.File(ll[ii])
        a=f['entry']
        b=a['scan_data']
        nxsimage=b['mkimage_image']
        for jj in range(nxsimage.shape[0]):
            line = nxsimage[jj, :, :].sum(0)
            output_image[ndx, :] = line
            ndx+=1

    # Display the image
    pylab.figure()
    print "Modifying table2z position for strange offset - try to understand this!!!"
    z_end = z_end - 0.1
    z_start = z_start - 0.1
    pylab.imshow(output_image, extent=[txend, txstart, z_end, z_start])
    pylab.gray()
    pylab.ylabel('table2z position')
    pylab.xlabel('tomorefx position')
    pylab.axis('scaled')

    # Save this image somehow
    if os.path.isfile(image_logfile):
        print "recording image in log file: %s" % image_logfile
        log = pickle.load(open(image_logfile, 'rb'))
    else:
        print "will create log file: %s" % image_logfile
        log = {"filename":["extent, pixelsize, time"]}
    suffix = 0
    if imagename in log:
        suffix = 0
        newname = imagename+"_"+str(suffix)
        while newname in log:
            suffix+=1
            newname = imagename+"_"+str(suffix)
        imagename = newname
        print "the image name will become %s" % imagename
    pylab.title(imagename)
    # update and save the log
    extent=[txstart, txend, z_end, z_start]
    log.update({imagename:[extent, pixelsize, time.localtime]})
    pickle.dump(log, open(image_logfile, 'wb'))
    print "updated image log file : %s" % image_logfile
    # now save the image data as a pickle
    picklename = image_dir + os.sep + imagename + '.pickle'
    pickle.dump([output_image, extent], open(picklename, 'wb'))
    print "saved image data in %s" % picklename
    # and as a .png
    pngname = image_dir + os.sep + imagename + '.png'
    pylab.savefig(pngname)
    print "saved a copy of the figure in %s" % pngname
    # copy image to ruche
    print "copy image to ruche directory"
    ruchepngname = ruche_image_dir + os.sep + imagename + '.png'
    os.system('cp %s %s' % (pngname, ruchepngname))
##################################################################################################


##################################################################################################
def mkprofile(profilename=None, x_start=None, x_end=None, x_ref=None, scan_counttime=0.28, ref_counttime = 0.14):
    '''
    mkprofile(profilename, x_start, x_end, x_ref)
    works in the experimental hutch! (not optics hutch)
    amove slits to 0.02 x 0.1 - hardcoded...
    Flyscans tomorefx from x_start to x_end in steps of 0.02
    takes references at tomorefx = x_ref
    uses hard-coded count times?
    it seems that the different trigger modes of the camera have slightly different dark currents
    stay in external multi and trigger with the spietbox
    Take some dummy images first in the hope that the camera will be stabilised a bit
    ### perhaps to test - there could be some interest in scanning straight across the sample in one run, to avoid stopping and starting.  The refs would then be the beginning and end of the scan, and teh dark could even be done in the same sequence by closing the fs.
    '''
    # parameters
    nref = 31
    flyscanSafetyTime = 0.015
    readout_time = 0.01006592388 # full 2k x 2k
    if x_end > x_start:
        x_margin = 0.25 # in mm - time for x movement to stabilise?
    else:
        x_margin = -0.25 # in mm - time for x movement to stabilise?
    x_step = 0.02 # hardcode this for the mo

    # check name
    if profilename==None:
        print "Must supply an profile name"
        return
    # deal with the scan range
    if x_start==None or x_end==None or x_ref==None:
        print "Must supply tomorefx start, end, and reference positions"
        return

    # original speeds and positions
    tomorefx_orig_velocity = 3
    get_ipython().magic('setspeed tomorefx 3')
    tomorefx_orig_position = dm.actors['tomorefx'].value * 1.
    s2hg_orig_position = dm.actors['s1hg'].value
    s2vg_orig_position = dm.actors['s1vg'].value

    # close the slits
    get_ipython().magic('amove s1hg 0.02 s1vg 0.1')
    print "close slits to s2hg 0.02 s2vg 0.10"

    get_ipython().magic('fwbsopen # fastshutteropen')
    print "open fastshutter"
    get_ipython().magic('feopen # shopen')
    print "open shutter"

    # scan calculations
    nsteps = np.ceil(np.abs((x_end - x_start) / x_step))
    frame_time = scan_counttime + readout_time + flyscanSafetyTime
    ref_frame_time = ref_counttime + readout_time + flyscanSafetyTime
    tomorefx_scan_velocity = x_step / frame_time
    # time for the initial acceleration and margin
    xacc = dm.actors['tomorefx'].proxy.acceleration
    tmargin = (np.abs(x_margin)/tomorefx_scan_velocity) + (tomorefx_scan_velocity/(2*xacc))

    # deal with the scan name
    if os.path.isfile(profile_logfile):
        print "recording profile in log file: %s" % profile_logfile
        log = pickle.load(open(profile_logfile, 'rb'))
    else:
        print "will create log file: %s" % profile_logfile
        log = {"filename":["x_start, x_end, x_ref, x_step, scan_counttime, ref_counttime, time"]}
    suffix = 0
    if profilename in log:
        suffix = 0
        newname = profilename+"_"+str(suffix)
        while newname in log:
            suffix+=1
            newname = profilename+"_"+str(suffix)
        profilename = newname
        print "the profile name will become %s" % profilename
    # update and save the log
    log.update({profilename:[x_start, x_end, x_ref, x_step, scan_counttime, ref_counttime, time.localtime]})
    pickle.dump(log, open(profile_logfile, 'wb'))
    print "updated image log file : %s" % profile_logfile

    # make a folder for this scan
    scandir = profile_dir + os.sep + profilename
    os.system("mkdir %s" % scandir)
    os.system("mkdir %s/images" % scandir)
    os.system("mkdir %s/refA" % scandir)
    os.system("mkdir %s/refB" % scandir)
    os.system("mkdir %s/imagedark" % scandir)
    os.system("mkdir %s/refdark" % scandir)

    # stop camera in case it is running
    mycamera.proxy.Stop()
    # use the whole image
    mycamera.setROI()
    # same prefix in all cases?
    mycamera.proxy.filePrefix = 'mkimage'
    '''
    # dummy refs before
    get_ipython().magic('amove tomorefx %0.3f' % x_ref)
    mycamera.proxy.exposuretime = ref_counttime * 1000 # for milliseconds
    mycamera.proxy.fileTargetPath = profile_dir_windows + os.sep + profilename + os.sep + 'refA'
    mycamera.proxy.nbFrames = 250
    mycamera.proxy.triggerMode = 'EXTERNAL_MULTI'
    mycamera.proxy.fileNbFrames = 10
    mycamera.proxy.fileGeneration = False
    mycamera.proxy.resetfileindex()

    # setup the spietbox
    sbox.mode = 0
    sbox.pulsePeriod = ref_frame_time*1000
    sbox.sequenceLength = 250
    sbox.firstPulseAtFirstPosition = False
    sbox.pulseWidth=0.001
    sbox.Prepare()
    # snap the camera
    print "take some dummy images to stabilise camera... 25 seconds..."
    mycamera.proxy.snap()
    time.sleep(1)
    sbox.Start()
    time.sleep(1)
    while sbox.State() == PyTango._PyTango.DevState.RUNNING:
        time.sleep(0.1)
    time.sleep(1)
    '''
    # refs before
    get_ipython().magic('amove tomorefx %0.3f' % x_ref)
    mycamera.proxy.exposuretime = ref_counttime * 1000 # for milliseconds
    mycamera.proxy.fileTargetPath = profile_dir_windows + os.sep + profilename + os.sep + 'refA'
    mycamera.proxy.nbFrames = nref
    mycamera.proxy.triggerMode = 'EXTERNAL_MULTI'
    mycamera.proxy.fileNbFrames = 10
    mycamera.proxy.fileGeneration = True
    mycamera.proxy.resetfileindex()

    # setup the spietbox
    sbox.mode = 0
    sbox.pulsePeriod = ref_frame_time*1000
    sbox.sequenceLength = int(nref)
    sbox.firstPulseAtFirstPosition = False
    sbox.pulseWidth=0.001
    sbox.Prepare()
    # snap the camera
    mycamera.proxy.snap()
    time.sleep(1)
    sbox.Start()
    time.sleep(1)
    while mycamera.proxy.state() == PyTango._PyTango.DevState.RUNNING:
        time.sleep(1)
    time.sleep(1)

    # set count times, movement, try/except loop
    # set up camera
    mycamera.proxy.exposuretime = scan_counttime * 1000 # for milliseconds
    mycamera.proxy.fileTargetPath = profile_dir_windows + os.sep + profilename + os.sep + 'images'
    mycamera.proxy.nbFrames = int(nsteps)
    mycamera.proxy.triggerMode = 'EXTERNAL_MULTI'
    mycamera.proxy.fileNbFrames = 10
    mycamera.proxy.fileGeneration = True
    mycamera.proxy.resetfileindex()

    # setup the spietbox
    sbox.mode = 1
    sbox.inputCoder = 2 # tomorefx
    sbox.pulsePeriod = x_step
    sbox.sequenceLength = int(nsteps)
    sbox.firstPulseAtFirstPosition = False
    sbox.pulseWidth=0.001
    sbox.Prepare()
    x_step = sbox.pulsePeriod * 1.

    # prepare axis
    get_ipython().magic('amove tomorefx %0.3f' % (x_start-x_margin))
    get_ipython().magic('setspeed tomorefx %0.6f' % tomorefx_scan_velocity)

    try:
        # start the camera
        mycamera.proxy.snap()
        # start the movement
        dm.actors['tomorefx'].proxy.position = (x_end + (2*x_margin))
        # wait tacc
        time.sleep(tmargin)
        # start the spietbox
        sbox.start()

        # dialogue while moving
        sys.stdout.write("scanning: \n")
        while dm.actors['tomorefx'].state == PyTango._PyTango.DevState.MOVING:
            sys.stdout.write("\rimage %d [tomorefx %0.3f]" % (mycamera.proxy.currentFrame, dm.actors['tomorefx'].value))
            sys.stdout.flush()
            time.sleep(0.3)

    except KeyboardInterrupt:
        # stop all the motors
        print "stop moving!"
        mycamera.proxy.Stop()
        dm.actors['tomorefx'].proxy.Stop()
        mycamera.proxy.Stop()
        time.sleep(1)
        sbox.abort()

    finally:
        get_ipython().magic('setspeed tomorefx %0.6f' % tomorefx_orig_velocity)


    # refs after
    get_ipython().magic('amove tomorefx %0.3f' % x_ref)
    mycamera.proxy.exposuretime = ref_counttime * 1000 # for milliseconds
    mycamera.proxy.fileTargetPath = profile_dir_windows + os.sep + profilename + os.sep + 'refB'
    mycamera.proxy.nbFrames = nref
    mycamera.proxy.triggerMode = 'EXTERNAL_MULTI'
    mycamera.proxy.fileNbFrames = 10
    mycamera.proxy.fileGeneration = True
    mycamera.proxy.resetfileindex()
    # setup the spietbox
    sbox.mode = 0
    sbox.pulsePeriod = ref_frame_time*1000
    sbox.sequenceLength = int(nref)
    sbox.firstPulseAtFirstPosition = False
    sbox.pulseWidth=0.001
    sbox.Prepare()
    # snap the camera
    mycamera.proxy.snap()
    time.sleep(1)
    sbox.Start()
    time.sleep(1)
    while mycamera.proxy.state() == PyTango._PyTango.DevState.RUNNING:
        time.sleep(1)
    time.sleep(1)

    # darks - close the fastshutter
    print "dont touch the shutter!"
    get_ipython().magic('fwbsclose # fastshutterclose')
    # ref darks
    print "Take the darks for the references"
    mycamera.proxy.fileTargetPath = profile_dir_windows + os.sep + profilename + os.sep + 'refdark'
    mycamera.proxy.nbFrames = nref
    mycamera.proxy.triggerMode = 'EXTERNAL_MULTI'
    mycamera.proxy.fileNbFrames = 10
    mycamera.proxy.fileGeneration = True
    mycamera.proxy.resetfileindex()
    # setup the spietbox
    sbox.mode = 0
    sbox.pulsePeriod = ref_frame_time*1000
    sbox.sequenceLength = int(nref)
    sbox.firstPulseAtFirstPosition = False
    sbox.pulseWidth=0.001
    sbox.Prepare()
    # snap the camera
    mycamera.proxy.snap()
    time.sleep(1)
    sbox.Start()
    time.sleep(1)
    while mycamera.proxy.state() == PyTango._PyTango.DevState.RUNNING:
        time.sleep(1)
    time.sleep(1)

    # image darks
    print "Take the darks for the images"
    mycamera.proxy.exposuretime = scan_counttime * 1000 # for milliseconds
    mycamera.proxy.fileTargetPath = profile_dir_windows + os.sep + profilename + os.sep + 'imagedark'
    mycamera.proxy.nbFrames = nref
    mycamera.proxy.triggerMode = 'EXTERNAL_MULTI'
    mycamera.proxy.fileNbFrames = 10
    mycamera.proxy.fileGeneration = True
    mycamera.proxy.resetfileindex()
    # setup the spietbox
    sbox.mode = 0
    sbox.pulsePeriod = frame_time*1000
    sbox.sequenceLength = int(nref)
    sbox.firstPulseAtFirstPosition = False
    sbox.pulseWidth=0.001
    sbox.Prepare()
    # snap the camera
    mycamera.proxy.snap()
    time.sleep(1)
    sbox.Start()
    time.sleep(1)
    while mycamera.proxy.state() == PyTango._PyTango.DevState.RUNNING:
        time.sleep(1)
    time.sleep(1)
    # don't re-open fastshutter, slits

    #get_ipython().magic('fastshutteropen')
    print "Not reopening fast shutter ! "
    get_ipython().magic('amove s1hg %0.3f s1vg %0.3f' % (s2hg_orig_position, s2vg_orig_position))
    get_ipython().magic('amove tomorefx %0.3f' % tomorefx_orig_position)


    # construct the profile
    print "building the profile"
    profile = read_profile(scandir, scan_counttime, ref_counttime, x_start, x_end, readroi=[824, 724, 400, 600])


def read_profile(scandir=None, imagetime=None, reftime=None, x_start=None, x_end=None, readroi=None, color=None):
    # need to be careful here because we can break through uint32 datatype
    # work in float32
    # looks like we should skip the first image of each ref group

    if scandir == None:
        print "must supply the scan directory"
        return

    # check if a txt file exists - if it does, offer to load it
    scanname = scandir.split(os.sep)[-1]
    filename = scandir + os.sep + 'profile_' + scanname + '.txt'
    if os.path.isfile(filename):
        print "profile file %s already exists!" % filename
        check = raw_input('load the existing profile? [y]/n')
        check = 'y' if check == '' else check
        if check == 'y':
            profnorm = np.loadtxt(filename, delimiter=',', skiprows=1)
            print "Add profile ot the current active figure"
            if color==None:
              pylab.plot(profnorm[:,0], profnorm[:, 1], label=scanname)
            else:
              pylab.plot(profnorm[:,0], profnorm[:, 1], color, label=scanname)
            pylab.legend(loc=3)
            return profnorm

    if imagetime == None or reftime == None:
        print "must supply the image and reference count times"
        return

    # read refdark
    refdark = 0.
    count = 0
    ll = glob.glob(scandir + os.sep + 'refdark' + os.sep + '*.nxs')
    ll.sort()
    for ii in range(len(ll)):
        f = h5py.File(ll[ii], 'r')
        im = f['entry']['scan_data']['mkimage_image']
        if readroi != None:
            im = im[:, readroi[1]:(readroi[1]+readroi[3]), readroi[0]:(readroi[0]+readroi[2])]
        if ii==0:
            # bin the first image
            refdark = refdark + np.sum(im[1::,:,:], dtype=np.float32)
            count = count + im.shape[0] - 1
        else:
            refdark = refdark + np.sum(im[:], dtype=np.float32)
            count = count + im.shape[0]
        f.close()
    refdark = refdark / count
    print 'read the reference dark'

    # read imagedark
    imagedark = 0.
    count = 0
    ll = glob.glob(scandir + os.sep + 'imagedark' + os.sep + '*.nxs')
    ll.sort()
    for ii in range(len(ll)):
        f = h5py.File(ll[ii], 'r')
        im = f['entry']['scan_data']['mkimage_image']
        if readroi != None:
            im = im[:, readroi[1]:(readroi[1]+readroi[3]), readroi[0]:(readroi[0]+readroi[2])]
        if ii==0:
            # bin the first image
            imagedark = imagedark + np.sum(im[1::,:,:], dtype=np.float32)
            count = count + im.shape[0] - 1
        else:
            imagedark = imagedark + np.sum(im[:], dtype=np.float32)
            count = count + im.shape[0]
        f.close()
    imagedark = imagedark / count
    print 'read the image dark'

    # read refA
    refA = 0.
    count = 0
    ll = glob.glob(scandir + os.sep + 'refA' + os.sep + '*.nxs')
    ll.sort()
    for ii in range(len(ll)):
        f = h5py.File(ll[ii], 'r')
        im = f['entry']['scan_data']['mkimage_image']
        if readroi != None:
            im = im[:, readroi[1]:(readroi[1]+readroi[3]), readroi[0]:(readroi[0]+readroi[2])]
        if ii==0:
            # bin the first image
            refA = refA + np.sum(im[1::,:,:], dtype=np.float32)
            count = count + im.shape[0] - 1
        else:
            refA = refA + np.sum(im[:], dtype=np.float32)
            count = count + im.shape[0]
        f.close()
    refA = refA / count
    refA = refA - refdark
    print 'read the initial reference'

    # read refB
    refB = 0.
    count = 0
    ll = glob.glob(scandir + os.sep + 'refB' + os.sep + '*.nxs')
    ll.sort()
    for ii in range(len(ll)):
        f = h5py.File(ll[ii], 'r')
        im = f['entry']['scan_data']['mkimage_image']
        if readroi != None:
            im = im[:, readroi[1]:(readroi[1]+readroi[3]), readroi[0]:(readroi[0]+readroi[2])]
        if ii==0:
            # bin the first image
            refB = refB + np.sum(im[1::,:,:], dtype=np.float32)
            count = count + im.shape[0] - 1
        else:
            refB = refB + np.sum(im[:], dtype=np.float32)
            count = count + im.shape[0]
        f.close()

    refB = refB*1.0 / count
    refB = refB - refdark
    print 'read the final reference'

    # rescale refs
    refA = refA * imagetime/reftime
    refB = refB * imagetime/reftime

    # read the profile
    count = 0
    ll = glob.glob(scandir + os.sep + 'images' + os.sep + '*.nxs')
    ll.sort()
    prof = np.zeros((len(ll)*10))
    #print "profile with no dark correction!!!"
    for ii in range(len(ll)):
        f = h5py.File(ll[ii], 'r')
        im = f['entry']['scan_data']['mkimage_image']
        if readroi != None:
            im = im[:, readroi[1]:(readroi[1]+readroi[3]), readroi[0]:(readroi[0]+readroi[2])]
        for jj in range(im.shape[0]):
            prof[count] = np.sum(im[jj,:,:], dtype=np.float32) - imagedark
            count+=1
            sys.stdout.write("\rreading profile %d..." % count)
            sys.stdout.flush()
        f.close()

    # crop profile to the right length
    prof = prof[0:count]
    # normalise by refs
    profnorm = np.zeros((len(prof), 2))
    ref = np.zeros(count)
    for ii in range(count):
        f = 1.0*ii/(count-1)
        ref[ii] = (refA*(1-f)) + (refB*f)
    profnorm[:, 1] = prof / ref
    print 'read and normalised the profile'

    if x_start!=None and x_end!=None:
        profnorm[:, 0] = np.linspace(x_start, x_end, len(prof))
    else:
        print "No x range supplied"

    # save the profile
    f = open(filename, 'wt')
    f.write('transmission\n')
    for ii in range(count):
        f.write('%f,%f\n' % (profnorm[ii, 0], profnorm[ii, 1]))
    f.close()
    print 'normalised profile saved in %s' % filename

    # copy profile to ruche
    print "copy profile to ruche directory"
    profname = filename.split(os.sep)[-1]
    os.system('cp %s %s/%s' % (filename, ruche_profile_dir, profname))
    # dump the partially processed data?

    # plot profile on figure 100
    print "Add profile to figure 100"
    pylab.figure(100)
    if color==None:
      pylab.plot(profnorm[:,0], profnorm[:, 1], label=scanname)
    else:
      pylab.plot(profnorm[:,0], profnorm[:, 1], color, label=scanname)
    pylab.legend(loc=3)

    #return profnorm, prof, refA, refB, refdark, imagedark, ref
    return profnorm


def takediff(exptime=None, prefix=None):
    '''Write the table position into a .txt file in the same directory'''

    if exptime==None or prefix==None:
        print "must supply exposure time and image name"
        return

    print "close s2hg to 0.4"
    get_ipython().magic('amove s2hg 0.4')
    print "open the shutter"
    get_ipython().magic('shopen')
    print "and the fast shutter"
    get_ipython().magic('fastshutteropen')
    # set the exposure time
    myPE.setExpTime(exptime)

    # setup saving
    filename = findNextFileName(diffraction_dir, prefix, 'tif')
    # parse this for ndx
    ndx = int(filename[-8:-4])
    myPE.setupSaving(diffraction_dir, prefix, ndx)
    # take the image using Camera
    myPE.snap()
    myPE.saveCurrent()
    print "close the shutter for dark"
    get_ipython().magic('shclose')
    myPE.setupSaving(diffraction_dir, (prefix+'_dark'), ndx)
    # take the image using Camera
    myPE.snap()
    myPE.saveCurrent()

    # now, write a text file containing the table z position
    table2z_position = dm.actors['table2z'].value

    # get a txt file name
    filename = findNextFileName(diffraction_dir, (prefix+'_centre'),'txt', ndx)
    f = open(filename, 'wt')
    f.write('table2z position\n')
    f.write('%f\n' % table2z_position)
    f.write('beam centre\n')
    # calc new centre
    #pixshift = (table2z_position + 79.836000) / 0.2
    pixshift = (table2z_position - 20.298) / 0.2
    #xpos = 991.351 + pixshift
    xpos = 1001.554 + pixshift
    #ypos = -492.148
    ypos = -486.007
    f.write('%f,%f\n' % (xpos, ypos))
    f.close()
    print "text file with table z and centre saved as %s" % filename
    print "Center X : %0.3f pix" % xpos
    print "Center Y : %0.3f pix" % ypos



