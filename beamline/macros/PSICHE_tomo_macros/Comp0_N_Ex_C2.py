# microstep switch set to A (1/40) so motor speed is /4
#loads = [-10,-10,-20,-20,-20,-20,-20,-20,-20]
#suffixs = 'abcdefghi' # scan at 'i' around 188 N

mt.refDisplacement = -12
tension.speed = 1.0
radio_dir = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/radios'
# adjust parameters
sample = 'Comp0_N_Ex_C2'
loads = [-2,-2,-2,-2,-2,-2]
suffixs = 'jklmno'

for i, load in enumerate(loads):
    mt.scanname = '%s_%c_' % (sample, suffixs[i])
    print('next scan will be %s' % mt.scanname)
    Henry_traction_machine.measure(loads[i], '%s.log' % mt.scanname, relax_time=180)
    mt.doTomo()
