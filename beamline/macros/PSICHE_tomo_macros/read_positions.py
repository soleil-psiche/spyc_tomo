# -*- coding: utf-8 -*-

import tkFileDialog
import par_tools

def showMotorPositions():

    print "select the par file for the scan of interest"

    filename = tkFileDialog.askopenfilename(initialdir='/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche', filetypes=[('par files', '.par')])

    pars = par_tools.par_read(filename)

    tx = float(pars['#tomotx'][0])
    ts = float(pars['#tomots'][0])
    tz = float(pars['#tomotz'][0])

    print "The previous scan position was"
    print "tomotx : %0.2f" % tx
    print "tomots : %0.2f" % ts
    print "tomotz : %0.2f" % tz

    #tmp = raw_input('Move to this position? y/[n] : ')
    #tmp = True if tmp.lower() in ['y', 'yes', 'true'] else False

    #if tmp:
    #    print "move to the scan position"
    #    get_ipython().magic('amove tomotx %f tomots %f tomotz %f' % (tx, ts, tz))
    #else:
    #    print 'not moving...'

    imsizeh = float(pars['END_VOXEL_1'][0]) - float(pars['START_VOXEL_1'][0]) + 1
    imsizev = float(pars['NUM_IMAGE_2'][0])

    print 'Naively, I would guess that the reconstruction was %d x %d x %d' % (imsizeh, imsizeh, imsizev)
    print 'If this was a 360 degree, half acquisition scan, it may be bigger in x and y!'
    xdim = raw_input('Enter the x dimension of the reconstruction [%d] : ' % imsizeh)
    imsizeh = imsizeh if xdim == '' else int(xdim)

    xcen = imsizeh / 2
    ycen = imsizeh / 2
    zcen = imsizev / 2


    print "Can try to align WITHIN this volume..."
    print "Open the volume in ImageJ, get the x/y/z coordinates of the feature of interest"
    xco = raw_input('What is the x coordinate? [%d] : ' % xcen)
    xco = 1024. if xco=='' else float(xco)
    yco = raw_input('What is the y coordinate? [%d] : ' % ycen)
    yco = 1024. if yco=='' else float(yco)
    zco = raw_input('What is the z coordinate? [%d] : ' % zcen)
    zco = 1024. if zco=='' else float(zco)
    # calculate the deltas
    pixelsize = float(pars['IMAGE_PIXEL_SIZE_1'][0])
    pix = raw_input('Please confirm the pixel size [%0.3f] : ' % pixelsize)
    pixelsize = pixelsize if pix == '' else float(pix)


    dx = ((imsizeh/2) - xco) * (pixelsize/1000) # low x value; move tomotx positive
    ds = - ((imsizeh/2) - yco) * (pixelsize/1000) # low y value move tomots negative
    dz = ((imsizev/2) - zco) * (pixelsize/1000)# low z value; move tomotz positive
    tx2 = tx + dx
    ts2 = ts + ds
    tz2 = tz + dz
    print "The proposed scan position is"
    print "tomotx : %0.2f" % tx2
    print "tomots : %0.2f" % ts2
    print "tomotz : %0.2f" % tz2
    tmp = raw_input('Move to this position? y/[n] : ')
    tmp = True if tmp.lower() in ['y', 'yes', 'true'] else False
    if tmp:
        get_ipython().magic('amove tomotx %f tomots %f tomotz %f' % (tx2, ts2, tz2))
        print "Now you should probably do a preview scan to adjust alignment..."
    else:
        print 'not moving...'


