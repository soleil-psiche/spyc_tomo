# -*- coding: utf-8 -*-


import pickle
import os
import time
import tkFileDialog
import numpy as np
import GUI_tools
gui = GUI_tools.GUIListBox()
import glob
import PyTango
#ge = PyTango.DeviceProxy('i03-C-CX1/dt/dtc-mca_xmap.1')
ge = PyTango.DeviceProxy('i03-C-CX1/dt/dtc-falconx8.1')

import updateon

# handling motors with spyc
from spyc.core import DeviceManager
dm = DeviceManager()



def runDiffractionMacroAuto(filename, counttime=10., oscrange=10.):

    #load the macro
    macro = pickle.load(open(filename, 'rb'))
    mapoutname = filename[0:filename.rfind('.')] + '.pickle_macro_data'

    # make sure the diffraction geometry corresponds to the macro file
    get_ipython().magic('amove theta %f' % macro['tth'])
    get_ipython().magic('amove cs1hg %f cs2hg %f' % (macro['cs1hg'], macro['cs2hg']))
    get_ipython().magic('amove s1vg %f s1hg %f' % (macro['s1vg'], macro['s1hg']))

    # record the acquisition parameters used
    macro.update({'oscillation_range':oscrange})
    macro.update({'counttime':counttime})

    # move to the starting point
    ii = 0
    get_ipython().magic('amove ptomotx %f ptomots %f ptomotz %f ptomorz %f' % (macro['ptomotx'][ii], macro['ptomots'][ii], macro['ptomotz'][ii], macro['ptomorz'][ii]))

    # important that refx is at zero!
    get_ipython().magic('amove ptomorefx 0')

    # set rotation speed
    speed = oscrange / counttime
    get_ipython().magic('setspeed ptomorz %f' % speed)
    # setup detector
    ge.presetType = 'FIXED_REAL'
    ge.presetValue = counttime

    ge.stop()
    updateon.updateoff()

    get_ipython().magic('feopen')
    get_ipython().magic('fwbsopen')
    #run the macro

    try:
        for ii in range(len(macro['ptomotx'])):

            print "---------- start point %d ------------" % ii

            # move to the point
            if oscrange == 360:
                # move to the point
                get_ipython().magic('amove ptomotx %f ptomots %f ptomotz %f' % (macro['ptomotx'][ii], macro['ptomots'][ii], macro['ptomotz'][ii]))
                ge.snap()
                # rotate...
                if np.mod(ii, 2)==0:
                    get_ipython().magic('amove ptomorz 360')
                else:
                    get_ipython().magic('amove ptomorz 0')
            elif oscrange == 0:
                # move to point and start
                get_ipython().magic('amove ptomotx %f ptomots %f ptomotz %f ptomorz %f' % (macro['ptomotx'][ii], macro['ptomots'][ii], macro['ptomotz'][ii], macro['ptomorz'][ii]))
                ge.snap()
            else:
                # move to point and oscillate
                if np.mod(ii, 2)==0:
                    rzstart = macro['ptomorz'][ii] - (oscrange / 2.)
                else:
                    rzstart = macro['ptomorz'][ii] + (oscrange / 2.)
                get_ipython().magic('amove ptomotx %f ptomots %f ptomotz %f ptomorz %f' % (macro['ptomotx'][ii], macro['ptomots'][ii], macro['ptomotz'][ii], rzstart))
                ge.snap()
                # move in the right direction
                if np.mod(ii, 2)==0:
                    get_ipython().magic('dmove ptomorz %f' % oscrange)
                else:
                    get_ipython().magic('dmove ptomorz %f' % -oscrange)

            # wait detector standby
            wait = True
            while wait:
                if ge.state() == PyTango._PyTango.DevState.STANDBY:
                    wait=False
                else:
                    time.sleep(0.5)
            time.sleep(0.5)
            # get the spectra
            macro['spectra'][ii, 0, :] = ge.channel00 * 1.
            macro['spectra'][ii, 1, :] = ge.channel01 * 1.
            macro['spectra'][ii, 2, :] = ge.channel02 * 1.
            macro['spectra'][ii, 3, :] = ge.channel03 * 1.

            if np.mod(ii, 50)==49:
                print "intermediate data save..."
                subspectra = macro['spectra'][(ii-49):(ii+1), :, :]
                subspectraname = mapoutname + str(ii-49)
                pickle.dump(subspectra, open(subspectraname, 'wb'))
                #pickle.dump(macro, open(mapoutname, 'wb'))
    except KeyboardInterrupt:
        print "stopping the macro!"
    finally:
        # and save finally, otherwise lose the last 9 points
        subspectra = macro['spectra'][(ii-49):(ii+1), :, :]
        subspectraname = mapoutname + 'finally'
        pickle.dump(subspectra, open(subspectraname, 'wb'))
        #pickle.dump(macro, open(mapoutname, 'wb'))
        get_ipython().magic('fwbsclose')
        get_ipython().magic('feclose')
        get_ipython().magic('setspeed ptomorz 60')
        updateon.updateon('OH', 3) # hard coded ORCA 3 :-(

    print "scan finished :-) "
    print "Data saved in %s" % mapoutname

