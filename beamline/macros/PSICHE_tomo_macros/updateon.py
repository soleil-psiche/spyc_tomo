# -*- coding: utf-8 -*-


# a pair of macros to handle taking an image automatically after each move

from spyc.core.macro import register_spyc_macro
from spycmacros.move import PsicheEHMoveCmd1, PsicheOHMoveCmd1,  PsicheEHMoveCmd3, PsicheOHMoveCmd3, PsicheEHMoveCmd4, PsicheOHMoveCmd4
from spyc.commands.actuators import MoveCmd

import Camera
mycamera1 = Camera.Camera('i03-c-c00/dt/hamamatsu.1') # get both
mycamera3 = Camera.Camera('i03-c-c00/dt/hamamatsu.3') # get both
mycamera4 = Camera.Camera('i03-c-c00/dt/pco-dimax.1') # get both

def updateon(cabin="EH", camera=1):
    print "will take an image with fastshutter after each move"
    if cabin=="EH":
        print 'using fastshutteropen / fastshutterclose'
        if camera==1:
            register_spyc_macro('move', PsicheEHMoveCmd1)
            mycamera1.proxy.nframes = 1
            mycamera1.proxy.triggerMode = 'INTERNAL_SINGLE'
        elif camera==3:
            register_spyc_macro('move', PsicheEHMoveCmd3)
            mycamera3.proxy.nframes = 1
            mycamera3.proxy.triggerMode = 'INTERNAL_SINGLE'
        elif camera==4:
            register_spyc_macro('move', PsicheEHMoveCmd4)
            mycamera4.proxy.nframes = 1
            mycamera4.proxy.triggerMode = 'INTERNAL_SINGLE'
        else:
            print "wrong camera?"
    elif cabin=="OH":
        print 'using fwbsopen / fwbsclose'
        if camera==1:
            register_spyc_macro('move', PsicheOHMoveCmd1)
            mycamera1.proxy.nframes = 1
            mycamera1.proxy.triggerMode = 'INTERNAL_SINGLE'
        elif camera==3:
            register_spyc_macro('move', PsicheOHMoveCmd3)
            mycamera3.proxy.nframes = 1
            mycamera3.proxy.triggerMode = 'INTERNAL_SINGLE'
        elif camera==4:
            register_spyc_macro('move', PsicheOHMoveCmd4)
            mycamera4.proxy.nframes = 1
            mycamera4.proxy.triggerMode = 'INTERNAL_SINGLE'
        else:
            print "wrong camera?"
    else:
        print "cabin should be EH or OH ! "
        return



def updateoff():
    print "no image after each move"
    register_spyc_macro('move', MoveCmd)
