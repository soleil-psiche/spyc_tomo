# -*- coding: utf-8 -*-


import pickle
import os
import time
import tkFileDialog
import numpy as np
import GUI_tools
gui = GUI_tools.GUIListBox()
import glob
import PyTango
#ge = PyTango.DeviceProxy('i03-C-CX1/dt/dtc-mca_xmap.1')
ge = PyTango.DeviceProxy('i03-C-CX1/dt/dtc-falconx8.1')

# handling motors with spyc
from spyc.core import DeviceManager
dm = DeviceManager()



def runDiffractionMacro(ruchedir='Iacconi_1020'):

    #load the macro
    print "select the macro file (...pickle_macro)"
    initialdir='/nfs/ruche-psiche/psiche-soleil/com-psiche/proposal/2020/%s/' % ruchedir
    os.chdir(initialdir)
    mylist = []
    tmp = glob.glob('*pickle_macro')
    for elem in tmp:
        mylist.append(elem)
    tmp = glob.glob('*/*pickle_macro')
    for elem in tmp:
        mylist.append(elem)
    tmp = glob.glob('*/*/*pickle_macro')
    for elem in tmp:
        mylist.append(elem)
    tmp = glob.glob('*/*/*/*pickle_macro')
    for elem in tmp:
        mylist.append(elem)
    #gui.buildGUI(mylist)
    #mapname = gui.get_selection()
    for ii,name in enumerate(mylist):
        print '(%d) : %s' % (ii, name)
    ndx = raw_input('select by number! which number ? : ')
    if ndx == '':
        print 'need to choose a number! quitting!'
        return
    else:
        ndx = int(ndx)
    mapname = mylist[ndx]


    #filename = tkFileDialog.askopenfilename(initialdir='/nfs/ruche-psiche/psiche-soleil/com-psiche/%s/' % ruchedir)
    mapoutname = mapname[0:mapname.rfind('.')] + '.pickle_macro_data'
    macro = pickle.load(open(mapname, 'rb'))

    nok = False
    ndx = 0
    if os.path.isfile(mapoutname):
        nok = True
    while nok:
        # try a new savename
        ndx = ndx + 1
        mapoutname = mapname[0:mapname.rfind('.')] + '_%d.pickle_macro_data' % ndx
        if not os.path.isfile(mapoutname):
            nok = False
    print "will save the data in %s" % mapoutname

    tmp = raw_input("enter the count time / seconds [30] ")
    counttime = 30 if tmp == '' else float(tmp)
    tmp = raw_input("oscillation range / deg (0=no osc., 360=circles) [5] ")
    oscrange = 5. if tmp == '' else float(tmp)

    # make sure the diffraction geometry corresponds to the macro file
    get_ipython().magic('amove theta %f' % macro['tth'])
    get_ipython().magic('amove cs1hg %f cs2hg %f' % (macro['cs1hg'], macro['cs2hg']))
    get_ipython().magic('amove s1vg %f s1hg %f' % (macro['s1vg'], macro['s1hg']))

    # record the acquisition parameters used
    macro.update({'oscillation_range':oscrange})
    macro.update({'counttime':counttime})

    # move to the starting point
    ii = 0
    get_ipython().magic('amove ptomotx %f ptomots %f ptomotz %f ptomorz %f' % (macro['ptomotx'][ii], macro['ptomots'][ii], macro['ptomotz'][ii], macro['ptomorz'][ii]))

    # important that refx is at zero!
    get_ipython().magic('amove ptomorefx 0')

    # set rotation speed
    speed = oscrange / counttime
    get_ipython().magic('setspeed ptomorz %f' % speed)
    # setup detector
    ge.presetType = 'FIXED_REAL'
    ge.presetValue = counttime

    ge.stop()
    get_ipython().magic('feopen')
    get_ipython().magic('fwbsopen')
    #run the macro

    try:
        for ii in range(len(macro['ptomotx'])):

            print "---------- start point %d ------------" % ii

            # move to the point
            if oscrange == 360:
                # move to the point
                get_ipython().magic('amove ptomotx %f ptomots %f ptomotz %f' % (macro['ptomotx'][ii], macro['ptomots'][ii], macro['ptomotz'][ii]))
                ge.snap()
                # rotate...
                if np.mod(ii, 2)==0:
                    get_ipython().magic('amove ptomorz 360')
                else:
                    get_ipython().magic('amove ptomorz 0')
            elif oscrange == 0:
                # move to point and start
                get_ipython().magic('amove ptomotx %f ptomots %f ptomotz %f ptomorz %f' % (macro['ptomotx'][ii], macro['ptomots'][ii], macro['ptomotz'][ii], macro['ptomorz'][ii]))
                ge.snap()
            else:
                # move to point and oscillate
                if np.mod(ii, 2)==0:
                    rzstart = macro['ptomorz'][ii] - (oscrange / 2.)
                else:
                    rzstart = macro['ptomorz'][ii] + (oscrange / 2.)
                get_ipython().magic('amove ptomotx %f ptomots %f ptomotz %f ptomorz %f' % (macro['ptomotx'][ii], macro['ptomots'][ii], macro['ptomotz'][ii], rzstart))
                ge.snap()
                # move in the right direction
                if np.mod(ii, 2)==0:
                    get_ipython().magic('dmove ptomorz %f' % oscrange)
                else:
                    get_ipython().magic('dmove ptomorz %f' % -oscrange)

            # wait detector standby
            wait = True
            while wait:
                if ge.state() == PyTango._PyTango.DevState.STANDBY:
                    wait=False
                else:
                    time.sleep(0.5)
            time.sleep(0.5)
            # get the spectra
            macro['spectra'][ii, 0, :] = ge.channel00 * 1.
            macro['spectra'][ii, 1, :] = ge.channel01 * 1.
            macro['spectra'][ii, 2, :] = ge.channel02 * 1.
            macro['spectra'][ii, 3, :] = ge.channel03 * 1.

            if np.mod(ii, 10)==0:
                print "intermediate data save..."
                pickle.dump(macro, open(mapoutname, 'wb'))
    except KeyboardInterrupt:
        print "stopping the macro!"
    finally:
        # and save finally, otherwise lose the last 9 points
        pickle.dump(macro, open(mapoutname, 'wb'))
        get_ipython().magic('fwbsclose')
        get_ipython().magic('feclose')
        get_ipython().magic('setspeed tomorz 60')

    print "scan finished :-) "
    print "Data saved in %s" % mapoutname

