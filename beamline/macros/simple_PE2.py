# -*- coding: utf-8 -*-
import PyTango
from PyTango import DevState,DeviceProxy,DevFailed
from time import sleep
import time
import os, sys
from Camera import Camera
import numpy as np
import sys
import os.path
import glob
from spyc import beamline_print

from spyc.core import DeviceManager
dm = DeviceManager()

# new version - using tiffile, using spyc dm, based on Camera class

# use the ds_SingleShotDIO (portE) to control the fast shutter -> fastshutter in spyc
dev_shut = PyTango.DeviceProxy('i03-c-c00/ca/dio_o.1')
t_shut = 0.25 # declare this here once and for all
# with the fastshutter, a=time.time();get_ipython().magic(u'amove fastshutter 1');b=time.time();b-a=~0.5
# take half of this as the moving time

# make a simple version to use the accumulation mode in the DS

class PE(Camera):

    '''
    BEWARE: All times are translated in seconds!
    '''
    def __init__(self, mycamera="i03-c-c07/dt/perkinelmer.1"):
        # initialise camera
        Camera.__init__(self, mycamera)

    def fsopen(self):
        #att_shut.write(self.fsopen_pos)
        #get_ipython().magic(u'amove fastshutter 0')
        dev_shut.resetline('PG7')

    def fsclose(self):
        #att_shut.write(self.fsclose_pos)
        #get_ipython().magic(u'amove fastshutter 1')
        dev_shut.setline('PG7')


    def sweep(self, motor="tomorz", start_pos=None, end_pos=None, t_exp=None, return_to_start=True):

        # make sure that we use the first image
        dp = PyTango.DeviceProxy('i03-c-c07/dt/perkinelmer.1-specific')
        dp.keepFirstImage=True

        # with the fastshutter, a=time.time();get_ipython().magic(u'amove fastshutter 1');b=time.time();b-a=~0.5
        # take half of this as the moving time?
        t_shut = .25 # shutter moving time
        t_pad = 0.1 # safety time

        # treat the exposure time and number of frames
        t_exp = t_exp * 1.0 # time for ccd

        # check that the ccd is ready
        if self.proxy.state() <> DevState.STANDBY:
            raise Exception("PE: cannot snap if device not in STANDBY state.")

        # set the exposure time - no limit in ACCUMULATION
        if self.proxy.acquisitionMode == 'SINGLE' and t_exp>5:
            beamline_print("Device must be in ACCUMULATION mode for >5 seconds exposures")
            beamline_print("quitting...")
            return
        else:
            self.proxy.write_attribute("exposureTime", t_exp * 1000)

        # one image, no saving
        self.proxy.nbframes = 1
        self.proxy.filegeneration = False

        # save the original motor position and speed
        init_pos = dm.actors[motor].proxy.position
        init_v = dm.actors[motor].proxy.velocity
        init_acc = dm.actors[motor].proxy.read_attribute("acceleration").value

        # calculate the new values
        new_v = np.abs(end_pos - start_pos) / t_exp # motor speed required
        t_acc = new_v / init_acc # acceleration time
        s_acc = new_v * t_acc * 0.5 # acceleration distance
        s_shut = t_shut * new_v # shutter distance
        s_pad = t_pad * new_v # pad distance
        if end_pos > start_pos:
            s_start = start_pos - s_acc - s_shut - s_pad
            s_end = end_pos + s_acc + s_shut + s_pad
        else:
            s_start = start_pos + s_acc + s_shut + s_pad
            s_end = end_pos - s_acc - s_shut - s_pad


        # check the velocity
        if new_v > init_v:
            beamline_print("Max motor speed is %0.2f !" % init_v)
            beamline_print("Change your sweep parameters to go slower")
            return
        if new_v < 0.006231:
            beamline_print("The calculated velocity is too slow")
            beamline_print("Change your sweep parameters to go faster")
            return

        # check that the ccd is ready
        if self.proxy.state() <> DevState.STANDBY:
            raise Exception("PE: cannot snap if device not in STANDBY state.")

        # move to the start position
        beamline_print("move to start position...")
        #mv(motor, s_start)
        get_ipython().magic(u'amove %s %0.2f' % (motor, s_start))

        # put everything inside a try loop for safety
        try:

            # trigger the shutter
            beamline_print("close fast shutter")
            self.fsclose()
            # wait to be sure that the shutter is closed
            time.sleep(t_shut*3)

            # set the motor speed
            beamline_print("set motor speed %0.2f ..." % new_v)
            dm.actors[motor].proxy.velocity = new_v

            # start the movement
            beamline_print("start movement to %0.2f ..." % s_end)
            dm.actors[motor].proxy.position = s_end

            # wait the acceleration time and the safety time
            time.sleep(t_acc + t_pad)

            # trigger the shutter
            beamline_print("open shutter")
            self.fsopen()

            # wait the shutter opening time
            time.sleep(t_shut)

            # then start the detector
            beamline_print("start PE for image aquisition")
            self.proxy.snap()

            # wait the acquistion time - the shutter time
            time.sleep(t_exp - t_shut)

            # trigger the shutter
            beamline_print("close shutter")
            self.fsclose()

            # wait the shutter opening time
            time.sleep(t_shut)

            # now wait for the image to appear
            timeout = time.time() + 30.
            timeout_flag = False
            while self.proxy.state() <> DevState.STANDBY:
                beamline_print("waiting for detector to finish")
                sleep(0.5)
                if time.time() > timeout:
                    timeout_flag = True
                    beamline_print("PE: timeout >30 seconds while performing a sweep!!!")
                    break

            # save the image and show the statistics
            if not timeout_flag:
                self.image = self.proxy.image
                if self.python_save:
                    # save the image
                    self.saveCurrent()

        except KeyboardInterrupt:
            # stop the motor
            dm.actors[motor].proxy.stop()
            beamline_print("Stopping!")

        finally:
            # stop the motor in case it is still moving
            dm.actors[motor].proxy.stop()
            sleep(0.5)
            # reset the speed
            beamline_print("reset motor speed %0.2f ..." % init_v)
            dm.actors[motor].proxy.velocity = init_v
            if return_to_start:
                # return to the start position
                beamline_print("return to original position")
                #mv(motor, init_pos)
                get_ipython().magic(u'amove %s %0.2f' % (motor, init_pos))
            else:
                # don't return to start position
                beamline_print("not returning to original position")
                # mv without new position just displays the current position
                #mv(motor)
                get_ipython().magic(u'wm %s' % motor)
            beamline_print("finished - remember that fast shutter is closed! - fastshutteropen to open")



    def DCTsweep(self, scanname='toto', start_pos=0, end_pos=380, angstep=0.5, t_exp=None, bin=1, return_to_start=True, darks=0):
        '''DCTsweep(scanname, startpos/deg, endpos/deg, step/deg, exptime/s, binning(1 or 2))'''
        #motor = 'tomorz' # always!
        motor = 'tomorz'
        # make sure that we use the first image
        dp = PyTango.DeviceProxy('i03-c-c07/dt/perkinelmer.1-specific')
        dp.keepFirstImage=True
        self.stopLive()

        # darks - add this number to the series to get darks at the end

        # with the fastshutter, a=time.time();get_ipython().magic(u'amove fastshutter 1');b=time.time();b-a=~0.5
        # take half of this as the moving time?
        t_shut = .25 # shutter moving time
        t_pad = 0.1 # safety time

        # treat the exposure time and number of frames
        if t_exp==None:
            print "need the exposure time! quitting"
            return
        t_exp = t_exp * 1.0 # time for ccd

        # check that the ccd is ready
        if self.proxy.state() <> DevState.STANDBY:
            raise Exception("PE: cannot snap if device not in STANDBY state.")

        print "setting up for fast DCT sweep - one nxs file saved locally D:\\Deprez_0919"
        nframes = np.ceil((end_pos - start_pos) / (angstep*1.))
        # set binning to half the image
        self.setBinning(1)
        #self.setROI(0, 1024, 2048, 1024)
        self.setBinning(bin)
        self.proxy.acquisitionMode == 'SINGLE'
        self.proxy.exposureTime = t_exp * 1000
        self.proxy.nbFrames = nframes+darks
        self.proxy.fileNbFrames = nframes+darks
        self.proxy.fileFormat = 'NXS'
        self.proxy.fileTargetPath = 'D:\\Deprez_0919'
        self.proxy.filePrefix = scanname
        self.proxy.fileGeneration = True

        # save the original motor position and speed
        init_pos = dm.actors[motor].proxy.position
        init_v = dm.actors[motor].proxy.velocity
        init_acc = dm.actors[motor].proxy.read_attribute("acceleration").value

        # calculate the new values
        new_v = np.abs(end_pos - start_pos) / (t_exp*nframes) # motor speed required
        t_acc = new_v / init_acc # acceleration time
        s_acc = new_v * t_acc * 0.5 # acceleration distance
        s_shut = t_shut * new_v # shutter distance
        s_pad = t_pad * new_v # pad distance
        if end_pos > start_pos:
            s_start = start_pos - s_acc - s_shut - s_pad
            s_end = end_pos + s_acc + s_shut + s_pad
        else:
            s_start = start_pos + s_acc + s_shut + s_pad
            s_end = end_pos - s_acc - s_shut - s_pad


        # check the velocity
        if new_v > init_v:
            beamline_print("Max motor speed is %0.2f !" % init_v)
            beamline_print("Change your sweep parameters to go slower")
            return
        if new_v < 0.006231:
            beamline_print("The calculated velocity is too slow")
            beamline_print("Change your sweep parameters to go faster")
            return

        # check that the ccd is ready
        if self.proxy.state() <> DevState.STANDBY:
            raise Exception("PE: cannot snap if device not in STANDBY state.")

        # move to the start position
        beamline_print("move to start position...")
        #mv(motor, s_start)
        get_ipython().magic(u'amove %s %0.2f' % (motor, s_start))

        # put everything inside a try loop for safety
        try:

            # trigger the shutter
            beamline_print("close fast shutter")
            self.fsclose()
            # wait to be sure that the shutter is closed
            time.sleep(t_shut*3)

            # set the motor speed
            beamline_print("set motor speed %0.2f ..." % new_v)
            dm.actors[motor].proxy.velocity = new_v

            # start the movement
            beamline_print("start movement to %0.2f ..." % s_end)
            dm.actors[motor].proxy.position = s_end

            # wait the acceleration time and the safety time
            time.sleep(t_acc + t_pad)

            # trigger the shutter
            beamline_print("open shutter")
            self.fsopen()

            # wait the shutter opening time
            time.sleep(t_shut)

            # then start the detector
            beamline_print("start PE for image aquisition")
            self.proxy.snap()

            # wait the acquistion time - the shutter time
            time.sleep((t_exp*nframes) - t_shut)

            # trigger the shutter
            beamline_print("close shutter")
            self.fsclose()

            # wait the shutter opening time
            time.sleep(t_shut)

            # now wait for the image to appear
            timeout = time.time() + 30.
            timeout_flag = False
            while self.proxy.state() <> DevState.STANDBY:
                beamline_print("waiting for detector to finish")
                sleep(0.5)
                if time.time() > timeout:
                    timeout_flag = True
                    beamline_print("PE: timeout >30 seconds while performing a sweep!!!")
                    break

            # save the image and show the statistics
            if not timeout_flag:
                self.image = self.proxy.image
                if self.python_save:
                    # save the image
                    self.saveCurrent()

        except KeyboardInterrupt:
            # stop the motor
            dm.actors[motor].proxy.stop()
            beamline_print("Stopping!")

        finally:
            # stop the motor in case it is still moving
            dm.actors[motor].proxy.stop()
            sleep(0.5)
            # reset the speed
            beamline_print("reset motor speed %0.2f ..." % init_v)
            dm.actors[motor].proxy.velocity = init_v
            if return_to_start:
                # return to the start position
                beamline_print("return to original position")
                #mv(motor, init_pos)
                get_ipython().magic(u'amove %s %0.2f' % (motor, init_pos))
            else:
                # don't return to start position
                beamline_print("not returning to original position")
                # mv without new position just displays the current position
                #mv(motor)
                get_ipython().magic(u'wm %s' % motor)
            beamline_print("finished - remember that fast shutter is closed! - fastshutteropen to open")



    def DCTsweep2D(self, scanname='toto', t_exp=None, bin=1, return_to_start=True):
        '''DCTsweep(scanname, startpos/deg, endpos/deg, step/deg, exptime/s, binning(1 or 2))'''
        #motor = 'tomorz' # always!
        motor = 'tomorz'
        motor2 = 'tomorefx'
        start_pos=0
        end_pos=750 # èéà plus some margin
        angstep=3.
        xscan = 0.05 # 50 microns per 180 degrees
        xstart = -2*xscan
        xend = (end_pos/180.)*xscan + xstart


        # make sure that we use the first image
        dp = PyTango.DeviceProxy('i03-c-c07/dt/perkinelmer.1-specific')
        dp.keepFirstImage=True
        self.stopLive()

        # with the fastshutter, a=time.time();get_ipython().magic(u'amove fastshutter 1');b=time.time();b-a=~0.5
        # take half of this as the moving time?
        t_shut = .25 # shutter moving time
        t_pad = 0.1 # safety time

        # treat the exposure time and number of frames
        if t_exp==None:
            print "need the exposure time! quitting"
            return
        t_exp = t_exp * 1.0 # time for ccd

        # check that the ccd is ready
        if self.proxy.state() <> DevState.STANDBY:
            raise Exception("PE: cannot snap if device not in STANDBY state.")

        print "setting up for fast DCT sweep - one nxs file saved locally D:\\Deprez_0919"
        nframes = np.ceil((end_pos - start_pos) / (angstep*1.))
        # set binning to half the image
        self.setBinning(1)
        #self.setROI(0, 1024, 2048, 1024)
        self.setBinning(bin)
        self.proxy.acquisitionMode == 'SINGLE'
        self.proxy.exposureTime = t_exp * 1000
        self.proxy.nbFrames = nframes
        self.proxy.fileNbFrames = 10
        self.proxy.fileFormat = 'NXS'
        self.proxy.fileTargetPath = 'D:\\Deprez_0919'
        self.proxy.filePrefix = scanname
        self.proxy.fileGeneration = True

        # save the original motor position and speed
        init_pos = dm.actors[motor].proxy.position
        init_v = dm.actors[motor].proxy.velocity
        init_acc = dm.actors[motor].proxy.read_attribute("acceleration").value
        init_pos_x = dm.actors[motor2].proxy.position
        init_v_x = dm.actors[motor2].proxy.velocity
        init_acc_x = dm.actors[motor2].proxy.read_attribute("acceleration").value

        # calculate the new values
        new_v = np.abs(end_pos - start_pos) / (t_exp*nframes) # motor speed required
        t_acc = new_v / init_acc # acceleration time
        s_acc = new_v * t_acc * 0.5 # acceleration distance
        s_shut = t_shut * new_v # shutter distance
        s_pad = t_pad * new_v # pad distance
        if end_pos > start_pos:
            s_start = start_pos - s_acc - s_shut - s_pad
            s_end = end_pos + s_acc + s_shut + s_pad
        else:
            s_start = start_pos + s_acc + s_shut + s_pad
            s_end = end_pos - s_acc - s_shut - s_pad

        # ditto for x
        new_v_x = np.abs(xend - xstart) / (t_exp*nframes) # x motor speed required
        t_acc_x = new_v_x / init_acc_x # acceleration time
        s_acc_x = new_v_x * t_acc_x * 0.5 # acceleration distance
        s_shut_x = t_shut * new_v_x # shutter distance
        s_pad_x = t_pad * new_v_x # pad distance
        if xend > xstart:
            xstart = xstart - s_acc_x - s_shut_x - s_pad_x
            xend = xend + s_acc_x + s_shut_x + s_pad_x
        else:
            xstart = xstart + s_acc_x + s_shut_x + s_pad_x
            xend = xend - s_acc_x - s_shut_x - s_pad_x

        # check the velocity
        if new_v > init_v:
            beamline_print("Max motor speed is %0.2f !" % init_v)
            beamline_print("Change your sweep parameters to go slower")
            return
        if new_v < 0.006231:
            beamline_print("The calculated velocity is too slow")
            beamline_print("Change your sweep parameters to go faster")
            return

        # check that the ccd is ready
        if self.proxy.state() <> DevState.STANDBY:
            raise Exception("PE: cannot snap if device not in STANDBY state.")

        # move to the start position
        beamline_print("move to start position...")
        #mv(motor, s_start)
        get_ipython().magic(u'amove %s %0.2f %s %0.2f' % (motor, s_start, motor2, xstart))

        # put everything inside a try loop for safety
        try:

            # trigger the shutter
            beamline_print("close fast shutter")
            self.fsclose()
            # wait to be sure that the shutter is closed
            time.sleep(t_shut*3)

            # set the motor speed
            beamline_print("set motor speed %0.2f ..." % new_v)
            dm.actors[motor].proxy.velocity = new_v
            dm.actors[motor2].proxy.velocity = new_v_x

            # start the movement
            beamline_print("start movement to %0.2f ..." % s_end)
            dm.actors[motor].proxy.position = s_end
            dm.actors[motor2].proxy.position = xend

            # wait the acceleration time and the safety time
            time.sleep(t_acc + t_pad)

            # trigger the shutter
            beamline_print("open shutter")
            self.fsopen()

            # wait the shutter opening time
            time.sleep(t_shut)

            # then start the detector
            beamline_print("start PE for image aquisition")
            self.proxy.snap()

            # wait the acquistion time - the shutter time
            time.sleep((t_exp*nframes) - t_shut)

            # trigger the shutter
            beamline_print("close shutter")
            self.fsclose()

            # wait the shutter opening time
            time.sleep(t_shut)

            # now wait for the image to appear
            timeout = time.time() + 30.
            timeout_flag = False
            while self.proxy.state() <> DevState.STANDBY:
                beamline_print("waiting for detector to finish")
                sleep(0.5)
                if time.time() > timeout:
                    timeout_flag = True
                    beamline_print("PE: timeout >30 seconds while performing a sweep!!!")
                    break

            # save the image and show the statistics
            if not timeout_flag:
                self.image = self.proxy.image
                if self.python_save:
                    # save the image
                    self.saveCurrent()

        except KeyboardInterrupt:
            # stop the motor
            dm.actors[motor].proxy.stop()
            dm.actors[motor2].proxy.stop()
            beamline_print("Stopping!")

        finally:
            # stop the motor in case it is still moving
            dm.actors[motor].proxy.stop()
            dm.actors[motor2].proxy.stop()
            sleep(0.5)
            # reset the speed
            beamline_print("reset motor speed %0.2f ..." % init_v)
            dm.actors[motor].proxy.velocity = init_v
            time.sleep(0.5)
            beamline_print("reset motor speed x %0.2f ..." % init_v_x)
            dm.actors[motor2].proxy.velocity = init_v_x
            if return_to_start:
                # return to the start position
                beamline_print("return to original position")
                #mv(motor, init_pos)
                get_ipython().magic(u'amove %s %0.2f %s %0.2f' % (motor, init_pos, motor2, init_pos_x))
            else:
                # don't return to start position
                beamline_print("not returning to original position")
                # mv without new position just displays the current position
                #mv(motor)
                get_ipython().magic(u'wm %s %s' % (motor, motor2))
            beamline_print("finished - remember that fast shutter is closed! - fastshutteropen to open")







