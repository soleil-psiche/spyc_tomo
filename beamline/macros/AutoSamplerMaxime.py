# -*- coding: utf-8 -*-
from __future__ import division, print_function
from spyc.ipython.magics.basic import Magics, spyc_magics_class, line_magic
from spyc import beamline_print
from spyc.core import DeviceManager

dm = DeviceManager()

# sample is removed in tomots direction. Angle is defined by tomorz of loading position
# loading position is just underneath the sample
# a verifier - calcule de sens de rotation
# add a keyboard interrupt somewhere.

tx = dm.actors["mtomotx"]
ts = dm.actors["mtomots"]
tz = dm.actors["mtomotz"]
rz = dm.actors["mtomorz"]
Tx = dm.actors["mtomorefx"]
#ds = dm.actors["testmot"]
ds = dm.actors["carousel"]
beamline_print("Get the AutoSampler device")

def _move_absolute(motor, absolute_value):
	if type(motor) != list:
		motors = [motor.alias]
		absolute_values = [absolute_value]
	if len(motors) != len(absolute_values):
		print('Critical: Incompatible lengths!\nCheck for an extra or missing value in motors or absolute values.')
		raise(ValueError)
	get_ipython().magic((u'amove ' + '{} '*2*len(motors)).format(*[e for t in zip(motors, absolute_values) for e in t]))

def _move_relative(motor, relative_value):
	if type(motor) != list:
		motors = [motor.alias]
		relative_values = [relative_value]
	if len(motors) != len(relative_values):
		print('Critical: Incompatible lengths!\nCheck for an extra or missing value in motors or relative values.')
		raise(ValueError)
	get_ipython().magic((u'dmove ' + '{} '*2*len(motors)).format(*[e for t in zip(motors, relative_values) for e in t]))

class AutoSampler:

	def __init__(self):
		"""Create an AutoSample instance with default plate"""
		self.i_active = None
		self.n_positions = 38
		self.travel = 360.
		#self.approach_position = 3
		#self.type = 'circle' #'chain' or 'linear' not implemented
		self.step = self.travel/self.n_positions
		#self.approach_angle = self.approach_position*self.step
		self.approach_vertical = 27 #mm
		self.approach_horizontal = 20

	def define_positions(self, loading_position = (tx.value, ts.value, tz.value, rz.value, Tx.value), measuring_position = (0,0,0,0,0)):
		"""tomotx, tomots, tomotz, tomorz, tomorefx"""
		self.loading_position = loading_position
		self.measuring_position = measuring_position

	def _rotate(self, i):
		"""Find the fastest way to present sample number `i`"""
		if abs(ds.value%self.travel - i*self.step)<abs(360 + ds.value%self.travel - i*self.step):
			_move_relative(ds, i*self.step - ds.value%self.travel)
		else:
			_move_relative(ds, (i*self.step) - 360 - ds.value%self.travel)

	def _go_to_load(self):
		_move_absolute(tz, self.loading_position[2])
		_move_absolute(rz, self.loading_position[3])
		_move_absolute(ts, self.loading_position[1])
		_move_absolute(tx, self.loading_position[0])
		_move_absolute(Tx, self.loading_position[4])

	def _load(self):
		_move_relative(tz, self.approach_vertical)
		_move_relative(ts, +self.approach_horizontal)

	def _go_to_measure(self):
		_move_absolute(Tx, self.measuring_position[4])
		_move_absolute(tz, self.measuring_position[2])
		_move_absolute(ts, self.measuring_position[1])
		_move_absolute(tx, self.measuring_position[0])
		_move_absolute(rz, self.measuring_position[3])

	def _go_to_replace(self):
		_move_absolute(rz, self.loading_position[3])
		_move_absolute(tx, self.loading_position[0])
		_move_absolute(tz, self.loading_position[2] + self.approach_vertical)
		_move_absolute(ts, self.loading_position[1] + self.approach_horizontal)
		_move_absolute(Tx, self.loading_position[4])

	def _unload(self):
		_move_relative(ts, -self.approach_horizontal)
		_move_relative(tz, -self.approach_vertical)

	def _replace_sample(self, i):
		self._rotate(i)
		self._go_to_replace()
        self._unload()
        self.i_active = None # we have unloaded - no active sample!


	def load_sample(self, i):
		if self.i_active is not None:
			self._replace_sample(self.i_active)
		self._go_to_load()
		self._rotate(i)
		self._load()
		self.i_active = i
		self._go_to_measure()


#myCarrousel = AutoSampler()
#myCarrousel.define_positions(loading_position = (9, -38, 118, 28.4, 45), measuring_position = (0, -38, 100, 0, 10))
#myCarrousel.load_sample(0)
