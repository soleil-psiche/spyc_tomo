# -*- coding: utf-8 -*-
from spyc import beamline_print
from PyTango import DeviceProxy

beamline_print("Get the festo device")
dev_festo = DeviceProxy('i03-c-c00/ca/dio_o.1')

def diode0on():
    dev_festo.setline('PG0')
    beamline_print("Diode0 On")

def diode0off():
    dev_festo.resetline('PG0')
    beamline_print("Diode0 Off")

def diode1on():
    dev_festo.setline('PG1')
    beamline_print("Diode1 On")

def diode1off():
    dev_festo.resetline('PG1')
    beamline_print("Diode1 Off")

def light1on():
    dev_festo.setline('PG2')
    beamline_print("light1 On")

def light1off():
    dev_festo.resetline('PG2')
    beamline_print("light1 Off")

def light2on():
    dev_festo.setline('PG3')
    beamline_print("light2 On")

def light2off():
    dev_festo.resetline('PG3')
    beamline_print("light2 Off")

def image1on():
    dev_festo.setline('PG4')
    beamline_print("image1 On")

def image1off():
    dev_festo.resetline('PG4')
    beamline_print("image1 Off")

def image2on():
    dev_festo.setline('PG5')
    beamline_print("image2 On")

def image2off():
    dev_festo.resetline('PG5')
    beamline_print("image2 Off")