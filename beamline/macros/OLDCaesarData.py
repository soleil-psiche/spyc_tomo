'''
OK:  Make a class that handles caesar data
'''

# to go further - add export of ADX patterns with rebinning
# set the rebinning range - auto or specified
# clean up so that away from soleil it still works!
# try / except around pyTango bits?

# extract a gsas file - in energy or angle
# background corrections based on caesar data

# note that in Anaconda on windows, the tkFileDialog GUIs seem to work.  
# So this could be the standard option.

# for the moment we read caesar data from text files extracted from nxs
# but could get data directly from nxs.  
# should be able to open a single spectrum from a text file

# tool for converting all txt to gsas...  in here, or separate?

try:
	import PyTango
	SOLEIL = True 
except:
	print "Failed to import PyTango - assume not at SOLEIL!"
	SOLEIL = False
	
import glob # for listing files in a directory
import os # for sep, ...
import numpy as np
import sys
import time
import pylab
pylab.ion() # switch on interactive figure mode?
import pickle # first, easy saving option
from scipy.optimize import leastsq
from scipy import interpolate

# for GUI selecting files and folders
if SOLEIL:
	import GUI_tools
else:
	import tkFileDialog

# get the Spyc DeviceManager for reading values
if SOLEIL:
    from spyc.core import DeviceManager
    dm = DeviceManager()

# Emiliano's findNextFileName
def findNextFileName(prefix,ext,file_index=1):
    #
    #Prepare correct filename to avoid overwriting
    #
    psep=prefix.rfind(os.sep)
    if(psep<>-1): 
        fdir=prefix[:psep]
    else:
        fdir="."
    if(psep<>-1): prefix=prefix[psep+1:]
    if ext<>"":
        fname=prefix+"_"+"%04i"%(file_index)+"."+ext
    else:
        fname=prefix+"_"+"%04i"%(file_index)
    _dir=os.listdir(fdir)
    while(fname in _dir):
        file_index+=1
        if ext<>"":
            fname=prefix+"_"+"%04i"%(file_index)+"."+ext
        else:
            fname=prefix+"_"+"%04i"%(file_index)
    fname=fdir+os.sep+fname
    return fname


def simple_medfilt(im, filt, spacing=[1, 1]):
	# filt is [v,h]
	vr = (filt[0]-1)/2
	vrange = range(-vr, vr+1, spacing[0])
	hr = (filt[1]-1)/2
	hrange = range(-hr, hr+1, spacing[1])
	stack=np.zeros((im.shape[0], im.shape[1], len(vrange)*len(hrange)))
	ndx=0   
	for vv in vrange:
		for hh in hrange:
			stack[:,:,ndx] = np.roll(np.roll(im, hh, 1), vv, 0)
			ndx+=1
	imfilt = np.median(stack, 2)
	return imfilt
	
	
def make_bkg(im, step=1):
	# make a 2D background from the two summed profiles
	nrows, ncols = im.shape	
	bkg_e = im.sum(0)/nrows
	# smooth this out with a spline? - reducing to 20 steps
	tck = interpolate.splrep(np.arange(ncols), bkg_e, w=1./np.sqrt(bkg_e-bkg_e.min()+1), s=ncols, t=np.arange(2,ncols-2,step))
	bkg_e_s = interpolate.splev(np.arange(ncols), tck, der=0)
	# can also get a profile of scattered intensity as f(angle)
	bkg_a = im.sum(1)/ncols
	step_a = nrows/20
	tck = interpolate.splrep(np.arange(nrows), bkg_a, w=1./np.sqrt(bkg_a-bkg_a.min()+1), s=nrows, t=np.arange(step_a,nrows-step_a,step_a))
	bkg_a_s = interpolate.splev(np.arange(nrows), tck, der=0)
	# so this is a simple guess to a 2D background
	bkg_area = np.multiply(np.reshape(bkg_e_s, (1, ncols)), np.reshape(bkg_a_s, (nrows, 1)))
	# return the 2D and 1D backgrounds
	return bkg_area


# The CaesarData class:
class CaesarData:

	def __init__(self, detector="i03-C-CX1/dt/dtc-mca_xmap.1", dataname="channel03", experimentdir=""):
		# set the detector device, if specified
		self.setDetector(detector, dataname)
		# set the experiment directory, if specified
		self.setExperimentdir(experimentdir)
		self.datadir = ""
		# get the Caesar device
		if SOLEIL:
			self.__APcaesar = PyTango.AttributeProxy("I03-C-C05/EX/CAESAR/theta")
			# for GUI menu things
			self.__gui = GUI_tools.GUIListBox()
		# empty attributes
		self.energy = ""
		# add some empty attributes for the caesar dataset
		self.caesar_image = ""
		self.caesar_image_original = ""
		self.caesar_tth_nominal = ""
		self.caesar_tth = ""
		self.caesar_image_dsp = ""
		self.caesar_image_dspx = ""
		self.caesar_image_dsp_profile = ""
		self.caesar_image_Q = ""
		self.caesar_image_Qx = ""
		self.caesar_image_Q_profile = ""	
		self.caesar_rebin_range = "" # in keV (?)
		# add some empty attributes for the spectrum
		self.spectrum=""
		self.spectrum_tth_nominal = ""
		self.spectrum_tth = ""
		self.spectrum_dspy = ""
		self.spectrum_dspx = ""
		self.spectrum_qy = ""
		self.spectrum_qx = ""
		# use the standard calibration
		self.angle_calibration= ""
		self.setAngleCalibration()
		self.energy_calibration = ""
		self.setEnergyCalibration()
		# default rebin range
		self.setRebinRange()
		self.caesar_rebin_nbins = 2048
		self.caesar_rebin_method = "rebin"
		# for plotting figures
		self.logscale=True
		self.plotdsp=True # True for dsp, False for Q

	##### DRIVING FUNCTIONS - THINGS THAT THE USER CALLS #######

	def toggle_dsp_q(self):
		'''Change from Q to dsp, replot figures'''
		self.plotdsp = not self.plotdsp
		self.__showSpectrum()
		self.__showImage()

	def updateSpectrum(self):
		''' read, convert, plot '''
		self.__getSpectrum()
		self.__convertSpectrum()
		self.__showSpectrum()
		
	def doCaesarScan(self, scanname, startangle, endangle, stepsizeangle, counttime, starts1hg):
		'''Do a CAESAR scan
		with fancy live slit adjustments
		doCaesarScan(scanname, startangle, endangle, stepsizeangle, counttime, starts1hg)
		'''
		# make a new directory
		datadir = os.path.join(self.experimentdir, scanname)
		if not os.path.isdir(datadir):
			os.mkdir(datadir)
		self.setDatadir(datadir)
		# generate the scan positions
		scanangles = np.arange(startangle, endangle+stepsizeangle, stepsizeangle)
		# constants for the instrument
		R = 250 # sample - slit cs1
		deltaR = 980 # slit cs1 - slit cs2
		w1 = dm.actors['cs1hg'].value # gap cs1
		w2 = dm.actors['cs2hg'].value # gap cs2
		r = (w1/w2)*(deltaR/(1+(w1/w2)))
		# calculate a,b,K for the starting position
		sinA1 = np.sin(scanangles*np.pi/180)
		cosA1 = np.cos(scanangles*np.pi/180)
		tanA1 = np.tan(scanangles*np.pi/180)
		a = 1./tanA1 # changed from a = 2./cosA1
		b = (w1*(R+r)) / (r*(sinA1 + (cosA1*w1/(2*r))))
		K = a[0]*(starts1hg**2) + b[0]*starts1hg
		# generate the gaps for all angles
		delta = (b**2) + (4*a*K)
		scangaps = (-b + (np.sqrt(delta))) / (2*a)
		print "gap = %0.3f at angle %0.1f; gap = %0.3f at angle %0.1f" % (scangaps[0], scanangles[0], scangaps[-1], scanangles[-1])
		
		# call getSpectrum to make sure spectrum is up to data 
		self.__getSpectrum()
		# prepare an array to put the data in
		self.caesar_image = np.zeros([len(scanangles), len(self.spectrum)])
		self.caesar_image_original = ""
		# apply the angle_calibration to the tth_angles
		self.caesar_tth_nominal = scanangles
		self.__applyAngleCalibration()		
		# make sure detector is stopped
		self.__DP.abort()
		for ii in range(len(scanangles)):
			# move to the position
			get_ipython().magic(u'amove theta %f s1hg %f' % (scanangles[ii], scangaps[ii]))
			# start acquisition
			self.__DP.start()
			time.sleep(counttime)
			self.__DP.abort()
			spectrum = getattr(self.__DP, self.__dataname)
			# add to the image
			self.caesar_image[ii, :] = spectrum
		print "return to original s1hg: %f" % scangaps[0]
		get_ipython().magic(u'amove s1hg %f' % scangaps[0])
		# save the data
		myfilename = findNextFileName(os.path.join(datadir, "caesar"), "pickle")
		pickle.dump([self.caesar_image, self.caesar_tth_nominal, self.caesar_tth, self.energy, scanangles, scangaps], open(myfilename, 'wb'))
		print "scan data dumped in %s" % myfilename
		# after the scan - convert and display
		self.__convertImage()
		self.__correctCaesarSpectrum()
		self.__showImage()
		# export the summed profile
		self.__exportGSAS_combined()

	def doCaesarScan2(self, scanname, startangle, endangle, stepsizeangle, counttime, starts1hg, anglelim):
		'''Do a CAESAR scan
		with fancy live slit adjustments
		doCaesarScan2(scanname, startangle, endangle, stepsizeangle, counttime, starts1hg, anglelim)
		'''
		# make a new directory
		datadir = os.path.join(self.experimentdir, scanname)
		if not os.path.isdir(datadir):
			os.mkdir(datadir)
		self.setDatadir(datadir)
		# generate the scan positions
		scanangles = np.arange(startangle, endangle+stepsizeangle, stepsizeangle)
		# constants for the instrument
		R = 250 # sample - slit cs1
		deltaR = 980 # slit cs1 - slit cs2
		w1 = dm.actors['cs1hg'].value # gap cs1
		w2 = dm.actors['cs2hg'].value # gap cs2
		r = (w1/w2)*(deltaR/(1+(w1/w2)))
		# calculate a,b,K for the starting position
		sinA1 = np.sin(scanangles*np.pi/180)
		cosA1 = np.cos(scanangles*np.pi/180)
		tanA1 = np.tan(scanangles*np.pi/180)
		a = 1./tanA1 # changed from a = 2./cosA1
		b = (w1*(R+r)) / (r*(sinA1 + (cosA1*w1/(2*r))))
		# find the scanangle after the anglelim
		ndx = np.nonzero(scanangles>=anglelim)[0][0]
		K = a[ndx]*(starts1hg**2) + b[ndx]*starts1hg
		# generate the gaps for all angles
		delta = (b**2) + (4*a*K)
		scangaps = (-b + (np.sqrt(delta))) / (2*a)
		scangaps[0:ndx] = starts1hg

		print "gap = %0.3f at angles %0.1f to %0.1f; gap = %0.3f at angle %0.1f" % (scangaps[0], scanangles[0], scanangles[ndx], scangaps[-1], scanangles[-1])
		
		# call getSpectrum to make sure spectrum is up to data 
		self.__getSpectrum()
		# prepare an array to put the data in
		self.caesar_image = np.zeros([len(scanangles), len(self.spectrum)])
		self.caesar_image_original = ""
		# apply the angle_calibration to the tth_angles
		self.caesar_tth_nominal = scanangles
		self.__applyAngleCalibration()		
		# make sure detector is stopped
		self.__DP.abort()
		for ii in range(len(scanangles)):
			# move to the position
			get_ipython().magic(u'amove theta %f s1hg %f' % (scanangles[ii], scangaps[ii]))
			# start acquisition
			self.__DP.start()
			time.sleep(counttime)
			self.__DP.abort()
			spectrum = getattr(self.__DP, self.__dataname)
			# add to the image
			self.caesar_image[ii, :] = spectrum
		print "return to original s1hg: %f" % scangaps[0]
		get_ipython().magic(u'amove s1hg %f' % scangaps[0])
		# save the data
		myfilename = findNextFileName(os.path.join(datadir, "caesar"), "pickle")
		pickle.dump([self.caesar_image, self.caesar_tth_nominal, self.caesar_tth, self.energy, scanangles, scangaps], open(myfilename, 'wb'))
		print "scan data dumped in %s" % myfilename
		# after the scan - convert and display
		self.__convertImage()
		self.__correctCaesarSpectrum()
		self.__showImage()
		# export the summed profile
		self.__exportGSAS_combined()

######  CALIBRATION FUNCTIONS #######

	def setEnergyCalibration(self, calibration_string="0.0058079, 0.049974, 0"):
		''' Set the energy calibration as a comma separated list'''
		calibration_string = calibration_string.split(',')
		# convert each bit to float
		calibration_string[:] = [np.float(elem) for elem in calibration_string]
		self.energy_calibration = np.array(calibration_string)
		print "energy calibration is : %s" % self.energy_calibration
		print "Apply calibration to current data"
		self.__applyEnergyCalibration()
		# update figures to take account of the new energies
		self.__updateFigures()
		
	def setAngleCalibration(self, calibration_string="0.0005, 0.0094"):
		''' Set the Caesar angle calibration as a comma separated list
			offset = m*(nominal angle) + constant
			myCaesar.setAngleCalibration(\"m, c\")'''
		calibration_string = calibration_string.split(',')
		# convert each bit to float
		calibration_string[:] = [np.float(elem) for elem in calibration_string]
		self.angle_calibration = np.array(calibration_string)
		print "angle calibration is : %s" % self.angle_calibration
		print "Apply caibration to current data"
		self.__applyAngleCalibration()
		# update figures to take account of the new energies
		self.__updateFigures()
		

	def __applyEnergyCalibration(self):
		'''Apply the current energy calibration to the current data
		simple ensures that the energy values are correct - does not convert data or show data
		'''
		# is there data?
		if self.caesar_image != "":
			nchan = self.caesar_image.shape[1]
		elif self.spectrum != "":
			nchan = len(self.spectrum)
		else:
			nchan = 0
		# if there is, convert it
		if nchan>0:
			channels = np.arange(nchan)
			self.energy = self.energy_calibration[0] + self.energy_calibration[1]*channels + self.energy_calibration[2]*channels**2
     			# to avoid divide by zero error, fake the zero/negative values in energy
     			self.energy[np.nonzero(self.energy<0.0000001)]=0.0000001		
		else:
			print "No energy data to convert"
	
		
	def __applyAngleCalibration(self):
		'''Apply the current angle calibration to the current nominal angles
		simple ensures that the tth values are correct - does not convert data or show data
		'''
		if self.caesar_tth_nominal != "":
			tth_offset = (self.caesar_tth_nominal*self.angle_calibration[0]) + self.angle_calibration[1]
			self.caesar_tth = self.caesar_tth_nominal + tth_offset
		else:
			print "No caesar angles to convert"
		if self.spectrum_tth_nominal != "":
			tth_offset = (self.spectrum_tth_nominal*self.angle_calibration[0]) + self.angle_calibration[1]
			self.spectrum_tth = self.spectrum_tth_nominal + tth_offset
		else:
			print "No spectrum angle to convert"				


	def setDetector(self, detector="", dataname=""):
		''' 
			If at Soleil, set the detector 
			setDetector(detector="i03-C-CX1/dt/dtc-mca_xmap.1", dataname="channel02")
		'''
		if SOLEIL and detector!="":
			self.__DP = PyTango.DeviceProxy(detector)
			self.__dataname = dataname
			print "detector is : %s, dataname is %s" % (detector, dataname)
		else:
			self.__DP = ""
			self.__dataname = ""
			print "no detector set"
		# in both cases, reinitialise the spectrum
		self.spectrum = ""


	def setExperimentdir(self, experimentdir=""):
		''' set the data directory '''
		if experimentdir!="":
			if os.path.isdir(experimentdir):
				self.experimentdir = experimentdir
				print "experiment directory is %s" % experimentdir
			else:
				print "%s is not a valid directory - try again please..."
				self.experimentdir = ""
		elif not(SOLEIL):
			# not at SOLEIL, so can use GUI
			print "select experiment directory - you may have to search for the dialog window!"
			sys.stdout.flush()
			self.experimentdir = tkFileDialog.askdirectory()
			self.experimentdir = os.path.normpath(self.experimentdir)
			print "experiment directory is %s" % self.experimentdir
		else:
			self.experimentdir = ""
			print "no experiment directory - set using setExperimentdir()"
		# in both cases, reinitialise the image
		self.caesar_image = ""
		
		
	def setDatadir(self, datadir=""):
		'''Set or select a data directory'''
		if datadir == "":
			if SOLEIL:
				if self.experimentdir!="":
					# list the experiment directory
					ldir = os.listdir(self.experimentdir)
					dlist = [] # list of subdirectories - Caesar scans
					for x in ldir:
						if os.path.isdir(self.experimentdir + os.sep + x):
							dlist.append(x)
					dlist.sort(reverse=True)
					# close all matplotlib images - does this help?
					pylab.close("all")
					# open the GUI list
					self.__gui.buildGUI(dlist)
					# after this is closed, get the selection
					selection = self.__gui.get_selection()
					self.datadir = self.experimentdir + os.sep + selection
					print "data directory is %s" % self.datadir
				else:
					print "You must first set the experiment directory using setExperimentdir()"
			else: 
				# not at SOLEIL - therefore can use tkFileDialog
				print "select data directory - you may have to search for the dialog window!"
				sys.stdout.flush()
				self.datadir = tkFileDialog.askdirectory(initialdir=self.experimentdir)
				self.datadir = os.path.normpath(self.datadir)
				psep = self.datadir.rfind(os.sep)
				# update the experiment dir as the parent dir of the datadir
				self.experimentdir = self.datadir[0:psep]
				print "data directory is %s" % self.datadir
		else:
			if os.path.isdir(datadir):
				self.datadir = datadir
				print "data directory is %s" % self.datadir
			else:
				print "%s is not a valid directory - try again please..."	
				self.datadir = ""
				
	def loadPickle(self):
		'''Load data from a Python .pickle file (used for the home-made Caesar)'''
		self.setExperimentdir(self.experimentdir)
		if self.experimentdir!="":
			# select a data directory
			self.setDatadir()
			if self.datadir!="":
				if SOLEIL:
					# choose a text file in this directory
					# list the data directory
					ldir = os.listdir(self.datadir)
					flist = [] # list of text files 
					for x in ldir:
						if x.endswith(".pickle"):
							flist.append(x)
					flist.sort(reverse=True)
					# close all matplotlib images - does this help?
					pylab.close("all")
					# open the GUI list
					self.__gui.buildGUI(flist)
					# after this is closed, get the selection
					selection = self.__gui.get_selection()
					filename = self.datadir + os.sep + selection
				else:
					filename = tkFileDialog.askopenfilename(initialdir=self.datadir)
				# now open the file
				try:
					tmp = pickle.load(open(filename, 'rb'))
					if len(tmp)==5:
						# version 0
						[self.caesar_image, self.caesar_tth, self.energy, scanangles, scangaps] = tmp
						print "warning - this is an original pickle file, and has some missing info"
						print "trying to guess the underlying nominal caesar angles"
						start=np.round(self.caesar_tth[0])
						stop=np.round(self.caesar_tth[-1])
						steps = self.caesar_image.shape[0]
						print "looks like from %d to %d degrees in %d steps" % (start,stop,steps)						
						self.caesar_tth_nominal = np.linspace(start, stop, steps)
					elif len(tmp)==6:
						print "thi looks like a new pickle file"
						[self.caesar_image, self.caesar_tth_nominal, self.caesar_tth, self.energy, scanangles, scangaps] = tmp
					else:
						print "this is another format, to be defined - not loading"
						return						
						
					# bin noisy data?
					print "for broad peaks, can bin in energy to reduce noise"
					check = str(raw_input("bin data x2 to reduce noise? y/[n]  : "))		
					if check == 'y' or check == 'yes':
						self.caesar_image = self.caesar_image[:, 0::2] + self.caesar_image[:, 1::2]
						self.energy = (self.energy[0::2]+self.energy[1::2])/2
					else:
						print "not binning data"
					# correct background?
					print "Can try to correct the background -- EXPERIMENTAL!!! --"
					check = str(raw_input("correct background? y/[n]  : "))		
					if check == 'y' or check == 'yes':
						self.__backgroundCorrection()
					else:
						print "not correcting background - you can use the correctBackground() method later"

					self.__convertImage()
					self.__correctCaesarSpectrum()
					self.__showImage()
					# export the summed profile
					self.__exportGSAS_combined()
				except:
					print "problem reading file %s" % filename

										
	def loadCaesar(self):
		'''select a data directory, load, convert and show'''
		self.setExperimentdir(self.experimentdir)
		self.setDatadir()
		if self.datadir != "":
			# if at Soleil, redraw the figures which are closed for the GUI
			if SOLEIL:
				self.__showSpectrum()
			print "loading and processing scan"
			self.__getImage()
			self.__convertImage()
			self.__correctCaesarSpectrum()
			self.__showImage()
			# export the summed profile
			self.__exportGSAS_combined()
		else:
			print "no experiment directory set - use setExperimentdir()"



	def __getSpectrum(self):
		''' get the latest spectrum from the detector '''
		if self.__dataname!="" and self.__DP!="":
			print "read the current spectrum, correct Caesar angle" 
			self.spectrum = getattr(self.__DP, self.__dataname)
			tth = self.__APcaesar.read().value
			self.spectrum_tth_nominal = tth	
			# calculate the true angle
			self.__applyAngleCalibration()
			# recalculate the channel energies
			self.__applyEnergyCalibration()
		else:
			print "No detector / data defined !"
			self.spectrum = ""
			self.spectrum_tth = ""
		# reset the converted spectrum
		self.spectrum_dspy = ""
		self.spectrum_dspx = ""
		
	def loadSpectrum(self):
		'''open a spectrum from a txt file in data directory'''
		if SOLEIL:
			if self.experimentdir!="":
				# select a data directory
				self.setDatadir()
				if self.datadir!="":
					# choose a text file in this directory
					# list the data directory
					ldir = os.listdir(self.datadir)
					flist = [] # list of text files 
					for x in ldir:
						if x.endswith(".txt"):
							flist.append(x)
					flist.sort(reverse=True)
					# close all matplotlib images - does this help?
					pylab.close("all")
					# open the GUI list
					self.__gui.buildGUI(flist)
					# after this is closed, get the selection
					selection = self.__gui.get_selection()
					filename = self.datadir + os.sep + selection
			else:
				self.datadir = ""
				print "no experiment directory set - use setExperimentdir()"					
					
		else:
			# not at SOLEIL - therefore can use tkFileDialog
			print "select spectrum .txt datafile - you may have to search for the dialog window!"
			sys.stdout.flush()
			filename = tkFileDialog.askopenfilename(initialdir=self.experimentdir, filetypes=[("text files", ".txt")])
			filename = os.path.normpath(filename)
		try:
			# Now read the spectrum - same at SOLEILor at home
			self.spectrum = np.loadtxt(filename)
			# read the angle
			tthvalue = filename[-10:-4]
			if tthvalue[0]=='_':
				tthvalue = tthvalue[1::]
			# this is the nominal value - convert it
			self.spectrum_tth_nominal = float(tthvalue)
			self.__applyAngleCalibration()
			self.__applyEnergyCalibration()
			self.__convertSpectrum()
			self.__showSpectrum()
		except:
			print "Failed to read %s" % filename			
		
		
	def __getImage(self):
		''' read in an image from the Caesar acquistion text files'''
		if self.datadir!="":
			# get the name of the scan:
			parts = self.datadir.split(os.sep)
			# strip out the zero length parts
			parts[:] = [elem for elem in parts if len(elem)!=0]
			dataname = parts[-1]
			# get the length of the name
			namelength = len(dataname)
			
			# get a list of all the text files
			filelist = glob.glob(self.datadir+os.sep+dataname+"*txt")
			# need to go through this list, understanding the filenames
			fnames = [elem.split(os.sep)[-1] for elem in filelist]
			# filename format: dirname/dirname_%04d-[_%0.3f.txt / %0.3.txt]
			# values are the step and the actuator position
			#print filelist[0]
			# read a first spectrum
			a = np.loadtxt(filelist[0])
			# define the image
			image = np.zeros([len(fnames), len(a)])
			# will record the nominal tth angles
			tth_nom = np.zeros(len(fnames))
			
			# read all the steps
			step = np.zeros(len(fnames))
			sys.stdout.write("reading files")
			for ii in range(len(fnames)):
				# which step is this?
				step[ii] = int(fnames[ii][(namelength+1):(namelength+5)])
				# read the spectrum
				image[step[ii], :] = np.loadtxt(filelist[ii])
				# read the angle from the filename
				tthvalue = fnames[ii][-10:-4]
				if tthvalue[0]=='_':
					tthvalue = tthvalue[1::]
				tth_nom[step[ii]] = float(tthvalue)
				# display something
				sys.stdout.write(".")
				sys.stdout.flush()
				if np.mod(ii, 5)==4:
					sys.stdout.write("\b\b\b\b\b")
			sys.stdout.write("\nfinished reading\n")
			# return the image
			self.caesar_tth_nominal = tth_nom
			self.caesar_image = image
			self.caesar_image_original = ""
			# apply the angle_calibration to the tth_angles
			self.__applyAngleCalibration()
			# make sure the energy calibration is up to date
			self.__applyEnergyCalibration()
			# avoid strange image behaviour in timescan case (all tth values the same)
			if self.caesar_tth[-1] == self.caesar_tth[0]:
				print "### TWEAK CONSTANT ANGLES TO DISPLAY DATA CORRECTLY ###"
				self.caesar_tth[-1] = self.caesar_tth[-1] + 0.00001
			# apply the gauge volume correction
			print "If sample is larger than gauge volume, should correct for gauge volume as f(angle)"
			check = str(raw_input("Correct for gauge volume? y/[n]  : "))		
			if check == 'y' or check == 'yes':
				self.__gaugeVolumeCorrection()
			else:
				print "not correcting gauge volume"
			# bin noisy data?
			print "for broad peaks, can bin in energy to reduce noise"
			check = str(raw_input("bin data x2 to reduce noise? y/[n]  : "))		
			if check == 'y' or check == 'yes':
				self.caesar_image = self.caesar_image[:, 0::2] + self.caesar_image[:, 1::2]
				self.energy = (self.energy[0::2]+self.energy[1::2])/2
			else:
				print "not binning data"
			print "Can try to correct the background -- EXPERIMENTAL!!! --"
			check = str(raw_input("correct background? y/[n]  : "))		
			if check == 'y' or check == 'yes':
				self.__backgroundCorrection()
			else:
				print "not correcting background - you can use the correctBackground() method later"

		else:
			print "No data directory defined !"
			self.caesar_image=""
			self.caesar_tth=""

##### DATA PROCESSING FUNCTIONS #########

	def __gaugeVolumeCorrection(self):
		'''correct for gauge volume proportional to 1/sin(tth)'''
		for ii in range(len(self.caesar_tth)):
			self.caesar_image[ii, :] = self.caesar_image[ii, :] * np.sin(self.caesar_tth[ii]*np.pi/180)
		print "Applied sin(two theta) correction for gauge volume as f(angle)"

	def correctBackground(self, filtsize=[3,25], step=100, negweight=3):
		'''Try correcting the background of a Caesar acquistion'''
		if self.caesar_image != "":
			print "correcting the current Caesar data"
			self.__backgroundCorrection(filtsize, step, negweight)
			self.__convertImage()
			self.__correctCaesarSpectrum()
			self.__showImage()
		else:
			print "Load Caesar data first!"

	def undoCorrectBackground(self):
		'''Try correcting the background of a Caesar acquistion'''
		if self.caesar_image_original != "":
			print "Undo the background correction"
			self.caesar_image = self.caesar_image_original
			self.__convertImage()
			self.__correctCaesarSpectrum()
			self.__showImage()
		else:
			print "No original data found!"

	def __backgroundCorrectionOLD(self, fraction=0.05):
		'''A background correction using the idea that the ADX spectra have constant background'''
		imcor = self.caesar_image * 1.0		
		bkg = np.zeros(self.caesar_image.shape[1])
		# take the 5th percentile value as background (less noisy that min)
		ndx = np.round(self.caesar_image.shape[0]*fraction)
		for ii in range(self.caesar_image.shape[1]):
			bkg[ii] = np.sort(imcor[:, ii])[ndx]
			imcor[:, ii] = imcor[:,ii]-bkg[ii]
		self.background = bkg
		self.caesar_image_original = self.caesar_image
		self.caesar_image = imcor
		# can force the background calculated to match the one derived from the sum of the caesar
		# can force negative values to 0, but this is a bad sign
		# can fit something of the form C + A exp (-Bx**2), where x is sin(theta)/lambda, which should describe 
		# the form factor very roughly  - should be 9 params... per element..

	def __backgroundCorrection(self, filtsize=[3,25], step=100, negweight=3):
		'''A background correction that make an approximate background using profiles from
		summing the caesar image in the two directions with smoothing.  The approximate background
		is then fitted to the each energy profile, with a term that allows some distortion of the shape.  
		The least squares fitting is modifiedto allow peaks above the data, but to penalise negative values 
		(i.e. background value > data)
		The fitting results are smoothed, and used to produce a background function which is subtracted.
		Arguments are the median filter size for smoothing, and a step size (in detector channels) 
		to smooth out stripes if diffraction peaks move by a step greater than the peak width, 
		and the extra weight given to negative values when fitting the background
		It seems to work pretty well with the defaults'''

		# what ratio to use?
		im = self.caesar_image*1.
		nrows, ncols = self.caesar_image.shape

		# filter to reduce noise - removes spikes from liquid data - removes peaks?
		# in case the filter is large, we can use a step size > 1
		filtstep = np.ceil(np.array(filtsize)/10.)
		filtstep = [int(ff) for ff in filtstep]
		im2 = simple_medfilt(im, filtsize, filtstep)
		bkg_area = make_bkg(im2, step)

		# the bkg_area is a good guess...
		# can it be tweaked to fit better?
		out = np.zeros((nrows, 2))
		bkg_improved = np.zeros(bkg_area.shape)
		for ii in range(nrows):	
			prof = im2[ii, :]
			bkg = bkg_area[ii, :]	

			def optfunc(x):
				# function for fitting, including the extra weight for negative values
				bkg_mod = bkg*(x[0] + np.arange(ncols)*x[1]) 
				# dif = background - data
				dif = prof - bkg_mod
				# modify the differences - want a large penalty for negatives, and small (-> 0) for large values
				pos = np.nonzero(dif>1)
				dif[pos] = 1 + np.log(dif[pos])
				neg = np.nonzero(dif<0)
				dif[neg] = dif[neg]*negweight
				return dif

			xfit = leastsq(optfunc, [1, 0])[0]
			out[ii, :] = xfit
		# smooth the fit results
		out_s = np.zeros(out.shape)
		tck = interpolate.splrep(np.arange(nrows), out[:,0], s=nrows)
		out_s[:,0] = interpolate.splev(np.arange(nrows), tck, der=0)
		tck = interpolate.splrep(np.arange(nrows), out[:,1], s=nrows)
		out_s[:,1] = interpolate.splev(np.arange(nrows), tck, der=0)
		# build the final background		
		for ii in range(nrows):
			xfit = out_s[ii, :]
			bkg_improved[ii, :] = bkg_area[ii, :]*(xfit[0] + np.arange(ncols)*xfit[1])
		# apply this to the original, unsmoothed image
		imnew = im-bkg_improved
		# get rid of stupid tiny values - does silly things to log()
		imnew[np.nonzero(abs(imnew)<0.0000001)]=0
		# return
		self.background = bkg
		self.caesar_image_original = self.caesar_image
		self.caesar_image = imnew
		
	def __convertSpectrum(self):
		''' convert data from energy to dspacing and Q'''
		# convert the spectrum, if available
		if self.spectrum != "":
			# current theta angle, radians
			theta = (self.spectrum_tth/2)*np.pi/180
			# current data in dsp and Q		
			dsp = 12.398 / (2*np.sin(theta)*self.energy)
			Q = 2*np.pi/dsp
			# limits for interpolation
			dmax = 12.398 / (2*np.sin(theta)*10) # current angle, lowest energy
			dmin = 12.398 / (2*np.sin(theta)*self.energy[-1]) # current angle, highest energy
			dspgrid = np.linspace(dmin, dmax, 4096)
			Qmin = 2*np.pi/dmax
			Qmax = 2*np.pi/dmin
			Qgrid = np.linspace(Qmin, Qmax, 4096)    
			# interpolate - invert dsp and data for ascending order
			self.spectrum_dspy = np.interp(dspgrid, dsp[::-1], self.spectrum[::-1], 0, 0)
			self.spectrum_dspx = dspgrid
			self.spectrum_qy = np.interp(Qgrid, Q, self.spectrum, 0, 0)
			self.spectrum_qx = Qgrid
		else:
			self.spectrum_dspy = ""
			self.spectrum_dspx = ""
			self.spectrum_qy = ""
			self.spectrum_qx = ""

	def __convertImage(self):
		''' convert data from energy to dspacing and Q '''
		# convert the spectrum, if available
		if self.caesar_image != "":
     			# determine the theta values
			theta = self.caesar_tth/2
			# preallocate an output image
			image2 = np.zeros([self.caesar_image.shape[0], 4096])
			image3 = np.zeros([self.caesar_image.shape[0], 4096])
			# dspacing limits for the whole acquistion
			dmax = 12.398 / (2*np.sin(theta[0]*np.pi/180)*10) # lowest angle, lowest energy
			dmin = 12.398 / (2*np.sin(theta[-1]*np.pi/180)*self.energy[-1]) # highest angle, highest energy
			dspgrid = np.linspace(dmin, dmax, 4096)
			Qmin = 2*np.pi/dmax
			Qmax = 2*np.pi/dmin
			Qgrid = np.linspace(Qmin, Qmax, 4096)
			# convert line by line
			for ii in range(self.caesar_image.shape[0]):
				# energy profile
				prof = self.caesar_image[ii, :]
				prof = prof[::-1]	
				# as d spacing				
				dsp = 12.398 / (2*np.sin(theta[ii]*np.pi/180)*self.energy)
				dsp = dsp[::-1] # increasing
				image2[ii, :] = np.interp(dspgrid, dsp, prof, 0, 0)
				# as Q - flip profiles for increasing
				Q = 2*np.pi/dsp
				image3[ii, :] = np.interp(Qgrid, Q[::-1], prof[::-1], 0, 0)
			# build the (naive) combined dsp profile
			dspprofile = image2.sum(0)
			dspprofile_n = (image2!=0).sum(0)
			dspprofile = dspprofile/dspprofile_n
			dspprofile[np.nonzero(np.isnan(dspprofile))] = 0 # remove nans
			print "correcting combined profile for number of datapoints"
			# build the (naive) combined Q profile
			Qprofile = image3.sum(0)
			Qprofile_n = (image3!=0).sum(0)
			Qprofile = Qprofile/Qprofile_n			
			Qprofile[np.nonzero(np.isnan(Qprofile))] = 0 # remove nans

			self.caesar_image_dsp = image2
			self.caesar_image_dspx = dspgrid
			self.caesar_image_dsp_profile = dspprofile
			self.caesar_image_Q = image3
			self.caesar_image_Qx = Qgrid
			self.caesar_image_Q_profile = Qprofile			
		else:
			self.caesar_image_dsp = ""
			self.caesar_image_dspx = ""
			self.caesar_image_dsp_profile = ""
			self.caesar_image_Q = ""
			self.caesar_image_Qx = ""
			self.caesar_image_Q_profile = ""			

	def __correctCaesarSpectrum(self):
		'''Correct the intensities in the combined profiles by fitting 
		Gaussian function to the converted images'''
		# correct the spectrum using gaussian fitting technique
		if self.caesar_image_Q  != "":
			print "Correcting intensities in the combined profiles"
			# fit a gaussian to the simple estimate of the spectrum
			xdata = self.energy
			ydata = self.caesar_image.sum(0)
			optfunc = lambda x: (x[0]*np.exp(-((xdata-x[1])**2)/(2*x[2]**2))) + x[3] - ydata
			x0 = [ydata.max()-ydata.min(), (xdata[0]+xdata[-1])/2, (xdata[0]+xdata[-1])/8, ydata.min()]
			xfit = leastsq(optfunc, x0)[0]
			e_cen = xfit[1]
			e_sigma = xfit[2]
			# will use this approximation to fit intensities 
			# convert to wavelength
			l_cen = 12.398/e_cen
			l_sigma = (12.398/(e_cen-(e_sigma/2)))-(12.398/(e_cen+(e_sigma/2)))
			# allocate the ouput
			self.caesar_image_Q_profile_cor = np.zeros(self.caesar_image_Q_profile.shape)
			self.caesar_image_dsp_profile_cor = np.zeros(self.caesar_image_dsp_profile.shape)
			# go through the columns of the two images
			xdata = self.caesar_tth/2
			nsigma = 2. # fit up to this many sigmas outside the theta range
			for ii in range(self.caesar_image_Q.shape[1]):
				# for Q
				prof = self.caesar_image_Q[:,ii]
				q = self.caesar_image_Qx[ii]
				d = 2*np.pi/q
				th_cen = (180/np.pi)*np.arcsin(l_cen/(2*d))	
				th_sigma = np.abs((180/np.pi)*np.arcsin((l_cen-(l_sigma/2))/(2*d)) - (180/np.pi)*np.arcsin((l_cen+(l_sigma/2))/(2*d)))
				# don't try to fit if we are too far from the peak				
				if th_cen<(xdata.max()+nsigma*th_sigma) and th_cen>(xdata.min()-nsigma*th_sigma):
					# fixed parameters in the Gaussian
					bkg = 0
					sigma = th_sigma
					pos = th_cen
					optfunc = lambda x: (x[0]*np.exp(-((xdata-pos)**2)/(2*sigma**2))) + bkg - prof
					xfit = leastsq(optfunc, prof.max())[0]
					self.caesar_image_Q_profile_cor[ii] = xfit[0]*sigma # integral gaussian propotional to a x sigma
				# for dsp
				prof = self.caesar_image_dsp[:,ii]
				d = self.caesar_image_dspx[ii]
				th_cen = (180/np.pi)*np.arcsin(l_cen/(2*d))	
				th_sigma = np.abs((180/np.pi)*np.arcsin((l_cen-(l_sigma/2))/(2*d)) - (180/np.pi)*np.arcsin((l_cen+(l_sigma/2))/(2*d)))
				# don't try to fit if we are too far from the peak				
				if th_cen<(xdata.max()+nsigma*th_sigma) and th_cen>(xdata.min()-nsigma*th_sigma):
					# fixed parameters in the Gaussian
					bkg = 0
					sigma = th_sigma
					pos = th_cen
					optfunc = lambda x: (x[0]*np.exp(-((xdata-pos)**2)/(2*sigma**2))) + bkg - prof
					xfit = leastsq(optfunc, prof.max())[0]
					self.caesar_image_dsp_profile_cor[ii] = xfit[0]*sigma # integral gaussian propotional to a x sigma				
			# rescale roughly
			ratio = self.caesar_image_Q_profile.sum()/self.caesar_image_Q_profile_cor.sum()
			self.caesar_image_Q_profile_cor = self.caesar_image_Q_profile_cor * ratio
			ratio = self.caesar_image_dsp_profile.sum()/self.caesar_image_dsp_profile_cor.sum()
			self.caesar_image_dsp_profile_cor = self.caesar_image_dsp_profile_cor * ratio


####### FIGURE HANDLING FUNCTIONS ############

	def __updateFigures(self):
		'''Calculate and show results'''
		# update figures if needed
		if self.caesar_image != "":
			self.__convertImage()
			self.__showImage()
		if self.spectrum != "":
			self.__convertSpectrum()
			self.__showSpectrum()

	def __showSpectrum(self, clear_fig=True):
		''' plot the spectrum if we have one '''
		fig = pylab.figure(100) # use figure 100 for the line profile
		fig.canvas.manager.set_window_title('Spectra')
		# position the figure top-right
		#pylab.get_current_fig_manager().window.wm_geometry("650x526+1000+20")
		if self.spectrum != "":
			if clear_fig:
				pylab.clf()
			# uppper figure - spectrum in energy
			pylab.subplot(2,1,1)
			pylab.plot(self.energy, self.spectrum, label='Raw spectrum, %0.3f deg' % self.spectrum_tth)
			pylab.legend()
			pylab.xlabel('energy / keV')
			pylab.ylabel('intensity / counts')

		else:
			print "No spectrum data yet"

		if self.spectrum_dspx != "":
			# lower figure - spectrum in dspacing
			pylab.subplot(2,1,2)
			if self.plotdsp:
				pylab.plot(self.spectrum_dspx, self.spectrum_dspy, label='Converted spectrum, %0.3f deg' % self.spectrum_tth)
				pylab.legend()
				pylab.xlabel('d spacing / Angstroms')
				pylab.ylabel('intensity / AU')
			else:
				pylab.plot(self.spectrum_qx, self.spectrum_qy, label='Converted spectrum, %0.3f deg' % self.spectrum_tth)
				pylab.legend()
				pylab.xlabel('Q / Angstroms-1')
				pylab.ylabel('intensity / AU')				
		else:	
			print "No converted spectrum yet"

		# if possible, raise the window
		if callable(getattr(fig.canvas.manager.window, 'tkraise', None)):
			fig.canvas.manager.window.tkraise()
		# otherwise, can activateWindow, but lose the focus
		elif callable(getattr(fig.canvas.manager.window, 'activateWindow', None)):
			fig.canvas.manager.window.activateWindow()


	def __showImage(self):
		''' show the image if we have one '''
		fig = pylab.figure(101) # use figure 101 for the line profile
		fig.canvas.manager.set_window_title('Caesar acquisition')
		# position the figure below top-right
		#tmp = pylab.get_current_fig_manager().window.wm_geometry("650x526+1000+450")
		# still need to add extents, axis labels, etc
		if self.caesar_image != "":
			pylab.clf()
			pylab.subplot(3,1,1)
			if self.logscale==True:
				pylab.imshow(np.log(self.caesar_image), extent=[self.energy[0], self.energy[-1], self.caesar_tth[-1], self.caesar_tth[0]])
			else:
				pylab.imshow(self.caesar_image, extent=[self.energy[0], self.energy[-1], self.caesar_tth[-1], self.caesar_tth[0]])
			pylab.xlabel('energy / keV')
			pylab.ylabel('two theta / degrees')
			pylab.axis("tight")
		else:
			print "No Caesar data yet"

		if self.caesar_image_dsp!="":
			pylab.subplot(3,1,2)
			if self.plotdsp:
				if self.logscale==True:
					pylab.imshow(np.log(self.caesar_image_dsp), extent=[self.caesar_image_dspx[0], self.caesar_image_dspx[-1], self.caesar_tth[-1], self.caesar_tth[0]])
				else:
					pylab.imshow(self.caesar_image_dsp, extent=[self.caesar_image_dspx[0], self.caesar_image_dspx[-1], self.caesar_tth[-1], self.caesar_tth[0]])
				pylab.xlabel('d spacing / Angstroms')
				pylab.ylabel('two theta / degrees')
			else:
				if self.logscale==True:
					pylab.imshow(np.log(self.caesar_image_Q), extent=[self.caesar_image_Qx[0], self.caesar_image_Qx[-1], self.caesar_tth[-1], self.caesar_tth[0]])
				else:
					pylab.imshow(self.caesar_image_Q, extent=[self.caesar_image_Qx[0], self.caesar_image_Qx[-1], self.caesar_tth[-1], self.caesar_tth[0]])
				pylab.xlabel('Q / Angstroms-1')
				pylab.ylabel('two theta / degrees')				
			pylab.axis("tight")

			pylab.subplot(3,1,3)
			if self.plotdsp:
				pylab.plot(self.caesar_image_dspx, self.caesar_image_dsp_profile, label='Integrated profile')
				pylab.plot(self.caesar_image_dspx, self.caesar_image_dsp_profile_cor, 'r', label='Corrected intensities')
				pylab.legend()
				pylab.xlabel('d spacing / Angstroms')
				pylab.ylabel('Intensity')
			else:
				pylab.plot(self.caesar_image_Qx, self.caesar_image_Q_profile, label='Integrated profile')
				pylab.plot(self.caesar_image_Qx, self.caesar_image_Q_profile_cor, 'r', label='Corrected intensities')
				pylab.legend()
				pylab.xlabel('Q / Angstroms-1')
				pylab.ylabel('Intensity')			
			pylab.axis("tight")

		else:
			print "No converted Caesar data yet"
		# if possible, raise the window
		if callable(getattr(fig.canvas.manager.window, 'tkraise', None)):
			fig.canvas.manager.window.tkraise()
		# otherwise, can activateWindow, but lose the focus
		elif callable(getattr(fig.canvas.manager.window, 'activateWindow', None)):
			fig.canvas.manager.window.activateWindow()

######## CAESAR DATA FUNCTIONS  ###########
	# This is not useful if we use linear interpolation for rebinning	

	def setRebinRange(self, rebin_range=0):
		'''set the energy range for rebinning Caesar data to ADX'''
		self.caesar_rebin_range = rebin_range
		print "Caesar rebin range is %0.3f keV" % self. caesar_rebin_range
		print "Use setRebinRange() to modify"
		
	def showADXSpectrum(self, set_energy=40, clear_fig=True):
		''' show an ADX spectrum extracted from the caesar data, with rebinning '''
		# this should do the actual Caesar rebinning of adjacent channels to improve the spectrum...
		if self.caesar_image!="":
		
			# find the channel closest to the set_energy
			tmp = abs(self.energy-set_energy)
			channel = np.nonzero(tmp==min(tmp))[0]
			true_energy = self.energy[channel]
			
			# get the simple profile
			ADX = self.caesar_image[:, channel].squeeze()
			tth_values = self.caesar_tth
			theta = self.caesar_tth*np.pi/360

			# check the caesar_rebin_range
			dtheta = theta[1]-theta[0]
			# worst case required dE
			dE = set_energy*dtheta/theta[0]
			if dE > self.caesar_rebin_range:
				print "Using %0.2f keV rebin_range to ensure no gaps" % dE
				rebin_range = dE
			else:
				print "Using specified %0.2f keV rebin_range" % self.caesar_rebin_range
				rebin_range = self.caesar_rebin_range
			print "To modify this parameter, use setRebinRange method"

			# select all channels within the energy range specified
			tmp = abs(self.energy-(set_energy-(rebin_range/2)))
			channelA = np.nonzero(tmp==min(tmp))[0]-1
			tmp = abs(self.energy-(set_energy+(rebin_range/2)))
			channelB = np.nonzero(tmp==min(tmp))[0]+1	
			
			# extract the relevent block of data
			blockint = self.caesar_image[:, channelA:channelB]
			blocklambda = 12.398/self.energy[channelA:channelB]
			
			# recalculate dspacings and equivilent thetas
			grid = np.meshgrid(np.r_[blocklambda], np.r_[theta])
			blockdsp = grid[0]/(2*np.sin(grid[1]))
			newlambda = 12.398/set_energy
			newtheta = np.arcsin(newlambda/(2*blockdsp))
			blocktth = newtheta*360/np.pi
			# grid for rebinning data
			tth_grid = np.linspace(tth_values[0], tth_values[-1], self.caesar_rebin_nbins)
			ptth = blocktth.flatten(1)
			pint = blockint.flatten(1)
			ii = np.argsort(ptth)
			ptth = ptth[ii]
			pint = pint[ii]
			
			# rebin - simple linear interpolation?
			# may not be the best solution for noise
			if self.caesar_rebin_method=='linear':
				print "Resampling data using linear interpolation"
				print "Note for this case, no benefit to using a wider rebin range... "
				ADX2 = np.round(np.interp(tth_grid, ptth, pint, 0, 0)) # round to whole numbers
			elif self.caesar_rebin_method=='rebin':
				print "Rebinning with range %0.1f keV and %d bins" % (self.caesar_rebin_range, self.caesar_rebin_nbins)
				ADX2 = np.zeros(tth_grid.shape)
				hbinw = (tth_grid[1]-tth_grid[0])/2
				for ii in range(len(tth_grid)):
					ndx = np.nonzero(np.logical_and(ptth>(tth_grid[ii]-hbinw),  ptth<(tth_grid[ii]+hbinw)))    
					if len(ndx[0])!=0:					
						ADX2[ii] = pint[ndx].mean()
			else:
				print "rebin method [%s] not recognised!" % self.caesar_rebin_method
				return
						
			# add the current profiles to self
			self.caesar_ADXx = tth_grid
			self.caesar_ADXy = ADX2
			self.caesar_ADXenergy = set_energy
			
			# automatically export the ADX spectrum as a GSAS file
			self.exportGSAS_ADX()

			fig = pylab.figure(102) # use figure 102 for the extracted profile
			fig.canvas.manager.set_window_title('Extracted ADX spectrum')
			if clear_fig:
				pylab.clf()
			pylab.plot(tth_values, ADX, 'o', label='single ADX spectrum at %0.1f keV' % true_energy)
			pylab.plot(tth_grid, ADX2, '-x', label='rebinned ADX spectrum at %0.1f keV' % set_energy)
			pylab.xlabel('two theta / deg')
			pylab.ylabel('intensity / AU')
			pylab.legend()
			# if possible, raise the window
			if callable(getattr(fig.canvas.manager.window, 'tkraise', None)):
				fig.canvas.manager.window.tkraise()
			# otherwise, can activateWindow, but lose the focus
			elif callable(getattr(fig.canvas.manager.window, 'activateWindow', None)):
				fig.canvas.manager.window.activateWindow()
		else:
			print "No Caesar data yet"


	def showEDXSpectrum(self, caesar_angle=0, clear_fig=True):
		''' Pick an EDX spectrum from the current caesar'''
		if self.caesar_tth != "" and caesar_angle>=self.caesar_tth[0] and caesar_angle<=self.caesar_tth[-1]:
			# find the profile closest to the requested angle
			#angles = np.linspace(self.caesar_tth_range[0], self.caesar_tth_range[1], self.caesar_image.shape[0], True)
			angles = self.caesar_tth
			
			tmp = abs(angles - caesar_angle)
			step = np.nonzero(tmp==min(tmp))[0]
			self.spectrum = self.caesar_image[step[0], :]
			self.spectrum_tth = angles[step][0]

			self.__convertSpectrum()
			self.__showSpectrum(clear_fig)
		else:
			print "can't find this angle in the current Caesar - load data?"
	


	def exportGSAS_EDX(self, filename="psiche", directory="/nfs/ruche-psiche/psiche-soleil/tempdata/com-psiche/test_directory"):
		'''Export the current energy dispersive spectrum as a .gsas file
			Exports also a text file (.x_y) in two column format '''

		# get the next filename - if a name is supplied or at SOLEIL (no tk)
		if (filename!="psiche") or SOLEIL:
			# use the given arguments
			myfilename = findNextFileName(directory+os.sep+filename, "gsas")
		else: # not at SOLEIL, and no filename supplied, so ask user via tk
			# use the GUI dialog
			print "enter filename to export EDX spectrum - you may have to search for the dialog window!"
			sys.stdout.flush()
			myfilename = tkFileDialog.asksaveasfilename(initialdir=self.experimentdir, filetypes=[("gsas files", ".gsas")])
			if myfilename==():
				print "no file name given - can\'t save spectrum!"
			else:
				myfilename = os.path.normpath(myfilename)

		# here we will export the current spectrum to GSAS		
		nchan = len(self.spectrum)
		lrec = 5 # data points per row for ESD
		nrec = np.ceil(nchan/lrec)
		calib = self.energy_calibration

		# write a gsas file in the correct format...
		# open the file
		myfile = open(myfilename, 'w')
		# looks like we want 80 characters, plus \r\n 
		# value, err, value, err, etc
		# use the filename for the title
		myline = filename.ljust(80)+"\r\n"
		myfile.write(myline)
		# write the calibration line
		myline = "BANK 1 {} {} CONST {:0.3f} {:0.5f} {:0.12f} 0 ESD".format(nchan-1, nrec-1, calib[0], calib[1], calib[2])
		myline = myline.ljust(80)+"\r\n"    
		myfile.write(myline)
		# write the data...
		for ii in range(int(nrec)):
			myline = ""
			for jj in range(lrec):
				ndx = (ii*lrec)+jj
				if ndx<nchan:
					mynum = "{:d},".format(int(self.spectrum[ndx])).rjust(8)
					myerr = "{:d},".format(int(np.sqrt(self.spectrum[ndx]))).rjust(8)
					myline = myline + mynum + myerr
			myline = myline.ljust(80)+"\r\n"
			myfile.write(myline)
		myfile.close()
		print "gsas file: \n  %s\nwritten" % myfilename 
		
		# write a text file as well
		textfilename = myfilename[0:-5] + ".x_y"
		# open the file
		myfile = open(textfilename, 'w')
		# do not write a header of CowderCell		
		#myfile.write(filename + "\r\n")
		for ii in range(nchan):
			myfile.write("{:.3f}".format(self.energy[ii]) + " " + "{:d}".format(int(self.spectrum[ii])) + "\r\n")
		myfile.close()
		print "txt file: \n  %s\nwritten" % textfilename 

	def exportGSAS_ADX(self):
		'''Export the current energy dispersive spectrum as a .gsas file'''
		# export into the datadirectory with all the caesar text files
		# filename is "scan name" _ "set energy" keV_ "X".gsas, where X is a number to prevent overwriting
	
		# "scan name" is the name of the directory
		scanname = self.datadir.split(os.sep)[-1]
		filename = self.datadir + os.sep + scanname + "_" + str(self.caesar_ADXenergy) + "keV"
		myfilename = findNextFileName(filename, "gsas")
		
		# open the file
		myfile = open(myfilename, 'w')
		# write the title
		myline = myfilename.split(os.sep)[-1] + " (rebinned Caesar)"
		myline = myline[0:65] # crop to the 66 characters used by GSAS
		myline = myline.ljust(80)+"\r\n"
		myfile.write(myline)
		# write the calibration line
		nchan = len(self.caesar_ADXx)
		lrec = 10 # data points per row for STD
		nrec = np.ceil(float(nchan)/lrec)
		tth0 = self.caesar_ADXx[0]
		tthstep = (self.caesar_ADXx[-1]-self.caesar_ADXx[0])/(nchan-1)
		calib = [tth0*100, tthstep*100] # for centidegrees
		myline = "BANK 1 {} {} CONST {:0.8f} {:0.8f} 0 0 STD".format(nchan-1, nrec-1, calib[0], calib[1])
		myline = myline.ljust(80)+"\r\n"    
		myfile.write(myline)		
		# write the data...
		for ii in range(int(nrec)):
			myline = ""
			for jj in range(lrec):
				ndx = (ii*lrec)+jj
				if ndx<nchan:
					mynum = "{:d},".format(int(self.caesar_ADXy[ndx])).rjust(8)
					myline = myline + mynum
			myline = myline.ljust(80)+"\r\n"
			myfile.write(myline)
		myfile.close()
		print "gsas file: \n  %s\nwritten" % myfilename 
	
	
	def exportAllToGSAS(self):
		'''Export ALL possible EDX text files in the experiment directory (and sub directories) to GSAS'''
	
		# check:
		print "Current experiment directory is %s" % self.experimentdir
		print "This will export all EDX files in this directory and subdirectories to GSAS"
		check = raw_input("Are you sure? yes/[no] : ")
		if check=="yes" or check=='y':
			
			# get all the files in this directory and sub directories
			# go through the list, converting valid files
			for root, dirs, files in os.walk(self.experimentdir):

				for myfile in files:
            
            		# is it a text file?
					if myfile[-4::] == '.txt':

 						# build the full filename
						myfilepath = root + os.sep + myfile
						# read the file
						spectrum = []
						try:
							spectrum = np.loadtxt(myfilepath)
						except:
							print "file: %s not suitable" % myfilepath
                        
						# is the spectrum empty, does it have length 2048?
						if len(spectrum)==2048 and not(spectrum==0).all():
                    
							# write a gsas file 
							# remove the .txt to leave the prefix
							myfileprefix = myfile[0:-4]
							# split into filename and directory for exportGSAS_EDX
							print "export from file %s" % myfilepath
							self.spectrum = spectrum # to be visible in export method
							self.exportGSAS_EDX(filename=myfileprefix, directory=root)
						else:
							print "file %s is the wrong length or only zeros" % myfile
	

	def __exportGSAS_combined(self):
		'''export the single profile summary of a Caesar scan as a pseudo ADX spectra at 30 keV'''
		if self.caesar_image_dsp!="":
			print "Exporting single ADX profile summarising Caesar acquistion (Using corrected intensities): "
			# convert to ADX profile
			pint = self.caesar_image_dsp_profile*1.
			pdsp = self.caesar_image_dspx*1.
			wlength = 12.398/30			
			ptth = 2*np.arcsin(wlength/(2*pdsp))
			# put in ascending order
			pint = pint[::-1]
			ptth = ptth[::-1]
			# interpolate to constant steps in tth
			ptthgrid = np.linspace(ptth[0], ptth[-1], 4096)
			pintgrid = np.interp(ptthgrid, ptth, pint, 0, 0)
			# convert to degrees
			ptthgrid = ptthgrid*180/np.pi
			# limit max value
			if pintgrid.max() > 99999:
				fac = np.ceil(np.log10(pintgrid.max()/100000))
				pintgrid = pintgrid / (10**fac)
			# write this as a GSAS file
			# "scan name" is the name of the directory
			scanname = self.datadir.split(os.sep)[-1]
			filename = self.datadir + os.sep + scanname + "_combined_30keV"
			myfilename = findNextFileName(filename, "gsas")
			# open the file
			myfile = open(myfilename, 'w')
			# write the title
			myline = myfilename.split(os.sep)[-1] + " (combined Caesar scan)"
			myline = myline[0:65] # crop to the 66 characters used by GSAS
			myline = myline.ljust(80)+"\r\n"
			myfile.write(myline)
			# write the calibration line
			nchan = len(ptthgrid)
			lrec = 10 # data points per row for STD
			nrec = np.ceil(float(nchan)/lrec)
			tth0 = ptthgrid[0]
			tthstep = (ptthgrid[-1]-ptthgrid[0])/(nchan-1)
			calib = [tth0*100, tthstep*100] # for centidegrees
			myline = "BANK 1 {} {} CONST {:0.8f} {:0.8f} 0 0 STD".format(nchan-1, nrec-1, calib[0], calib[1])
			myline = myline.ljust(80)+"\r\n"    
			myfile.write(myline)		
			# write the data...
			for ii in range(int(nrec)):
				myline = ""
				for jj in range(lrec):
					ndx = (ii*lrec)+jj
					if ndx<nchan:
						mynum = "{:d},".format(int(pintgrid[ndx])).rjust(8)
						myline = myline + mynum
				myline = myline.ljust(80)+"\r\n"
				myfile.write(myline)
			myfile.close()
			print "gsas file: \n  %s\nwritten" % myfilename
			# export two column text file too - Q
			filenametxt = self.datadir + os.sep + scanname + "_Q"
			myfilenametxt = findNextFileName(filenametxt, "txt")
			myfiletxt = open(myfilenametxt, 'wt')
			myfiletxt.write('Q(A-1), intensity\n')
			for ii in range(len(self.caesar_image_Qx)):
				myfiletxt.write('%f,%f\n' % (self.caesar_image_Qx[ii], self.caesar_image_Q_profile_cor[ii]))			
			myfiletxt.close()
			print "text file %s written and closed..." % myfilenametxt
			# export two column text file too - dsp
			filenametxt = self.datadir + os.sep + scanname + "_dsp"
			myfilenametxt = findNextFileName(filenametxt, "txt")
			myfiletxt = open(myfilenametxt, 'wt')
			myfiletxt.write('d(A), intensity\n')
			for ii in range(len(self.caesar_image_dspx)):
				myfiletxt.write('%0.8f,%0.8f\n' % (self.caesar_image_dspx[ii], self.caesar_image_dsp_profile_cor[ii]))			
			myfiletxt.close()
			print "text file %s written and closed..." % myfilenametxt
			
		else:
			print "No converted Caesar data found"


					
	
