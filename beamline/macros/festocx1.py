# -*- coding: utf-8 -*-
from spyc import beamline_print
from PyTango import DeviceProxy

beamline_print("Get the CX1 festo device")
dev_festo = DeviceProxy('i03-c-c00/ca/dio_o.1')

def festCX11on():
    dev_festo.setline('PF0')
    beamline_print("Polychromator deactivated")
    
def festCX11off():
    dev_festo.resetline('PF0')
    beamline_print("Polychromator activated")
