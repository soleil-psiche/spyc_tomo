

energy = mc.energy * 1.
Q = mc.caesar_image_Qx * 1.
def calcF(Q):
    s = Q / (4*np.pi)
    
    # Silicon
    aSi = np.array([3.7464, 4.2959, 3.5786, 0.9544])
    bSi = np.array([1.3104, 2.8652, 36.3701, 97.9643])
    cSi = 1.4345
    mklSi = np.array([0.5254, 1.1646, 14.3259])
    zSi = 14
    fSi = cSi * 1.
    for ii in range(4):
        fSi = fSi + aSi[ii]*np.exp(-bSi[ii]*(s**2))
    IcohSi= fSi**2
    IincSi = (zSi - (IcohSi/zSi))*(1 - (mklSi[0]*(np.exp(-mklSi[1]*s) - np.exp(-mklSi[2]*s))))

    # Oxygen
    aOx = np.array([1.3721, 2.0624, 3.0566, 1.0743])
    bOx = np.array([0.3870, 5.5416, 12.3320, 29.8800])
    cOx = 0.4346
    mklOx = np.array([0.3933, 1.2843, 32.6820])
    zOx = 8
    fOx = cOx * 1.
    for ii in range(4):
        fOx = fOx + aOx[ii]*np.exp(-bOx[ii]*(s**2))
    IcohOx = fOx**2
    IincOx = (zOx - (IcohOx/zOx))*(1 - (mklOx[0]*(np.exp(-mklOx[1]*s) - np.exp(-mklOx[2]*s))))
    # average for silica
    fSilica = (fSi + (2*fOx))/3.
    IcohSilica = fSilica**2
    IincSilica = (IincSi + (2*IincOx))/3.
    return IcohSilica, IincSilica

# compton scattering 
tth = 30.
lam = 12.398/energy * (1E-10)
lam2 = lam + (2.43E-12 * (1 - np.cos(np.pi*tth/180.)))
energy2 = 12.398/(lam2 / (1E-10))
# given a spectrum, shift to these new values
I0 = np.zeros(energy.shape)
I0[100:373] = mc.fitted_spectrum * 1.
I1 = np.interp(energy, energy2, I0)
# if we divide the new profile by the old, the effect is very small
# at 30 degrees and 20 keV, I1/I0 = 1.04
# at 30 degrees and 70 keV, I1/I0 = 0.87 ... although the noise is bigger


# convert between energy and Q (using mc)
# dsp = 12.398 / (2*np.sin(theta)*mc.energy)
ndx = 63
tth = mc.caesar_tth[ndx]
# Q = 2*np.pi/dsp
thetarad = tth/2. * (np.pi/180)
Q = 2*np.pi*(2*np.sin(thetarad)*mc.energy) / 12.398
# for a given profile, can calculate Q for each energy channel, and hence the coh and inc contributions expected...

coh = np.zeros(mc.caesar_image.shape)
inc = np.zeros(mc.caesar_image.shape)
qim = np.zeros(mc.caesar_image.shape)
for ii in range(mc.caesar_image.shape[0]):
    tth = mc.caesar_tth[ii]
    thetarad = tth/2. * (np.pi/180)
    Q = 2*np.pi*(2*np.sin(thetarad)*mc.energy) / 12.398
    coh[ii, :], inc[ii, :] = calcF(Q)
    qim[ii, :] = Q
Qmap = []
for ii in np.arange(0,18,0.1):
    Qmap.append(np.nonzero((qim>ii) & (qim<=(ii+0.1))))
# this could give an easier way of plotting or calculating Q profiles in a loop
plot(qim[100,:], mc.caesar_image[100, :])

def optfunc(x, imE, Qmap):

    Izero = np.interp(x to size of imE[1])
    Izero = np.tile(Izero, (imE.shape[0], 1))
    imEnorm = imE / Izero
    

