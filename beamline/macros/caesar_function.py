# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 13:40:48 2013

@author: aking


This is version 1...  rewrite in a more elegant form.
"""


# an error here - when plotting caesar data the angle axis needs to be inverted

import numpy as np
import pylab as plt

def read():
    im = np.zeros([401, 2048])
    angle = (np.arange(0, 401)*0.025)+4 # using arange with integer steps

    for ii in range(401):        
        # print the bloody filename....
        if angle[ii]<10:
            filename = "data/CaB6_boronX_caesar_00001_%04d-_%.3f.txt" % (ii, angle[ii])    
        else:
            filename = "data/CaB6_boronX_caesar_00001_%04d-%.3f.txt" % (ii, angle[ii])        
        a = np.loadtxt(filename)
        im[ii, :]=a 
        
    plt.figure()
    plt.imshow(im, extent=[0, 83, 14, 4])
    plt.title('caesar data')
    plt.xlabel('energy')
    plt.ylabel('two theta angle')        
    plt.axis('tight')

        
    return im, angle
    
def backgrounds(im):
    # here, try to correct the intensities via the 

    # make an energy profile by summing the theta/energy image
    bkgd = im.sum(0)
    # try converting this to dspacing and scaling
    angle = (np.arange(0, 401)*0.025)+4 # using arange with integer steps
    angleR = angle*np.pi/180
    theta = angleR/2 # theta not 2theta
    energy = np.arange(0, 2048)*0.0401 # in keV
    
    dmax = 12.398 / (2*np.sin(theta[0])*20) # lowest angle, lowest energy
    dmin = 12.398 / (2*np.sin(theta[-1])*energy[-1]) # highest angle, highest energy
    dspgrid = np.linspace(dmin, dmax, 4096)    
    imbkgd_d = np.zeros([401, 4096])  
    imbkgd_e = np.zeros([401, 2048])  
    
    for ii in range(0, 401, 1):
        # scale to the intensity in this line of the image
        mybkgd = bkgd*im[ii, :].sum()/bkgd.sum()
        # this is an over-estimate, because of the intensity in the peaks
        # if we assume there is some true background, without peaks, can estimate the difference
        tmp = (im[ii, :]-mybkgd)/mybkgd 
        dif = tmp[512:1536].min()
        mybkgd = mybkgd * (1+dif)
        imbkgd_e[ii, :]=mybkgd
        # at the moment, to do better should deal with escape peaks        
        
        # convert to dspacing
        mybkgd = mybkgd[::-1] # flip
        dsp = 12.398 / (2*np.sin(theta[ii])*energy)
        dsp = dsp[::-1] # increasing
        mybkgd2 = np.interp(dspgrid, dsp, mybkgd, 0, 0)
        imbkgd_d[ii, :]=mybkgd2

    return imbkgd_e, imbkgd_d    
        
    
def convert(im):
    
    angle = (np.arange(0, 401)*0.025)+4 # using arange with integer steps
    angleR = angle*np.pi/180
    theta = angleR/2 # theta not 2theta
    energy = np.arange(0, 2048)*0.0401 # in keV
    
    dmax = 12.398 / (2*np.sin(theta[0])*20) # lowest angle, lowest energy
    dmin = 12.398 / (2*np.sin(theta[-1])*energy[-1]) # highest angle, highest energy
    dspgrid = np.linspace(dmin, dmax, 4096)    
    im2 = np.zeros([401, 4096])    
    
    # each profile needs to be convert to dspacing and interpolated onto this interval
    for ii in range(0, 401, 1):
        
        prof = im[ii, :]
        prof = prof[::-1]
        dsp = 12.398 / (2*np.sin(theta[ii])*energy)
        dsp = dsp[::-1] # increasing
        prof2 = np.interp(dspgrid, dsp, prof, 0, 0)
        im2[ii, :]=prof2
        prof3 = im2.sum(0)
    
    plt.figure()
    plt.subplot(2,1,1)
    plt.imshow(im2, extent=[0.62, 8.88, 14, 4])
    plt.title('caesar data')
    plt.xlabel('d spacing')
    plt.ylabel('two theta angle')        
    plt.axis('tight')
    plt.subplot(2,1,2)
    
    plt.plot(dspgrid, prof3)
    plt.axis([dspgrid[0], dspgrid[-1], 0, prof3.max()])
    
    # return some stuff
    return im2, dspgrid, prof3
    
def compare(im2, dspgrid):
    
    # given the convert data, plot a few profiles and the sum   
    plt.figure()
    plt.subplot(2,1,1)
    sumprf = np.zeros([1, im2.shape[1]])    
    
    for ii in range(0, 401, 80):
        # 4, 6, 8, 10, 12, 14
        prf = im2[ii, :]  
        sumprf = sumprf + prf
        myangle = (ii*0.025)+4
        mylabel = "angle = %0.1f" % myangle
        plt.plot(dspgrid, prf, label=mylabel)
        plt.axis([dspgrid[0], dspgrid[-1], 0, im2.max()])
        
    plt.legend()
    plt.xlabel('d spacing')
    plt.ylabel('intensity')
    
    plt.subplot(2,1,2)
    
    plt.plot(dspgrid, sumprf.T, label="sum of 6 profiles")
    plt.axis([dspgrid[0], dspgrid[-1], 0, sumprf.max()])
    plt.legend()
    plt.xlabel('d spacing')
    plt.ylabel('intensity')
   
    
