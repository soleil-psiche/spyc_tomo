# -*- coding: utf-8 -*-
from spyc import beamline_print
from PyTango import DeviceProxy
import time

beamline_print("Get CX3 laser commands")
dev_PulseGen = DeviceProxy('I03-C-C00/CA/CPT.2')

def IllumStart():
    dev_PulseGen.start()

def IllumStop():
    dev_PulseGen.stop()
    
    
