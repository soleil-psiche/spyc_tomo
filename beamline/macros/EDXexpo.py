
import PyTango
import sys
import time
_scan = PyTango.DeviceProxy("i03-C-C00/ex/scan.1")


def EDXexpo(theta=None, exptime=None):
    """
    EDXexpo(theta=None, exptime=None)
    you must specify both the theta angle and the time
    """
    if theta==None or time==None:
        print "Must specify caesar angle and exposure time"
        return

    _scan.Clean()
    _scan.sensors = ['i03-c-cx1/dt/dtc-mca_xmap.1/roi02_01']
    _scan.actuators=['i03-c-c05/ex/caesar/theta']
    _scan.trajectories = [[float(theta)]]
    _scan.integrationTimes = [float(exptime)]
    _scan.afterRunActionType=2
    _scan.actuatorsdelay = 0.1
    _scan.Start()
    sys.stdout.write("\nacquiring EDX...")
    
    # while loop for safety
    try:
        while _scan.state() not in [PyTango.DevState.ON, PyTango.DevState.ALARM]:

            # update user on progress
            sys.stdout.write("\b")
            sys.stdout.flush()
            time.sleep(.2)
            sys.stdout.write("\b")
            sys.stdout.flush()
            time.sleep(.2)
            sys.stdout.write("\b")
            sys.stdout.flush()
            time.sleep(.2)
            sys.stdout.write("...")
            sys.stdout.flush()
            time.sleep(.2)

    except KeyboardInterrupt:
        _scan.abort()

    sys.stdout.write("\n")
	
