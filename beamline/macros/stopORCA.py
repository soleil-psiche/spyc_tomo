# -*- coding: utf-8 -*-
import PyTango
import time
# get the devices
sbox = PyTango.DeviceProxy('flyscan/clock/spietbox-timebase.1')
ham  = PyTango.DeviceProxy('i03-c-c00/dt/hamamatsu.3')


def stopORCA():
    '''Equivilent to the FlyScan hook - stop ORCA if stuck waiting for an external trigger'''
    print "stop the camera"
    # program speitbox
    sbox.Abort()
    time.sleep(0.1)
    sbox.mode = 0
    sbox.pulsePeriod = 50
    sbox.sequenceLength = 100
    sbox.Prepare()
    # prepare camera
    sbox.start()
    ham.stop()
    
