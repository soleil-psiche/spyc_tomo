
''' small test of a GUI to select an item from a list'''

from Tkinter import *

class GUIListBox:

	def doubleClickFunction(self, event):
		self.selection = self.lbox.selection_get()
		self.top.destroy()
		return

	def buildGUI(self, mylist):
		self.top = Tk()
		# make a scrollbar
		scrollbar = Scrollbar(self.top)
		scrollbar.pack(side="right", fill="y")
		# make a listbox
		self.lbox = Listbox(self.top, height=15, width=50, yscrollcommand=scrollbar.set)
		for item in mylist:
			self.lbox.insert(-1, item)
		self.lbox.pack(side="left", fill="both", expand=True)
		scrollbar.config(command = self.lbox.yview)
		self.lbox.bind("<Double-Button-1>", self.doubleClickFunction)
		self.lbox.yview_moveto(0)
		# set title
		self.top.title("select with double click")
		# preallocate selection
		self.selection = None
		mainloop()

	def get_selection(self):
		return self.selection

