
import PyTango
import time


def getAllMotorPositions():
    '''Should run this in a spyc session with all the motors visible'''

    t0 = time.localtime()
    filename = '/home/experiences/psiche/com-psiche/archiving/psiche/motor_positions_%d_%d_%d.txt' % (t0[0], t0[1], t0[2])
    f = open(filename, 'wt')

    # Get the TPP positions
    tppnames = ['i03-c-c03/op/mir1-tpp', 'i03-c-c04/op/mono1-tpp',
        'i03-c-c05/ex/tab.1-tpp', 'i03-c-c07/ex/tab.2-tpp', 'i03-c-c08/ex/tab.3-tpp']

    for tppname in tppnames:

        f.write('\n\nTPP device: %s\n' % tppname)
        dp = PyTango.DeviceProxy(tppname)
        f.write('t1z = %f\n' % dp.t1z)
        f.write('t2z = %f\n' % dp.t2z)
        f.write('t3z = %f\n' % dp.t3z)
    
    # Get all the motors declared in spyc
    from spyc.core import DeviceManager
    dm = DeviceManager()
    names = sorted(dm.actuators)
    f.write('\n\nAll spyc motor positions\n')
    for name in names:
        pos = dm.actors[name]
        f.write('%s\n' % pos)

    f.close()

    print 'File %s written and closed' % filename
