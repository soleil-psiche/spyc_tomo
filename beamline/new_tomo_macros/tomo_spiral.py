# -*- coding: utf-8 -*-
# spiral acquisition macro

import os
import PyTango
import numpy as np
import sys
# spyc for motors
from spyc.core import DeviceManager
dm = DeviceManager()

# update for Tomo that uses recordingmanager and filetransfer
# save directly into the spiral directory (using spool_subdirectory)
# use an empty file as a flag to say whether the spiral has been finished or not.
# old version still available...
use_recording_manager = True

def tomo_spiral(mt, nameroot, zstart=None, zend=None, pixelstep=None, motor='tomotz', nscans=None, fast=False):

    # could add some extra info to mt for spiral scans - ref interval, sense, etc
    # easiest will be refs on first scan and last scan
    # change syntax to start/end position plus number of scans

    if (pixelstep==None):
        print "You must give the step size in pixels (pixelstep=...)"
    else:
        pixelsize = mt.pixelsize * mt.proxy.binningV * 1.
        zstep = pixelsize * pixelstep / 1000 # mm

    # deal with scan limits
    relativescan=False
    if (zstart==None) & (zend==None):
        if nscans != None:
            relativescan=True # special case - we don't specify start and stop, instead we go relative to the current pos.
            orig_pos = dm.actors[motor].get()
            print "will scan relative to the current %s position (%f)" % (motor, orig_pos)
            zstart = orig_pos - (((nscans-1)/2.)*zstep)
        else:
          print "Need to supply at least the start or end tomotz position !"
          print "quitting..."
          return
    if ((zstart==None) or (zend==None)) & (nscans==None):
        print "If you give only the start or finish position, you must give the number of scans (nscans=...) !"
        print "quitting..."
        return


    # if we have a start and an end
    if (zstart!=None) & (zend!=None):
        if zstart > zend:
            zstart,zend = zend,zstart
        zpos = np.arange(zstart, zend+zstep, zstep) # make sure that we do the last position
    # otherwise, we have a number of scans
    elif zend==None:
        # from start, in nscans
        zpos = np.arange(zstart, zstart+((nscans-1)*zstep)+0.0001, zstep)
    else:
        # to end, in nscans
        zpos = np.arange(zend-((nscans-1)*zstep), zend+0.0001, zstep)

    # test limits
    a = dm.actors[motor].get_limits()
    llim = float(a[0])
    hlim = float(a[1])
    if zpos[0]<llim :
        print('Can\'t do this scan because of low limit in %s (%0.2f) !!' % (motor, llim))
        print "quitting..."
        return

    if zpos[-1]>hlim :
        print('Can\'t do this scan because of high limit in %s (%0.2f) !!' % (motor, hlim))
        print "quitting..."
        return

    # make a folder for the results
    spooldir_orig = mt.spool_directory
    spooldir_new = spooldir_orig + os.sep + nameroot + '_spiral'
    spoolsubdir_orig = mt.spool_subdirectory
    spoolsubdir_new = mt.spool_subdirectory + os.sep + nameroot + '_spiral'
    # only used if not using recording_manager
    spiraldir = spooldir_orig + os.sep + nameroot + '_unfinished'
    spiraldirfinal = spooldir_orig + os.sep + nameroot + '_spiral'
    if os.path.isdir(spooldir_new):
        print "This name (%s) already exists. Please choose another... " % nameroot
        return
    else:
        os.mkdir(spooldir_new)
    if use_recording_manager:
        # change mt.spool_subdirectory so data saved directly into the spiral dir final
        mt.spool_subdirectory = spoolsubdir_new
        # change mt.spool_directory so parfiles saved directly into the spiral dir final
        mt.spool_directory = spooldir_new
        # unfinished flag - empty file called unfinished spiral
        os.system('touch %s/unfinished_spiral' % spooldir_new)



    mynref = mt.nref * 1.
    myndark = mt.ndark * 1.
    try:
        for ii, myzpos in enumerate(zpos):

            # treat flyscan parameters
            if fast:
                update=False
                if ii == 0:
                    # first scan - refs, but no darks
                    mt.nref = int(mynref * 1.)
                    mt.ndark = int(0)
                    update=True
                if ii == 1:
                    # first scan - refs, but no darks
                    mt.nref = int(0)
                    mt.ndark = int(0)
                    update=True
                if ii==(len(zpos)-1):
                    # last scan - refs and darks
                    mt.nref = int(mynref * 1.)
                    mt.ndark = int(myndark * 1.)
                    update=True
                if update:
                    mt.setFlyScanParameters()

            # move in z
            get_ipython().magic('amove %s %f' % (motor, myzpos))
            # if we are moving tomorefx, need to update flyscan parameters here
            if motor == 'tomorefx':
                print "insist on getting to the good tomorefx position"
                retry = 0
                marge = mt.pixelsize* mt.proxy.binningH / 10000 # 1/10 of a pixel in mm
                diff = np.abs(dm.actors['tomorefx'].get() - myzpos)
                while (diff > marge) and retry<5:
                    get_ipython().magic('amove %s %f' % (motor, myzpos))
                    diff = np.abs(dm.actors['tomorefx'].get() - myzpos)
                    retry+=1
                    if retry == 5: # last time around loop
                        print "Tried 5 times to reach good tomorefx position - still at %f distance" % diff
                print "apply the new measurement position for subscan %d" % ii
                mt.measurementPosition = myzpos
                mt.setFlyScanParameters()

            # fix the scanname
            mt.scanname = nameroot + "_spiral_%02d" % ii
            # do the scan
            mt.doTomo()

            if not use_recording_manager:
                # copy the scan to the spiral folder
                os.system("mv %s/%s %s" % (spooldir_orig, mt.savename, spiraldir))
    except:
        print('Problem...?')
        print('#################')
        print(sys.exc_info())
        print('#################')
    finally:
        if use_recording_manager:
            # finally - change the unfinished flag to finished
            os.system('mv %s/unfinished_spiral %s/finished_spiral' % (spooldir_new, spooldir_new))
            # reset mt.spool_subdirectory - this should be a try/except/finally
            mt.spool_subdirectory = spoolsubdir_orig
            mt.spool_directory = spooldir_orig
        else:
            # finally - rename the folder to make it look like a spiral scan
            os.system("mv %s %s" % (spiraldir, spiraldirfinal))

        if relativescan:
            print "return to original position in z, reset measurementPosition"
            get_ipython().magic('amove %s %f' % (motor, orig_pos))
            mt.measurementPosition = orig_pos
            mt.setFlyScanParameters()
        else:
            print "return to start position in z"
            get_ipython().magic('amove %s %f' % (motor, zpos[0]))


