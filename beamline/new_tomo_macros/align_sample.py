# -*- coding: utf-8 -*-


# align sample in half acquisition...
# use the basis of the mosaic macro, plus pointy clicky from caesar
# make mosaics of plus minus myrange at 0 and 90

# should add a try-except-finally so that it doesn't leave the sample misaligned if the user doesnt finish correctly

# updated to use motor aliases in mt
# needs to handle updateon / updateoff.
# probably, mt.alignSample should set updateoff, and here we should use snap

import PyTango
import numpy as np
# pylab for figures
import pylab
pylab.ion()
# try handling reference motor with spyc
from spyc.core import DeviceManager
import sys
dm = DeviceManager()

import time

#import updateon
#updateon.updateon()

# globals for figure function
global cid, fig, coords, nclicks
coords = []

def onclick(event):
    ix, iy = event.xdata, event.ydata
    if len(coords)!=0:
        sys.stdout.write("\b"*3)
        sys.stdout.flush()
    sys.stdout.write("(%d)" % (len(coords)+1))
    sys.stdout.flush()
    coords.append((ix, iy))
    if len(coords)==nclicks:
        fig.canvas.mpl_disconnect(cid)
        return

def align_sample(mt=None, myrange=1, square=False, oneclick=False):

    # if the sample is square, assume that the diagonal is 1.41 x apparent size

    # get the sample radius in the one click case
    if oneclick:
        tmp = raw_input('What is the sample radius in mm? [0.88] : ')
        samrad = 0.88 if tmp == '' else float(tmp)

    # need the Tomo object to work
    if mt==None:
        print "Can\'t work without the Tomo object (mt)... quitting"
        return

    mt.stopLive()
    mt.proxy.nbFrames = 1 # for taking images
    pylab.close('all')

    # keep the images reasonable
    bin = (2*myrange) + 1

    # get the motor names from mt
    rot_alias = mt.rotation_motor_alias
    ref_alias = mt.reference_motor_alias
    tx_alias = mt.sample_tx_motor_alias
    ts_alias = mt.sample_ts_motor_alias
    tz_alias = mt.sample_tz_motor_alias

    # get the starting postion
    rzorig = dm.actors[rot_alias].get()
    refxorig = dm.actors[ref_alias].get()
    txorig = dm.actors[tx_alias].get()
    tsorig = dm.actors[ts_alias].get()

    # start from tomotx 0, tomots 0, tomorefx 0
    print "start from %s = %s = %s = 0 ... " % (ref_alias, tx_alias, ts_alias)
    get_ipython().magic(u"amove %s 0 %s 0 %s 0" % (ref_alias, tx_alias, ts_alias))
    refxpos = dm.actors[ref_alias].get()
    txpos = dm.actors[tx_alias].get()
    tspos = dm.actors[ts_alias].get()
    zpos = dm.actors[tz_alias].get()

    # get the image size
    imsize = np.array([mt.proxy.roiHeight, mt.proxy.roiWidth])
    # will crop slightly before binned to ensure integers
    imsizesmall = np.floor(imsize/bin)
    imsizecrop = imsizesmall*bin
    imsizemm = imsize * mt.pixelsize * mt.proxy.binningH / 1000

    # take a reference and a dark...
    print "Using the saved reference - in case of artefacts in the alignment images, use mt.updateRef() after aligning the sample"
    if mt.ref == '':
        mt.updateRef()

    ref = mt.ref * 1.

    ref = ref[0:imsizecrop[0], 0:imsizecrop[1]]
    ref = ref.reshape(int(imsizesmall[0]), bin, int(imsizesmall[1]), bin).mean(3).mean(1)

    # measurement positions - move in tomorefx
    xpos = refxpos + np.arange(-myrange, myrange+1) * imsizemm[1]

    # image extents
    sam_xpos = txpos + np.arange(-myrange, myrange+1) * imsizemm[1]
    imext_tx = [sam_xpos[-1]+(imsizemm[1]/2), sam_xpos[0]-(imsizemm[1]/2), zpos-(imsizemm[0]/2), zpos+(imsizemm[0]/2)]
    sam_spos = tspos + np.arange(-myrange, myrange+1) * imsizemm[1]
    imext_ts = [sam_spos[-1]+(imsizemm[1]/2), sam_spos[0]-(imsizemm[1]/2), zpos-(imsizemm[0]/2), zpos+(imsizemm[0]/2)]

    # prepare the output image - two images
    output = np.zeros((int(imsizesmall[0]), int(imsizesmall[1]*len(xpos)), 2))

    # image at 0, 90 degrees
    for jj,ang in enumerate([0., 90.]):
        for ii,xx in enumerate(xpos):
            # move to the position
            get_ipython().magic(u'amove %s %f %s %f' % (rot_alias, ang, ref_alias, xx))
            # updateon so will snap at the end of the movement
            # get the image, cropped if needed
            mt.snap()
            while mt.proxy.state() == PyTango._PyTango.DevState.RUNNING:
                time.sleep(0.1)
            time.sleep(0.1)
            im = mt.proxy.image[0:imsizecrop[0], 0:imsizecrop[1]]*1.
            # downsize and add to output - magic from stackoverflow
            im = im.reshape(int(imsizesmall[0]), bin, int(imsizesmall[1]), bin).mean(3).mean(1)
            # flatfield
            im = im/ref
            # paste into output - where?
            c = (len(xpos)-ii-1)*imsizesmall[1]
            d = c + imsizesmall[1]
            output[:, int(c):int(d), jj] = im

    # show this to the user
    mymin = output.mean()-(3*output.std())
    mymax = output.mean()+(3*output.std())
    global cid, fig, coords, nclicks

    if oneclick:
        nclicks=1
    else:
        nclicks=2

    nok = True
    while nok:
        coords = []

        fig = pylab.figure()
        pylab.gray()
        pylab.imshow(output[:,:,0], extent=imext_tx)
        if oneclick:
            pylab.title('%s = 0, %s - click centre of sample' % (rot_alias, tx_alias))
        else:
            pylab.title('%s = 0, %s - click left and right edges of sample' % (rot_alias, tx_alias))
        pylab.clim(mymin, mymax)
        cid = fig.canvas.mpl_connect('button_press_event', onclick)
        raw_input('Click figure, then enter to continue...')
        if oneclick:
            onepointx = np.array(coords[0][0])
            pointsx = np.array([onepointx - samrad, onepointx + samrad])
        else:
            pointsx = np.array((coords[0][0], coords[1][0]))

        coords = []
        pylab.clf()
        pylab.imshow(output[:,:,1], extent=imext_ts)
        if oneclick:
            pylab.title('%s = 90, %s - click centre of sample' % (rot_alias, ts_alias))
        else:
            pylab.title('%s = 90, %s - click left and right edges of sample' % (rot_alias, ts_alias))
        pylab.clim(mymin, mymax)
        cid = fig.canvas.mpl_connect('button_press_event', onclick)
        raw_input('Click figure, then enter to continue...')
        if oneclick:
            onepoints = np.array(coords[0][0])
            pointss = np.array([onepoints - samrad, onepoints + samrad])
        else:
            pointss = np.array((coords[0][0], coords[1][0]))
        coords = []
        pylab.clf()

        pylab.subplot(2, 1, 1)
        pylab.imshow(output[:,:,0], extent=imext_tx)
        pylab.clim(mymin, mymax)
        if oneclick:
            pylab.plot([onepointx, onepointx], [imext_tx[2], imext_tx[3]], 'g')
        pylab.plot([pointsx[0], pointsx[0]], [imext_tx[2], imext_tx[3]], 'r')
        pylab.plot([pointsx[1], pointsx[1]], [imext_tx[2], imext_tx[3]], 'r')
        pylab.axis(imext_tx)
        pylab.subplot(2, 1, 2)
        pylab.imshow(output[:,:,1], extent=imext_ts)
        pylab.clim(mymin, mymax)
        if oneclick:
            pylab.plot([onepoints, onepoints], [imext_ts[2], imext_ts[3]], 'g')
        pylab.plot([pointss[0], pointss[0]], [imext_ts[2], imext_ts[3]], 'r')
        pylab.plot([pointss[1], pointss[1]], [imext_ts[2], imext_ts[3]], 'r')
        pylab.axis(imext_ts)
        print pointsx
        print pointss
        selection = raw_input("Happy with positions defined ? [y]/n : ")
        nok = True if selection=="n" else False


    print "return to original position"
    get_ipython().magic(u'amove %s %f %s %f %s %f %s %f' % (rot_alias, rzorig, ref_alias, refxorig, tx_alias, txorig, ts_alias, tsorig))

    # now use these values
    mysize = max(np.abs(pointsx[1]-pointsx[0]), np.abs(pointss[1]-pointss[0])) # in mm
    if square:
        print('Sqaure section sample! Increasing size by 1.41 x')
        mysize = mysize * 1.41

    mysizepix = 1000 * mysize / (mt.pixelsize * mt.proxy.binningH)
    mysizepix = mysizepix * 1.1 # add 10% for safety
    mysizepix = 100*np.ceil(mysizepix/100) # round up to nearest 100 pixels
    if mysizepix > (imsize[1]*1.9):
        print "sample is very big - you will probably clip the edges"
        print "limit the size to 2 x detector width"
        mysizepix = imsize[1]*1.9

    # where should the axis go?
    myradpix = mysizepix /2.
    delta = myradpix - (imsize[1] / 2.) # add margin for error
    # how many projections for a normal scan?
    nproj180 = imsize[1] * 1500 / 2048.
    nproj180 = int(100*np.ceil(nproj180/100)) # round up to 100 projections
    if delta<0:
        print "This can be a normal 180 degree, %d projections scan" % nproj180
        myrefxmm = 0.
    else:
        print "This should be a 360 degree, half acquisition scan"
        # make sure we don't go too far
        delta = min(delta, imsize[1]*0.45)
        myrefxmm = - delta * (mt.pixelsize * mt.proxy.binningH) / 1000

    # where should tomotx and tomots go?
    myxpos = (pointsx[0]+pointsx[1]) / 2.
    myspos = (pointss[0]+pointss[1]) / 2.

    # some output
    print "_________________________________________________"
    print "suggestions:"
    print "amove %s %0.2f %s %0.2f" % (tx_alias, myxpos, ts_alias, myspos)
    print "amove %s %0.2f" % (ref_alias, myrefxmm)
    print "mt.setMeasurementPos()"
    print "mt.setScanParameters()"
    print "... with:"
    if myrefxmm == 0.:
        print "180 degrees and %d projections" % nproj180
    else:
        nproj = myradpix*3.
        nproj = 100*np.ceil(nproj/100) # round up to 100 projections
        print "360 degrees and %d projections ?" % nproj
    print "_________________________________________________"

    # apply this to the sample and mt
    test = raw_input('Apply the sample alignment? [y]/n : ')
    test = 'y' if test.lower() in ['', 'y', 'yes', 'true'] else 'n'
    if test == 'y':
        # is there an offset on the rotation axis?
        offset = dm.actors[rot_alias].proxy.offset
        if np.abs(offset)<1:
            get_ipython().magic(u'amove %s %0.2f %s %0.2f' % (tx_alias, myxpos, ts_alias, myspos))
        else:
            print "accounting for the offset"
            myxpos_new = (myxpos*np.cos(np.deg2rad(offset))) + (myspos*np.sin(np.deg2rad(offset)))
            myspos_new = (myspos*np.cos(np.deg2rad(offset))) - (myxpos*np.sin(np.deg2rad(offset)))
            get_ipython().magic(u'amove %s %0.2f %s %0.2f' % (tx_alias, myxpos_new, ts_alias, myspos_new))
    else:
        print "Not aligning the sample"
    if myrefxmm == 0.:
        print "This is a 180 degree, %d projection scan - applying this to mt" % nproj180
        get_ipython().magic(u'amove %s 0.0' % ref_alias)
        mt.measurementPosition = 0.0
        print "setting measurement position"
        mt.nproj = nproj180
        mt.scanrange = 180.
    else:
        print("Proposed scan [y] is 360 degree, %d projection double acquisition" % nproj)
        print "Otherwise [n] will do a normal 180 degree scan"
        test = raw_input('Do the 360 degree scan?  [y]/n : ')
        test = 'y' if test.lower() in ['', 'y', 'yes', 'true', ' '] else 'n'
        if test == 'y':
            get_ipython().magic(u'amove %s %0.2f' % (ref_alias, myrefxmm))
            mt.measurementPosition = myrefxmm
            print "setting measurement position"
            mt.nproj = int(nproj)
            mt.scanrange = 360.
        else:
            print "Forcing a 180 degree, normal scan"
            get_ipython().magic(u'amove %s 0.0' % ref_alias)
            mt.measurementPosition = 0.0
            print "setting measurement position"
            mt.nproj = nproj180
            mt.scanrange = 180.

    print "_____________________________________________________"
    print "Now run setScanParameters to set the sample name"
    print "All other scan parameters are is set"
    print "_____________________________________________________"

    # important - return mt
    return mt


