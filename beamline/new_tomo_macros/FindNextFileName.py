# -*- coding: utf-8 -*-
"""
Emilano's findNextFileName function made into a stand alone.
Use directry / prefix / extension format"""

import os

# Emiliano's findNextFileName
def findNextFileName(directory, prefix, ext, file_index=0):
    '''
    filename = findNextFileName(prefix,ext,file_index=1)
    directory is the directory, prefix is the root of filename, ext is the suffix, index is optional number
    '''
    # clean component parts    
    # remove . from extension    
    if ext[0]=='.':
        ext=ext[1::]
    # if directory is empty, make it '.'    
    # remove trailing / from directory
    if directory == '':
        directory='.'
    elif directory[-1]==os.sep:
        directory=directory[0:-1]
    # deal with empty file_index
    if file_index=='':
        file_index=0

    # first guess at complete name    
    if ext!="":
        fname = prefix + "_%04i"%(file_index) + "." + ext
    else:
        fname = prefix + "_%04i"%(file_index)
    
    # list the existing files
    _dir=os.listdir(directory)
    
    while(fname in _dir):
        file_index+=1
        # increment, new guess at complete name    
        if ext!="":
            fname = prefix + "_%04i"%(file_index) + "." + ext
        else:
            fname = prefix + "_%04i"%(file_index)
            
    # build the full name        
    fname = directory + os.sep + fname
    return fname