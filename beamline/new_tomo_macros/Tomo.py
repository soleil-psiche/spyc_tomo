# -*- coding: utf-8 -*-

# Cleaned up Tomo that should work at PSICHE or ANATOMIX
# All beamline specific configuration and defaults should go in ~/BEAMLINE_PARAMETERS.par
# the actual file should be in the sypc folder $PYBEAMLINEROOT/beamline/macros, with a link to the ~/ folder
# this is a pyHST style par file, read using par_tools
# saveSettings / loadSettings allows the last config to be reloaded
# uses a FlyScan easyconfig script to do the FlyScan configuration


######### all imports ##############
import time
import numpy as np
import subprocess
import PyTango
import sys
import h5py
import os
import glob
import pickle
import par_tools
import align_sample
######### all imports ##############

# test recording manager
use_recording_manager = True
if use_recording_manager:
    recordingmanager = PyTango.DeviceProxy('flyscan/core/recording-manager.1')


######### load required objects ##############
# handling motors with spyc
from spyc.core import DeviceManager
dm = DeviceManager()

########## load beamline acquisition parameters ###############
# load beamline parameters - keep PSICHE and ANATOMIX compatible...
from load_beamline import load_beamline
beamline = load_beamline()
print('{}'.format(beamline['WELCOME_MESSAGE'][0]))
# Read the tomo cameras from the beamline parameters file.
mycameras = {}
mytomographs = {}
for key in beamline.keys():
    # Get the possible cameras
    if (key[0:6] == 'camera') and (key[-4:]=='name'):
        name = beamline[key][0]
        ndx = int(key[6])
        device = beamline['camera%d_device' % ndx][0]
        rotc_alias = beamline['camera%d_rotation_motor_alias' % ndx][0]
        ftp_device = beamline['camera%d_ftp_device' % ndx][0]
        mycameras.update({name:[device, rotc_alias, ftp_device]}) # get both the device and the rotc motor and the ftp device
    # Get the possible tomographs - the key is the rotation stage alias
    elif (key[0:4] == 'tomo') and (key[-21:]=='_rotation_motor_alias'):
        rot_alias = beamline[key][0]
        ndx = int(key[4])
        tomo_name = beamline['tomo%d_name' % ndx][0]
        ref_alias = beamline['tomo%d_reference_motor_alias' % ndx][0]
        sbox_device = beamline['tomo%d_speitbox_device' % ndx][0]
        sbox_input = beamline['tomo%d_speitbox_input' % ndx][0]
        tx_alias = beamline['tomo%d_sample_tx_motor_alias' % ndx][0]
        ts_alias = beamline['tomo%d_sample_ts_motor_alias' % ndx][0]
        tz_alias = beamline['tomo%d_sample_tz_motor_alias' % ndx][0]
        mytomographs.update({rot_alias:[tomo_name, ref_alias, sbox_device, sbox_input, tx_alias, ts_alias, tz_alias]}) # get everything?

########## Is there a special detector? ###############
if beamline['detector_class'][0] == 'Psiche_Detector':
    from Psiche_Detector import Detector
    # Psiche_Detector inherits from Detector...
    import updateon
else:
    from Detector import Detector

########## Get the FlyScan server ###############
print "Get everything for FlyScan and tomoscan"
# flyscan server
# Proxy versus proxy in the new/old version
# path that needs to be added to Spyc
sys.path.append('/usr/Local/FlyscanRoot/python2/lib/python2.7/site-packages/')
from fs.client.proxy import FlyScanServerWrapper

loop = True
timeout = 0
while loop and (timeout<3):
    try:
        fss = FlyScanServerWrapper()
        #fss.relocate('flyscan/core/server.1')
        fss.relocate(beamline['flyscan_server_device'][0])
        loop = False
    except:
        print "___ get fss failed, sleep 1 and try again...____"
        timeout+=1
        time.sleep(1)

######### finished loading required objects ##############


######### the Tomo class ##############
class Tomo(Detector):


    def __init__(self, camera_name=None, tomo_alias=None):
        # initialise detector(camera_name, tomo_alias)
        # camera handles taking and displaying images
        # detector handles the optics, focusing, alignment etc
        # tomo handles data acquisition

        if camera_name == None:
            camera_name = beamline['default_camera_name'][0]
        self.camera_name = camera_name
        Detector.__init__(self, mycameras[camera_name][0])

        # preallocate with default values
        self.scanname = None
        self.reconstruction_directory = '' # where reconstruction is done for the current experiment
        self.spool_directory = '' # where raw data is stored on the spool for the current experiment
        self.backup_directory = '' # where data is archived on the ruche for the current experiment
        self.sample_logfile = "/nfs/ruche-psiche/psiche-soleil/com-psiche/default/scan_log.pickle" # logfile 1
        self.sample_logfile2 = "/nfs/ruche-psiche/psiche-soleil/king/logs/default/scan_log.pickle" # logfile 2

        # the tomo hardware
        if tomo_alias == None:
            tomo_alias = beamline['default_tomo_alias'][0]
        #self.setRotationMotor(tomo_alias) # get the defaults for this axis

        # initialise variable names
        self.rotation_motor_alias = 'not configured'
        self.tomo_name = 'not configured'
        self.reference_motor_alias = 'not configured'
        self.sbox_input_coder = 'not configured'
        self.sample_tx_motor_alias = 'not configured'
        self.sample_ts_motor_alias = 'not configured'
        self.sample_tz_motor_alias = 'not configured'

        ## get motor device proxies via spyc device manager, plus other device proxies
        self.rotation_proxy =  'not configured'
        self.reference_proxy =  'not configured'
        self.sbox_devname = 'not configured'
        self.sbox_proxy =  'not configured'

        self.merger_proxy = PyTango.DeviceProxy(beamline['merger_device'][0])
        self.merger_proxy.set_timeout_millis(30000)

        # scan parameters
        self.nproj = 1500
        self.nref = 21
        self.ndark = 21
        self.scanrange = float(360)
        self.block = 100 # for tomoscan - do blocks of N images to avoid memory problems with camera device
        self.safetytimeauto = True # calculate safety time.  If safetytimeauto == False, manually set safetytime
        self.safetytime = 0.005 # safety time added to readout to ensure no missed triggers (flyscan AND tomoscan)
        self.initial_sleep = 0 # possibly used in the dark and bright hook for flyscan
        self.calib_images = 0 # if greater than 0, take this many images after scan
        self.rotation_velocity_nominal = 60 # use this for default, and for calib image
        self.rotation_velocity_max = 360 # use this as an upper limit in scans

        # is there a better way to handle half acquisition scans?
        # all fast shutter things
        self.fast_shutter_for_snap = False
        self.fast_shutter_type = ''
        self.fast_shutter_alias = ''
        self.fast_shutter_device_name = ''
        self.fast_shutter_close_command = ''
        self.fast_shutter_close_pos = ''
        self.fast_shutter_open_command = ''
        self.fast_shutter_open_pos = ''
        self.fast_shutter_line = 0
        self.fast_shutter_port = ''
        self.fast_shutter_sense = 1

        # define main shutter as well, just in case
        self.shutter_open_command = beamline['shutter_spyc_magic_open'][0]
        self.shutter_close_command = beamline['shutter_spyc_magic_close'][0]

        # aliases for logging in the log file and in the par file
        self.log_attribute_list = []
        self.par_attribute_list = []
        self.getLogAttributeList() # this gets both lists
        self.save_motor_positions = True
        # Annoying things that cause problems!
        self.pinkenergy = ''
        self.my_nxs_file = '' # can disappear with the 100% recording manager version
        self.active = True




    def saveSettings(self):
        # save everything that can be set by the user
        settings = {}
        for name in vars(self):
            # don't save images, file handles, or device proxies
            if (name not in ['ref', 'dark', 'image', 'flat', 'synchro_log_file']) and (name.find('proxy') == -1):
                print 'save %s' % name
                settings.update({name:getattr(self, name)})
        print "save all user parameters in %s" % beamline['logfile_path'][0]
        fnameA = '%s/configs/%s_%s_config.pickle' % (beamline['logfile_path'][0], self.camera_name, self.rotation_motor_alias)
        name_with_time = '%s_%s_config_%d_%d_%d_%dh.pickle' % (self.camera_name, self.rotation_motor_alias, time.localtime()[0], time.localtime()[1], time.localtime()[2], time.localtime()[3])
        fnameB = '%s/configs/%s' % (beamline['logfile_path'][0], name_with_time)
        pickle.dump(settings, open(fnameA, 'wb'))
        print 'saved in %s (load by default)' % fnameA
        pickle.dump(settings, open(fnameB, 'wb'))
        print 'saved in %s (load by name)' % fnameB


    def loadSettings(self, configname=None):
        # load everything that can be set by the user
        if configname==None:
            # load the last saved settings
            os.system('ls -lrth %s/configs' %  beamline['logfile_path'][0])
            print "You need to supply a config file from the list"
            tmp = raw_input('Config file : ')
            if tmp == '':
                print "Not loading a configuration; you have default parameters."
            else:
                name = tmp.split(os.sep)[-1]
                fname = '%s/configs/%s' % (beamline['logfile_path'][0], name)
                print "loading settings from %s" % fname
                settings = pickle.load(open(fname, 'rb'))
        else:
            # load specified file
            fname = '%s/configs/%s' % (beamline['logfile_path'][0], configname)
            print "loading settings from %s" % fname
            settings = pickle.load(open(fname, 'rb'))
        # apply these
        for key in settings.keys():
            setattr(self, key, settings[key])
            #print "setting %s to %s" % (key, str(settings[key]))
        print "Loading the config as it was saved"
        print "If something is wrong, use mt.setRotationMotor() to configure Tomo"
        # get the right device servers - start with the cameras
        self.setCamera(self.camera_name)
        # use setRotationMotor which sets everything to BEAMLINE_PARAMETERS.par configuration
        try:
            self.rotation_proxy = dm.actors[self.rotation_motor_alias].proxy
            self.reference_proxy = dm.actors[self.reference_motor_alias].proxy
            self.sbox_proxy = PyTango.DeviceProxy(self.sbox_devname)
        except:
            print("FAILED TO SET ONE OF THE DEVICE PROXYS")
        # normally, we alrady have these, but get again.
        self.merger_proxy = PyTango.DeviceProxy(beamline['merger_device'][0])
        self.merger_proxy.set_timeout_millis(30000)


    # need to test this, and think about the use cases
    # this should probably become setConfig(), and handle the camera as well
    def setConfig(self, tomo_alias=None):
        '''
        Here we set up the configuration of the tomograph (which rotation motor, reference motor, s
        sample motors, speitbox, camera, etc
        '''
        if tomo_alias==None:
            tomo_alias = raw_input('Which rotation motor [%s] : ' % self.rotation_motor_alias)
            tomo_alias = self.rotation_motor_alias if tomo_alias == '' else tomo_alias
        # does this motor exist in this spyc session?
        if not dm.actors.has_key(tomo_alias):
            print "Rotation motor %s does not exist in the current Spyc session ! " % tomo_alias
            print "Try again with a different motor or in a different session."
            return
        # motor exists. Is it in BEAMLINE_PARAMETERS.par ?
        if mytomographs.has_key(tomo_alias) :
            # this motor exists in Spyc, and BEAMLINE_PARAMETERS:
            print "A standard config for %s exists in BEAMLINE_PARAMETERS.par" % tomo_alias
            tmp = raw_input('Load standard config? Or create custom config? [y=standard]/n=custom : ')
            loadstandard = True if tmp.lower() in ['', 'y', 'yes', 'true', '1', 'standard'] else False
            buildcustom = not loadstandard
        else:
            tmp = raw_input('There is no standard config for %s in BEAMLINE_PARAMETERS.par. Build a custom config [y]/n: ' % tomo_alias)
            buildcustom = True if tmp.lower() in ['y', 'yes', 'true', '1'] else False
            loadstandard = False
        # now, do this:
        if loadstandard:
            # load the corresponding configuration as defined in BEAMLINE_PARAMETERS.par
            # get motor device proxies via spyc device manager, others using PyTango
            self.rotation_motor_alias = tomo_alias
            self.rotation_proxy = dm.actors[self.rotation_motor_alias].proxy
            self.tomo_name = mytomographs[tomo_alias][0]
            self.reference_motor_alias = mytomographs[tomo_alias][1]
            self.reference_proxy = dm.actors[self.reference_motor_alias].proxy
            self.sbox_devname = mytomographs[tomo_alias][2]
            self.sbox_proxy = PyTango.DeviceProxy(self.sbox_devname)
            self.sbox_input_coder = mytomographs[tomo_alias][3]
            self.sample_tx_motor_alias = mytomographs[tomo_alias][4]
            self.sample_ts_motor_alias = mytomographs[tomo_alias][5]
            self.sample_tz_motor_alias = mytomographs[tomo_alias][6]
            print "Loaded standard config for %s from BEAMLINE_PARAMETERS.par" % tomo_alias
        if buildcustom:
            # get motor device proxies via spyc device manager, others using PyTango
            self.rotation_motor_alias = tomo_alias
            self.rotation_proxy = dm.actors[self.rotation_motor_alias].proxy
            name = raw_input("Give a name for this config (no spaces) : ")
            self.tomo_name = name
            self.setRefMotor()
            self.sbox_devname = raw_input("Give the speitbox device name for this motor : ")
            self.sbox_proxy = PyTango.DeviceProxy(self.sbox_devname)
            self.sbox_input_coder = raw_input("Give the speitbox input coder for this motor : ")
            self.setSampleMotors()
            print "Created custom config %s for %s" % (self.tomo_name, tomo_alias)
        # now, in both cases, we should deal with the measurement position
        self.setMeasurementPos()
        # now, in both cases, we should deal with the fast shutter
        self.setupFastShutter()
        # now, in both cases, we should deal with the camera
        self.setCamera()
        print "Hopefully, everything is now set. Deal with little stupid things that cause problems"
        self.setOptic() # make sure the optic is setup. This sets refDisplacement
        tmp = raw_input('\"pinkenergy\" memo string for log file? [nothing] : ')
        self.pinkenergy = tmp



    # need to test this, and think about the use cases
    def setCamera(self, new_name=None):
        if new_name == None:
            print "Tomo cameras in ~/BEAMLINE_PARAMETERS.par :"
            items = mycameras.items()
            for ii,item in enumerate(items):
                print "[%d] %s (%s)" % (ii, item[0], item[1])
            if self.proxy.dev_name() in mycameras.keys():
                default_ndx = mycameras.keys().index(self.proxy.dev_name())
            else:
                default_ndx = 0
            tmp = raw_input('Which camera? [%d] : ' % default_ndx)
            ndx = default_ndx if tmp == '' else int(tmp)
            new_name = items[ndx][0]
        # Now apply this
        print "Setting %s : %s" % (new_name, mycameras[new_name])
        self.camera_name = new_name
        self.proxy = PyTango.DeviceProxy(mycameras[new_name][0])
        # set the rotc alias as well
        self.camera_rotation_motor = mycameras[new_name][1]
        self.ftp_proxy = PyTango.DeviceProxy(mycameras[new_name][2])

    # need to test this, and think about the use cases
    def setSampleMotors(self):
        tmp = raw_input('Which sample tz motor [%s] : ' % self.sample_tz_motor_alias)
        tmp = self.sample_tz_motor_alias if tmp == '' else tmp
        self.sample_tz_motor_alias = tmp
        tmp = raw_input('Which sample ts motor (parallel to beam at rz=0) [%s] : ' % self.sample_ts_motor_alias)
        tmp = self.sample_ts_motor_alias if tmp == '' else tmp
        self.sample_ts_motor_alias = tmp
        tmp = raw_input('Which sample tx motor (perpendicular to beam at rz=0) [%s] : ' % self.sample_tx_motor_alias)
        tmp = self.sample_tx_motor_alias if tmp == '' else tmp
        self.sample_tx_motor_alias = tmp


    def getLogAttributeList(self):
        # read the attributes to be logged from the beamline parameters file
        for key in beamline.keys():
              if key[0:12] == 'logattribute':
                  self.log_attribute_list.append(beamline[key][0])
              if key[0:12] == 'parattribute':
                  self.par_attribute_list.append(beamline[key][0])

    def setupFastShutter(self, selection=None):
        # select a fast shutter from those defined in the beamline parameters file
        if selection==None:
          print 'Select one of the following fast shutters :'
          print '(defined in ~/BEAMLINE_PARAMETERS.par)'
          for key in beamline.keys():
              if (key[0:2] == 'fs') and (key[-4:]=='name'):
                  name = beamline[key][0]
                  ndx = int(key[2])
                  print '[%d] : %s' % (ndx, name)
          selection = raw_input('Which fastshutter [1] : ')
          selection = 1 if selection == '' else int(selection)
        # setup the fastshutter
        self.fast_shutter_type = beamline['fs%d_type' % selection][0]
        try:
            # motor type - get the positions from the user? Otherwise can define the positions in the pars
            if self.fast_shutter_type == 'motor':
                default_alias = beamline['fs%d_alias' % selection][0]
                alias = raw_input('Which motor to use for fastshutter? [%s] : ' % default_alias)
                alias = default_alias if alias == '' else alias
                pos = raw_input('Position of %s for fast shutter OPEN : ' % alias)
                pos = float(pos)
                self.fast_shutter_open_pos = pos
                self.fast_shutter_open_command = 'amove %s %0.2f' % (alias, pos)
                pos = raw_input('Position of %s for fast shutter CLOSE : ' % alias)
                pos = float(pos)
                self.fast_shutter_close_pos = pos
                self.fast_shutter_close_command = 'amove %s %0.2f' % (alias, pos)
                self.fast_shutter_device_name = dm.actors[alias].device_name

            elif self.fast_shutter_type == 'dio_line':
                self.fast_shutter_device_name = beamline['fs%d_device' % selection][0]
                self.fast_shutter_line = beamline['fs%d_line' % selection][0]
                self.fast_shutter_sense = int(beamline['fs%d_sense' % selection][0])
                self.fast_shutter_open_command = beamline['fs%d_spyc_magic_open' % selection][0]
                self.fast_shutter_close_command = beamline['fs%d_spyc_magic_close' % selection][0]
                self.fast_shutter_open_pos = 0 # to avoid error
                self.fast_shutter_close_pos = 0 # to avoid error

            elif self.fast_shutter_type == 'dio_port':
                self.fast_shutter_device_name = beamline['fs%d_device' % selection][0]
                self.fast_shutter_port = beamline['fs%d_port' % selection][0]
                self.fast_shutter_sense = int(beamline['fs%d_sense' % selection][0])
                self.fast_shutter_open_command = beamline['fs%d_spyc_magic_open' % selection][0]
                self.fast_shutter_close_command = beamline['fs%d_spyc_magic_close' % selection][0]
                self.fast_shutter_open_pos = 0 # to avoid error
                self.fast_shutter_close_pos = 0 # to avoid error

            elif self.fast_shutter_type == 'valve':
                self.fast_shutter_device_name = beamline['fs%d_device' % selection][0]
                self.fast_shutter_open_command = beamline['fs%d_spyc_magic_open' % selection][0]
                self.fast_shutter_close_command = beamline['fs%d_spyc_magic_close' % selection][0]
                self.fast_shutter_open_pos = 0 # to avoid error
                self.fast_shutter_close_pos = 0 # to avoid error
            else:
                print 'fast shutter type defined in ~/BEAMLINE_PARAMETERS.par not recognised'
            print 'Using fastshutter %d (%s type)' % (selection, self.fast_shutter_type)
        except:
            print 'problem with the fast shutter definition in ~/BEAMLINE_PARAMETERS.par'



    def setExperiment(self):
        '''Set the sample logfiles and the parfile directory'''

        experiment_name = raw_input('Experiment name [usually Proposer_MMYY]: ')
        if experiment_name == '':
            print "No name given, please try again!"
            return

        print "What type of proposal is this?"
        options = {1:'proposal', 2:'inhouse', 3:'industry', 4:'test'}
        for ii in range(1, 5):
            print '[%d] : %s' % (ii, options[ii])
        tmp = raw_input('Experiment type [1] : ')
        tmp = 1 if tmp == '' else int(tmp)
        experiment_type = options[tmp]

        year = str(time.localtime()[0]) # year as a string

        print 'Will create structure : .../%s/%s/%s ' % (experiment_type, year, experiment_name)
        tmp = raw_input('Proceed with creating directories, etc ? [y]/n')
        ok = True if tmp.lower() in ['', 'y', 'yes', 'true'] else False

        if ok:

            # setup the directories: RUCHE
            # ruche_path/experiment_type/2018/experiment_name ...
            ruche_path = beamline['ruche_path'][0]
            ruchedir = os.path.join(ruche_path, experiment_type, year, experiment_name)
            if not os.path.isdir(ruchedir):
                os.system('mkdir -p %s' % ruchedir) # mkdir -p ... create multi directories if needed
                print "Create directory on RUCHE for archiving: %s" % ruchedir
            else:
                print "RUCHE directory already exists: %s" % ruchedir

            # setup the directories: RECONSTRUCTION
            # ssh from srv4 to the reconstruction machine
            reconstruction_path = beamline['reconstruction_path'][0]
            reconstruction_machine_IP = beamline['reconstruction_machine_IP'][0]
            recondir = os.path.join(reconstruction_path, experiment_type, year, experiment_name)
            a = os.system('ssh %s \'mkdir -p %s\'' % (reconstruction_machine_IP, recondir))
            if a == 0:
                print "Create RECONSTRUCTION directory for reconstruction: %s" % recondir
                # copy a machinefile if defined
                if beamline.has_key('machinefile') and (beamline['machinefile'][0] != ''):
                    machinefile = beamline['machinefile'][0]
                    a = os.system('ssh %s \'cp %s %s/.\'' % (reconstruction_machine_IP, machinefile, recondir))
                    print "copy the machinefile"
            else:
                print "RECONSTRUCTION directory already exists: %s" % recondir
            self.reconstruction_directory = recondir

            # setup the directories on SRV3
            spool_path = beamline['spool_path'][0]
            spooldir = os.path.join(spool_path, experiment_type, year, experiment_name)
            spool_subdir = os.path.join(experiment_type, year, experiment_name)
            if not os.path.isdir(spooldir):
                os.system('mkdir -p %s' % spooldir)
                print "Create directory on SPOOL for raw data and logfile: %s" % spooldir
            else:
                print "SPOOL directory already exists: %s" % spooldir
            self.spool_directory = spooldir
            self.spool_subdirectory = spool_subdir

            # setup the directories for the second log file
            logfile_path = beamline['logfile_path'][0]
            logfiledir = os.path.join(logfile_path, experiment_type, year, experiment_name)
            if not os.path.isdir(logfiledir):
                os.system('mkdir -p %s' % logfiledir)
                print "Create directory for the second logfile: %s" % logfiledir
            else:
                print "second logfile directory already exists: %s" % logfiledir

            # logfile paths for recording scans
            self.sample_logfile = "%s/scan_log.pickle" % spooldir # this is with the raw data
            self.sample_logfile2 = "%s/scan_log.pickle" % logfiledir # this is with the config files

            # create a script on  for rsync of data to RUCHE
            ### this has two problems - spool path and ruche paths are not the same seen from psichegpu

            print 'create a rsync script to backup the raw data'
            rsync_script_template = beamline['rsync_script_template'][0]
            a = os.system('ssh %s \'cp %s %s/.\'' % (reconstruction_machine_IP, rsync_script_template, recondir))
            ruche_path = beamline['ruche_path_from_recon'][0]
            ruchedir = os.path.join(ruche_path, experiment_type, year, experiment_name)
            spool_path = beamline['spool_path_from_recon'][0]
            spooldir = os.path.join(spool_path, experiment_type, year, experiment_name)
            recondirX = recondir.replace('/', '\/')
            ruchedirX = ruchedir.replace('/', '\/')
            spooldirX = spooldir.replace('/', '\/')
            a = os.system('ssh %s \"sed -i \'s/AAA/%s/g\' %s/rsync_raw_data_to_ruche.sh\"' % (reconstruction_machine_IP, recondirX, recondir))
            a = os.system('ssh %s \"sed -i \'s/BBB/%s/g\' %s/rsync_raw_data_to_ruche.sh\"' % (reconstruction_machine_IP, ruchedirX, recondir))
            a = os.system('ssh %s \"sed -i \'s/CCC/%s/g\' %s/rsync_raw_data_to_ruche.sh\"' % (reconstruction_machine_IP, spooldirX, recondir))

        else:
            print 'Not creating directories, etc'

    def setFlyScanParameters(self):
        ''' Calculate the parameters for flyscan and apply the configuration'''

        print "Setup the FTP data transfer for Flyscan"
        self.ftp_proxy.stop()
        time.sleep(0.1)
        self.ftp_proxy.put_property({'FilesDestinationDirectory':['/nfs/srv3/spool1/flyscan-tmp-data/']})
        time.sleep(0.1)
        self.ftp_proxy.init()
        time.sleep(1)

        # TEMPORARY
        print "not recording the storage ring current..."
        fss.cfgs.tomo_1D.sensors.storage_ring_current.enable = False
        # TEMPORARY
        print "not recording the cpt..."
        fss.cfgs.tomo_1D.sensors.cpt1.enable = False

        # which rotation stage?
        fss.cfgs.tomo_1D.easy_config.parameters.rotation_motor = self.rotation_motor_alias
        # which camera?
        fss.cfgs.tomo_1D.easy_config.parameters.camera = self.camera_name

        # Now, send scan parameters to easy config, which does the calculations
        # for the scan
        fss.cfgs.tomo_1D.easy_config.parameters.rotation_range = [0.0, self.scanrange]
        fss.cfgs.tomo_1D.easy_config.parameters.number_of_images = self.nproj
        fss.cfgs.tomo_1D.easy_config.parameters.safetytime = self.safetytime
        fss.cfgs.tomo_1D.easy_config.parameters.safetytimeauto = self.safetytimeauto
        fss.cfgs.tomo_1D.easy_config.parameters.clock_mode = 'position' # should prob remove this from easy config

        # for the bright and dark hook
        fss.cfgs.tomo_1D.easy_config.parameters.number_of_references = self.nref
        fss.cfgs.tomo_1D.easy_config.parameters.number_of_darks = self.ndark
        fss.cfgs.tomo_1D.easy_config.parameters.measurement_position = self.measurementPosition
        fss.cfgs.tomo_1D.easy_config.parameters.reference_displacement = self.refDisplacement
        fss.cfgs.tomo_1D.easy_config.parameters.reference_motor = dm.actors[self.reference_motor_alias].device_name

        # fast shutter parameters for dark_and_bright hook
        fss.cfgs.tomo_1D.easy_config.parameters.shutter_type = self.fast_shutter_type
        fss.cfgs.tomo_1D.easy_config.parameters.shutter_device_name = self.fast_shutter_device_name
        fss.cfgs.tomo_1D.easy_config.parameters.shutter_motor_open_pos = self.fast_shutter_open_pos
        fss.cfgs.tomo_1D.easy_config.parameters.shutter_motor_close_pos = self.fast_shutter_close_pos
        fss.cfgs.tomo_1D.easy_config.parameters.shutter_line = self.fast_shutter_line
        fss.cfgs.tomo_1D.easy_config.parameters.shutter_port = self.fast_shutter_port
        fss.cfgs.tomo_1D.easy_config.parameters.shutter_sense = self.fast_shutter_sense
        fss.cfgs.tomo_1D.easy_config.parameters.initial_sleep_val = self.initial_sleep

        # for the take calib images hook
        fss.cfgs.tomo_1D.easy_config.parameters.number_of_calibs = self.calib_images

        # stop the camera!
        self.stopLive()
        time.sleep(1)

        # set the updated config
        fss.set_cfg("tomo_1D")

        # retrive the true angular step from the speitbox...
        # modify for new speitbox device
        self.flyscan_pulse_period = self.sbox_proxy.actualPulsePeriod * 1.


    def FlyScan(self):
        ''' This should launch flyscan with the current config'''

        # handle file names with recording manager
        if use_recording_manager:
            # update the logfile, and make sure the scan name is ok
            self.writeLogFile(flyscan=True)
            # after the scan - write par file
            axis = (self.proxy.roiwidth+1)/2.
            self.writeParFile(axis, flyscan=True)

        print "Launching FlyScan - move to zero to start..."
        self.stopLive()
        if beamline['BEAMLINE'][0] == 'PSICHE':
            updateon.updateoff()

        get_ipython().magic('amove %s 0 %s %0.3f' % (self.rotation_motor_alias, self.reference_motor_alias, self.measurementPosition))
        refx_initial = self.reference_proxy.position * 1.
        if self.fast_shutter_for_snap:
            # open the fast shutter
            get_ipython().magic(self.fast_shutter_open_command)

        #refxdata = np.zeros(int(self.nproj*(1+(self.proxy.exposureTime/1000))))
        #refxsamples = 0
        # launch the FlyScan

        try:
            fss.proxy.set_timeout_millis(30000)
            fss.start()
            sys.stdout.write('FlyScan is running ')
            time.sleep(5)
            # wait for fss and merger...
            # could check the raw data streams in the merger - if no orca, stop!
            if (fss.state != PyTango._PyTango.DevState.RUNNING) and (fss.status.find('raw data streams list is empty!') != -1):
                    print "RAW DATA STREAMS ERROR... "
                    print "########## init and reconfigure flyscan ###########"
                    # wait for the camera to restart
                    wait = True
                    while wait:
                        print "wait while the camera restarts"
                        try:
                            if self.proxy.state() == PyTango._PyTango.DevState.STANDBY:
                                wait = False
                        except:
                            time.sleep(1)
                    try:
                        fss.proxy.init()
                    except:
                        time.sleep(5)
                        fss.proxy.init()
                    get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
                    time.sleep(10)
                    self.setFlyScanParameters()
                    fss.start()

            #elif len(self.merger_proxy.rawstreams)<2:
                    #print "ONLY ONE RAW DATA STREAM !!! NOT TWO... "
                    #print "########## abort, init and reconfigure flyscan ###########"
                    #fss.proxy.abort()
                    ## wait for the camera to restart
                    #time.sleep(10)
                    #wait = True
                    #while wait:
                        #print "wait while the camera restarts"
                        #try:
                            #if self.proxy.state() == PyTango._PyTango.DevState.STANDBY:
                                #wait = False
                        #except:
                            #time.sleep(1)
                    #try:
                        #fss.proxy.init()
                    #except:
                        #time.sleep(5)
                        #fss.proxy.init()
                    #get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
                    #time.sleep(10)
                    #self.setFlyScanParameters()
                    #fss.start()
            else:
                print "scan looks normal..."

            time.sleep(5)
            if (fss.state != PyTango._PyTango.DevState.RUNNING) and (fss.status.find('raw data streams list is empty!') != -1):
                    print "RAW DATA STREAMS ERROR... "
                    print "########## init and reconfigure flyscan ###########"
                    # wait for the camera to restart
                    wait = True
                    while wait:
                        print "wait while the camera restarts"
                        try:
                            if self.proxy.state() == PyTango._PyTango.DevState.STANDBY:
                                wait = False
                        except:
                            time.sleep(1)
                    try:
                        fss.proxy.init()
                    except:
                        time.sleep(5)
                        fss.proxy.init()
                    get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
                    time.sleep(10)
                    self.setFlyScanParameters()
                    fss.start()

            #elif len(self.merger_proxy.rawstreams)<2:
                    #print "ONLY ONE RAW DATA STREAM !!! NOT TWO... "
                    #print "########## abort, init and reconfigure flyscan ###########"
                    #fss.proxy.abort()
                    ## wait for the camera to restart
                    #time.sleep(10)
                    #wait = True
                    #while wait:
                        #print "wait while the camera restarts"
                        #try:
                            #if self.proxy.state() == PyTango._PyTango.DevState.STANDBY:
                                #wait = False
                        #except:
                            #time.sleep(1)
                    #try:
                        #fss.proxy.init()
                    #except:
                        #time.sleep(5)
                        #fss.proxy.init()
                    #get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
                    #time.sleep(10)
                    #self.setFlyScanParameters()
                    #fss.start()
            else:
                print "scan looks normal..."

            print('NOT CHECKING MERGER STATE AT END OF SCAN...')
            loop = True
            while loop:

                try:
                    test = (self.merger_proxy.state() == PyTango._PyTango.DevState.RUNNING) or (fss.state == PyTango._PyTango.DevState.RUNNING)
                    #test = fss.state == PyTango._PyTango.DevState.RUNNING
                    if not test:
                        loop = False
                except PyTango.CommunicationFailed:
                    print '_____  FAILED TO READ FSS OR MERGER!!!  CONTINUING LOOP _____'
                    print e
                    print type(e)

                sys.stdout.write(' ... ')
                sys.stdout.flush()
                time.sleep(0.5)
                sys.stdout.write('\b'*5)
                sys.stdout.write('_'*5)
                sys.stdout.flush()
                time.sleep(0.5)
                sys.stdout.write('\b'*5)


        except KeyboardInterrupt:
            print "STOPPING SCAN..."
            fss.abort()

        finally:
            if self.fast_shutter_for_snap:
                # close the fast shutter
                get_ipython().magic(self.fast_shutter_close_command)
            # reset tomorz speed
            get_ipython().magic('setspeed %s %f' % (self.rotation_motor_alias, self.rotation_velocity_nominal))
            # reset tomorefx position
            if True:
                get_ipython().magic('amove %s %f' % (self.reference_motor_alias, self.measurementPosition))
            else:
                print "DISABLED THE MOVING OF TOMO1TX"

        print "FlyScan has finished..."

        print "___________________"
        #print "tomorefx report from %d samples during scan:" % refxsamples
        #refxdata = refxdata[0:refxsamples]
        #moderefx = mode(refxdata)[0][0]
        #print "mode position:  %0.5f" % moderefx
        #subset = np.nonzero((np.abs(refxdata-moderefx))<0.25)
        #meanrefx = refxdata[subset].mean()
        #stdrefx = refxdata[subset].std()
        #print "in scan position: %0.5f +- %0.6f" % (meanrefx, stdrefx)
        #if stdrefx > 0.0001:
        #    print "standard deviation over 100 nm ... possible problem"
        loop = True
        timeout = 0
        while loop and (timeout<60):
            try:
                print self.merger_proxy.state()
                self.my_nxs_file = self.merger_proxy.currentNexusFile
                loop = False
            except:
                timeout+=1
                time.sleep(1)
        print "FlyScan finished"

        #print "___________________"
        if not use_recording_manager:#try: # change this for recording manager approach
            print 'Not using a try in the post flyscan bit'
            # FlyScan has finished - now we rename the files, and start PSICHE processing
            # where are the data?!
            # wait for the merger to finish
            loop = True
            timeout = 0
            while loop and (timeout<60):
                try:
                    print self.merger_proxy.state()
                    self.my_nxs_file = self.merger_proxy.currentNexusFile
                    loop = False
                except:
                    timeout+=1
                    time.sleep(1)

            # update the logfile, and make sure the scan name is ok
            self.writeLogFile(flyscan=True)
            # rename the file and the folder
            self.__renameNexus()
            # after the scan - write par file
            axis = (self.proxy.roiwidth+1)/2.
            self.writeParFile(axis, flyscan=True)
            #self.writeTransferScript(flyscan=True)
            print "finished post scan treatments"
        #except:
        #    print "problem in the log/rename/parfile/scripts section"


    def writeLogFile(self, flyscan=False):
        '''Write useful settings to the log file
            Could (try) to write all log files directly on Ruche
            Log is a pickle dictionary, with unique scannames and all meta data
            Update this to be a dictionary of dictionaries, to be more flexible'''

        if os.path.isfile(self.sample_logfile):
            print "recording scan in log file: %s" % self.sample_logfile
            log = pickle.load(open(self.sample_logfile))
        else:
            print "will create log file: %s" % self.sample_logfile
            #log = {"filename":["scanname, nxs_filename, nproj, scanrange, exposuretime, nref, ndark, optic, detpos, energy, pinkenergy, linuxtime"]}
            log = {}
        # test the filename
        suffix = 0
        self.savename = self.scanname
        while self.savename in log:
            suffix+=1
            self.savename = self.scanname+"_"+str(suffix)
        print "scan name will become %s" % self.savename
        if not flyscan:
            self.my_nxs_file = None
        # prepare my dictionary
        mydata = {}
        # fixed fields
        mydata.update({'scanname':self.scanname})
        mydata.update({'nxs_filename':self.my_nxs_file})
        mydata.update({'nproj':self.nproj})
        mydata.update({'scanrange':self.scanrange})
        mydata.update({'exposuretime':self.proxy.exposuretime})
        mydata.update({'nref':self.nref})
        mydata.update({'ndark':self.ndark})
        mydata.update({'optic':self.optic})
        mydata.update({'pinkenergy':self.pinkenergy})
        mydata.update({'linuxtime':time.localtime()})
        # variable fields - can add to the list defined in the beamline_parameters file
        for key in self.log_attribute_list:
            if dm.actors.has_key(key):
                mydata.update({key:dm.actors[key].get()})
            else:
                print("Not recording {} in log as it does not exist in this Spyc session".format(key))
        #log.update({self.savename:[self.scanname, self.my_nxs_file, self.nproj, self.scanrange, self.proxy.exposuretime, self.nref, self.ndark, self.optic, detposvalue, 'mono.read().value', self.pinkenergy, time.localtime()]})
        log.update({self.savename:mydata})
        pickle.dump(log, open(self.sample_logfile, 'wb'))
        # save to the other backup as well
        pickle.dump(log, open(self.sample_logfile2, 'wb'))

    def __renameNexus(self):
        '''rename the FlyScan nexus file to something user friendly -
        we have written the logfile and have savename - a name not already used in this experiment'''
        # rename the nxs files

        # this should move data to the spooldir
        my_nxs_dir = self.my_nxs_file[0:self.my_nxs_file.rfind(os.sep)]
        new_nxs_file = my_nxs_dir + os.sep + self.savename + ".nxs"
        # rename the main projections file
        os.system("mv %s %s" % (self.my_nxs_file, new_nxs_file))
        # rename the folder
        new_nxs_dir = my_nxs_dir[0:my_nxs_dir.rfind(os.sep)] + os.sep + self.savename
        os.system("mv %s %s" % (my_nxs_dir, new_nxs_dir))
        # now, move the folder to the experiment spool directory
        os.system('mv %s %s' % (new_nxs_dir, self.spool_directory))

        # resulting nxs name and path:
        new_nxs_dir = self.spool_directory + os.sep + self.savename
        new_nxs_file = new_nxs_dir + os.sep + self.savename + ".nxs"
        # update file in self
        self.my_nxs_file = new_nxs_file
        # meta data nxs files are not renamed, but are in the renamed folder

        print 'rename .nxs file as %s.nxs' % self.savename
        print 'move data to %s' % new_nxs_dir


    #def setupOffsetTomo(self):
        #'''setup a half acquisition scan without going through align sample'''
        #print('Not ready yet')
        #return
        #pixsizemm = self.pixelsize * self.proxy.binningH / 1000
        #imsize = self.proxy.roiWidth
        #print('standard acquisition diameter : %d voxels / %0.1 mm' % (imsize, imsize*pixsizemm))
        #print('max possible offset acquisition : %d voxels / %0.1 mm' % (1.9*imsize, 1.9*imsize*pixsizemm))
        #val = raw_input('How big do you want? (in mm if <100, voxels if >100) : ')
        #if val == '':
            #print('No value given, quitting')
            #return
        #else:
            #val = float(val)
        #if val > 100:
            #print('Aim for %d voxel diameter reconstruction')
            #dpix = val
        #else:
            #print('Aim for %0.1f mm diameter reconstruction')
            #dpix = val / pixsizemm
        ## where should the axis go?
        #myradpix = dpix /2.
        #delta = myradpix - (imsize[1] / 2.)
        #nproj180 = imsize[1] * 1500 / 2048.
        #nproj180 = int(100*np.ceil(nproj180/100)) # round up to 100 projections
        #if delta<0:
            #print "This can be a normal 180 degree, %d projections scan" % nproj180
            #myrefxmm = 0.
            #self.nproj = int(nproj180)
            #self.scanrange = 180.
        #else:
            #print "This should be a 360 degree, half acquisition scan"
            ## make sure we don't go too far
            #delta = min(delta, imsize[1]*0.45)
            #myrefxmm = - delta * pixsizemm

            #get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, myrefxmm))
            #self.measurementPosition = myrefxmm
            #print "setting measurement position"
            #self.nproj = int(nproj)
            #self.scanrange = 360.

    def setScanParameters(self):
        '''User select tomo scan parameters'''

        userinput = raw_input("scan name [%s]:  " % self.scanname)
        if userinput != '':
            if userinput.find(' ') != -1:
                print "No spaces in the sample name please !"
                return
            self.scanname = userinput

        userinput = raw_input("scan range 180 or 360 [%d]:  " % self.scanrange)
        if userinput != '':
            self.scanrange = float(userinput)

        if self.reference_motor_alias.find('x') != -1:
            if (self.scanrange == 180.) and (np.abs(self.measurementPosition)>0.2):
                print "WARNING! Scanrange 180 degrees but rotation axis may be offset!"
                print "measurementPosition currently %s = %0.2f" % (self.reference_motor_alias, self.measurementPosition)
                userinput = raw_input("Set measurement position to zero? [y=0]/n=%0.2f" % self.measurementPosition)
                if userinput.lower() in ['', 'y', 'yes', '0']:
                    self.measurementPosition = 0.0
                print "Will scan at %s = %0.2f" % (self.reference_motor_alias, self.measurementPosition)


        userinput = raw_input("number of projections [%d]:  " % self.nproj)
        if userinput != '':
            self.nproj = int(userinput)

        userinput = raw_input("number of references [%d]:  " % self.nref)
        if userinput != '':
            self.nref = int(userinput)

        userinput = raw_input("number of darks [%d]:  " % self.ndark)
        if userinput != '':
            self.ndark = int(userinput)

        if self.calib_images>0:
            tmp='y'
        else:
            tmp='n'
        userinput = raw_input("take images after scan for movement correction? [%s]:  " % tmp)
        if userinput=="":
            userinput = tmp
        if userinput.lower() == 'y' or userinput.lower() == 'yes':
            userinput = raw_input("how many images for movement correction? [%d]:  " % self.calib_images)
            if userinput != '':
                self.calib_images = int(userinput)
        else:
            self.calib_images=0

        userinput = raw_input("Setup for a fast synchro sequence? y/[n] :  ")
        if userinput.lower() in ['y', 'yes']:
            # get the special parameters for the dimax, if using
            if self.proxy.dev_name().find('dimax') != -1:
                print "DIMAX camera - special options"
                ntomos = np.floor((6429. * 2000 * 2000) / (self.proxy.roiWidth * self.proxy.roiHeight * self.nproj))
                userinput = raw_input('Do multiple tomos? y/[n] : ')
                test = True if userinput.lower() in ['y', 'yes', 'true'] else False
                if test:
                    userinput = raw_input('How many tomos? (possible %d) [%d] : ' % (ntomos, ntomos))
                    self.dimax_n_tomos = ntomos if userinput == '' else int(userinput)
                    userinput = raw_input('Time between tomos / s [0] : ')
                    self.dimax_interval_tomos = 0 if userinput == '' else float(userinput)
                    print "Will do %d tomos, separated by %0.2f seconds" % (self.dimax_n_tomos, self.dimax_interval_tomos)
                else:
                    self.dimax_n_tomos = 1
                    self.dimax_interval_tomos = 0.
                    print "Will do single tomos"

            else:
                self.dimax_n_tomos = 1
                self.dimax_interval_tomos = 0.
                print "Will do single tomos"
            print "no flyscan setup for this mode - run mt.synchroStartRotation() when ready..."
            return

        # stop the camera before trying to change anything
        self.stopLive()
        if self.proxy.acquisitionMode == 'SINGLE':
            print 'In acquisition mode SINGLE - setting up for Flyscan()'
            self.proxy.fileFormat = 'NXS'
            self.proxy.filePrefix = 'orca'
            self.proxy.fileTargetPath = 'S:\\flyscan_local_in' # ADD TO BEAMLINE SPECIFIC CONFIG
            self.proxy.fileNbFrames = 10
            # autocalc safety time?
            if self.safetytimeauto == True:
                self.safetytime = np.max([(self.proxy.exposuretime/1000.) * 0.05, 0.002])
            # push these parameters to the flyscan config
            print "Apply new parameters to the flyscan config"
            self.setFlyScanParameters()
        elif self.proxy.acquisitionMode == 'ACCUMULATION':
            print 'In acquisition mode ACCUMULATION - setting up for tomoscan()'
            self.proxy.fileFormat = 'EDF'
            self.proxy.filePrefix = 'orca'
            self.proxy.fileNbFrames = 1
            # autocalc safety time?
            if self.safetytimeauto == True:
                self.safetytime = np.max([(self.proxy.exposuretime/1000.) * 0.05, 0.002])
        else:
            print 'acquisition mode not recognised! Problem with the camera?'


    def showScanParameters(self):
        '''Display the current parameters'''
        print "scan name = %s" % self.scanname
        print "reconstruction directory = %s" % self.reconstruction_directory
        print "%d projections over %d degrees" % (self.nproj, self.scanrange)
        print "%d references, %d darks" % (self.nref, self.ndark)


    def doTomo(self, scanname=None):
        '''
        Launch the appropriate scan for the acquisition mode (FlyScan or Tomoscan)
        after checking the pressure, etc
        '''
        # check the vaccuum
        #check = self.pressureCheck()
        check = True
        if check:
            if scanname != None:
                # supply a new scanname without changing anything else
                self.scanname = scanname
            if self.proxy.acquisitionMode == 'SINGLE':
                self.FlyScan()
            elif self.proxy.acquisitionMode == 'ACCUMULATION':
                self.tomoscan()
            else:
                print 'acquisition mode not recognised! Problem with the camera?'
        else:
            print 'Not launching scan because of vacuum problem'


    def pressureCheck(self):
        '''check VFM and mono'''
        check = True
        dp = PyTango.DeviceProxy('i03-c-c03/vi/jpen.1')
        dp2 = PyTango.DeviceProxy('i03-c-c04/vi/jpen.1')
        vfm = dp.read_attribute('pressure').value
        mono = dp2.read_attribute('pressure').value
        print "VFM pressure: %e   Mono pressure: %e " % (vfm, mono)
        if (vfm>5e-7) or (mono>5e-7):
            time.sleep(10)
            vfm = dp.read_attribute('pressure').value
            mono = dp2.read_attribute('pressure').value
            print "Second try:  VFM pressure: %e   Mono pressure: %e " % (vfm, mono)
            if (vfm>5e-7) or (mono>5e-7):
                print "Help! Vaccuum problem in mono or mirror -"
                print "close front end...  Call local contact or hall coordinator!"
                get_ipython().magic(u'feclose')
                check = False
            else:
                print "Possible vacuum problem: Watch _very_ carefully!!!"
        else:
            print "Pressure OK"
        return check


    def tomoscan(self):
        '''set up everything, launch scan'''
        #try:
        print "THIS IS IN DEBUG MODE WITHOUT THE TRY/EXCEPT "
        if True:
            # write the logfile first, to make sure the scan name is ok
            self.writeLogFile(flyscan=False)
            # writeLogFile returns the good name, with right appended number
            self.scanname = self.savename
            # make a folder for this scan to be moved to - keep all data in the flyscan spool
            scandir = self.spool_directory + os.sep + self.scanname
            os.system("mkdir %s" % scandir)
            os.system("mkdir %s/images" % scandir)
            os.system("mkdir %s/refA" % scandir)
            os.system("mkdir %s/refB" % scandir)
            os.system("mkdir %s/dark" % scandir)
            os.system("mkdir %s/calib" % scandir)

            # we don't need to read the images into python
            self.stopLive()
            self.readimage = False
            self.proxy.resetfileindex()
            self.proxy.fileGeneration = True

            # switch off the update !
            updateon.updateoff()

            # open the shutter
            try:
                get_ipython().magic(self.shutter_open_command)
                print "opened shutter"
            except:
                print "front end or shutter responding slowly! Probably nothing to worry about - continuing"
            self.fastshutteropen()
            fastshuttermemo = self.fast_shutter_for_snap

            if self.fast_shutter_for_snap:
                self.fast_shutter_for_snap = False

            # first references - write directly into the good folder
            # note this was 'Y:\\flyscan-data\\psiche-soleil\\com-psiche\\%s\\refA'
            mypath = self.spool_directory.replace('/', '\\')
            mypath = mypath.replace('\\nfs', '\\')
            self.proxy.fileTargetPath = '%s\\%s\\refA' % (mypath, self.scanname)
            self.proxy.nbFrames = self.nref*1.0
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            #self.proxy.Init()
            print "move to tomorz = 0 to start scan"
            get_ipython().magic(u'setspeed tomorz %0.1f' % self.rotation_velocity_nominal)
            get_ipython().magic(u'amove tomorz 0')

            if self.measurementPosition == None:
                self.measurementPosition == dm.actors[self.reference_motor_alias].value*1.
                print "Using the current %s position (%f) for measurementPosition" % (self.reference_motor_alias, self.measurementPosition)
            print "move in x for references"
            get_ipython().magic(u'dmove %s %0.2f' % (self.reference_motor_alias, self.refDisplacement))
            self.snap()
            print "moving back x after references"
            retry = 0
            marge = self.pixelsize / 10000 # 1/10 of a pixel in mm
            diff = 50. # to start with
            while (diff > marge) and retry<10:
                get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
                diff = np.abs(dm.actors[self.reference_motor_alias].get() - self.measurementPosition)
                retry+=1
                if retry == 10:
                    print "Tried 10 times to reach good tomorefx position - still at %f distance" % diff

            # main scan
            print "semi flyscan option"
            # calculations
            blockangle = self.block*self.scanrange/self.nproj
            accfac = self.proxy.exposuretime / self.proxy.exposureacctime
            # program speitbox
            self.sbox_proxy.mode = 1
            self.sbox_proxy.pulsePeriod = self.scanrange/(self.nproj*accfac)
            self.sbox_proxy.sequenceLength = int(self.block*accfac)
            self.sbox_proxy.firstpulseatfirstposition = False # note this is different to FlyScan
            self.sbox_proxy.Prepare()
            # the block angle is slightly different due to rounding errors
            blockanglereal = self.sbox_proxy.pulsePeriod * int(self.block*accfac)
            # prepare camera
            self.proxy.triggerMode = 'EXTERNAL_MULTI'
            self.proxy.nbFrames = self.block
            self.proxy.fileTargetPath = '%s\\%s\\images' % (mypath, self.scanname)
            self.proxy.resetfileindex()
            #self.proxy.init()
            readtime = self.calcReadoutTime()
            frametime = (self.proxy.exposureacctime/1000) + readtime + self.safetytime
            # prepare rotation
            speed = self.sbox_proxy.pulsePeriod / frametime
            get_ipython().magic(u'setspeed tomorz %0.3f' % speed)
            # block start angles
            startangles = np.linspace(0, self.scanrange, self.nproj/self.block, False)

            # if we have an initial sleep, do it
            sys.stdout.write("initial sleep of %0.1f seconds" % float(self.initial_sleep))
            time.sleep(self.initial_sleep)

            # scan loop
            sys.stdout.write("\ntomorz =   0.000")
            for ii in range(len(startangles)):
                # move to start of block - wait to arrive
                self.rotation_proxy.position = startangles[ii]
                sys.stdout.write("\b"*7)
                sys.stdout.write(("%0.3f" % startangles[ii]).rjust(7))
                sys.stdout.flush()
                while self.rotation_proxy.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]:
                    time.sleep(0.1)
                time.sleep(0.1)
                # start the camera, the speitbox, and then the movement
                self.proxy.snap()
                # wait for the camera to respond
                timeout = 0
                while (self.proxy.state() != PyTango.DevState.RUNNING) and (timeout<300):
                    time.sleep(0.1)
                    timeout+=1
                if timeout==300:
                    print "WAITED 30 SECONDS FOR THE CAMERA TO RESPOND !!! - problem ? ---"
                time.sleep(0.1)
                # start the speitbox and then the rotation
                self.sbox_proxy.start()
                self.rotation_proxy.position = startangles[ii]+blockanglereal+0.05
                # wait for the camera to finish writing
                while (self.proxy.state() == PyTango._PyTango.DevState.RUNNING) or \
                    (self.rotation_proxy.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]):
                    time.sleep(0.1)
                time.sleep(0.1)

            # special block to finish a scan that is not a whole multiple of blocks
            # start the final block, and stop the camera at the end of the scan...
            if len(startangles)*blockangle < self.scanrange:
                # how many projections ?
                specialblock = self.nproj - (len(startangles)*self.block)
                # modify speitbox sequence, camera
                self.sbox_proxy.sequenceLength = int(specialblock*accfac)
                self.sbox_proxy.Prepare()
                blockanglereal = self.sbox_proxy.pulsePeriod * int(specialblock*accfac)
                # move to start of block - wait to arrive
                self.rotation_proxy.position = startangles[ii] + blockangle
                sys.stdout.write("\b"*7)
                sys.stdout.write(("%0.3f" % (startangles[ii]+ blockangle)).rjust(7))
                sys.stdout.flush()
                while self.rotation_proxy.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]:
                    time.sleep(0.1)
                time.sleep(0.1)
                # start the camera, the speitbox, and then the movement
                self.proxy.snap()
                self.sbox_proxy.start()
                self.rotation_proxy.position = startangles[ii] + blockangle + blockanglereal + 0.05 # should move just past the end of the scan
                # wait until the rotation stage has finished moving
                while self.rotation_proxy.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]:
                    time.sleep(0.1)
                time.sleep(1)
                # at this point should have all the images, and sbox should have finished.
                # camera is still running, because the block was shorter than normal.
                # reprogram sbox
                self.sbox_proxy.mode = 0
                self.sbox_proxy.pulsePeriod = 50
                self.sbox_proxy.sequenceLength = 100
                self.sbox_proxy.Prepare()
                self.sbox_proxy.start()
                # stop the camera
                self.proxy.stop()

                # wait for the camera to finish writing
                while (self.proxy.state() == PyTango._PyTango.DevState.RUNNING) or \
                    (tomorz.state() not in [PyTango.DevState.STANDBY, PyTango.DevState.ON, PyTango.DevState.ALARM]):
                    time.sleep(0.1)
                time.sleep(0.1)

                # stop the sbox
                self.sbox_proxy.abort()

            # after the loop
            print "reset motor speed"
            get_ipython().magic(u'setspeed tomorz %0.1f' % self.rotation_velocity_nominal)

            # calib images if we are doing them
            if self.calib_images > 0:
                # setup the camera for this
                self.proxy.triggerMode = 'INTERNAL_SINGLE'
                self.proxy.nbFrames = 1
                self.proxy.fileTargetPath = '%s\\%s\\calib' % (mypath, self.scanname)
                self.proxy.resetfileindex()
                print "Taking calibration images after scan..."
                calib_positions = np.linspace(self.scanrange, 0, self.calib_images, True)
                for calib_pos in calib_positions:
                    get_ipython().magic(u'amove tomorz %0.1f' % calib_pos)
                    self.snap()
            time.sleep(0.5)

            # last references
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            self.proxy.nbFrames = self.nref
            self.proxy.fileTargetPath = '%s\\%s\\refB' % (mypath, self.scanname)
            self.proxy.resetfileindex()

            print "move to tomorz = 0 for references"
            get_ipython().magic(u'amove tomorz 0')
            print "move in x for references"
            get_ipython().magic(u'dmove %s %0.2f' % (self.reference_motor_alias, self.refDisplacement))
            self.snap()
            print "moving back x after references"
            get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))

            # update the reference in mt
            self.ref = self.proxy.image

            # darks
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            time.sleep(0.5)
            self.proxy.nbFrames = self.ndark
            self.proxy.resetfileindex()
            self.proxy.fileTargetPath = '%s\\%s\\dark' % (mypath, self.scanname)

            # close the shutter / fast shutter
            if fastshuttermemo:
                # close the fast shutter
                get_ipython().magic(self.fast_shutter_close_command)
            else:
                get_ipython().magic(self.shutter_close_command)
            # take the darks
            self.snap()

            # update the dark in mt
            self.dark = self.proxy.image

            # switch off saving
            print "scan finished successfully"
            self.proxy.fileGeneration = False
            axis = (self.proxy.roiwidth+1)/2.
            self.writeParFile(axis, flyscan=False)

        #except KeyboardInterrupt:
            #print "STOPPING SCAN"
            #tomorzDP.stop()
            #time.sleep(0.5)
            #get_ipython().magic(u'setspeed tomorz %0.1f' % self.rotation_velocity_nominal)

            #print "stop the camera"
            ## program speitbox
            #sbox.Abort()
            #time.sleep(0.1)
            #sbox.mode = 0
            #sbox.pulsePeriod = 50
            #sbox.sequenceLength = 100
            #sbox.Prepare()
            ## prepare camera
            #sbox.start()
            #self.proxy.stop()

        #finally:
            ## restore the fast shutter configuration
            #self.fast_shutter_for_snap = fastshuttermemo
            ## close the fast shutter - should the main shutter also close?
            #self.fastshutterclose()
            ## switch on the update !
            #if self.config == 'pink':
                #updateon.updateon('OH')
            #else:
                #updateon.updateon('EH')


    def writeParFile(self, axispos, flyscan):
        '''write a pyHST2 parfile to go with the data'''
        # open and read the template file using par_tools
        pars = par_tools.par_read(beamline['parfile_template'][0])

        # open the file to write to:
        if flyscan:
            if use_recording_manager:
                # savename is the "output" name
                recordingmanager.datasubdirectory = self.spool_subdirectory + os.sep + self.savename
                recordingmanager.filename = self.savename
                # might have to create this directory
                if not os.path.exists('%s/%s' % (self.spool_directory,self.savename)):
                    os.mkdir('%s/%s' % (self.spool_directory, self.savename))
                parfilename = '%s/%s/%s.par' % (self.spool_directory, self.savename, self.savename)
                dirname = self.savename
                nxs_name = self.savename
            else:
                # old style without recording manager
                myfile = self.my_nxs_file
                mydir = myfile[0:myfile.rfind(os.sep)]
                nxs_name = self.my_nxs_file.split(os.sep)[-1][0:-4:]
                dirname = self.my_nxs_file.split(os.sep)[-2]
                parfilename = '%s/%s.par' % (mydir, nxs_name)
        else:
            # tomoscan case - data are here:
            #parfile_directory = '/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/'
            parfilename = '%s/%s/%s.par' % (self.spool_directory, self.scanname, self.scanname)
            # might have to create this directory
            if not os.path.exists('%s/%s' % (self.spool_directory,self.scanname)):
                os.mkdir('%s/%s' % (self.spool_directory, self.scanname))
            dirname = self.scanname
            nxs_name = self.scanname

        # modify the lines that need modifying
        if beamline.has_key('PYHST_PROJECTION_FORMAT') and (beamline['PYHST_PROJECTION_FORMAT'][0] == 'NXS'):
            # This is effectively hard-coded for FlyScan...
            pars = par_tools.par_mod(pars, 'FILE_PREFIX', '%s/%s/%s.nxs' % (self.reconstruction_directory, nxs_name, nxs_name))
            pars = par_tools.par_mod(pars, 'BACKGROUND_FILE', '%s/%s/post_dark.nxs' % (self.reconstruction_directory, nxs_name))
            pars = par_tools.par_mod(pars, 'FF_PREFIX', '[ \"%s/%s/pre_ref.nxs\" , \"%s/%s/post_ref.nxs\" ]' % (self.reconstruction_directory, nxs_name, self.reconstruction_directory, nxs_name))
            pars = par_tools.par_mod(pars, 'PROJ_DS_NAME', '/flyscan_00001/scan_data/orca_image')
            pars = par_tools.par_mod(pars, 'FF_DS_NAME', 'ref_0')
            pars = par_tools.par_mod(pars, 'BACKGROUND_DS_NAME', 'dark_0')
        else: # assume edfs will be used
            pars = par_tools.par_mod(pars, 'FILE_PREFIX', '%s/%s/%s_edfs/%s' % (self.reconstruction_directory, dirname, nxs_name, nxs_name))
            pars = par_tools.par_mod(pars, 'BACKGROUND_FILE', '%s/%s/%s_edfs/dark.edf' % (self.reconstruction_directory, dirname, nxs_name))
            pars = par_tools.par_mod(pars, 'FF_PREFIX', '%s/%s/%s_edfs/ref' % (self.reconstruction_directory, dirname, nxs_name))

        # always use zero numbering - renumber in preprocessing if required
        pars = par_tools.par_mod(pars, 'NUM_FIRST_IMAGE', 0)
        pars = par_tools.par_mod(pars, 'NUM_LAST_IMAGE', (self.nproj-1))
        pars = par_tools.par_mod(pars, 'FF_NUM_FIRST_IMAGE', 0)
        pars = par_tools.par_mod(pars, 'FF_NUM_LAST_IMAGE', self.nproj)

        pars = par_tools.par_mod(pars, 'FF_FILE_INTERVAL', self.nproj)
        pars = par_tools.par_mod(pars, 'NUM_IMAGE_1', self.proxy.roiWidth)
        pars = par_tools.par_mod(pars, 'NUM_IMAGE_2', self.proxy.roiHeight)

        if flyscan:
            pars = par_tools.par_mod(pars, 'ANGLE_BETWEEN_PROJECTIONS', self.flyscan_pulse_period)
        else:
            pars = par_tools.par_mod(pars, 'ANGLE_BETWEEN_PROJECTIONS', (float(self.scanrange)/self.nproj))
        pars = par_tools.par_mod(pars, 'ROTATION_AXIS_POSITION', axispos)
        # account for any binning here
        pars = par_tools.par_mod(pars, 'IMAGE_PIXEL_SIZE_1', self.pixelsize*self.proxy.binningH)
        pars = par_tools.par_mod(pars, 'IMAGE_PIXEL_SIZE_2', self.pixelsize*self.proxy.binningV)

        # by default reconstruct the whole of the centre slice
        pars = par_tools.par_mod(pars, 'START_VOXEL_1', 1)
        pars = par_tools.par_mod(pars, 'START_VOXEL_2', 1)
        pars = par_tools.par_mod(pars, 'START_VOXEL_3', int(round(self.proxy.roiHeight/2.)))
        pars = par_tools.par_mod(pars, 'END_VOXEL_1', self.proxy.roiWidth)
        pars = par_tools.par_mod(pars, 'END_VOXEL_2', self.proxy.roiWidth)
        pars = par_tools.par_mod(pars, 'END_VOXEL_3', int(round(self.proxy.roiHeight/2.)))
        pars = par_tools.par_mod(pars, 'OUTPUT_FILE', '%s/%s/%s_slice.vol' % (self.reconstruction_directory, dirname, nxs_name))

        # identify the motor which positions the rotation axis in the beam
        # this is usually but not always the reference motor
        for key in beamline.keys():
            # find the standard configuration for this rotation
            if('tomo' in key) & ('rotation_motor_alias' in key):
                ndx = int(key[4])
                if beamline[key][0] == self.rotation_motor_alias:
                    axis_motor = beamline['tomo%d_reference_motor_alias' % ndx][0]

        # write some motor positions - defined in par_attribute_list
        if self.save_motor_positions:
            for key in self.par_attribute_list:
                try:
                    if key == axis_motor:
                        # add a comment in this case
                        pars = par_tools.par_mod(pars, '#%s' % key, dm.actors[key].value, 'AXIS_MOTOR')
                    else:
                        pars = par_tools.par_mod(pars, '#%s' % key, dm.actors[key].value)
                except:
                    print "could not read attribute : %s" % key

        # write the new par file
        par_tools.par_write(parfilename, pars)
        print "pyHST par file %s written and closed" % parfilename

    def stopORCA(self):
        '''Equivilent to the FlyScan hook - stop ORCA if stuck waiting for an external trigger'''
        print "stop the camera"
        # program speitbox
        self.sbox_proxy.Abort()
        time.sleep(0.1)
        self.sbox_proxy.mode = 0
        self.sbox_proxy.pulsePeriod = 50
        self.sbox_proxy.sequenceLength = 100
        self.sbox_proxy.Prepare()
        # prepare camera
        self.sbox_proxy.start()
        self.proxy.stop()


    def alignmentMemo(self):
        '''print a memo list of what to align'''
        print "detector focus"
        print "axis perpendicular to beam - table tilt"
        print "rotation axis centred wrt detector"
        print "rotation axis parallel to detector columns - tomorotc"
        print "set energy"
        print "set exposure time"

    def alignSample(self, myrange=1, square=False, oneclick=False):
        '''Align the sample using two composite images taken at 0 and 90 degrees'''
        # needs to handle the updateon / updateoff situation correctly
        # fast shutter for snap
        self = align_sample.align_sample(self, myrange, square, oneclick)


    def synchroStartRotation(self, override_ask=False):
        '''Calculate the rotation speed, start a forward rotation
        Note that this is slightly different depending on the camera mode
        Maybe check (EXTERNAL_MULTI vs EXTERNAL_READOUT) to choose the right thing
        There is also the dimax case of recording n scans into the memory before writing
        Can take references before, but not really after (unless we count the number of frames)
        '''

        # updateoff to start with
        updateon.updateoff()


        # the axis speed:
        if self.proxy.dev_name().find('hamamatsu') != -1:
            # hamamatsu orca - use the synchronous readout mode
            self.synchro_mode = True
            print 'CALCULATE FOR HAMAMATSU SYNCHRONOUS READOUT'
            readout_time = self.calcReadoutTime()
            safe_readout_time = readout_time * 1.1
            # assume that the exposure time start in the device is the good value
            exptime = self.proxy.exposureTime/1000
            if readout_time > exptime:
                print "The exposure time (%0.4f) should be greater than 110 percent of the readout time (%0.4f)" % (exptime, safe_readout_time)
                print "Make sure that you can expose this long with saturating !"
                return
            # now the frame time is the exposure time, which we know to be safe
            frame_time = self.proxy.exposureTime/1000
            # this is the ORCA - write to the flyscan-tmp-data folder
            self.proxy.fileTargetPath = 'S:\\flyscan_local_in' #'S:\\spool1\\orca\\flyscan-tmp-data'

        elif self.proxy.dev_name().find('dimax') != -1:
            self.synchro_mode = False
            # pco dimax - not synchronous, just fast
            print 'CALCULATE FOR DIMAX'
            #readout_time = self.calcReadoutTime()
            # use the safety time
            frame_time = (self.proxy.exposureTime/1000) + self.safetytime
            # here I need to select how many frames to hold in memory (1 scan, or n scans plus final refs)
            # this is the DIMAX - write to the D:
            self.proxy.fileTargetPath = 'D:\ANDY_TESTS'
        # to add - where the data are saved (depends on the camera)
        # references...

        frame_angle = float(self.scanrange)/self.nproj
        rot_speed = frame_angle/frame_time
        self.synchro_scantime = self.nproj * frame_time
        if rot_speed > self.rotation_velocity_max:
            print "Max rotation speed asked is %0.1f degrees/second.  Max allowed is %d.  Scan is too fast!" % (rot_speed, self.rotation_velocity_max)
            print "Modify parameters (more images, or longer exposure time)"
            return
        # if this is okay...
        print "setting rotation speed"
        if rot_speed < 60:
            # normal case
            get_ipython().magic(u'setspeed %s %0.3f' % (self.rotation_motor_alias, rot_speed))
        elif rot_speed < 360:
            # reduce acceleration, set speed the normal way
            print "Reducing the acceleration for high speed rotation !!!"
            try:
                dm.actors[self.rotation_motor_alias].proxy.acceleration = 10
                time.sleep(0.5)
                dm.actors[self.rotation_motor_alias].proxy.deceleration = 10
                time.sleep(0.5)
                get_ipython().magic(u'setspeed %s %0.3f' % (self.rotation_motor_alias, rot_speed))
            except:
                print "trying again to set the acceleration and speed"
                time.sleep(1)
                dm.actors[self.rotation_motor_alias].proxy.acceleration = 10
                time.sleep(0.5)
                dm.actors[self.rotation_motor_alias].proxy.deceleration = 10
                time.sleep(0.5)
                get_ipython().magic(u'setspeed %s %0.3f' % (self.rotation_motor_alias, rot_speed))
        else:
            print "rotation too fast !!!"

        self.synchro_rotation_velocity = rot_speed * 1.
        # program speitbox for one tomo
        self.sbox_proxy.mode = 1
        self.sbox_proxy.inputCoder = 1
        self.sbox_proxy.pulsePeriod = self.scanrange/self.nproj
        if self.synchro_mode:
            self.sbox_proxy.sequenceLength = int(self.nproj + 1)
        else:
            self.sbox_proxy.sequenceLength = int(self.nproj)
        self.sbox_proxy.firstpulseatfirstposition = False # note this is different to FlyScan
        self.sbox_proxy.Prepare()

        # create an entry in the logfile
        self.writeLogFile()
        print "scanname will become %s" % self.savename
        self.scanname = self.savename
        print "Create a directory for the data on the spool"
        self.synchro_datadir = self.spool_directory + os.sep + self.scanname
        #self.synchro_datadir = "/nfs/srv3/spool1/flyscan-data/psiche-soleil/com-psiche/%s" % self.scanname
        if not os.path.isdir(self.synchro_datadir):
            os.system('mkdir %s' % self.synchro_datadir)
            print "Create directory on SRV3 for logfile: %s" % self.synchro_datadir
        else:
            print "SRV3 logfile directory already exists: %s" % self.synchro_datadir

        # start the ftp transfer
        print "Setup the FTP data transfer"
        self.ftp_proxy.stop()
        time.sleep(0.1)
        self.ftp_proxy.put_property({'FilesDestinationDirectory':[self.synchro_datadir]})
        time.sleep(0.1)
        self.ftp_proxy.init()
        time.sleep(1)
        self.ftp_proxy.start()

        print "Creating a logfile in %s/%s.synchro_log for scan angles" % (self.synchro_datadir,self.scanname)
        self.synchro_log_file = open("%s/%s.synchro_log" % (self.synchro_datadir,self.scanname), "wt")
        self.stopLive()
        print "write a par file"
        self.writeParFile(1024.5, flyscan=False)
        # take initial references
        print "make sure front end is open"
        get_ipython().magic(u'feopen')
        self.__synchroInitialRefs()
        # setup camera
        self.proxy.fileFormat = 'NXS'
        self.proxy.filePrefix = self.scanname
        #self.proxy.fileTargetPath = 'S:\\spool1\\orca\\flyscan-tmp-data'
        self.proxy.NbFrames = self.nproj * self.dimax_n_tomos
        self.proxy.fileNbFrames = self.nproj

        if self.synchro_mode:
            self.proxy.triggerMode = 'EXTERNAL_READOUT'
        else:
            self.proxy.triggerMode = 'EXTERNAL_MULTI'

        self.proxy.resetfileindex()
        self.proxy.filegeneration = True
        # start the rotation
        checklimit = self.rotation_proxy.get_attribute_config('position').max_value
        if checklimit == 'Not specified':
            print "No upper limit on rotation motor"
            if override_ask:
                print "start rotation - tomorz.proxy.stop() to stop"
                self.rotation_proxy.forward()
                self.synchro_continuous = True
            else:
                tmp = raw_input('Do you want references after each snap? - HAMAMATSU ONLY! NOT DIMAX! y/[n] : ')
                self.synchro_intermediate_refs = True if tmp.lower() in ['y','yes','true','1'] else False
                test = raw_input('Start continuous forward rotation? -- !!! Slip ring only! No fixed cables !!! --  y/[n]')
                if test.lower() in ['y', 'yes']:
                    print "start rotation - tomorz.proxy.stop() to stop"
                    self.rotation_proxy.forward()
                    self.synchro_continuous = True
                    print "waiting for the acceleration..."
                    acctime = rot_speed / dm.actors[self.rotation_motor_alias].proxy.acceleration
                    time.sleep(acctime)
                    print "ready to snap"
        else:
            print "rotation motor has upper limit - continuous rotation not available"
            print "mt.synchroSnap will start a limited rotation!"
            self.synchro_continuous = False
            tmp = raw_input('Do you want references after each snap? y/[n] : ')
            self.synchro_intermediate_refs = True if tmp.lower() in ['y','yes','true','1'] else False


    def synchroSnap(self, nscans=1, maxinterval=0., bulkyinfo=None):
        '''
        snap N tomos, and log the start position
        If an interval is specified, wait for this time to elapse before starting the next tomo
        '''
        if self.synchro_continuous:
            self.proxy.filegeneration = True # start saving

            for jj in range(nscans):
                print "start tomo %d..." % jj

                # check that we have the right number of frames in the device
                if self.proxy.NbFrames != (self.nproj * self.dimax_n_tomos):
                    self.proxy.NbFrames = self.nproj * self.dimax_n_tomos
                    time.sleep(1)

                # already rotating - snap a tomo

                # start the camera, the speitbox, and then the movement
                self.proxy.snap()
                # wait for the camera to respond
                timeout = 0
                time.sleep(0.1)
                while (self.proxy.state() != PyTango.DevState.RUNNING) and (timeout<300):
                    time.sleep(0.1)
                    timeout+=1
                    if np.mod(timeout, 10)==9:
                        print "try another snap...."
                        self.proxy.snap()
                    if timeout==50:
                        print "WAITED 5 SECONDS FOR THE CAMERA TO RESPOND !!! - problem ? ---"

                if bulkyinfo!=None:
                    # if we want to jog bulky
                    dm.actors['bulky'].proxy.velocity = bulkyinfo[0]
                    dm.actors['bulky'].proxy.position += bulkyinfo[1]
                    print "STARTED BULKY MOVEMENT"

                starttime = time.time()

                for ii in range(self.dimax_n_tomos):

                    # start the hardware sampling
                    #cpt_sampling.start()
                    #if self.synchro_mode:
                    #    spi_udp.start(self.nproj + 1)
                    #else:
                    #    spi_udp.start(self.nproj)

                    # check - is the camera running?
                    if self.proxy.state() != PyTango._PyTango.DevState.RUNNING:
                        print "CAMERA IS NOT RUNNING --- PROBLEM?"

                    # the camera should be ready... now open the shutter
                    self.fastshutteropen()
                    time.sleep(1) # wait to be sure shutter is open
                    # read position
                    rzpos = self.rotation_proxy.position
                    mytime = time.time()
                    # start the speitbox
                    self.sbox_proxy.start()
                    # wait for the scan time
                    waittime = self.synchro_scantime * 1.25
                    wait = True
                    while wait:
                        scantime = time.time() - mytime
                        if scantime > waittime:
                            wait = False
                        else:
                            time.sleep(0.1)

                    # write the logfile
                    self.synchro_log_file.write('%f, %f \n' % (mytime, rzpos))
                    self.synchro_log_file.flush()
                    # wait for the scan time plus the interval time
                    waittime = self.synchro_scantime + self.dimax_interval_tomos
                    wait = True
                    while wait:
                        scantime = time.time() - mytime
                        if scantime > waittime:
                            wait = False
                        else:
                            time.sleep(0.1)
                    # POSSIBLY TAKE SOME REFERENCES AFTER THE SCAN

                if bulkyinfo!=None:
                    # if we want to jog bulky
                    dm.actors['bulky'].proxy.Stop()
                    print "STOPPED BULKY MOVEMENT"

                # close the fast shutter
                self.fastshutterclose()

                # should perhaps test here if camera is in fault state
                if self.proxy.state() == PyTango.DevState.FAULT:
                    print('CAMERA IN FAULT STATE, TRY AN INIT!')
                    self.proxy.Init()
                    time.sleep(5)
                    print('CAMERA IN FAULT STATE, TRIED AN INIT!')

                # wait for the camera to finish readout
                timeout = 0
                sys.stdout.write('\n\nREADING DIMAX FOR 0000 seconds')
                while (self.proxy.state() == PyTango.DevState.RUNNING) and (timeout<1200):
                    time.sleep(1)
                    timeout+=1
                    sys.stdout.write('\b\b\b\b\b\b\b\b\b\b\b\b%04d seconds' % timeout)
                    sys.stdout.flush()
                    if (self.proxy.dev_name().find('hamamatsu') != -1) and (timeout==3):
                        print "WAITED 3 SECONDS FOR THE HAMAMATSU TO FINISH RUNNING!!! - stop the camera ---"
                        self.proxy.Stop()




                if self.synchro_intermediate_refs == True:
                    # record nrefs before closing fastshutter
                    self.fastshutteropen()
                    self.proxy.NbFrames = self.nref
                    self.proxy.triggerMode = 'INTERNAL_SINGLE'
                    get_ipython().magic(u'dmove %s %0.2f' % (self.reference_motor_alias, self.refDisplacement))
                    self.snap()
                    self.ref = self.image * 1.
                    get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
                    # write the logfile
                    self.synchro_log_file.write('%f, refs \n' % mytime)
                    self.synchro_log_file.flush()
                    # restore the camera config for the next snap
                    self.proxy.NbFrames = self.nproj
                    self.proxy.triggerMode = 'EXTERNAL_READOUT'
                    self.proxy.filegeneration = True # make sure this is true

                # close the shutter
                self.fastshutterclose()

                # wait for the time between tomos
                # wait for the scan time plus the interval time
                wait = True
                sys.stdout.write('\n\nWAIT BEFORE STARTING THE NEXT SCAN ... ')
                sys.stdout.flush()
                while wait:
                    scantime = time.time() - mytime
                    if scantime > maxinterval:
                        wait = False
                    else:
                        sys.stdout.write('\b\b\b\b%04d' % (maxinterval-scantime))
                        time.sleep(1)

        else: # Non continuous case
            get_ipython().magic(u'amove %s 0' % self.rotation_motor_alias)
            self.stopLive()
            # not rotating - snap a tomo
            self.fastshutteropen()
            print "SLEEP BEFORE STARTING SNAP!!! SPECIAL MEASURES...."
            time.sleep(2)
            # start the camera, the speitbox, and then the movement
            self.proxy.NbFrames = self.nproj
            self.proxy.triggerMode = 'EXTERNAL_READOUT'
            self.proxy.filegeneration = True # make sure this is true
            self.proxy.snap()
            # while waiting, set the rotation speed
            get_ipython().magic(u'setspeed %s %0.3f' %  (self.rotation_motor_alias, self.synchro_rotation_velocity))
            t_acc = self.synchro_rotation_velocity / self.rotation_proxy.acceleration # acceleration time
            s_acc = self.synchro_rotation_velocity * t_acc * 0.5 # acceleration distance
            # wait for the camera to respond
            timeout = 0
            time.sleep(0.1)
            while (self.proxy.state() != PyTango.DevState.RUNNING) and (timeout<300):
                time.sleep(0.1)
                timeout+=1
                if timeout==50:
                    print "WAITED 5 SECONDS FOR THE CAMERA TO RESPOND !!! - problem ? ---"
            rzpos = self.rotation_proxy.position * 1.
            # need to read and use sbox pulse period
            mytime = time.time()
            self.rotation_proxy.position = self.scanrange + (s_acc * 10) # x 10 to have a good margin
            time.sleep(t_acc*3)
            # start the speitbox
            self.sbox_proxy.start()
            # write the logfile
            self.synchro_log_file.write('%f, %f \n' % (mytime, rzpos))
            self.synchro_log_file.flush()
            # wait for the camera to respond
            timeout = 0
            while (self.proxy.state() == PyTango.DevState.RUNNING) and (timeout<300):
                time.sleep(1)
                timeout+=1
                if timeout==120:
                    print "WAITED 120 SECONDS FOR THE CAMERA TO RESPOND !!! - problem ? ---"
            if self.synchro_intermediate_refs == False:
                # close the shutter
                self.fastshutterclose()
                # reset rotation speed and return to zero
                get_ipython().magic(u'setspeed %s %0.3f' % (self.rotation_motor_alias, self.rotation_velocity_nominal))
                time.sleep(0.1)
                get_ipython().magic(u'amove %s 0' % self.rotation_motor_alias)
            else:
                # take some references while returning to zero
                get_ipython().magic(u'setspeed %s %0.3f' %  (self.rotation_motor_alias, self.rotation_velocity_nominal))
                time.sleep(0.1)
                self.rotation_proxy.position = 0 # return to zero
                # record nrefs
                self.proxy.NbFrames = self.nref
                self.proxy.triggerMode = 'INTERNAL_SINGLE'
                get_ipython().magic(u'dmove %s %0.2f' % (self.reference_motor_alias, self.refDisplacement))
                self.snap()
                self.ref = self.image * 1.
                get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
                # write the logfile
                self.synchro_log_file.write('%f, refs \n' % mytime)
                self.synchro_log_file.flush()
                # close the shutter
                self.fastshutterclose()
                # restore the snap config
            # stop saving
            self.proxy.filegeneration = False
            print "done!"


    def synchroTakeRefs(self):
        # take some references during a synchro sequence
        # record nrefs before closing fastshutter
        # make sure we are saving
        self.proxy.filegeneration = True
        mytime = time.time()
        self.fastshutteropen()
        self.proxy.NbFrames = self.nref
        self.proxy.triggerMode = 'INTERNAL_SINGLE'
        get_ipython().magic(u'dmove %s %0.2f' % (self.reference_motor_alias, self.refDisplacement))
        self.snap()
        self.ref = self.image * 1.
        get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
        # write the logfile
        self.synchro_log_file.write('%f, refs \n' % mytime)
        self.synchro_log_file.flush()
        # restore the camera config for the next snap
        self.proxy.NbFrames = self.nproj
        self.proxy.triggerMode = 'EXTERNAL_READOUT'
        # close the shutter
        self.fastshutterclose()
        self.proxy.filegeneration = False # stop saving


    def __synchroInitialRefs(self):
        '''end of scan - take refs and close logfile'''
        print "take refs"
        # setup camera
        self.proxy.filegeneration = True
        self.proxy.fileFormat = 'NXS'
        self.proxy.filePrefix = "initial_ref"
        #self.proxy.exposuretime = self.magic_ref_time
        #self.proxy.fileTargetPath = 'S:\\spool1\\orca\\flyscan-tmp-data'
        self.proxy.NbFrames = self.nref
        self.proxy.fileNbFrames = self.nref
        self.proxy.triggerMode = 'INTERNAL_SINGLE'
        self.proxy.resetfileindex()
        self.fastshutteropen()
        get_ipython().magic(u'dmove %s %0.2f' % (self.reference_motor_alias, self.refDisplacement))
        self.snap()
        self.ref = self.image * 1.
        get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
        #self.proxy.exposuretime = self.magic_exp_time

        print "take darks"
        # setup camera
        self.proxy.fileFormat = 'NXS'
        self.proxy.filePrefix = "dark"
        #self.proxy.fileTargetPath = 'S:\\spool1\\orca\\flyscan-tmp-data'
        self.proxy.NbFrames = self.ndark
        self.proxy.fileNbFrames = self.ndark
        self.proxy.triggerMode = 'INTERNAL_SINGLE'
        self.proxy.resetfileindex()
        self.fastshutterclose()
        if self.fast_shutter_for_snap:
            fast_shutter_memo = True
            self.fast_shutter_for_snap = False
        else:
            fast_shutter_memo = False
        # take the images
        self.snap()
        # restore fast_shutter_for_snap
        if fast_shutter_memo:
            self.fast_shutter_for_snap = True
        self.dark = self.image * 1.
        #get_ipython().magic(u'amove tomorz 0')
        self.proxy.filegeneration = False # stop saving
        self.proxy.NbFrames = 1 # for updateon behaviour

    def synchroStopRotation(self):
        print "stop the rotation"
        waittime = 1.1 * self.synchro_rotation_velocity / dm.actors[self.rotation_motor_alias].proxy.acceleration
        try:
            self.rotation_proxy.stop()
        except:
            print "stop failed..."
        print "wait for tomorz to stop..."
        time.sleep(waittime)
        if self.rotation_proxy.state() == PyTango._PyTango.DevState.OFF:
            # rotation motor if off!
            get_ipython().magic(u'seton %s' % self.rotation_motor_alias)
            print("Rotation motor was off - switching on")
        t0 = time.time()
        while self.rotation_proxy.state() != PyTango._PyTango.DevState.STANDBY:
            time.sleep(0.1)
            if time.time() > (t0+5):
                print('not stopped and timeout... what\'s wrong?')
                break
        print "stopped!"
        get_ipython().magic(u'setspeed %s %f' % (self.rotation_motor_alias, self.rotation_velocity_nominal))

        if self.synchro_rotation_velocity > 60:
            # reset the acceleration
            time.sleep(0.1)
            dm.actors[self.rotation_motor_alias].proxy.acceleration = 60
            time.sleep(0.1)
            dm.actors[self.rotation_motor_alias].proxy.deceleration = 60
        # put rotation back on 0-360
        self.rotation_proxy.defineposition(np.mod(self.rotation_proxy.position, 360))
        time.sleep(0.1)

    def synchroFinalRefs(self):
        '''end of scan - take refs and close logfile'''
        print "close the log file"
        self.synchro_log_file.close()

        self.synchroStopRotation()

        print "take refs"
        # setup camera
        self.proxy.fileFormat = 'NXS'
        self.proxy.filePrefix = "final_ref"
        #self.proxy.exposuretime = self.magic_ref_time
        self.proxy.filegeneration = True # make sure this is true
        #self.proxy.fileTargetPath = 'S:\\spool1\\orca\\flyscan-tmp-data'
        self.proxy.NbFrames = self.nref
        self.proxy.fileNbFrames = self.nref
        self.proxy.triggerMode = 'INTERNAL_SINGLE'
        self.proxy.resetfileindex()
        print('Move rotation back to zero for references')
        get_ipython().magic(u'amove %s 0' % self.rotation_motor_alias)
        self.fastshutteropen()
        get_ipython().magic(u'dmove %s %0.2f' % (self.reference_motor_alias, self.refDisplacement))
        self.snap()
        self.ref = self.image * 1.
        get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
        self.fastshutterclose()
        #self.proxy.exposuretime = self.magic_exp_time

        self.proxy.filegeneration = False
        self.proxy.NbFrames = 1 # for updateon behaviour

        # wait for the final refs to be transfered via ftp
        print "wait for the last refs to be transfered"
        #while not os.path.exists(self.synchro_datadir + os.sep + 'final_ref_000001.nxs'):
        while not self.ftp_proxy.transferredfile == 'final_ref_000001.nxs':
            time.sleep(1)
            print "wait for the FTP transfer to finish"



