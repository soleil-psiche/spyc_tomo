# -*- coding: utf-8 -*-
"""
Created on Mon Apr 28 08:51:04 2014

@author: aking
"""

# a class to use a camera "live" -
# have a window to show a flat fielded image, etc
# this needs to know about the reference position/motor,
# and also the shutter.

#  Should it also know about focusing?
#  Zooming?
#
# Possibly need an object for the dectector, which can contain the camera
# This could inherit the camera class things from this file.

# thus the camera really only needs to handle taking images
# exception is driving the x stage for references, and controling the shutter for darks

# several possible options for saving: python saves using PIL --> tifs
# presumably slow, like saving from SPEC.
# In this case, should probably save image as acquired, and save darks and flats separately.
# Thus, methods that update ref/dark should save them out.
# otherwise, the lima device should be able to save automatically.
# ANDY 16/3/2016 change this behaviour a bit
# keep autosave behaviour as it is now.
# add saveCurrent and saveFlat to save current im, or current im ref and dark (if present)

# should be able to read the pixel value of an image

# the next level up in more like the instrument - this scan calculate ref pos, measurement pos etc
# handle focus and pixel size, etc

# use figure 200 for display

# add accumulation mode for longer or higher dynamic range exposures

# note - have removed flipY, as this has disappeared from the new version of the hamamatsu device

# start live needs to set INTERNAL_SINGLE

import time
import os
import numpy as np
import pylab
pylab.ion()
#from PIL import Image

import maxime_tifffile as tifffile

from FindNextFileName import findNextFileName
import PyTango

# try handling reference motor with spyc
from spyc.core import DeviceManager
dm = DeviceManager()

class Camera:

    def __init__(self, mycamera="i03-c-c00/dt/hamamatsu.1"):

        # get the camera device proxy
        self.proxy = PyTango.DeviceProxy(mycamera)
        # use spyc to drive motors

        # give the reference motor as device address:
        self.rotation_motor_alias = 'tomorefx'
        self.rotation_motor_alias = 'tomorz'
        self.refDisplacement = None
        self.measurementPosition = None

        # fast shutter open and close commands
        self.fast_shutter_open_command = ''
        self.fast_shutter_close_command = ''
        self.fast_shutter_for_snap = False

        # for image display
        self.display = 'flat' # by default
        self.image = ''
        self.image_clims = [0, 5000]
        self.flat = ''
        self.flat_clims = [0, 1]
        self.autolims = [None, None] # or percentiles [3, 90]
        # does python save images?
        self.python_save = False
        self.python_dir = ''
        self.python_prefix = ''
        self.python_ndx = ''
        self.python_lastfilename = ''

        # don't read images during acquisition
        self.readimage = True
        self.ref = ''
        self.dark = ''

    ######### FOR SAVING ######################################################
    def setupSaving(self, dir='./', prefix='tomo', ndx=0):
        '''setup file saving via python'''
        self.python_dir = dir
        self.python_prefix = prefix
        self.python_ndx = ndx
        self.readimage = True
        print 'Next file will be ' + findNextFileName(self.python_dir,
                                                      self.python_prefix,
                                                      'tif', self.python_ndx)
        print 'Use autoSaveOn() to save after every snap()'
        print 'Otherwise, use saveCurrent() or saveFlat() after snap() to save the current image'


    def autoSaveOn(self):
        '''Auto save on'''
        self.python_save = True
        self.readimage = True

    def autoSaveOff(self):
        '''Auto save off'''
        self.python_save = False

    def saveCurrent(self):
        '''Save the current image only'''
        if self.python_prefix == '':
            print 'First use setupSaving to set prefix and directory'
            return

        filename = findNextFileName(self.python_dir, self.python_prefix,
                                                  'tif', self.python_ndx)
        #im = Image.fromarray(self.image*1.0)
        #im.save(filename)
        tifffile.imsave(filename, self.image)

        print 'image saved as: ' + filename
        self.python_lastfilename = filename
        # increment ndx
        self.python_ndx+=1

    def saveFlat(self):
        '''Save the current image, ref, and dark'''
        if self.python_prefix == '':
            print 'First use setupSaving to set prefix and directory'
            return

        filename = findNextFileName(self.python_dir, self.python_prefix,
                                                  'tif', self.python_ndx)
        #im = Image.fromarray(self.image*1.0)
        #im.save(filename)
        tifffile.imsave(filename, self.image)

        print 'image saved as: ' + filename
        # save the reference image with the same filename
        if (self.ref != '') & (self.ref.shape == self.image.shape):
            refname = filename[0:-4] + '_ref.tif'
            #im = Image.fromarray(self.ref*1.0)
            #im.save(refname)
            tifffile.imsave(refname, self.ref)
            print 'ref saved as: ' + filename
        else:
            print 'No valid reference image was found - .updateRef()?'

        if (self.dark != '') & (self.dark.shape == self.image.shape):
            refname = filename[0:-4] + '_dark.tif'
            #im = Image.fromarray(self.dark*1.0)
            #im.save(refname)
            tifffile.imsave(refname, self.dark)
            print 'dark saved as: ' + filename
        else:
            print 'No valid dark image was found - .updateDark()?'
        # increment ndx
        self.python_ndx+=1
        self.python_lastfilename = filename

    ######### FOR SAVING ######################################################

    ######### FOR CONTROLLING BEAMLINE ########################################
    def setRefMotor(self, alias=None):
        if alias==None:
            check = raw_input('Which motor to use for references? : ')
            alias = 'tomorefx' if check=='' else check
        if hasattr(alias, 'alias'):
            alias = alias.alias
        self.reference_motor_alias = alias
        self.reference_proxy = dm.actors[self.reference_motor_alias].proxy
        print "will use motor: %s for references" % self.reference_motor_alias
        # reset all the positions
        self.measurementPosition = None
        self.refDisplacement = None
        self.setMeasurementPos()
        # note that pixelsize is in microns but refDisplacement in mm
        guess = self.pixelsize * -2.5
        test = raw_input("What reference displacement to use? [%f] :  " % guess)
        self.refDisplacement = guess if test == "" else float(test)

    def setRefPos(self):
        '''set the current reference motor position as the reference position'''
        if self.measurementPosition==None:
            print "Before setting the reference displacement, you must set the measurement position"
        else:
            print 'get the current position'
            curpos = dm.actors[self.reference_motor_alias].get()
            check = raw_input('Use current offset from measurement position (%0.1f) for refs? (y/n):  ' % (curpos - self.measurementPosition))
            if check=='y' or check=='yes':
                self.refDisplacement = curpos - self.measurementPosition
                print "setting reference displacement of %0.1f" % self.refDisplacement

    def setMeasurementPos(self):
        '''set the current reference motor position as the reference position'''
        print 'get the current position'
        curpos = dm.actors[self.reference_motor_alias].get()
        check = raw_input('Use current %s position [%0.1f] for measurement? ([y]/n):  ' % (self.reference_motor_alias, curpos))
        check = True if check.lower() in ['y', 'yes', '', 'true'] else False
        if check:
            self.measurementPosition = curpos
            print "setting measurement position of %s = %0.1f" % (self.reference_motor_alias, self.measurementPosition)
        else:
            print "not changing measurement position"

    def fastshutteropen(self):
        '''Open fast shutter with defined fast_shutter_open_command'''
        if self.fast_shutter_open_command != '':
            get_ipython().magic(u'%s' % (self.fast_shutter_open_command))

    def fastshutterclose(self):
        '''Close fast shutter with defined fast_shutter_close_command'''
        if self.fast_shutter_close_command != '':
            get_ipython().magic(u'%s' % (self.fast_shutter_close_command))

    ######### FOR CONTROLLING BEAMLINE ########################################

    ######### FOR CHANGE CAMERA PARAMETERS ####################################
    def setExpTime(self, exptime=1):
        '''Set exposure time, convert s to ms'''
        if self.proxy.state() == PyTango._PyTango.DevState.RUNNING:
            waslive = True
        else:
            waslive = False
        self.stopLive()
        exptime = exptime*1000
        if self.proxy.acquisitionMode == 'ACCUMULATION':
            nacc = int(self.proxy.exposureTime / self.proxy.exposureAccTime)
            # keep the same nacc
            expacctime = exptime / nacc
            exptime = expacctime * nacc
            self.proxy.exposureAccTime = expacctime
            self.proxy.exposureTime = exptime
        else:
            self.proxy.exposureTime = exptime
        print 'set exposure time to %i ms' % exptime
        if waslive:
            self.startLive()

    def setBinning(self, binning=1):
        if binning in [1, 2, 4]:
            if self.proxy.state() == PyTango._PyTango.DevState.RUNNING:
                waslive = True
            else:
                waslive = False
            self.stopLive()
            print 'set binning to [%d x %d] [V x H]' % (binning, binning)
            self.proxy.setbinning((binning, binning))
            if waslive:
                self.startLive()
        else:
            print "For Hamamatsu ORCA, binning value must be 1, 2, or 4 only"

    def setROI(self, startX=0, startY=0, sizeX=2048, sizeY=2048):
        '''setROI(x1, y1, xrange, yrange), respecting 4n line requirement'''
        # unflip the camera, flip the Y roi
        self.stopLive()
        #self.proxy.flipY = False
        #startY = 2048 - startY - sizeY
        startY = np.floor(startY/4.)*4
        sizeY = np.ceil(sizeY/4.)*4
        startX = np.floor(startX/4.)*4
        sizeX = np.ceil(sizeX/4.)*4
        roi = np.int32([startX, startY, sizeX, sizeY])
        self.proxy.setroi(roi)
        # replace the flip
        #self.proxy.flipY = True
        self.ref = ''
        self.dark = ''

    def setROIcen(self, sizeX, sizeY):
        '''setROIcen(xrange, yrange), centred ROI, respecting 4n line requirement'''
        sizeY = np.ceil(sizeY/4.)*4
        sizeX = np.ceil(sizeX/4.)*4
        startX = 1024-(sizeX/2)
        startY = 1024-(sizeY/2)
        self.setROI(startX, startY, sizeX, sizeY)

    def setAccMode(self, nframes=10):
        '''Set accumulation with n frames'''
        if self.proxy.acquisitionMode == 'ACCUMULATION':
            print 'Already in ACCUMULATION mode'
        else:
            self.stopLive()
            acctime = self.proxy.exposureTime
            exptime = acctime*nframes
            self.proxy.acquisitionMode = 'ACCUMULATION'
            self.proxy.exposureTime = exptime
            self.proxy.exposureAccTime = acctime
            self.proxy.Init()
            print 'setting exposure time of %i ms (as %i x %i ms)' % (exptime, nframes, acctime)


    def setSingleMode(self):
        '''Un-set accumulation mode'''
        self.stopLive()
        if self.proxy.acquisitionMode == 'SINGLE':
            print 'Already in SINGLE mode'
        else:
            exptime = self.proxy.exposureAccTime
            self.proxy.acquisitionMode = 'SINGLE'
            self.proxy.exposureTime = exptime
            self.proxy.Init()
            print 'setting single frame exposure time of %i ms' % exptime

    ######### FOR CHANGE CAMERA PARAMETERS ####################################


    ######### FOR TAKING IMAGES ###############################################
    def snap(self):
        # are we using the fast shutter ?
        if self.fast_shutter_for_snap:
            get_ipython().magic(u'%s' % (self.fast_shutter_open_command))

        self.proxy.snap()
        # need to wait for the image to be ready
        time.sleep(0.1)
        while self.proxy.state() == PyTango._PyTango.DevState.RUNNING:
            time.sleep(0.1)
        time.sleep(0.1)

        # are we using the fast shutter ?
        if self.fast_shutter_for_snap:
            get_ipython().magic(u'%s' % (self.fast_shutter_close_command))
        if self.readimage:
            self.image = self.proxy.image

        # if auto saving, save the image
        if self.python_save:
            filename = findNextFileName(self.python_dir, self.python_prefix,
                                                      'tif', self.python_ndx)
            im = Image.fromarray(self.image*1.0)
            im.save(filename)
            print 'image saved as: ' + filename
            self.python_ndx+=1

    def takeFlat(self):
        self.readimage = True
        self.stopLive()
        self.snap()
        im = self.image * 1. # save the image
        # are the reference and dark the right size?
        if (self.ref == '') or (self.ref.shape != self.image.shape):
            self.updateRef()
        if (self.dark == '') or (self.dark.shape != self.image.shape):
            self.updateDark()
        # replace image, and apply flatfield correction
        self.image = im * 1.
        self.flat = (self.image*1.-self.dark)/(self.ref*1.-self.dark)
        # display flatfielded image
        self.showCurrentImage('flat')


    def takeIm(self):
        self.readimage = True
        self.stopLive()
        self.snap()
        # display raw image
        self.showCurrentImage('image')


    def updateRef(self):
        '''move to the ref position, take an image, move back'''
        self.readimage = True
        if self.measurementPosition == None:
            self.setMeasurementPos()
        print "move to reference position"
        get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition+self.refDisplacement))
        time.sleep(1) # sleep added for fwbs / updateon seems shutter a bit too slow to keep up
        print "wait to be sure fast shutter is ready"
        self.stopLive()
        self.proxy.nbFrames = 1
        self.snap()
        self.ref = self.image * 1.
        self.showCurrentImage('image')
        get_ipython().magic(u'amove %s %0.2f' % (self.reference_motor_alias, self.measurementPosition))
        print "return to measurement position"


    def updateDark(self):
        self.readimage = True
        # If a fast shutter command has been defined, use it
        if self.fast_shutter_close_command != '':
            print "close fast shutter"
            get_ipython().magic(u'%s' % (self.fast_shutter_close_command))
        # Otherwise, use the shutter close command
        else:
            print "close shutter"
            get_ipython().magic(u'%s' % self.shutter_close_command)
        self.stopLive()
        self.proxy.nbFrames = 1
        # dont open the fast shutter when we snap
        if self.fast_shutter_for_snap:
            fast_shutter_memo = True
            self.fast_shutter_for_snap = False
        else:
            fast_shutter_memo = False
        self.snap()
        self.dark = self.image * 1.
        # restore fast_shutter_for_snap
        if fast_shutter_memo:
            self.fast_shutter_for_snap = True
        # try not reopening, as we don't want this in the updateon type scenario
        #if self.fast_shutter_close_command != '':
        #    print "reopen fast shutter"
        #    get_ipython().magic(u'%s' % (self.fast_shutter_open_command))
        #else:
        #    print "reopen shutter"
        #    get_ipython().magic(u'%s' % (self.shutter_open_command))


    def startLive(self):
        '''Helper function to start live on the camera'''
        if self.proxy.state()==PyTango._PyTango.DevState.STANDBY:
            print "starting LIVE mode"
            # can't save images in this mode
            self.proxy.fileGeneration=False
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            self.proxy.Start()
        elif self.proxy.state()==PyTango._PyTango.DevState.RUNNING:
            print "LIVE mode is ON"
        else:
            print "Camera is in a strange state...!!!"


    def stopLive(self):
        '''Helper function to stop live on the camera'''
        if self.proxy.state()==PyTango._PyTango.DevState.RUNNING:
            print "stopping LIVE mode"
            self.proxy.Stop()
        elif self.proxy.state()==PyTango._PyTango.DevState.STANDBY:
            print "camera is STOPPED"
        else:
            print "Camera is in a strange state...!!!"
        # set nframes to one to allow snap after
        self.proxy.nframes = 1

    ######### FOR TAKING IMAGES ###############################################

    ######### FOR SHOWING IMAGES ##############################################
    def setClims(self, selection='flat', clims=[0, 1]):
        '''update display colour limits'''
        if selection=='flat':
            self.flat_clims = clims
        elif selection=='image':
            self.image_clims = clims
        else:
            print "selection < %s > not understood - use flat/image" % selection
            return
        self.showCurrentImage(selection)

    def setAutoClims(self, climlow=None, climhigh=None):
        '''set percentile auto clims. Without an argument, switches off autolims'''
        self.autolims = [climlow, climhigh]
        # update the display
        self.showCurrentImage(self.currentimage)

    def showCurrentImage(self, selection='image'):
        self.currentimage = selection
        fig = pylab.figure(200)
        pylab.clf() # try this...
        if selection=='image':
            if len(self.image)>1024:
                im = self.image[0::4, 0::4]
            else:
                im = self.image
            #fig = pylab.figure(200, figsize=im.shape, dpi=1)
            pylab.imshow(im)
            #fig.figimage(im)
            #pylab.show()
            if self.autolims[0]==None or self.autolims[1]==None:
                print "using fixed limits"
                climlow = self.image_clims[0]
                climhigh = self.image_clims[1]
            else:
                print "using auto limits"
                climlow = np.percentile(self.image, self.autolims[0])
                climhigh = np.percentile(self.image, self.autolims[1])
        elif selection=='flat' and self.flat!='':
            if len(self.flat)>1024:
                im = self.flat[0::4, 0::4]
            else:
                im = self.flat
            #fig = pylab.figure(200, figsize=im.shape, dpi=1)
            pylab.imshow(im)
            #fig.figimage(im)
            #pylab.show()
            if self.autolims[0]==None or self.autolims[1]==None:
                climlow = self.flat_clims[0]
                climhigh = self.flat_clims[1]
            else:
                climlow = np.percentile(self.flat, self.autolims[0])
                climhigh = np.percentile(self.flat, self.autolims[1])
        pylab.clim(climlow, climhigh)
        pylab.axis("tight")
        pylab.show()
        fig.canvas.manager.set_window_title('%s clims [%0.1f %0.1f]' % (selection, climlow, climhigh))

    def showFullSize(self, selection='image'):
        self.currentimage = selection
        fig = pylab.figure(200)
        if selection=='image':
            im = self.image
            pylab.imshow(im)
            if self.autolims[0]==None or self.autolims[1]==None:
                print "using fixed limits"
                climlow = self.image_clims[0]
                climhigh = self.image_clims[1]
            else:
                print "using auto limits"
                climlow = np.percentile(self.image, self.autolims[0])
                climhigh = np.percentile(self.image, self.autolims[1])
        elif selection=='flat' and self.flat!='':
            im = self.flat
            pylab.imshow(im)
            if self.autolims[0]==None or self.autolims[1]==None:
                climlow = self.flat_clims[0]
                climhigh = self.flat_clims[1]
            else:
                climlow = np.percentile(self.flat, self.autolims[0])
                climhigh = np.percentile(self.flat, self.autolims[1])
        pylab.clim(climlow, climhigh)
        pylab.axis("tight")
        fig.canvas.manager.set_window_title('%s clims [%0.1f %0.1f]' % (selection, climlow, climhigh))

    def showHistogram(self, selection=None):
        if selection==None:
            selection = self.currentimage
        if selection=='image':
            hist, bedges = np.histogram(self.image, bins=100,
                                     range=[self.image.min(), self.image.max()])
        elif selection=='flat' and self.flat!='':
            hist, bedges = np.histogram(self.flat, bins=100,
                                     range=[self.flat.min(), self.flat.max()])
        fig = pylab.figure(201)
        pylab.clf()
        pylab.plot((bedges[0:-1]+bedges[1::])/2, hist)
        fig.canvas.manager.set_window_title('histogram of %s' % selection)
    ######### FOR SHOWING IMAGES ##############################################

    ######### FOR HAMAMATSU SPECIFICALLY - should be somewhere else!  #########

    def calcReadoutTime(self):
        ''' calculate the current and max frame rate and readout time for FlyScan
            external trigger mode
            from the Hamamatsu ORCA manual page 29'''
        dpsname = self.proxy.dev_name() + '-specific'
        dps=PyTango.DeviceProxy(dpsname)

        if self.proxy.acquisitionMode != 'SINGLE':
            bin = self.proxy.binningV
            nlines = max((1024/bin)-self.proxy.roiY, self.proxy.roiY+self.proxy.roiHeight-(1024/bin))
            nlines = nlines * bin
            acctime = self.proxy.exposureTime/1000
            nacc = self.proxy.exposureTime/self.proxy.exposureAccTime
            if dps.readoutSpeed=="NORMAL":
                readouttime = (nlines*9.74436E-6) + (9*9.74436E-6)
            elif dps.readoutSpeed=="SLOW":
                readouttime = (nlines*32.4812E-6) + (9*32.4812E-6)
            else:
                print "didn't understand the readout speed !!!"
            imtime = acctime + (nacc*readouttime)
            print "%d frames accumulated" % nacc
        else:
            bin = self.proxy.binningV
            nlines = max((1024/bin)-self.proxy.roiY, self.proxy.roiY+self.proxy.roiHeight-(1024/bin))
            nlines = nlines * bin
            acctime = self.proxy.exposureTime/1000
            if dps.readoutSpeed=="NORMAL":
                readouttime = (nlines*9.74436E-6) + (9*9.74436E-6)
            elif dps.readoutSpeed=="SLOW":
                readouttime = (nlines*32.4812E-6) + (9*32.4812E-6)
            else:
                print "didn't understand the readout speed !!!"
            imtime = acctime + readouttime
        print "for the current vertical roi: %d lines (x binning %d), starting line %d" % (self.proxy.roiHeight, self.proxy.binningV, self.proxy.roiY)
        print "readout time is %0.4f seconds" % readouttime
        print "total time per image is %0.4f seconds" % imtime
        print "max frame rate is %0.3f fps" % (1/imtime)
        return readouttime



