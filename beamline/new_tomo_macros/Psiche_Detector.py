# -*- coding: utf-8 -*-
"""
Created on Sat May 24 15:57:28 2014

@author: aking
"""

# zoom function

# avoid setting limits that are inconsistant with actual position
# limits don't reqlly work for changing lenses when at 1x and 2x positions

# should know how to set safe limits if no optic has been defined

# possible setup would be that a detector contains a camera and some motors
# that change focus, zoom, etc
# how can I configure everything?  a text file?
#
# detector? or tomograph?

# add something to handle different scintillators - tilting and not tilting

# use figure 201 for plotting and figures

# modify such that detector inherits from camera - i.e. a detector is a special class of camera

# add - start / stop live
# stop live if running before doing snaps

# should perhaps disable the fastshutter in snap in the focus scans...

import numpy as np
ifft = np.fft.ifft
fft = np.fft.fft
import pylab
import PyTango
import time

# try handling reference motor with spyc
from spyc.core import DeviceManager
dm = DeviceManager()

from Camera import Camera

# big question is how to deal with this for ANATOMIX / PSICHE
# possibly my detector should be a special case (camera - detector - psichedetector - tomo heritance)

# format of alloptics:
# [focus_min focus_max focus_nominal zoom_min zoom_max zoom_nominal delta_min delta_max focal_length mag]
# note that positions are for non-tiltable scintillator
# note zoom = 183 is almost the hard limit
# note 1x is WITH bellows - without, you could go further
# note NO BELLOWS for 2.5x, and that nominal mag is 2.15, not 2.5

# note that the BELLOWS AND RING must be removed for 2.5x to get to tomofocus=0
# note - have added 6.5 mm spacer to 2.5x
# note - with the spacer added and the ring removed, tomofocus=0 gives 2.5x

# values are without Pb glass !!!

alloptics = {
    "1x":[15., 100., 43.9, 28., 180., 80.6, 13., 180., 75., 1.],
    "2.5x":[0., 100., 3.63, 13., 180., 24.6, 13., 180., 50., 2.15],
    "2x":[57., 100., 64.92, 128., 183., 167., 71., 126., 33.33, 2.],
    "5x":[57., 100., 64.92, 128., 183., 167., 71., 126., 33.33, 5.],
    "7.5x":[59.1, 100., 65.32, 128., 183., 167., 71., 126., 23.53, 7.5],
    "10x":[58.8, 100., 64.86, 128., 183., 167., 71., 126., 18.18, 10.],
    "20x":[60.35, 100., 65.06, 128., 183., 167., 71., 126., 9.52, 20.],
    "20xhr":[69.3, 100., 72.9, 128., 183., 175.5, 71., 126., 3.92, 20.],
    "50x":[69.3, 100., 72.9, 128., 183., 175.5, 71., 126., 3.92, 50.]
    }
# 50x 0.42 with 8mm spacer for scintillator - ditto 20xhr
# expect 73 mm focus, 175 zoom (both -3.66 for tiltable mount)
# 68.25 mm lower limit for focus - collision objective and mirror
# in focus 69.33 / 175
# paper with ~180 mu spaced dots -> suggests fov is about right ~0.263 mm -> 128 nm pixels w/o binning
# image intensity 7730 in 0.25 sec
# compare 20x 0.28 - focused at 69.40 - 29250 in 0.25
# this suggests a difference of 3.8, should be 2.8.
# !!! adjust the tiltable mount to gain 0.75 mm wrt above values, table changed to reflect this.

class Detector(Camera):

    def __init__(self, mycamera="i03-c-c00/dt/hamamatsu.1"):
        # initialise camera, and setup the focus and zoom motors of detector
        Camera.__init__(self, mycamera)

        self.focusMotor = "tomofocus"
        self.zoomMotor = "tomozoom"
        self.optic = None
        self.scintillator = "simple" # or "tiltable"
        self.detector_measurement_pos = '' # a spyc command string for amove ("alias position alias2 position2 ")
        self.detector_safety_pos = '' # a spyc command string for amove ("alias position alias2 position2 ")
        self.pixelsize = 0.001 # small default value for safety ?


    def detectorIn(self):
        '''Move detector IN - goes first to the safety position, then measurement position'''
        # move with spyc
        if (self.detector_measurement_pos == '') or (self.detector_safety_pos == ''):
            print "Need to define detector measurement and safety positions first"
            return
        else:
            print "Move to the safety postion"
            get_ipython().magic(u'amove %s' % self.detector_safety_pos)
            print "Move to the measurement position"
            get_ipython().magic(u'amove %s' % self.detector_measurement_pos)


    def detectorOut(self):
        '''Move detector OUT - goes to the safety position'''
        # move with spyc
        if self.detector_safety_pos == '':
            print "Need to define detector measurement and safety positions first"
            return
        else:
            print "Move to the safety postion"
            get_ipython().magic(u'amove %s' % self.detector_safety_pos)


    def changeOptic(self):
        '''move to safe position to change optics'''
        # If a new optic is defined, check this position -
        # and moving to this position - is always safe
        get_ipython().magic(u'setlimits %s None None' % self.focusMotor)
        get_ipython().magic(u'setlimits %s None None' % self.zoomMotor)
        print "moving to safe position"
        get_ipython().magic(u'amove %s 95 %s 180' % (self.focusMotor, self.zoomMotor))
        self.updateLimits()


    def setScintillator(self, scintillator_type=None):
        '''select simple or tiltable scintillator mount'''
        if scintillator_type==None:
            scintillator_type = raw_input("select scintillator type [simple/tiltable] :  ")

        if scintillator_type=="simple":
            self.scintillator = "simple"
        elif scintillator_type=="special_50x":
            self.scintillator = "special_50x"
        elif scintillator_type=="tiltable":
            self.scintillator = "tiltable"
        else:
            print "selection %s not understood! - try again please" % scintillator_type


    def setOptic(self, opticname=None):
        ''' setOptic - position and limit values are hard coded'''
        if opticname==None:
            opticname = raw_input("select optic [1x/2.5x/5x/7.5x/10x/20x/20xhr/50x] :  ")

        if opticname=="50x" or opticname=="20xhr":
            print "note that the 20xhr/50x optic only work with the special scintillator mount!"
            test = raw_input('OK to proceed ? : y/[n] ')
            if test=='y' or test=='yes':
                print "Using the special scintillator mount"
            else:
                print "First install the special mount, then continue setup"
                return

        if opticname not in alloptics:
                print "selection %s not understood - try again please" % opticname
                self.optic = None
                return
        else:
            self.optic = opticname
        # has the scintillator changed?
        self.setScintillator()
        self.updateLimits()

        # move to the nominal position
        test = raw_input("Safe to move to the nominal focus/zoom position? yes/[no] :  ")
        if test=='y' or test=='yes':
            f_pos = alloptics[self.optic][2]
            z_pos = alloptics[self.optic][5]
            if self.scintillator=="tiltable":
                f_pos = f_pos - 3.661
                z_pos = z_pos - 3.661
            self.updateLimits()
            get_ipython().magic(u'amove %s %0.3f' % (self.focusMotor, f_pos))
            self.updateLimits()
            get_ipython().magic(u'amove %s %0.3f' % (self.zoomMotor, z_pos))
            self.updateLimits()
        else:
            print "When ready, use setOptic method to move to nominal focus/zoom position"
        self.pixelsize = 6.5/alloptics[opticname][9]
        self.focallength = alloptics[opticname][8]

        # note that pixelsize is in microns but refDisplacement in mm
        guess = self.pixelsize * -2.5
        test = raw_input("What reference displacement to use? [%f] :  " % guess)
        self.refDisplacement = guess if test == "" else float(test)

    def updateLimits(self):
        '''Based on the current positions, update the limits to protect detector'''
        focusdev = dm.actors[self.focusMotor]
        focuspos = focusdev.value
        zoomdev = dm.actors[self.zoomMotor]
        zoompos = zoomdev.value

        if self.optic==None:
            f_min = focuspos-2
            f_max = focuspos+2
            z_min = zoompos-2
            z_max = zoompos+2
        else:
            f_min = max(alloptics[self.optic][0], zoompos-alloptics[self.optic][7])
            f_max = min(alloptics[self.optic][1], zoompos-alloptics[self.optic][6])

            z_min = max(alloptics[self.optic][3], focuspos+alloptics[self.optic][6])
            z_max = min(alloptics[self.optic][4], focuspos+alloptics[self.optic][7])

        # set limits quietly...
        focusdev.set_limits([f_min, f_max])
        zoomdev.set_limits([z_min, z_max])
        #get_ipython().magic(u'setlimits %s %f %f' % (self.focusMotor, f_min, f_max))
        #get_ipython().magic(u'setlimits %s %f %f' % (self.zoomMotor, z_min, z_max))


    def scanFocus(self, focusrange=None, nsteps=20):
        '''dscan focus +-focus range, measure std dev'''
        print 'put an object with suitable contrast in the beam'
        check = raw_input('ok to scan focus +- %0.2f mm? [yes/no] : ' % focusrange)
        if check=='yes' or check=='y':

            focusdev = dm.actors[self.focusMotor]
            focuspos = focusdev.value
            focusA = focuspos-focusrange
            focusB = focuspos+focusrange
            focus_pos = np.linspace(focusA, focusB, nsteps)
            focus_std = np.zeros(nsteps)
            focus_av = np.zeros(nsteps)

            self.stopLive()
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            self.proxy.nbFrames = 1
            for ii in range(nsteps):
                # move with spyc
                get_ipython().magic(u'amove %s %0.3f' % (self.focusMotor, focus_pos[ii]))
                self.takeIm()
                #print "test std(grad) as a measure"
                #im = self.image * 1.
                #a = np.gradient(im)
                #b = a[0]**2 + a[1]**2 # this is the modulus of the gradient... not sure that's right
                #focus_std[ii] = b.std()
                focus_std[ii] = self.image.std()
                focus_av[ii] = self.image.mean()
                pylab.figure(202)
                pylab.clf()
                pylab.plot(focus_pos, focus_std, 'r-x', label='image std')
                pylab.legend()
                pylab.draw()
        else:
            print 'quitting focus routine...'


    def scanFocusTilts(self, focusrange=None, nsteps=20):
        '''as for scanFocus, but consider the left/right and up/down tilts'''
        if focusrange==None:
            focusrange = self.focusrange()
        print 'put an object with suitable contrast in the beam'
        check = raw_input('ok to scan focus +- %0.2f mm? [yes/no] : ' % focusrange)
        if check=='yes' or check=='y':
            focusdev = dm.actors[self.focusMotor]
            focuspos = focusdev.value
            focusA = focuspos-focusrange
            focusB = focuspos+focusrange
            focus_pos = np.linspace(focusA, focusB, nsteps)

            # data for tilt around z
            rz_std_left = np.zeros(nsteps)
            rz_std_cen = np.zeros(nsteps)
            rz_std_right = np.zeros(nsteps)

            # data for tilt around x
            rx_std_top = np.zeros(nsteps)
            rx_std_cen = np.zeros(nsteps)
            rx_std_bot = np.zeros(nsteps)

            self.stopLive()
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            self.proxy.nbFrames = 1
            for ii in range(nsteps):
                get_ipython().magic(u'amove %s %0.3f' % (self.focusMotor, focus_pos[ii]))
                # do move
                self.takeIm()
                im = self.image
                sizey, sizex = im.shape # x and y of image, not beamline

                rz_std_left[ii] = im[:, 0:np.ceil(sizex/3)].std()
                rz_std_cen[ii] = im[:, np.floor(sizex/3):np.ceil(2*sizex/3)].std()
                rz_std_right[ii] = im[:, np.floor(2*sizex/3):sizex].std()
                rx_std_top[ii] =  im[0:np.ceil(sizey/3), :].std()
                rx_std_cen[ii] = im[np.floor(sizey/3):np.ceil(2*sizey/3), :].std()
                rx_std_bot[ii] =   im[np.floor(2*sizey/3):sizey, :].std()
                pylab.draw()

            pylab.figure(201)
            pylab.clf()
            pylab.plot(focus_pos, rz_std_left, 'r-x', label='rz left')
            pylab.plot(focus_pos, rz_std_cen, 'r-.+', label='rz cen')
            pylab.plot(focus_pos, rz_std_right, 'r-o', label='rz right')
            pylab.plot(focus_pos, rx_std_top, 'b-x', label='rx top')
            pylab.plot(focus_pos, rx_std_cen, 'b-.+', label='rx cen')
            pylab.plot(focus_pos, rx_std_bot, 'b-o', label='rx bot')
            pylab.legend()

            pylab.figure(202)
            # normalise profiles...
            rz_std_left = (rz_std_left - rz_std_left.min()) / (rz_std_left.max() - rz_std_left.min())
            rz_std_cen = (rz_std_cen - rz_std_cen.min()) / (rz_std_cen.max() - rz_std_cen.min())
            rz_std_right = (rz_std_right - rz_std_right.min()) / (rz_std_right.max() - rz_std_right.min())
            rx_std_top = (rx_std_top - rx_std_top.min()) / (rx_std_top.max() - rx_std_top.min())
            rx_std_cen = (rx_std_cen - rx_std_cen.min()) / (rx_std_cen.max() - rx_std_cen.min())
            rx_std_bot = (rx_std_bot - rx_std_bot.min()) / (rx_std_bot.max() - rx_std_bot.min())

            pylab.clf()
            pylab.plot(focus_pos, rz_std_left, 'r-x', label='rz left')
            pylab.plot(focus_pos, rz_std_cen, 'r-.+', label='rz cen')
            pylab.plot(focus_pos, rz_std_right, 'r-o', label='rz right')
            pylab.plot(focus_pos, rx_std_top, 'b-x', label='rx top')
            pylab.plot(focus_pos, rx_std_cen, 'b-.+', label='rx cen')
            pylab.plot(focus_pos, rx_std_bot, 'b-o', label='rx bot')
            pylab.legend()



            # fit all the centres
            # calculate required tilts
            # display output
        else:
            print 'quitting focus routine...'
            # scan focusmotor over focus range
            # get the images, calculate std dev

    def zoomOptic(self, Mnew):
        ''' specify the desired magnification, recalculate and move if allowed'''

        # focal length is f
        # f1 scintillator to lens, f2 lens to camera
        # nominal magnification is M
        # lens has a thickness T
        # total length from camera position
        #    [1]        total length = f1 + f2 + T
        #    [2]         1/f = 1/f1 + 1/f2
        #    [3]         M = f2/f1
        #
        #        f2 = Mf1
        #        M/f = M/f1 + 1/f1
        #        M/f = (M+1)/f1
        #        f1 = f(M+1)/M
        #
        #        f2 = Mf1
        #        f2 = f(M+1)
        #
        #        T = total length - f1 - f2

        f = alloptics[self.optic][8]
        Mnom = alloptics[self.optic][9]
        # calculate nominal f1 and f2
        f1nom = f*(Mnom+1)/Mnom
        f2nom = Mnom * f1nom
        # calculate new f1 and f2
        f1new = f*(Mnew+1)/Mnew
        df1 = f1new - f1nom
        f2new = Mnew * f1new
        df2 = f2new - f2nom

        # new positions
        f_pos = alloptics[self.optic][2] + df1
        z_pos = alloptics[self.optic][5] + df1 + df2
        if self.scintillator=="tiltable":
            f_pos = f_pos - 3.661
            z_pos = z_pos - 3.661
        print "want focus=%0.1f and zoom=%0.1f" % (f_pos, z_pos)

        # make sure limits are up to date
        self.updateLimits()
        get_ipython().magic(u'amove %s %0.3f %s %0.3f' % (self.focusMotor, f_pos, self.zoomMotor, z_pos))
        self.updateLimits()

    def alignBaseTilt(self):
        '''Using a needle on the rotation axis, align the axis normal to the beam
        probably should find some real radiographs to work on'''


    def centreAxis(self, xpos=None):
        '''Find the rotation axis, and position at the specified position in the image
        Default is the centre of the image'''

        # update the references
        self.updateRef()
        #self.updateDark()
        # take the images
        get_ipython().magic(u'amove %s 0' % self.rotation_motor_alias)
        self.takeFlat()
        im0 = self.flat
        get_ipython().magic(u'amove %s 180' % self.rotation_motor_alias)
        self.takeFlat()
        im180flip = self.flat[:, ::-1]

        # an array for the line by line output
        sizey, sizex = im0.shape
        c1 = np.zeros((sizey))
        c2 = np.zeros((sizey))

        # what is the global centre position?
        line0, line180 = im0.sum(0), im180flip.sum(0)
        corResult = ifft(fft(line0)*np.conj(fft(line180)))
        corResult = np.abs(corResult)
        gc = np.nonzero(corResult==corResult.max())
        gc = float(gc[0])
        gc = np.mod(gc+(sizex/2), sizex)

        print "global result is %02f" % gc
        pylab.figure(201)
        pylab.clf()
        pylab.plot([0,sizey], [gc,gc], 'b-')

        # correlate for each rowim
        for ii in range(sizey):
            # find the nearest pixel
            corResult = ifft(fft(im0[ii,:])*np.conj(fft(im180flip[ii,:])))
            corResult = np.abs(corResult)
            corResultLocal = np.nonzero(corResult==corResult.max())[0][0]
            c1[ii] = corResultLocal

            # pick out the five pixels around the peak
            # must stop this from going outside the matrix
            xdata = c1[ii]+[-2, -1, 0, 1, 2]
            xdata_ndx = np.mod(xdata, sizex).tolist()
            ydata = corResult[xdata_ndx]
            # fit this with a quadratic
            # ydata = a(xdata)^2 + b(xdata) + c
            xdata = np.array(xdata)
            xdata = np.array([xdata**2,xdata,np.ones(len(xdata))]).T
            a,b,c = np.linalg.lstsq(xdata, ydata)[0]
            # find the maximum of this, derivate
            # dy/dx = 2a(xdata) + b = 0
            # x = -b / 2a
            c2[ii] = -b / (2*a)

        # need to get everything centred more or less
        c1 = np.mod(c1+(sizex/2.), sizex)
        c2 = np.mod(c2+(sizex/2.), sizex)
        pylab.plot(np.arange(sizey), c1, 'g-')
        pylab.plot(np.arange(sizey), c2, 'r-')

        # calculate rotc
        # select the data close to the global result
        deltac = sizey*5*np.pi/180/2
        ndx = np.nonzero(np.abs(c1-gc)<deltac)[0]

        # make data for fitting
        fitx = np.array(range(sizey))[ndx]
        fity = c2[ndx]
        pylab.plot(fitx, fity, 'g+')

        # fit with a straight line
        fitx = np.array([fitx, np.ones_like(fitx)]).T
        m,c = np.linalg.lstsq(fitx, fity)[0]

        # display the fit
        x = np.array(range(sizey))
        x = np.array([x, np.ones_like(x)]).T
        y = np.dot(x, [m, c])
        pylab.plot(x,y,'g')

        # results
        if xpos==None:
            xpos = sizex/2
        deltaX = ((xpos-gc)/2)
        deltaXmm = deltaX*self.pixelsize/1000.
        # here I add a negative sign for flipped images
        deltaRotC = -((m*180/np.pi)/2)
        print "\nShould move tomo X by %0.2f mm (%0.2f pixels)" % (deltaXmm, deltaX)
        check = raw_input('Apply this correction? [yes/no] : ')
        if check=='yes' or check=='y':
            print 'move to the new position'
            get_ipython().magic(u'dmove %s %0.3f' % (self.reference_motor_alias, deltaXmm))
            self.setMeasurementPos()
        else:
            print 'Not applying correction'

        print "\nShould move %s by %0.3f degrees" %  (self.camera_rotation_motor, deltaRotC)
        check = raw_input('Apply this correction? [yes/no] : ')
        if check=='yes' or check=='y':
            # seton and setoff for newport NSA12
            get_ipython().magic(u'seton %s' % self.camera_rotation_motor)
            get_ipython().magic(u'dmove %s %0.3f' % (self.camera_rotation_motor, deltaRotC))
            get_ipython().magic(u'setoff %s' % self.camera_rotation_motor)
        else:
            print 'Not applying correction'
            print 'Can correct motor position tomorotc manually'


    def tomorotcOn(self):
        '''Helper function to switch on the tomorotc motor'''
        dp = PyTango.DeviceProxy('i03-c-c07/ex/tomo.2-mt_rz.2')
        if dp.state()==PyTango._PyTango.DevState.OFF:
            print "switching tomorotc ON"
            dp.motoron()
            time.sleep(1)
        elif dp.state()==PyTango._PyTango.DevState.STANDBY:
            print "tomorotc is ON"
        else:
            print "tomorotc state not understood!!!"

    def tomorotcOff(self):
        '''Helper function to switch off the tomorotc motor'''
        dp = PyTango.DeviceProxy('i03-c-c07/ex/tomo.2-mt_rz.2')
        if dp.state()==PyTango._PyTango.DevState.STANDBY:
            print "switching tomorotc OFF"
            dp.motoroff()
            time.sleep(1)
        elif dp.state()==PyTango._PyTango.DevState.OFF:
            print "tomorotc is OFF"
        else:
            print "tomorotc state not understood!!!"


