# -*- coding: utf-8 -*-

import par_tools
import os
import sys

def load_beamline():
    """
    Load the beamline specific options for tomography
    These are defined in a PyHST style par file
    i.e. text file containing:  OPTION = value
    Note that these are parameters for both reconstruction and acquisition
    Need a file on both SRV4 and the reconstruction machine
    All beamline specific configuration and defaults should go in ~/BEAMLINE_PARAMETER.par
    the actual file should be in the sypc folder $PYBEAMLINEROOT/beamline/macros,
    with a link to the ~/ folder, for SVN purposes.
    """

    # get the home directory
    home = os.path.expanduser('~')
    parfile = os.path.join(home, 'BEAMLINE_PARAMETERS.par')
    if os.path.exists(parfile):
	print('Loading beamline parameters from :\n    {}'.format(parfile))
        beamline = par_tools.par_read(parfile)
        return beamline
    else:
        print 'beamline parfile %s not found ! Quitting...'
        sys.exit()