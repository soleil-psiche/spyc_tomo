# -*- coding: utf-8 -*-

# function to switch between mt1 and mt3
import updateon
import time
import numpy as np

def switchTomo(my_mts, selection=None, update=True, hutch='OH'):
    '''
    Switch between cameras
    mt = switchTomo([mt1, mt3, mt4], selection=None)
    '''
    my_actives = []
    for ii,mt in enumerate(my_mts):
        if mt.active:
            my_actives.append(ii)
            print "[%d] : tomo camera %s, pixel size %0.3f is available" % (ii, mt.camera_name, mt.pixelsize)

    if len(my_actives)==0:
        # Nothing is active
        print "No tomo camera is available - quitting"
        selection = None
        return

    if len(my_actives)==1:
        # one camera, no confusion
        selection = my_actives[0]

    else:
        # More than one active, we have a choice
        if selection == None:
            selection = raw_input('selection? : ')
            selection = None if selection == '' else int(selection)

    # apply the selection
    mt = my_mts[selection]
    print "Using : [%d] : tomo camera %s, pixel size %0.3f" % (selection, mt.camera_name, mt.pixelsize)
    mt.detectorIn()
    if update:
        time.sleep(0.5)
        mt.stopLive()
        time.sleep(0.5)
        #updateon.updateon(hutch, 1)
    return mt


def switchTomo_OLD(mt1, mt3, selection=None, update=True, hutch='OH'):
    '''
    Switch between cameras
    mt = switchTomo(mt1, mt3, selection=None)
    '''

    if mt1.active and mt3.active:
        # Both are active, we have a choice
        print "[1] : tomo camera %s, pixel size %0.3f is available" % (mt1.camera_name, mt1.pixelsize)
        print "[3] : tomo camera %s, pixel size %0.3f is available" % (mt3.camera_name, mt3.pixelsize)

        if selection == None:
            selection = raw_input('selection? : ')
            selection = None if selection == '' else int(selection)

    elif mt1.active:
        # Only mt1 is active, no choice
        print "Only [1] : tomo camera %s is available" % mt1.camera_name
        selection = 1

    elif mt3.active:
        # Only mt3 is active, no choice
        print "Only [3] : tomo camera %s is available" % mt3.camera_name
        selection = 3

    else:
        # Neither is active
        print "Neither tomo camera is available - quitting"
        selection = None
        return

    if selection == 1:
        mt1.detectorIn()
        if update:
            time.sleep(0.5)
            mt1.stopLive()
            time.sleep(0.5)
            #updateon.updateon(hutch, 1)
        return mt1
    elif selection == 3:
        mt3.detectorIn()
        if update:
            time.sleep(0.5)
            mt3.stopLive()
            time.sleep(0.5)
            #updateon.updateon(hutch, 3)
        return mt3
    else:
        print "selection should be 1 or 3"

