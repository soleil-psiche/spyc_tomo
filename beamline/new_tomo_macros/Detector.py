# -*- coding: utf-8 -*-
"""
Created on Sat May 24 15:57:28 2014

@author: aking
"""

# use figure 201 for plotting and figures
# detector inherits from camera - i.e. a detector is a special class of camera
# this is a standard detector, with only one motor and without special limit handling

import numpy as np
ifft = np.fft.ifft
fft = np.fft.fft
import pylab
import PyTango
import time

# try handling reference motor with spyc
from spyc.core import DeviceManager
dm = DeviceManager()

from Camera import Camera

# big question is how to deal with this for ANATOMIX / PSICHE
# possibly my detector should be a special case (camera - detector - psichedetector - tomo heritance)

class Detector(Camera):

    def __init__(self, mycamera="i03-c-c00/dt/hamamatsu.1"):
        # initialise camera, and setup the focus of detector
        Camera.__init__(self, mycamera)

        self.focusMotor = "tomofocus"
        self.camera_rotation_motor = 'tomorotc'
        self.optic = None
        self.scintillator = "simple" # or "tiltable"
        self.detector_measurement_pos = '' # a spyc command string for amove ("alias position alias2 position2 ")
        self.detector_safety_pos = '' # a spyc command string for amove ("alias position alias2 position2 ")
        self.pixelsize = 0.001 # small default value for safety ?

    def detectorIn(self, interactive=True):
        '''Move detector IN - goes first to the safety position, then measurement position'''
        # move with spyc
        if (self.detector_measurement_pos == '') or (self.detector_safety_pos == ''):
            print "Need to define detector measurement and safety positions first"
            return
        else:
            if interactive:
                tmp = raw_input('Move to detector measurement position? y/[n] :')
                move = True if tmp.lower() in ['y', 'yes', 'true'] else False
            else:
                move = True
            if move:
                print "Move to the safety postion"
                get_ipython().magic(u'amove %s' % self.detector_safety_pos)
                print "Move to the measurement position"
                get_ipython().magic(u'amove %s' % self.detector_measurement_pos)
            print "Not moving the detector"


    def detectorOut(self, interactive=True):
        '''Move detector OUT - goes to the safety position'''
        # move with spyc
        if self.detector_safety_pos == '':
            print "Need to define detector measurement and safety positions first"
            return
        else:
            if interactive:
                tmp = raw_input('Move to detector safety position? y/[n] :')
                move = True if tmp.lower() in ['y', 'yes', 'true'] else False
            else:
                move = True
            if move:
                print "Move to the safety postion"
                get_ipython().magic(u'amove %s' % self.detector_safety_pos)
            print "Not moving the detector"



    def setOptic(self, opticname=None):
        ''' setOptic - just set the name of the optic'''

        if opticname==None:
            opticname = raw_input("select optic [1x/2.5x/5x/7.5x/10x/20x/50x] :  ")

        self.optic = opticname
        # has the scintillator changed?
        self.setScintillator()
        self.pixelsize = 6.5/float(self.optic[0:-1])
        print 'Assuming a Hamamatsu ORCA, set the pixel size to %0.3f' % self.pixelsize

        # note that pixelsize is in microns but refDisplacement in mm
        guess = self.pixelsize * -2.5
        test = raw_input("What reference displacement to use? [%f] :  " % guess)
        self.refDisplacement = guess if test == "" else float(test)


    def scanFocus(self, focusrange=None, nsteps=20):
        '''dscan focus +-focus range, measure std dev'''
        print 'put an object with suitable contrast in the beam'
        check = raw_input('ok to scan focus +- %0.2f mm? [yes/no] : ' % focusrange)
        if check=='yes' or check=='y':

            focusdev = dm.actors[self.focusMotor]
            focuspos = focusdev.value
            focusA = focuspos-focusrange
            focusB = focuspos+focusrange
            focus_pos = np.linspace(focusA, focusB, nsteps)
            focus_std = np.zeros(nsteps)
            focus_av = np.zeros(nsteps)

            self.stopLive()
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            self.proxy.nbFrames = 1
            for ii in range(nsteps):
                # move with spyc
                get_ipython().magic(u'amove %s %0.3f' % (self.focusMotor, focus_pos[ii]))
                self.takeIm()
                print "test std(grad) as a measure"
                im = self.image * 1.
                a = np.gradient(im)
                b = a[0]**2 + a[1]**2 # this is the modulus of the gradient... not sure that's right
                focus_std[ii] = b.std()
                #focus_std[ii] = self.image.std()
                focus_av[ii] = self.image.mean()
                pylab.figure(202)
                pylab.clf()
                pylab.plot(focus_pos, focus_std, 'r-x', label='image std')
                pylab.legend()
                pylab.draw()
        else:
            print 'quitting focus routine...'


    def scanFocusTilts(self, focusrange=None, nsteps=20):
        '''as for scanFocus, but consider the left/right and up/down tilts'''
        if focusrange==None:
            focusrange = self.focusrange()
        print 'put an object with suitable contrast in the beam'
        check = raw_input('ok to scan focus +- %0.2f mm? [yes/no] : ' % focusrange)
        if check=='yes' or check=='y':
            focusdev = dm.actors[self.focusMotor]
            focuspos = focusdev.value
            focusA = focuspos-focusrange
            focusB = focuspos+focusrange
            focus_pos = np.linspace(focusA, focusB, nsteps)

            # data for tilt around z
            rz_std_left = np.zeros(nsteps)
            rz_std_cen = np.zeros(nsteps)
            rz_std_right = np.zeros(nsteps)

            # data for tilt around x
            rx_std_top = np.zeros(nsteps)
            rx_std_cen = np.zeros(nsteps)
            rx_std_bot = np.zeros(nsteps)

            self.stopLive()
            self.proxy.triggerMode = 'INTERNAL_SINGLE'
            self.proxy.nbFrames = 1
            for ii in range(nsteps):
                get_ipython().magic(u'amove %s %0.3f' % (self.focusMotor, focus_pos[ii]))
                # do move
                self.takeIm()
                im = self.image
                sizey, sizex = im.shape # x and y of image, not beamline

                rz_std_left[ii] = im[:, 0:np.ceil(sizex/3)].std()
                rz_std_cen[ii] = im[:, np.floor(sizex/3):np.ceil(2*sizex/3)].std()
                rz_std_right[ii] = im[:, np.floor(2*sizex/3):sizex].std()
                rx_std_top[ii] =  im[0:np.ceil(sizey/3), :].std()
                rx_std_cen[ii] = im[np.floor(sizey/3):np.ceil(2*sizey/3), :].std()
                rx_std_bot[ii] =   im[np.floor(2*sizey/3):sizey, :].std()
                pylab.draw()

            pylab.figure(201)
            pylab.clf()
            pylab.plot(focus_pos, rz_std_left, 'r-x', label='rz left')
            pylab.plot(focus_pos, rz_std_cen, 'r-.+', label='rz cen')
            pylab.plot(focus_pos, rz_std_right, 'r-o', label='rz right')
            pylab.plot(focus_pos, rx_std_top, 'b-x', label='rx top')
            pylab.plot(focus_pos, rx_std_cen, 'b-.+', label='rx cen')
            pylab.plot(focus_pos, rx_std_bot, 'b-o', label='rx bot')
            pylab.legend()

            pylab.figure(202)
            # normalise profiles...
            rz_std_left = (rz_std_left - rz_std_left.min()) / (rz_std_left.max() - rz_std_left.min())
            rz_std_cen = (rz_std_cen - rz_std_cen.min()) / (rz_std_cen.max() - rz_std_cen.min())
            rz_std_right = (rz_std_right - rz_std_right.min()) / (rz_std_right.max() - rz_std_right.min())
            rx_std_top = (rx_std_top - rx_std_top.min()) / (rx_std_top.max() - rx_std_top.min())
            rx_std_cen = (rx_std_cen - rx_std_cen.min()) / (rx_std_cen.max() - rx_std_cen.min())
            rx_std_bot = (rx_std_bot - rx_std_bot.min()) / (rx_std_bot.max() - rx_std_bot.min())

            pylab.clf()
            pylab.plot(focus_pos, rz_std_left, 'r-x', label='rz left')
            pylab.plot(focus_pos, rz_std_cen, 'r-.+', label='rz cen')
            pylab.plot(focus_pos, rz_std_right, 'r-o', label='rz right')
            pylab.plot(focus_pos, rx_std_top, 'b-x', label='rx top')
            pylab.plot(focus_pos, rx_std_cen, 'b-.+', label='rx cen')
            pylab.plot(focus_pos, rx_std_bot, 'b-o', label='rx bot')
            pylab.legend()



            # fit all the centres
            # calculate required tilts
            # display output
        else:
            print 'quitting focus routine...'
            # scan focusmotor over focus range
            # get the images, calculate std dev
    def alignBaseTilt(self):
        '''Using a needle on the rotation axis, align the axis normal to the beam
        probably should find some real radiographs to work on'''


    def centreAxis(self, xpos=None):
        '''Find the rotation axis, and position at the specified position in the image
        Default is the centre of the image'''

        # update the references
        self.updateRef()
        #self.updateDark()
        # take the images
        get_ipython().magic(u'amove %s 0' % self.rotation_motor_alias)
        self.takeFlat()
        im0 = self.flat
        get_ipython().magic(u'amove %s 180' % self.rotation_motor_alias)
        self.takeFlat()
        im180flip = self.flat[:, ::-1]

        # an array for the line by line output
        sizey, sizex = im0.shape
        c1 = np.zeros((sizey))
        c2 = np.zeros((sizey))

        # what is the global centre position?
        line0, line180 = im0.sum(0), im180flip.sum(0)
        corResult = ifft(fft(line0)*np.conj(fft(line180)))
        corResult = np.abs(corResult)
        gc = np.nonzero(corResult==corResult.max())
        gc = float(gc[0])
        gc = np.mod(gc+(sizex/2), sizex)

        print "global result is %02f" % gc
        pylab.figure(201)
        pylab.clf()
        pylab.plot([0,sizey], [gc,gc], 'b-')

        # correlate for each rowim
        for ii in range(sizey):
            # find the nearest pixel
            corResult = ifft(fft(im0[ii,:])*np.conj(fft(im180flip[ii,:])))
            corResult = np.abs(corResult)
            corResultLocal = np.nonzero(corResult==corResult.max())[0][0]
            c1[ii] = corResultLocal

            # pick out the five pixels around the peak
            # must stop this from going outside the matrix
            xdata = c1[ii]+[-2, -1, 0, 1, 2]
            xdata_ndx = np.mod(xdata, sizex).tolist()
            ydata = corResult[xdata_ndx]
            # fit this with a quadratic
            # ydata = a(xdata)^2 + b(xdata) + c
            xdata = np.array(xdata)
            xdata = np.array([xdata**2,xdata,np.ones(len(xdata))]).T
            a,b,c = np.linalg.lstsq(xdata, ydata)[0]
            # find the maximum of this, derivate
            # dy/dx = 2a(xdata) + b = 0
            # x = -b / 2a
            c2[ii] = -b / (2*a)

        # need to get everything centred more or less
        c1 = np.mod(c1+(sizex/2.), sizex)
        c2 = np.mod(c2+(sizex/2.), sizex)
        pylab.plot(np.arange(sizey), c1, 'g-')
        pylab.plot(np.arange(sizey), c2, 'r-')

        # calculate rotc
        # select the data close to the global result
        deltac = sizey*5*np.pi/180/2
        ndx = np.nonzero(np.abs(c1-gc)<deltac)[0]

        # make data for fitting
        fitx = np.array(range(sizey))[ndx]
        fity = c2[ndx]
        pylab.plot(fitx, fity, 'g+')

        # fit with a straight line
        fitx = np.array([fitx, np.ones_like(fitx)]).T
        m,c = np.linalg.lstsq(fitx, fity)[0]

        # display the fit
        x = np.array(range(sizey))
        x = np.array([x, np.ones_like(x)]).T
        y = np.dot(x, [m, c])
        pylab.plot(x,y,'g')

        # results
        if xpos==None:
            xpos = sizex/2
        deltaX = ((xpos-gc)/2)
        deltaXmm = deltaX*self.pixelsize/1000.
        # here I add a negative sign for flipped images
        deltaRotC = -((m*180/np.pi)/2)
        print "\nShould move tomo X by %0.2f mm (%0.2f pixels)" % (deltaXmm, deltaX)
        check = raw_input('Apply this correction? [yes/no] : ')
        if check=='yes' or check=='y':
            print 'move to the new position'
            get_ipython().magic(u'dmove %s %0.3f' % (self.reference_motor_alias, deltaXmm))
            self.setMeasurementPos()
        else:
            print 'Not applying correction'

        print "\nShould move rotc by %0.2f degrees" %  deltaRotC
        check = raw_input('Apply this correction? [yes/no] : ')
        if check=='yes' or check=='y':
            get_ipython().magic(u'seton %s' % self.camera_rotation_motor)
            time.sleep(0.5)
            get_ipython().magic(u'dmove %s %0.3f' % (self.camera_rotation_motor, deltaRotC))
            get_ipython().magic(u'setoff %s' % self.camera_rotation_motor)
            time.sleep(0.5)
        else:
            print 'Not applying correction'
            print 'Can correct motor position tomorotc manually'




