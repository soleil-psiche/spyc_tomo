
import PyTango
from spyc.commands.scanmacro import SimpleScanCmd
from math import isnan
import time

# data fitter device for fitting
_fitter = PyTango.DeviceProxy("i03-C-C00/ex/datafitter.1")

class PsicheSimpleScanCmd(SimpleScanCmd):

    def prepare(self, **kwargs):

        # mandatory line from Document Technique
        SimpleScanCmd.prepare(self, **kwargs)
        # initialise the data fitter
        print "reset the data fitter"
        _fitter.Init()
            
    def after_execute(self):
        # mandatory line from Document Technique
        SimpleScanCmd.after_execute(self)
        # call calcafterscan
        #print "...calling calcafterscan"
        #get_ipython().magic(u'calcafterscan')
        # fit the scan data
        print "calling the data fitter"
        _fitter.StartFit()
        # wait a little while if needed
        count = 0
        while (isnan(_fitter.position) and count<10):
            count+=1
            time.sleep(0.1)
        if isnan(_fitter.position):
            print "no fit result - scan or data fitter failed!"
        # put position - the peak center in workspace
        __builtins__['CEN'] = _fitter.position
        

    def on_abort(self):
        # mandatory line from Document Technique
        SimpleScanCmd.on_abort(self)
        print "Scan was aborted... return to original position"
        # call calcafterscan
        #print "...calling calcafterscan"
        #get_ipython().magic(u'calcafterscan')
        print "...calling the data fitter"
        _fitter.StartFit()
        # wait a little while if needed
        count = 0
        while (isnan(_fitter.position) and count<10):
            count+=1
            time.sleep(0.1)
        if isnan(_fitter.position):
            print "no fit result - scan or data fitter failed!"
        # put position - the peak center in workspace
        __builtins__['CEN'] = _fitter.position
        