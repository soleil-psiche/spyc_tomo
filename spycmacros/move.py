# -*- coding: utf-8 -*-


from spyc.commands.actuators import MoveCmd

import PyTango
# how to do this in an elegant way?
ham1 = PyTango.DeviceProxy("i03-c-c00/dt/hamamatsu.1")
ham3 = PyTango.DeviceProxy("i03-c-c00/dt/hamamatsu.3")
dim4 = PyTango.DeviceProxy("i03-c-c00/dt/pco-dimax.1")
import time

class PsicheOHMoveCmd1(MoveCmd):

    def prepare(self, requests, **kargs):

        # mandatory line from Document Technique
        MoveCmd.prepare(self, requests, **kargs)

    def after_execute(self):

        # mandatory line from Document Technique
        MoveCmd.after_execute(self)
        if ham1.triggerMode != 'INTERNAL_SINGLE':
            ham1.triggerMode = 'INTERNAL_SINGLE'
            time.sleep(0.5)
        if ham1.nbframes != 1:
            ham1.nbframes = 1
            ham1.filegeneration = False
            time.sleep(0.5)
        #get_ipython().magic("fastshutteropen")
        get_ipython().magic("fwbsopen")
        ham1.snap()
        time.sleep(0.25)
        time.sleep(ham1.exposuretime / 1000.)
        #get_ipython().magic("fastshutterclose")
        get_ipython().magic("fwbsclose")

class PsicheEHMoveCmd1(MoveCmd):

    def prepare(self, requests, **kargs):

        # mandatory line from Document Technique
        MoveCmd.prepare(self, requests, **kargs)

    def after_execute(self):

        # mandatory line from Document Technique
        MoveCmd.after_execute(self)
        if ham1.triggerMode != 'INTERNAL_SINGLE':
            ham1.triggerMode = 'INTERNAL_SINGLE'
            time.sleep(0.5)
        if ham1.nbframes != 1:
            ham1.nbframes = 1
            ham1.filegeneration = False
            time.sleep(0.5)
        get_ipython().magic("fastshutteropen")
        #get_ipython().magic("fwbsopen")
        ham1.snap()
        time.sleep(0.25)
        time.sleep(ham1.exposuretime / 1000.)
        get_ipython().magic("fastshutterclose")
        #get_ipython().magic("fwbsclose")


class PsicheOHMoveCmd3(MoveCmd):

    def prepare(self, requests, **kargs):

        # mandatory line from Document Technique
        MoveCmd.prepare(self, requests, **kargs)

    def after_execute(self):

        # mandatory line from Document Technique
        MoveCmd.after_execute(self)
        if ham3.triggerMode != 'INTERNAL_SINGLE':
            ham3.triggerMode = 'INTERNAL_SINGLE'
            time.sleep(0.5)
        if ham3.nbframes != 1:
            ham3.nbframes = 1
            ham3.filegeneration = False
            time.sleep(0.5)
        #get_ipython().magic("fastshutteropen")
        get_ipython().magic("fwbsopen")
        ham3.snap()
        time.sleep(0.25)
        time.sleep(ham3.exposuretime / 1000.)
        #get_ipython().magic("fastshutterclose")
        get_ipython().magic("fwbsclose")

class PsicheEHMoveCmd3(MoveCmd):

    def prepare(self, requests, **kargs):

        # mandatory line from Document Technique
        MoveCmd.prepare(self, requests, **kargs)

    def after_execute(self):

        # mandatory line from Document Technique
        MoveCmd.after_execute(self)
        if ham3.triggerMode != 'INTERNAL_SINGLE':
            ham3.triggerMode = 'INTERNAL_SINGLE'
            time.sleep(0.5)
        if ham3.nbframes != 1:
            ham3.nbframes = 1
            ham3.filegeneration = False
            time.sleep(0.5)
        get_ipython().magic("fastshutteropen")
        #get_ipython().magic("fwbsopen")
        ham3.snap()
        time.sleep(0.25)
        time.sleep(ham3.exposuretime / 1000.)
        get_ipython().magic("fastshutterclose")
        #get_ipython().magic("fwbsclose")

class PsicheOHMoveCmd4(MoveCmd):

    def prepare(self, requests, **kargs):

        # mandatory line from Document Technique
        MoveCmd.prepare(self, requests, **kargs)

    def after_execute(self):

        # mandatory line from Document Technique
        MoveCmd.after_execute(self)
        if dim4.triggerMode != 'INTERNAL_SINGLE':
            dim4.triggerMode = 'INTERNAL_SINGLE'
            time.sleep(0.5)
        if dim4.nbframes != 1:
            dim4.nbframes = 1
            dim4.filegeneration = False
            time.sleep(0.5)
        #get_ipython().magic("fastshutteropen")
        get_ipython().magic("fwbsopen")
        dim4.snap()
        time.sleep(0.25)
        time.sleep(dim4.exposuretime / 1000.)
        #get_ipython().magic("fastshutterclose")
        get_ipython().magic("fwbsclose")

class PsicheEHMoveCmd4(MoveCmd):

    def prepare(self, requests, **kargs):

        # mandatory line from Document Technique
        MoveCmd.prepare(self, requests, **kargs)

    def after_execute(self):

        # mandatory line from Document Technique
        MoveCmd.after_execute(self)
        if dim4.triggerMode != 'INTERNAL_SINGLE':
            dim4.triggerMode = 'INTERNAL_SINGLE'
            time.sleep(0.5)
        if dim4.nbframes != 1:
            dim4.nbframes = 1
            dim4.filegeneration = False
            time.sleep(0.5)
        get_ipython().magic("fastshutteropen")
        #get_ipython().magic("fwbsopen")
        dim4.snap()
        time.sleep(0.25)
        time.sleep(dim4.exposuretime / 1000.)
        get_ipython().magic("fastshutterclose")
        #get_ipython().magic("fwbsclose")