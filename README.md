# SPyC_tomo

Python code for running tomography experiments at SOLEIL, using SpyC/PyTango etc.


This is created from a copy of the PyBeamlineRoot directory
i.e, at PSICHE:

cd /usr/Local/configFiles/PyBeamlineRoot

scp -r * 195.221.7.41:~/dev/SPyC_tomo/

this is then committed and pushed to gitlab.

The resulting repository is copied back to PyBeamlineRoot

cd /usr/Local/configFiles/

mkdir PyBeamlineRoot_gitlab

rsync -avrWt  195.221.7.41:~/dev/SPyC_tomo/ PyBeamlineRoot_gitlab/

note -a to bring over the .git files

rm PyBeamlineRoot # remove the current symbolic link, pointing to PyBeamlineRoot-0_9_0-psiche3

ln -s /usr/Local/configFiles/PyBeamlineRoot_gitlab PyBeamlineRoot

this seems to work from the point of view of SPyC - can launch functions.
git repository seems to work.
can commit changes.

then, to push
cd /usr/Local/configFiles/
rsync -avrWt PyBeamlineRoot_gitlab/ 195.221.7.41:~/dev/SPyC_tomo/

and on psiche-gpu1, in ~/dev/SPyC_tomo/
git push

this should work, because the directory on psiche-gpu1 is only a transfer directory - we are not going to edit code here.
So there shouldn't be too much risk of confusion


