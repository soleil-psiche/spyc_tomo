# -*- coding: utf-8 -*-
from spyc.ipython.magics.basic import Magics, spyc_magics_class, line_magic
from spyc.devices.base import Actuator, Sensor
from spyc import beamline_print

# need this to parse boolean arguments
def testbool(v):
    if v.lower() in ('yes', 'true', '1', 't', 'y'):
        return True
    else:
        return False
# Here, aiming to make magics for the mt methods used by the user
# therefore, not all methods have magics (yet?)

@spyc_magics_class
class TomoMagics(Magics):
    ''' TOMOGRAPHY commands developed by PSICHE '''


    @line_magic
    def setScanParameters(self, parameters_s=''):
        ''' set scan parameters for tomography'''
        beamline_print("setScanParameters magic")
        mt = self.shell.ev('mt')
        mt.setScanParameters()

    @line_magic
    def doTomo(self, parameters_s=''):
        ''' do tomography acquisition'''
        beamline_print("doTomo magic")
        mt = self.shell.ev('mt')
        mt.doTomo()

    @line_magic
    def setExpTime(self, parameters_s=''):
        ''' set exposure time for tomography'''
        beamline_print("setExpTime magic")
        mt = self.shell.ev('mt')
        # try to convert to float
        try:
            param = float(parameters_s)
        except:
            beamline_print("Usage: setExpTime exptime \nUnits: seconds")
            return
        mt.setExpTime(param)

    @line_magic
    def startLive(self, parameters_s=''):
        ''' start live mode of tomography camera'''
        beamline_print("startLive magic")
        mt = self.shell.ev('mt')
        mt.startLive()

    @line_magic
    def stopLive(self, parameters_s=''):
        ''' stop live mode of tomography camera'''
        beamline_print("stopLive magic")
        mt = self.shell.ev('mt')
        mt.stopLive()

    @line_magic
    def setROI(self, parameters_s=''):
        ''' set ROI of tomography camera'''
        beamline_print("setROI magic")
        params = parameters_s.split()
        # try to convert to ints
        try:
            params=[int(aa) for aa in params]
            params_bad = False
        except:
            params_bad = True
        if len(params) not in [0,4] or params_bad:
            beamline_print("Usage: setROI xstart, zstart, xrange, zrange \nsetROI with no arguments resets ROI\nUnits: pixels")
            return
        mt = self.shell.ev('mt')
        if len(params)==0:
            mt.setROI()
        else:
            mt.setROI(params[0], params[1], params[2], params[3])

    @line_magic
    def setROIcen(self, parameters_s=''):
        ''' set centred ROI of tomography camera'''
        beamline_print("setROIcen magic")
        params = parameters_s.split()
        # try to convert to ints
        try:
            params=[int(aa) for aa in params]
            params_bad = False
        except:
            params_bad = True
        if len(params) != 2 or params_bad:
            beamline_print("Usage: setROIcen xrange, zrange \nUnits: pixels")
            return
        mt = self.shell.ev('mt')
        mt.setROIcen(params[0], params[1])

    @line_magic
    def setBinning(self, parameters_s=''):
        ''' set binning of tomography camera'''
        beamline_print("setBinning magic")
        params = parameters_s.split()
        # try to convert to ints
        try:
            params=[int(aa) for aa in params]
            params_bad = False
        except:
            params_bad = True
        if len(params) not in [0,1] or params_bad:
            beamline_print("Usage: setBinning binning\nsetBinning with no arguments resets binning \nUnits: 1,2,4 pixels")
            return
        mt = self.shell.ev('mt')
        if len(params)==0:
            mt.setBinning()
        else:
            mt.setBinning(params[0])

    @line_magic
    def alignSample(self, parameters_s=''):
        ''' semi-automatic sample alignment'''
        beamline_print("alignSample magic")
        params = parameters_s.split()
        # try to convert
        try:
            if len(params)>0:
                params[0] = int(params[0])
            if len(params)>1:
                params[1] = testbool(params[1])
            if len(params)>2:
                params[2] = testbool(params[2])
            params_bad = False
        except:
            params_bad = True
        if len(params) not in [0,1,2,3] or params_bad:
            beamline_print("Usage: alignSample <range> <sqaure> <oneclick>\nUnits: N images for mosaic (+-N), square (True/False) oneclick (True/False)")
            print len(params)
            print params
            return
        mt = self.shell.ev('mt')
        if len(params)==0:
            mt.alignSample()
        elif len(params)==1:
            mt.alignSample(params[0])
        elif len(params)==2:
            mt.alignSample(params[0], params[1])
        else:
            mt.alignSample(params[0], params[1], params[2])

