# -*- coding: utf-8 -*-
from spyc.ipython.magics.basic import Magics, spyc_magics_class, line_magic
from spyc.devices.base import Actuator, Sensor
from spyc import beamline_print

# in order to handle motor positions
from spyc.core import DeviceManager
dm = DeviceManager()

import sys
import os
beamlineroot = os.environ.get('PYBEAMLINE_ROOT')
sys.path.append(beamlineroot+"/beamline/macros")
sys.path.append(beamlineroot+"/beamline/macros/old_functions")
sys.path.append(beamlineroot+"/beamline/macros/PSICHE_tomo_macros")

import numpy as np

#from andy import centerS as ORIGcenterS
#from andy import centerSX as ORIGcenterSX
from DAC_alignment import centerS as ORIGcenterS
from psiche_mv_cen import mv_cen as ORIGmv_cen

from ruby import ruby_on as ORIGruby_on
from ruby import ruby_off as ORIGruby_off

from stopORCA import stopORCA as mystopORCA

from set_tomotz import set_mtomotz as my_set_mtomotz
from set_tomotz import set_ptomotz as my_set_ptomotz
from read_positions import showMotorPositions as myShowMotorPositions

# all shutters
import psicheshutters
import festocx3
import CX3Shutters
import festocx1
import CX3functions

import PyTango
ham = PyTango.DeviceProxy("i03-c-c00/dt/hamamatsu.1")
import time

#AutoSampler
#import AutoSamplerMaxime

#PositionSaving
import PosSave

@spyc_magics_class
class BeamLineMagics(Magics):
    ''' Commands developed by PSICHE '''

#    @line_magic
#    def test_cmd(self, parameters_s=''):
#        ''' a test thing '''
#        beamline_print("hello, test_cmd")

    @line_magic
    def centerS(self, parameters_s=''):
        '''
        Auto alignment of a DAC
        Usage: centerS gonio2/3, halfScanRange, oscillationAngle, rzCenter
        '''
        params = parameters_s.split()
        if len(params) != 4:
            beamline_print("Usage: centerS gonio2/3, halfScanRange, oscillationAngle, rzCenter")
            return
        # pass the variables to the original function
        ORIGcenterS(params[0], float(params[1]), float(params[2]), float(params[3]))

    @line_magic
    def centerSX(self, parameters_s=''):
        '''Auto align the cs2, cx2 position of a DAC'''
        params = parameters_s.split()
        # try to convert to floats
        try:
            params=[float(aa) for aa in params]
            float_bad = False
        except:
            float_bad = True
        if len(params) != 3 or float_bad:
            beamline_print("Usage: centerS halfScanRange, oscillationAngle, rzCenter")
            return
        # pass the variables to the original function
        ORIGcenterSX(params[0], params[1], params[2])

    @line_magic
    def shopen(self, parameters_s=''):
        '''Open the experimental hutch safety shutter'''
        psicheshutters.shopen()

    @line_magic
    def shclose(self, parameters_s=''):
        '''Close the experimental hutch safety shutter'''
        psicheshutters.shclose()

    @line_magic
    def feopen(self, parameters_s=''):
        '''Open the beamline front end'''
        psicheshutters.feopen()

    @line_magic
    def feclose(self, parameters_s=''):
        '''Close the beamline front end'''
        psicheshutters.feclose()

    @line_magic
    def mv_cen(self, parameters_s=''):
        '''Move to the fitted centre of the last scan'''
        ORIGmv_cen()

    @line_magic
    def ruby_on(self, parameters_s=''):
        '''check dts2, rz2 positions, bring in prl'''
        ORIGruby_on()

    @line_magic
    def ruby_off(self, parameters_s=''):
        '''move prl out'''
        ORIGruby_off()

    @line_magic
    def fastshutteropen(self, parameters_s=''):
        '''Open the experimental hutch fast shutter'''
        psicheshutters.fastshutteropen()

    @line_magic
    def fastshutterclose(self, parameters_s=''):
        '''Close the experimental hutch fast shutter'''
        psicheshutters.fastshutterclose()

    @line_magic
    def fwbsopen(self, parameters_s=''):
        '''Open the optics hutch fast shutter'''
        psicheshutters.fwbsopen()

    @line_magic
    def fwbsclose(self, parameters_s=''):
        '''Close the optics hutch fast shutter'''
        psicheshutters.fwbsclose()

    @line_magic
    def stopORCA(self, parameters_s=''):
        '''stop the hamamatsu ORCA if it is waiting for a trigger'''
        mystopORCA()

    @line_magic
    def dmovetomotablez(self, parameters_s=''):
        '''move table and detector together - relative movement of table2z'''
        params = parameters_s.split()
        delta = float(params[0])
        print "move table and detector together"
        get_ipython().magic(u'dmove table2z %0.3f dtz2 %0.3f' % (delta, -delta))

    @line_magic
    def amovetomotablez(self, parameters_s=''):
        '''move table and detector and together - absolute movement of table2z'''
        params = parameters_s.split()
        newpos = float(params[0])
        curpos = dm.actors['table2z'].value * 1.
        delta = newpos - curpos
        print "move table and detector together"
        get_ipython().magic(u'dmove table2z %0.3f dtz2 %0.3f' % (delta, -delta))

    @line_magic
    def ptomoright(self, parameters_s=''):
        '''move sample right at angle tomorz angle
           account for the offset in case new position has been set'''
        params = parameters_s.split()
        delta = float(params[0])
        ang = dm.actors['ptomorz'].read().value - dm.actors['ptomorz'].proxy.offset
        deltaX = np.cos(np.deg2rad(ang)) * delta
        deltaS = np.sin(np.deg2rad(ang)) * delta
        print "coupled movement of ptomotx and ptomots"
        get_ipython().magic(u'dmove ptomotx %0.3f ptomots %0.3f' % (deltaX, deltaS))

    @line_magic
    def mtomoright(self, parameters_s=''):
        '''move sample right at angle tomorz angle
           account for the offset in case new position has been set'''
        params = parameters_s.split()
        delta = float(params[0])
        ang = dm.actors['mtomorz'].read().value - dm.actors['mtomorz'].proxy.offset
        deltaX = np.cos(np.deg2rad(ang)) * delta
        deltaS = np.sin(np.deg2rad(ang)) * delta
        print "coupled movement of mtomotx and mtomots"
        get_ipython().magic(u'dmove mtomotx %0.3f mtomots %0.3f' % (deltaX, deltaS))

    @line_magic
    def set_mtomotz(self, parameters_s=''):
        '''Choose which z stage to use'''
        my_set_mtomotz()

    @line_magic
    def set_ptomotz(self, parameters_s=''):
        '''Choose which z stage to use'''
        my_set_ptomotz()

    @line_magic
    def showMotorPositions(self, parameters_s=''):
        '''set sample positions saved in a tomo par file'''
        myShowMotorPositions()

    #@line_magic
    #def tomofurnaceON(self, parameters_s=''):
        #'''power ON tomo furnace Z'''
        #psicheshutters.tomofurnaceON()

    #@line_magic
    #def tomofurnaceOFF(self, parameters_s=''):
        #'''power OFF tomo furnace Z'''
        #psicheshutters.tomofurnaceOFF()

    #@line_magic
    #def tomofurnaceUP(self, parameters_s=''):
        #'''direction UP tomo furnace Z'''
        #psicheshutters.tomofurnaceUP()

    #@line_magic
    #def tomofurnaceDOWN(self, parameters_s=''):
        #'''direction DOWN tomo furnace Z'''
        #psicheshutters.tomofurnaceDOWN()

    @line_magic
    def diode0on(self, parameters_s=''):
	    festocx3.diode0on()

    @line_magic
    def diode0off(self, parameters_s=''):
	    festocx3.diode0off()

    @line_magic
    def diode1on(self, parameters_s=''):
	    festocx3.diode1on()

    @line_magic
    def diode1off(self, parameters_s=''):
	    festocx3.diode1off()

    @line_magic
    def light1on(self, parameters_s=''):
	    festocx3.light1on()

    @line_magic
    def light1off(self, parameters_s=''):
	    festocx3.light1off()

    @line_magic
    def light2on(self, parameters_s=''):
	    festocx3.light2on()

    @line_magic
    def light2off(self, parameters_s=''):
	    festocx3.light2off()

    @line_magic
    def image1on(self, parameters_s=''):
	    festocx3.image1on()

    @line_magic
    def image1off(self, parameters_s=''):
	    festocx3.image1off()

    @line_magic
    def image2on(self, parameters_s=''):
	    festocx3.image2on()

    @line_magic
    def image2off(self, parameters_s=''):
	    festocx3.image2off()

    @line_magic
    def sh1open(self, parameters_s=''):
	    CX3Shutters.sh1open()

    @line_magic
    def sh1close(self, parameters_s=''):
	    CX3Shutters.sh1close()

    @line_magic
    def sh2open(self, parameters_s=''):
	    CX3Shutters.sh2open()

    @line_magic
    def sh2close(self, parameters_s=''):
	    CX3Shutters.sh2close()

    @line_magic
    def sh3open(self, parameters_s=''):
	    CX3Shutters.sh3open()

    @line_magic
    def sh3close(self, parameters_s=''):
	    CX3Shutters.sh3close()

    @line_magic
    def sh4open(self, parameters_s=''):
	    CX3Shutters.sh4open()

    @line_magic
    def sh4close(self, parameters_s=''):
	    CX3Shutters.sh4close()

    @line_magic
    def sh5open(self, parameters_s=''):
	    CX3Shutters.sh5open()

    @line_magic
    def sh5close(self, parameters_s=''):
	    CX3Shutters.sh5close()

    @line_magic
    def sh6open(self, parameters_s=''):
	    CX3Shutters.sh6open()

    @line_magic
    def sh6close(self, parameters_s=''):
	    CX3Shutters.sh6close()

    @line_magic
    def sh7open(self, parameters_s=''):
	    CX3Shutters.sh7open()

    @line_magic
    def sh7close(self, parameters_s=''):
	    CX3Shutters.sh7close()

    @line_magic
    def sh8open(self, parameters_s=''):
	    CX3Shutters.sh8open()

    @line_magic
    def sh8close(self, parameters_s=''):
	    CX3Shutters.sh8close()

    @line_magic
    def getAllMotorsPositions(self, parameters_s=''):
	    PosSave.getAllMotorPositions()

    @line_magic
    def MgO_off(self, parameters_s=''):
	    festocx1.festCX11off()

    @line_magic
    def MgO_on(self, parameters_s=''):
	    festocx1.festCX11on()

    @line_magic
    def illum_on(self, parameters_s=''):
	    CX3functions.IllumStart()

    @line_magic
    def illum_off(self, parameters_s=''):
	    CX3functions.IllumStop()